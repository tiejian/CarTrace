package com.jacqui.form;

import java.util.List;

public class TreeModel {
	String  id;
	String name;
	String code;
	boolean isParent ;
	boolean open;
	List children;
	String icon;
	String busiType;
	boolean checked;
	String treeType;
	String url;
	String target;
	public TreeModel(String id, String name, String code, boolean isParent,
			boolean open, String icon, String busiType,
			boolean checked, String treeType, String url, String target) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.isParent = isParent;
		this.open = open;
		this.icon = icon;
		this.busiType = busiType;
		this.checked = checked;
		this.treeType = treeType;
		this.url = url;
		this.target = target;
	}
	public TreeModel(String id, String name, String code, boolean isParent,
			boolean open, List children, String icon, String busiType,
			boolean checked, String treeType, String url, String target) {
		super();
		this.id = id;
		this.name = name;
		this.code = code;
		this.isParent = isParent;
		this.open = open;
		this.children = children;
		this.icon = icon;
		this.busiType = busiType;
		this.checked = checked;
		this.treeType = treeType;
		this.url = url;
		this.target = target;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public List getChildren() {
		return children;
	}
	public void setChildren(List children) {
		this.children = children;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getBusiType() {
		return busiType;
	}
	public void setBusiType(String busiType) {
		this.busiType = busiType;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public String getTreeType() {
		return treeType;
	}
	public void setTreeType(String treeType) {
		this.treeType = treeType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	
	
}
