package com.jacqui.form;

import java.util.LinkedHashMap;
import java.util.List;

public class MonthDataEntity {
	
	private LinkedHashMap<String,List<String>> map;
	
	private String mmonth;

	/** 
	 * @return map 
	 */
	public LinkedHashMap<String, List<String>> getMap() {
		return map;
	}

	/** 
	 * @param map 要设置的 map 
	 */
	public void setMap(LinkedHashMap<String, List<String>> map) {
		this.map = map;
	}

	/** 
	 * @return mmonth 
	 */
	public String getMmonth() {
		return mmonth;
	}

	/** 
	 * @param mmonth 要设置的 mmonth 
	 */
	public void setMmonth(String mmonth) {
		this.mmonth = mmonth;
	}

	

}
