package com.jacqui.form;

import java.io.Serializable;

public class SessionCount implements Serializable{
	
	private String jgbm;
	
	private String jgbmmc;
	
	private int qdCount;//已经处理的清单数量
	
	private int jgQdCount;//有监管主体的清单数量
	
	private int qyCount;//监管主体
	
    private long qdTotal;//部门清单总数

	public int getQdCount() {
		return qdCount;
	}

	public void setQdCount(int qdCount) {
		this.qdCount = qdCount;
	}

	public int getQyCount() {
		return qyCount;
	}

	public void setQyCount(int qyCount) {
		this.qyCount = qyCount;
	}

	public SessionCount(int qdCount, int qyCount) {
		super();
		this.qdCount = qdCount;
		this.qyCount = qyCount;
	}
	public SessionCount() {
		super();
	}

	public String getJgbm() {
		return jgbm;
	}

	public void setJgbm(String jgbm) {
		this.jgbm = jgbm;
	}

	public String getJgbmmc() {
		return jgbmmc;
	}

	public void setJgbmmc(String jgbmmc) {
		this.jgbmmc = jgbmmc;
	}

	public long getQdTotal() {
		return qdTotal;
	}

	public void setQdTotal(long qdTotal) {
		this.qdTotal = qdTotal;
	}

	public int getJgQdCount() {
		return jgQdCount;
	}

	public void setJgQdCount(int jgQdCount) {
		this.jgQdCount = jgQdCount;
	}
	

}
