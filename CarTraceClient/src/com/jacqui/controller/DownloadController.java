package com.jacqui.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "download")
public class DownloadController {
	
	private static final int BUFFER_SIZE = 4096;
	
	@RequestMapping(value = "log/excel")
	public void logExcel(@RequestParam String fileName,Model model,HttpServletRequest request,HttpServletResponse response) throws IOException {
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		fileName =new String(fileName.getBytes("ISO-8859-1"),"UTF-8");
		String ctxPath = request.getSession().getServletContext()
				.getRealPath("/")
				+ File.separator
				+ "upload"
				+ File.separator
				+ fileName;
		File file = new File(ctxPath);
//        FileInputStream inputStream = new FileInputStream(file);
        response.setCharacterEncoding("UTF-8");
		// 写明要下载的文件的大小
		response.setContentLength((int) file.length());
		response.reset();
		response.setContentType("application/octet-stream");
		String enableFileName = new String(fileName.getBytes("GBK"),"ISO-8859-1");
		response.setHeader("Content-Disposition", "attachment; filename="+enableFileName+".xlsx");
		response.setHeader("Connection", "close");
		try{
			bis = new BufferedInputStream(new FileInputStream(ctxPath));
		    bos = new BufferedOutputStream(response.getOutputStream());
		    byte[] buff = new byte[BUFFER_SIZE];
		    int bytesRead;
		    while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
		        bos.write(buff, 0, bytesRead);
		    }
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			bis.close();
			bos.close();
		}
	} 

}
