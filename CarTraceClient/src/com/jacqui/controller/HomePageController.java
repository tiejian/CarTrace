package com.jacqui.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/homepage")
public class HomePageController {
	
	@RequestMapping(value="",method=RequestMethod.GET)
	public String redirect_to_login_page(Model model,HttpServletRequest request, HttpServletResponse response){
	    return "homepage";  
	}

}
