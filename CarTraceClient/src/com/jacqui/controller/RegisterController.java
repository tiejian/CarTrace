package com.jacqui.controller;

import javax.servlet.ServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="reg")
public class RegisterController {
	
	@RequestMapping(value = "")
	public String index(Model model,ServletRequest request){
		return "register";
	}

}
