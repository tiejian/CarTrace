package com.jacqui.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jacqui.form.LoginForm;
import com.jacqui.form.LoginValidator;
import com.jacqui.main.domain.StUser;
import com.jacqui.main.service.StUserService;

@Controller
public class LoginController {
	
	@Autowired
	@Qualifier("loginValidator")
    protected LoginValidator loginValidator;
	
	@Autowired
	private StUserService stUserService;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.loginValidator);
	}
	
	@RequestMapping(value = "redirect/login", method = RequestMethod.GET)
	public String redirect_to_login_page(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("loginForm", new LoginForm());
		model.addAttribute("key", "none");
		model.addAttribute("errorCount", 0);
		return "login";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String redirect_to_login_page(LoginForm loginForm,Model model,BindingResult result,HttpServletRequest request, HttpServletResponse response){
		String username = loginForm.getUsername();
		String password = loginForm.getPassword();
		
		loginValidator.validate(loginForm, result);
		
		if(result.hasErrors()){
			return "login";
		}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("staffId", username);
		map.put("passwd", password);
		StUser user = stUserService.findUniqueBy(map);
		
		if(user==null){
			return "login";
		}
		
		user.setCreateBy("张三丰");
		user.setUpdateBy("张三丰");
		
		HttpSession sessoin = request.getSession(false);
		sessoin.setAttribute("stuser", user);
		
//		model.addAttribute("menus", menus);
	    return "redirect:/index";  
	}
	
	
	@RequestMapping(value="log/in",method=RequestMethod.POST)
	public String redirect_to_login_in(LoginForm loginForm,BindingResult result,Model model,HttpServletRequest request, HttpServletResponse response){
		return "redirect:/index"; 
	}
	
	@RequestMapping(value="loginout",method=RequestMethod.GET)
	public String loginout(Model model,HttpServletRequest request, HttpServletResponse response){
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				session.removeAttribute("stuser");
				session.invalidate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:/"; 
	}

}
