package com.jacqui.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jacqui.form.LoginForm;
/**
 * 
 * @ClassName: WelcomeController 
 * @Description: 跳转到登录页面
 * @author ilaotou@qq.com
 * @date 2014年9月20日 下午2:49:09 
 *
 */
@Controller
@RequestMapping(value = "/welcome")
public class WelcomeController {

	@RequestMapping(value="redirect/login",method=RequestMethod.GET)
	public String redirect_to_login_page(Model model,HttpServletRequest request, HttpServletResponse response){
		model.addAttribute("loginForm",new LoginForm());
		model.addAttribute("viewtimes", "1");//浏览次数，用于显示是否使用验证码
	    return "login";  
	}
}
