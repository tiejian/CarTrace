package com.jacqui.core.security;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SecurityConstant {
	
	public final static Map<String, String> KEYS = new HashMap<String, String>() {
		
		private static final long serialVersionUID = 1L;
		{
			put("1", "ABCDEFGHI=#abcdefghi");
			put("2", "ABCDEFGHI121=#abcdefghi");
			put("3", "ABCDEFGHI122=#abcdefghi");
			put("4", "ABCDEFGHI123=#abcdefghi");
			put("5", "ABCDEFGHI124=#abcdefghi");
			put("6", "ABCDEFGHI125=#abcdefghi");
			put("7", "ABCDEFGHI126=#abcdefghi");
			put("8", "ABCDEFGHI127=#abcdefghi");
			put("9", "ABCDEFGHI128=#abcdefghi");
			put("10","ABCDEFGHI129=#abcdefghi");
		}
	}; 
	public static String[] getKeyPairs(){
		int key=getRandom(1,11);
		return new String[]{String.valueOf(key),KEYS.get(String.valueOf(key))};
	}
	/**
	 * 通过KEY,取得密钥
	 * @param key
	 * @return
	 */
	public static String getKeyPairs(String key){
		return KEYS.get(String.valueOf(key));
	}
	/**
	 * 随机取得一个key
	 * @return
	 */
	public static String getKey(){
		return String.valueOf(getRandom(1,11));
	}
	
	/**
	 * 产生0～max的随机整数 包括0 不包括max
	 * 
	 * @param max
	 *            随机数的上限
	 * @return
	 */
	public static int getRandom(int max) {
		return new Random().nextInt(max);
	}

	/**
	 * 产生 min～max的随机整数 包括 min 但不包括 max
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandom(int min, int max) {
		int r = getRandom(max - min);
		return r + min;
	}

	public static void main(String[] args){
		String[] a = getKeyPairs();
		System.out.println(a[0]);
		System.out.println(a[1]);
	}
}
