package com.jacqui.core.persistence;

import java.util.List;

public class Criteria {
	
	private List<Criterion> criterions;
	
	private String tblName;

	public List<Criterion> getCriterions() {
		return criterions;
	}

	public void setCriterions(List<Criterion> criterions) {
		this.criterions = criterions;
	}

	public String getTblName() {
		return tblName;
	}

	public void setTblName(String tblName) {
		this.tblName = tblName;
	}

	public Criteria(List<Criterion> criterions, String tblName) {
		super();
		this.criterions = criterions;
		this.tblName = tblName;
	}
	public Criteria(List<Criterion> criterions) {
		super();
		this.criterions = criterions;
	}
	public Criteria() {
		super();
	}
	

}
