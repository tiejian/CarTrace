package com.jacqui.core.utils;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * 
 * <p>
 * Title: com.kleinst.core.util.SystemUtil.java
 * </p>
 * <p>
 * Description: 系统工具类
 * </p>
 * <p>
 * Copyright: Copyright (c) 2011 社区管理系统，克莱内斯特科技有限责任公司
 * </p>
 * <p>
 * Company: 克莱内斯特科技有限责任公司
 * </p>
 * 
 * @author ljx
 * @version 1.0
 */
public class SystemUtil {

	private static Log log = LogFactory.getLog(SystemUtil.class);
	
	/**
	 * 
	 * 方法说明：给当前的SQL语句赋值 参数： currentSQL 类型：StringBuffer 参数： values 类型：String[]
	 * 返回值：sql 类型：StringBuffer 建立人： ljx 建立时间：2012-8-1 下午08:27:31
	 * 
	 */
	public static StringBuffer setSQLForValue(StringBuffer currentSQL,
			String[] values) {
		try {
			int i = 0;
			if (currentSQL.indexOf("?") >= 0)
				while (currentSQL.indexOf("?") >= 0) {
					int _index = currentSQL.indexOf("?");

					if (values[i] == null)
						values[i] = "";
					currentSQL.replace(_index, _index + 1, values[i]);
					i++;
				}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return currentSQL;
	}

	/**
	 * 
	 * 方法说明：根据级别和代码获取相关代码内容 参数： dm 代码 类型：String 参数： level 级别 类型：int 返回值：return
	 * 类型：String 建立人： ljx 建立时间：2012-8-3 下午03:34:37
	 * 
	 */
	public static String getCode(String dm, int level) {
		String code = null;
		switch (level) {
		case 1:// 市级代码
			code = dm.substring(0, 4);
			break;
		case 2:// 行政区代码
			code = dm.substring(0, 6);
			break;
		case 3:// 街道代码
			code = dm.substring(0, 9);
			break;
		case 4:// 社区代码
			code = dm.substring(0, 13);
			break;
		default:
			break;
		}
		return code;
	}

	/**
	 * 
	 * 方法说明：根据代码获取所在行政区（区代码） 参数： dm 当前登录操作员权限范围 类型：HttpServletRequest 返回值：return
	 * 类型：String 建立人： ljx 建立时间：2012-10-12 上午07:59:09
	 * 
	 * @throws Exception
	 * 
	 */
	public static String getXzqCode(String dm) {
		String code = null;
		if (dm.length() == 2) {
			dm += "0000";
		} else if (dm.length() == 4) {
			dm += "00";
		} else if (dm.length() > 6) {
			dm = dm.substring(0, 6);
		}
		code = dm;
		return code;
	}

	/**
	 * 
	 * 方法说明：根据json和count生成用于分页查询控件使用 参数： json 分页结果集合 类型：String 参数： count 总数
	 * 类型：String 返回值：json 分页结果 类型：String 建立人： ljx 建立时间：2012-8-5 下午12:52:03
	 * 
	 */
	public static String createPagingJson(String json, String count)
			throws Exception {
		json = "{\"Rows\":" + json + ",\"Total\":\"" + count + "\"}";
		return json;
	}

	/**
	 * 
	 * 方法说明：生成用于分页查询错误使用 返回值：return 类型：String 建立人： ljx 建立时间：2012-12-7 上午9:14:00
	 * 
	 * @throws Exception
	 * 
	 */
	public static String createPagingJsonError() throws Exception {
		return "{\"Rows\":[],\"Total\":\"0\"}";
	}

	/**
	 * 
	 * 方法说明：根据流水号和字符长度格式化流水号 参数： lsh 流水号 类型：int 参数： length 长度 类型：int 返回值：return
	 * 类型：String 建立人： ljx 建立时间：2012-9-13 下午09:17:06
	 * 
	 */
	public static String getPolishingLsh(int lsh, int length) {
		StringBuffer format = new StringBuffer();
		int len = String.valueOf(lsh).length();
		for (int i = 0; i < length - len; i++) {
			format.append("0");
		}
		format.append(lsh);
		return format.toString();
	}


	public static void createFolder(String path) {
//		StringTokenizer st = new StringTokenizer(path, "\\");
//		String path1 = st.nextToken() + "\\";
//		String path2 = path1;
//		while (st.hasMoreTokens()) {
//			path1 = st.nextToken() + "\\";
//			path2 += path1;
//			File inbox = new File(path2);
//			if (!inbox.exists()){
//				inbox.mkdir();
//			}
//		}
		File f=new File(path);
		f.setWritable(true, false);
		f.mkdirs();
	}
	
	/**
	 * 
	 * 方法说明：获取客户端访问ip地址
	 * 参数： request  类型：HttpServletRequest
	 * 返回值：return  类型：String
	 * 建立人： ljx  建立时间：2013-5-3 上午10:31:52
	 *
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	/**
	 * 
	 * 方法说明：判断字符串是否为数字
	 * 参数：str 字符串  类型：String
	 * 返回值：return true是，false否  类型：boolean
	 * 建立人： ljx  建立时间：2013-5-3 下午9:16:49
	 *
	 */
	public static boolean isNumber(String str)
    {
        java.util.regex.Pattern pattern=java.util.regex.Pattern.compile("[0-9]*");
        java.util.regex.Matcher match=pattern.matcher(str);
        if(match.matches()==false)
        {
             return false;
        }
        else
        {
             return true;
        }
    }
	
	public static void main(String[] args) {
		String lj="e:\\s\\ss\\sss";
		File f=new File(lj);
		f.setWritable(true, false);
		f.mkdirs();
	}
}
