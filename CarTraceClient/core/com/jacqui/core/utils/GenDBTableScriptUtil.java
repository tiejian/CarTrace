package com.jacqui.core.utils;

import java.util.Random;

import org.apache.commons.lang.StringUtils;

public class GenDBTableScriptUtil {

	/**
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		//String str = "我是中国人";
		//System.out.println("中文首字母：" + getPYIndexStr(str, false));
		String tbScript = createPostgreSQLTable("meta_sj_gx_zyzhly","工信局-产品认定登记信息","申请单位名称,组织机构代码,法人姓名,居民身份证,单位地址,产品名称,综合利用资源项目名称,发证机关,批准证号,有效年限,联系电话,登记注册类型,税务登记规范名称,区域标志,行政区标志,经营范围,资源类型");
		System.out.println(tbScript);
	}
	
	public static String genMysqlFields(String fields4CN){
		//String str="管理机关,核算机关,入库日期,征收项目,待征属性,登记管理机关,登记注册类型,地区,调账类型,国库银行,国库帐号,汇总日期,机构申报日结时间,稽查机关,计税依据,缴款方式,缴款流水号,缴款凭证类型,缴款期限,缴款日期,开票机关,开票人员,扣款银行,扣款银行账号,隶属关系,联系电话,纳税人编码,纳税人名称,年度,票证号码,票证数量,票证种类,凭证序号,凭证种类,企业所得税管理机关,清算日期,入库金额,申报方式,申报人员,申报日结工作时间,申报日结人员,申报日期,申报属性,实际经营地址,收款账号ID,税费所属期起,税费所属期止,税款属性1,税款属性2,税率,特殊企业类别,填发时间,销号类型,销号人员,销号系统时间,行业大类,行业中小类,应征发生日期,预算分配比例,预算科目,原缴款期限,征收代理方式,征收方式,征收机关,征收品目,征收人员,滞纳金的滞纳天数,重点税源标志,注册地址所在行政区域,专管员";
		String[] arr = fields4CN.split(",");
		String[] arr1 = new String[arr.length];
		//`gljg` VARCHAR(50) NULL DEAULT NULL COMMENT '管理机关',
		String eName="";
		String field="";
		for(int i=0;i<arr.length;i++){
			eName=getPYIndexStr(arr[i], false);
			field="`"+eName+"`  VARCHAR(50) NULL DEFAULT NULL COMMENT '" +arr[i] + "'";
			arr1[i]=field;
		}
		return StringUtils.join(arr1,",");
	}
	public static String genPostgreSQLFields(String fields4CN){
		//String str="管理机关,核算机关,入库日期,征收项目,待征属性,登记管理机关,登记注册类型,地区,调账类型,国库银行,国库帐号,汇总日期,机构申报日结时间,稽查机关,计税依据,缴款方式,缴款流水号,缴款凭证类型,缴款期限,缴款日期,开票机关,开票人员,扣款银行,扣款银行账号,隶属关系,联系电话,纳税人编码,纳税人名称,年度,票证号码,票证数量,票证种类,凭证序号,凭证种类,企业所得税管理机关,清算日期,入库金额,申报方式,申报人员,申报日结工作时间,申报日结人员,申报日期,申报属性,实际经营地址,收款账号ID,税费所属期起,税费所属期止,税款属性1,税款属性2,税率,特殊企业类别,填发时间,销号类型,销号人员,销号系统时间,行业大类,行业中小类,应征发生日期,预算分配比例,预算科目,原缴款期限,征收代理方式,征收方式,征收机关,征收品目,征收人员,滞纳金的滞纳天数,重点税源标志,注册地址所在行政区域,专管员";
		String[] arr = fields4CN.split(",");
		String[] arr1 = new String[arr.length];
		//`gljg` VARCHAR(50) NULL DEAULT NULL COMMENT '管理机关',
		String eName="";
		String field="";
		for(int i=0;i<arr.length;i++){
			eName=getPYIndexStr(arr[i], false);
			field="   "+eName+" character varying(50),";
			arr1[i]=field;
		}
		return StringUtils.join(arr1,"\n");
	}
	public static String genPostgreSQLComment(String tname,String tCnName, String fields4CN){
		//String str="管理机关,核算机关,入库日期,征收项目,待征属性,登记管理机关,登记注册类型,地区,调账类型,国库银行,国库帐号,汇总日期,机构申报日结时间,稽查机关,计税依据,缴款方式,缴款流水号,缴款凭证类型,缴款期限,缴款日期,开票机关,开票人员,扣款银行,扣款银行账号,隶属关系,联系电话,纳税人编码,纳税人名称,年度,票证号码,票证数量,票证种类,凭证序号,凭证种类,企业所得税管理机关,清算日期,入库金额,申报方式,申报人员,申报日结工作时间,申报日结人员,申报日期,申报属性,实际经营地址,收款账号ID,税费所属期起,税费所属期止,税款属性1,税款属性2,税率,特殊企业类别,填发时间,销号类型,销号人员,销号系统时间,行业大类,行业中小类,应征发生日期,预算分配比例,预算科目,原缴款期限,征收代理方式,征收方式,征收机关,征收品目,征收人员,滞纳金的滞纳天数,重点税源标志,注册地址所在行政区域,专管员";
		String[] arr = fields4CN.split(",");
		String[] arr1 = new String[arr.length];
		//`gljg` VARCHAR(50) NULL DEAULT NULL COMMENT '管理机关',
		String eName="";
		String field="";
		StringBuffer sb =new StringBuffer(1024);
		sb.append("ALTER TABLE bn_jjzb  OWNER TO gpadmin;")
		  .append("\n")
		  .append("COMMENT ON TABLE "+tname+" IS '"+tCnName+"';")
		  .append("\n");
		for(int i=0;i<arr.length;i++){
			eName=getPYIndexStr(arr[i], false);
			sb.append("COMMENT ON COLUMN ")
			  .append(tname)
			  .append(".")
			  .append(eName)
			  .append(" IS ")
			  .append("'")
			  .append( arr[i])
			  .append("';")
			  .append("\n");
		}
		return sb.toString();
	}
	public static String genOracleCnFields(String fields4CN){
		//String str="管理机关,核算机关,入库日期,征收项目,待征属性,登记管理机关,登记注册类型,地区,调账类型,国库银行,国库帐号,汇总日期,机构申报日结时间,稽查机关,计税依据,缴款方式,缴款流水号,缴款凭证类型,缴款期限,缴款日期,开票机关,开票人员,扣款银行,扣款银行账号,隶属关系,联系电话,纳税人编码,纳税人名称,年度,票证号码,票证数量,票证种类,凭证序号,凭证种类,企业所得税管理机关,清算日期,入库金额,申报方式,申报人员,申报日结工作时间,申报日结人员,申报日期,申报属性,实际经营地址,收款账号ID,税费所属期起,税费所属期止,税款属性1,税款属性2,税率,特殊企业类别,填发时间,销号类型,销号人员,销号系统时间,行业大类,行业中小类,应征发生日期,预算分配比例,预算科目,原缴款期限,征收代理方式,征收方式,征收机关,征收品目,征收人员,滞纳金的滞纳天数,重点税源标志,注册地址所在行政区域,专管员";
		String[] arr = fields4CN.split(",");
		String[] arr1 = new String[arr.length];
		//`gljg` VARCHAR(50) NULL DEAULT NULL COMMENT '管理机关',
		String eName="";
		String field="";
		for(int i=0;i<arr.length;i++){
//			eName=getPYIndexStr(arr[i], false);
			field=" \n"+arr[i]+"  VARCHAR2(50)";
			arr1[i]=field;
		}
		return StringUtils.join(arr1,",");
	}
	
	public static String genOracleFields(String fields4CN){
		//String str="管理机关,核算机关,入库日期,征收项目,待征属性,登记管理机关,登记注册类型,地区,调账类型,国库银行,国库帐号,汇总日期,机构申报日结时间,稽查机关,计税依据,缴款方式,缴款流水号,缴款凭证类型,缴款期限,缴款日期,开票机关,开票人员,扣款银行,扣款银行账号,隶属关系,联系电话,纳税人编码,纳税人名称,年度,票证号码,票证数量,票证种类,凭证序号,凭证种类,企业所得税管理机关,清算日期,入库金额,申报方式,申报人员,申报日结工作时间,申报日结人员,申报日期,申报属性,实际经营地址,收款账号ID,税费所属期起,税费所属期止,税款属性1,税款属性2,税率,特殊企业类别,填发时间,销号类型,销号人员,销号系统时间,行业大类,行业中小类,应征发生日期,预算分配比例,预算科目,原缴款期限,征收代理方式,征收方式,征收机关,征收品目,征收人员,滞纳金的滞纳天数,重点税源标志,注册地址所在行政区域,专管员";
		String[] arr = fields4CN.split(",");
		String[] arr1 = new String[arr.length];
		//`gljg` VARCHAR(50) NULL DEAULT NULL COMMENT '管理机关',
		String eName="";
		String field="";
		for(int i=0;i<arr.length;i++){
			eName=getPYIndexStr(arr[i], false);
			field="`"+eName+"`  VARCHAR(50) NULL DEFAULT NULL COMMENT '" +arr[i] + "'";
			arr1[i]=field;
		}
		return StringUtils.join(arr1,",");
	}

	/**
	 * 
	 * 返回首字母
	 * 
	 * @param strChinese
	 * 
	 * @param bUpCase
	 * 
	 * @return
	 */

	public static String getPYIndexStr(String strChinese, boolean bUpCase) {
		try {
			StringBuffer buffer = new StringBuffer();
			byte b[] = strChinese.getBytes("GBK");// 把中文转化成byte数组
			for (int i = 0; i < b.length; i++) {
				if ((b[i] & 255) > 128) {
					int char1 = b[i++] & 255;
					char1 <<= 8;// 左移运算符用“<<”表示，是将运算符左边的对象，向左移动运算符右边指定的位数，并且在低位补零。其实，向左移n位，就相当于乘上2的n次方
					int chart = char1 + (b[i] & 255);
					buffer.append(getPYIndexChar((char) chart, bUpCase));
					continue;
				}
				char c = (char) b[i];
				if (!Character.isJavaIdentifierPart(c))// 确定指定字符是否可以是 Java
														// 标识符中首字符以外的部分。
					c = 'A';
				buffer.append(c);
			}
			return buffer.toString();
		} catch (Exception e) {
			System.out.println((new StringBuilder())
					.append("\u53D6\u4E2D\u6587\u62FC\u97F3\u6709\u9519")
					.append(e.getMessage()).toString());
		}
		return null;
	}

	/**
	 * 
	 * 得到首字母
	 * @param strChinese
	 * @param bUpCase
	 * @return
	 */

	private static char getPYIndexChar(char strChinese, boolean bUpCase) {
		int charGBK = strChinese;
		char result;
		if (charGBK >= 45217 && charGBK <= 45252)
			result = 'A';
		else
		if (charGBK >= 45253 && charGBK <= 45760)
			result = 'B';
		else
		if (charGBK >= 45761 && charGBK <= 46317)
			result = 'C';
		else
		if (charGBK >= 46318 && charGBK <= 46825)
			result = 'D';
		else
		if (charGBK >= 46826 && charGBK <= 47009)
			result = 'E';
		else
		if (charGBK >= 47010 && charGBK <= 47296)
			result = 'F';
		else
		if (charGBK >= 47297 && charGBK <= 47613)
			result = 'G';
		else
		if (charGBK >= 47614 && charGBK <= 48118)
			result = 'H';
		else
		if (charGBK >= 48119 && charGBK <= 49061)
			result = 'J';
		else
		if (charGBK >= 49062 && charGBK <= 49323)
			result = 'K';
		else
		if (charGBK >= 49324 && charGBK <= 49895)
			result = 'L';
		else
		if (charGBK >= 49896 && charGBK <= 50370)
			result = 'M';
		else
		if (charGBK >= 50371 && charGBK <= 50613)
			result = 'N';
		else
		if (charGBK >= 50614 && charGBK <= 50621)
			result = 'O';
		else
		if (charGBK >= 50622 && charGBK <= 50905)
			result = 'P';
		else
		if (charGBK >= 50906 && charGBK <= 51386)
			result = 'Q';
		else
		if (charGBK >= 51387 && charGBK <= 51445)
			result = 'R';
		else
		if (charGBK >= 51446 && charGBK <= 52217)
			result = 'S';
		else
		if (charGBK >= 52218 && charGBK <= 52697)
			result = 'T';
		else
		if (charGBK >= 52698 && charGBK <= 52979)
			result = 'W';
		else
		if (charGBK >= 52980 && charGBK <= 53688)
			result = 'X';
		else
		if (charGBK >= 53689 && charGBK <= 54480)
			result = 'Y';
		else
		if (charGBK >= 54481 && charGBK <= 55289)
			result = 'Z';
		else
			result = (char) (65 + (new Random()).nextInt(25));
		if (!bUpCase)
			result = Character.toLowerCase(result);
		return result;
	}
	public static String createMysqlTable(String tName,String tCnName,String fields4Cn){
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE `")
		  .append(tName)
		  .append("` (`id` VARCHAR(36) NOT NULL COMMENT '主键',");
		String str = genMysqlFields(fields4Cn);
		sb.append(str);
		sb.append(",`cj_at` VARCHAR(36) DATETIME NOT NULL COMMENT '采集时间', PRIMARY KEY (`id`))")
		  .append(" COMMENT='")
		  .append(tCnName)
		  .append("'")
		  .append(" COLLATE='utf8_general_ci' ")
		  .append(" ENGINE=InnoDB ")
		  .append(" ROW_FORMAT=COMPACT;");
		return sb.toString();
	}
	
	public static String createOracleTable(String tName,String tCnName,String fields4Cn){
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ")
		  .append(tName)
		  .append("\n")
		  .append("(")
		  .append("\n")
		  .append("id VARCHAR2(100),");
		String str = genOracleCnFields(fields4Cn);
		sb.append(str)
		  .append("\n")
		  .append(")")
		  .append("\n")
		  .append(" tablespace ZHZS")
		  .append("\n")
		  .append(" pctfree 10")
		  .append("\n")
		  .append(" initrans 1")
		  .append("\n")
		  .append(" maxtrans 255;")
		  .append("\n")
		  .append(" comment on table ")
		  .append(tName)
		  .append(" \n")
		  .append(" is '")
		  .append(tCnName)
		  .append("';")
		  .append("\n")
		  .append(" alter table ")
		  .append(tName)
		  .append("\n")
		  .append(" add constraint PK_")
		  .append(tName)
		  .append(" primary key (ID)")
		  .append("\n")
		  .append(" using index ")
		  .append(" \n")
		  .append(" tablespace ZHZS")
		  .append(" \n")
		  .append("pctfree 10")
		  .append(" \n")
		  .append("initrans 2")
		  .append(" \n")
		  .append("maxtrans 255;");
		  
		return sb.toString();
	}
	public static String createPostgreSQLTable(String tName,String tCnName,String fields4Cn){
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE ")
		  .append(tName)
		  .append("\n")
		  .append("( \n   id character varying(36) NOT NULL,\n")
		  .append("   nsrmc_bz character varying(36) NOT NULL,\n");
		String str = genPostgreSQLFields(fields4Cn);
		sb.append(str)
		  .append("\n")
		  .append("   cj_at character varying(36),")
		  .append(" \n")
		  .append("   xyml_dm character varying(36),")
		  .append(" \n")
		  .append("   xydl_dm character varying(36),")
		  .append(" \n")
		  .append("   xyzl_dm character varying(36),")
		  .append(" \n")
		  .append("   xyxl_dm character varying(36),")
		  .append(" \n")
		  .append("   djzclx_dm character varying(36),")
		  .append(" \n")
		  .append("   djzclx_dl_dm character varying(36),")
		  .append(" \n")
		  .append("   djzclx_in_dm character varying(36),")
		  .append(" \n")
//		  .append("   qybz character varying(12),")
//		  .append(" \n")
		  .append("   xzq character varying(36),")
		  .append(" \n")
//		  .append("   xzq_mc character varying(36),")
//		  .append(" \n")
		  .append("   nsrfl_dm character varying(1),")
		  .append(" \n")
		  
		  .append("   CONSTRAINT ")
		  .append(tName+"_pkey primary key (id)")
		  .append("\n")
		  .append(")")
		  .append("\n")
		  .append("WITH (  OIDS=FALSE)")
		  .append("\n")
		  .append("DISTRIBUTED BY (id);")
		  .append("\n")
		  .append(genPostgreSQLComment(tName,tCnName,fields4Cn));
		sb.append("COMMENT ON COLUMN "+tName+".nsrmc_bz"+" IS '纳税人标准名称';\n")
		  .append("COMMENT ON COLUMN "+tName+".xyml_dm"+" IS '行业门类代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".xydl_dm"+" IS '行业大类代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".xyzl_dm"+" IS '行业中类代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".xyxl_dm"+" IS '行业小类代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".djzclx_dm"+" IS '登记注册类型二级代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".djzclx_dl_dm"+" IS '登记注册类型一级代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".djzclx_in_dm"+" IS '登记注册类型三级代码';\n")
//		  .append("COMMENT ON COLUMN "+tName+".qybz"+" IS '区域标志';\n")
		  .append("COMMENT ON COLUMN "+tName+".xzq"+" IS '行政区代码';\n")
		  .append("COMMENT ON COLUMN "+tName+".nsrfl_dm"+" IS '纳税人分类代码';\n");
		
		
		return sb.toString();
	}
}
