package com.jacqui.core.jdbc.pool;

import org.apache.tomcat.jdbc.pool.PoolProperties;

import com.jacqui.constant.Constant;
import com.jacqui.core.security.AESCoder;

public class PoolPropertiesExt extends PoolProperties{

	/* (non-Javadoc)
	 * @see org.apache.tomcat.jdbc.pool.PoolProperties#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		byte[] decryptFrom = AESCoder.parseHexStr2Byte(password);   
		byte[] decryptResult = AESCoder.decrypt(decryptFrom,Constant.AES_PWKEY);
		String encodePwd = new String(decryptResult);
		super.setPassword(encodePwd);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	

}
