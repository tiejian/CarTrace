package com.jacqui.core.form;

/**
 * 
 * @ClassName: JSonReturnObject 
 * @Description: 返回成功失败时的包装类
 * @author ilaotou@qq.com
 * @date 2015年5月14日 下午6:17:08 
 *
 */
public class JSonReturnObject {
	private String errCode;
		private Object obj;
		private String errMsg;
		public String getErrCode() {
			return errCode;
		}
		public void setErrCode(String errCode) {
			this.errCode = errCode;
		}
		public Object getObj() {
			return obj;
		}
		public void setObj(Object obj) {
			this.obj = obj;
		}
		/** 
		 * @return errMsg 
		 */
		public String getErrMsg() {
			return errMsg;
		}
		/** 
		 * @param errMsg 要设置的 errMsg 
		 */
		public void setErrMsg(String errMsg) {
			this.errMsg = errMsg;
		}
}

