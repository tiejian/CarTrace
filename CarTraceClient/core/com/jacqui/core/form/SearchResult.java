package com.jacqui.core.form;


public class SearchResult {
	
	private String tableName;
	private String tableComment;
	private int totalCount;
	private String json;
	public SearchResult(String tableName, String tableComment, int totalCount,
			String json) {
		super();
		this.tableName = tableName;
		this.tableComment = tableComment;
		this.totalCount = totalCount;
		this.json = json;
	}
	/** 
	 * @return tableName 
	 */
	public String getTableName() {
		return tableName;
	}
	/** 
	 * @param tableName 要设置的 tableName 
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	/** 
	 * @return tableComment 
	 */
	public String getTableComment() {
		return tableComment;
	}
	/** 
	 * @param tableComment 要设置的 tableComment 
	 */
	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}
	/** 
	 * @return totalCount 
	 */
	public int getTotalCount() {
		return totalCount;
	}
	/** 
	 * @param totalCount 要设置的 totalCount 
	 */
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	/** 
	 * @return json 
	 */
	public String getJson() {
		return json;
	}
	/** 
	 * @param json 要设置的 json 
	 */
	public void setJson(String json) {
		this.json = json;
	}
	

}
