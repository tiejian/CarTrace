package com.jacqui.core.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.common.collect.Lists;

/**
 * 
 * @ClassName: ExcelUtilExt2 
 * @Description: 能过模板生成Excel
 * @author ilaotou@qq.com
 * @date 2014年8月12日 下午1:47:17 
 *
 */
public class ExcelUtilExt2 {
	
//	private static SXSSFWorkbook wb;

	private static  CellStyle titleStyle;		// 标题行样式
	private static  Font titleFont;				// 标题行字体		
	private static CellStyle dateStyle;			// 日期行样式
	private static Font dateFont;				// 日期行字体
	private static CellStyle headStyle;			// 表头行样式
	private static Font headFont;				// 表头行字体
	private static CellStyle contentStyle ;		// 内容行样式
	private static Font contentFont;			// 内容行字体
	private static String	srcXlsPath	= "";	// // excel模板路径
	private static String	desXlsPath	= "";
	static Workbook	wb			= null;
	static Sheet sheet = null;
	private static int titleRowCount;    //模板标题行数量
	
	
	/**
	 * 获取所读取excel模板的对象
	 * @throws InvalidFormatException 
	 */
	public static void getSheet() throws InvalidFormatException {
		try {
			File fi = new File(srcXlsPath);
			if (!fi.exists()) {
				System.out.println("模板文件:" + srcXlsPath + "不存在!");
				return;
			}
			InputStream is = new FileInputStream(fi.getAbsolutePath());
			/*
			 * 区别2007前后版本
			 */
			try {
				wb = create(is);
			} catch (Exception ex) {
				wb = create(is);
			}
			sheet =  wb.getSheetAt(0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * @throws InvalidFormatException 
     * 
     * @Title: init 
     * @Description: 初始化
     * @param srcXlsPath
     * @param titleRowCount
     * @return void    返回类型 
     * @throws
     */
	private static void init(String srcXlsPath,int titleRowCount) throws InvalidFormatException
	{
		ExcelUtilExt2.srcXlsPath = srcXlsPath;
		ExcelUtilExt2.titleRowCount = titleRowCount;
		getSheet();
		titleFont = wb.createFont();
		titleStyle = wb.createCellStyle();
		dateStyle = wb.createCellStyle();
		dateFont = wb.createFont();
		headStyle = wb.createCellStyle();
		headFont = wb.createFont();
		contentStyle = wb.createCellStyle();
		contentFont = wb.createFont();
		
		initTitleCellStyle();
		initTitleFont();
		initDateCellStyle();
		initDateFont();
		initHeadCellStyle();
		initHeadFont();
		initContentCellStyle();
		initContentFont();
	}
	
	public static Workbook create(InputStream inp) throws IOException,
		InvalidFormatException {
		if (!inp.markSupported()) {
			inp = new PushbackInputStream(inp, 8);
		}
		if (POIFSFileSystem.hasPOIFSHeader(inp)) {
			return new HSSFWorkbook(inp);
		}
		if (POIXMLDocument.hasOOXMLHeader(inp)) {
			return new XSSFWorkbook(OPCPackage.open(inp));
		}
		throw new IllegalArgumentException("你的excel版本目前poi解析不了");
	}

	public static void export2Excel(ExportSetInfo setInfo,String srcXlsPath,int titleRowCount) throws IOException, InvalidFormatException{
		init(srcXlsPath,titleRowCount);
		Set<Entry<String, List>> set = setInfo.getObjsMap().entrySet();
		String[] sheetNames = new String[setInfo.getObjsMap().size()];
		String[] mergeCells = setInfo.getMergeCells();
		int sheetNameNum = 0;
		for (Entry<String, List> entry : set)
		{
			sheetNames[sheetNameNum] = entry.getKey();
			sheetNameNum++;
		}
		int[] mergeIndex = new int[setInfo.getMergeCells().length];
		for(int i=0;i<mergeIndex.length;i++){
			mergeIndex[i]=titleRowCount;
		}
		int sheetNum = 0;
		for (Entry<String, List> entry : set)
		{
			// Sheet
			List objs = entry.getValue();
			String[] fieldNames = setInfo.getFieldNames().get(sheetNum);
			Object oldObj=null;
			for (Object obj : objs)
			{
				
				Row contentRow = sheet.createRow(titleRowCount);
				
				contentRow.setHeight((short) 300);
				Cell[] cells = getCells(contentRow, setInfo.getFieldNames().get(sheetNum).length);
				int cellNum = 1;					// 去掉一列序号，因此从1开始
				if(fieldNames != null)
				{
					for (int num = 0; num < fieldNames.length; num++) //循环列进行赋值
					{
						Object value = ReflectionUtils.invokeGetterMethod(obj, fieldNames[num]);
						System.out.print(value);
						if(null!=mergeCells && mergeCells.length>0 && null!=oldObj){
							for(int i=0;i<mergeCells.length;i++){
								if(fieldNames[num].equals(mergeCells[i])){
									if(ReflectionUtils.invokeGetterMethod(oldObj, fieldNames[num]).equals(value)){
//										System.out.print("**"+fieldNames[num]);
//										System.out.print(contentRow.getRowNum());
//										System.out.print(cells[cellNum].getColumnIndex() +"**,");
										
									}else{
										CellRangeAddress dateRange = new CellRangeAddress(mergeIndex[i],contentRow.getRowNum()-1,cells[cellNum].getColumnIndex(),cells[cellNum].getColumnIndex());
										sheet.addMergedRegion(dateRange);
										mergeIndex[i]=contentRow.getRowNum();
									}
								}								
							}
						}
						cells[cellNum].setCellValue(value == null ? "" : value.toString());
						cellNum++;
					}
					System.out.println("");
				}
				oldObj =obj;
				titleRowCount++;
			}
			for(int i=0;i<mergeIndex.length;i++){
				if(mergeIndex[i]==titleRowCount-1){
					
				}else{
					for(int j=0;j<fieldNames.length;j++){
						if(mergeCells[i].equals(fieldNames[j])){
							CellRangeAddress dateRange = new CellRangeAddress(mergeIndex[i],titleRowCount-1,j+1,j+1);
							sheet.addMergedRegion(dateRange);
						}
					}
				}
				
			}
			
			adjustColumnSize(sheet, sheetNum, fieldNames);	// 自动调整列宽
			sheetNum++;
		}
		
//		CellRangeAddress dateRange = new CellRangeAddress(4, 5, 2, 
//				2);
//		sheet.addMergedRegion(dateRange);
//		CellRangeAddress dateRange1 = new CellRangeAddress(5, 7, 2, 
//				2);
//		sheet.addMergedRegion(dateRange1);
		wb.write(setInfo.getOut());
		
	}
	
	/**
	 * @Description: 创建内容行的每一列(附加一列序号)
	 */
	private static Cell[] getCells(Row contentRow, int num)
	{
		Cell[] cells = new Cell[num + 1];

		for (int i = 0,len = cells.length; i < len; i++)
		{
			cells[i] = contentRow.createCell(i);
			cells[i].setCellStyle(contentStyle);
		}
		// 设置序号列值，除去标题行和日期行
		cells[0].setCellValue(contentRow.getRowNum() - titleRowCount);

		return cells;
	}
	
	/**
	 * @Description: 自动调整列宽
	 */
	@SuppressWarnings("unused")
	private static void adjustColumnSize(Sheet sheet, int sheetNum,
			String[] fieldNames)
	{
		for(int i = 0; i < fieldNames.length + 1; i++)
		{
			sheet.autoSizeColumn(i, true);
		}
	}
	
	
	
	
	
	
	public static void main(String[] args) throws IOException, InvalidFormatException{
//		vo.setBl("bl1,");
//		vo.setCjs("cjs123,");
//		vo.setDm("dmsss,");
//		vo.setRks("rksaaa,");
//		vo.setSqmc("sqmcaaa,");
//		JcsjVo vo1 = new JcsjVo();
//		vo1.setBl("bl6,");
//		vo1.setCjs("cjs777,");
//		vo1.setDm("dm888,");
//		vo1.setRks("rks999,");
//		vo1.setSqmc("sqmc000,");
//		String[] fields=new String[] { "dm", "sqmc", "rks", "cjs","bl"};
//		List<JcsjVo> page = Lists.newArrayList();
//		page.add(vo1);
//		page.add(vo1);
//		page.add(vo1);
//		page.add(vo);
//		page.add(vo);
//		page.add(vo);
//		page.add(vo);
//		page.add(vo);
//		page.add(vo);
//		page.add(vo1);
//		page.add(vo1);
//		LinkedHashMap<String, List> map = new LinkedHashMap<String, List>();
//		map.put("基础数据采集情况", page);
//		FileOutputStream out = new FileOutputStream("d:/aa.xlsx");
//		List<String[]> fieldNames = new ArrayList<String[]>();
//		fieldNames.add(fields);
//		ExportSetInfo setInfo = new ExportSetInfo();
//		setInfo.setObjsMap(map);
//		setInfo.setFieldNames(fieldNames);
//		setInfo.setTitles(new String[] { "基础数据采集情况"});
//		setInfo.setOut(out);
//		setInfo.setMergeCells(new String[]{"sqmc"});
//		ExcelUtilExt2.export2Excel(setInfo,"D:/项目文档/社区平台/2014-08-07-报表新需求/报表/sq_ptyw.xlsx",4);
	}
	
	/**
	 * @Description: 初始化标题行样式
	 */
	private static void initTitleCellStyle()
	{
		titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
		titleStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		titleStyle.setFont(titleFont);
		titleStyle.setFillBackgroundColor(IndexedColors.SKY_BLUE.index);
	}

	/**
	 * @Description: 初始化日期行样式
	 */
	private static void initDateCellStyle()
	{
		dateStyle.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
		dateStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		dateStyle.setFont(dateFont);
		dateStyle.setFillBackgroundColor(IndexedColors.SKY_BLUE.index);
	}

	/**
	 * @Description: 初始化表头行样式
	 */
	private static void initHeadCellStyle()
	{
		headStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		headStyle.setFont(headFont);
		headStyle.setFillBackgroundColor(IndexedColors.YELLOW.index);
		headStyle.setBorderTop(CellStyle.BORDER_MEDIUM);
		headStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headStyle.setBorderLeft(CellStyle.BORDER_THIN);
		headStyle.setBorderRight(CellStyle.BORDER_THIN);
		headStyle.setTopBorderColor(IndexedColors.BLUE.index);
		headStyle.setBottomBorderColor(IndexedColors.BLUE.index);
		headStyle.setLeftBorderColor(IndexedColors.BLUE.index);
		headStyle.setRightBorderColor(IndexedColors.BLUE.index);
	}

	/**
	 * @Description: 初始化内容行样式
	 */
	private static void initContentCellStyle()
	{
		contentStyle.setAlignment(CellStyle.ALIGN_CENTER);
		contentStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		contentStyle.setFont(contentFont);
		contentStyle.setBorderTop(CellStyle.BORDER_THIN);
		contentStyle.setBorderBottom(CellStyle.BORDER_THIN);
		contentStyle.setBorderLeft(CellStyle.BORDER_THIN);
		contentStyle.setBorderRight(CellStyle.BORDER_THIN);
		contentStyle.setTopBorderColor(IndexedColors.BLUE.index);
		contentStyle.setBottomBorderColor(IndexedColors.BLUE.index);
		contentStyle.setLeftBorderColor(IndexedColors.BLUE.index);
		contentStyle.setRightBorderColor(IndexedColors.BLUE.index);
		contentStyle.setWrapText(true);	// 字段换行
	}
	
	/**
	 * @Description: 初始化标题行字体
	 */
	private static void initTitleFont()
	{
		titleFont.setFontName("华文楷体");
		titleFont.setFontHeightInPoints((short) 20);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		titleFont.setCharSet(Font.DEFAULT_CHARSET);
		titleFont.setColor(IndexedColors.BLUE_GREY.index);
	}

	/**
	 * @Description: 初始化日期行字体
	 */
	private static void initDateFont()
	{
		dateFont.setFontName("隶书");
		dateFont.setFontHeightInPoints((short) 10);
		dateFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		dateFont.setCharSet(Font.DEFAULT_CHARSET);
		dateFont.setColor(IndexedColors.BLUE_GREY.index);
	}

	/**
	 * @Description: 初始化表头行字体
	 */
	private static void initHeadFont()
	{
		headFont.setFontName("宋体");
		headFont.setFontHeightInPoints((short) 10);
		headFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		headFont.setCharSet(Font.DEFAULT_CHARSET);
		headFont.setColor(IndexedColors.BLUE_GREY.index);
	}

	/**
	 * @Description: 初始化内容行字体
	 */
	private static void initContentFont()
	{
		contentFont.setFontName("宋体");
		contentFont.setFontHeightInPoints((short) 10);
		contentFont.setBoldweight(Font.BOLDWEIGHT_NORMAL);
		contentFont.setCharSet(Font.DEFAULT_CHARSET);
		contentFont.setColor(IndexedColors.BLUE_GREY.index);
	}
	

}
