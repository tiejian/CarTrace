<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
    $(function() {
    	$('#main-tab2').tabs({
			plain:true,
			narrow: true,
			border:false,
		});
		
		$(".list-group-item").click(function(){
    		$(".list-group-item").siblings().removeClass("on");
    		$(this).addClass("on");
    		var url=$(this).attr("url");
    		var text=$(this).attr("txt");
    		mainload(url,text);
    	});
    	
    	$('#userinfo-1').tooltip({
			position: 'right',
			content: '<div style="color:#fff;width:70px;text-align:center;">个人信息</div>',
			onShow: function(){
				$(this).tooltip('tip').css({
					backgroundColor: '#666',
					borderColor: '#666'
				});
			}
		});
		
		
		function mainload(url,txt){
			if($('#main-tab2').tabs('exists',txt)){
				$('#main-tab2').tabs('select',txt);
				var tab = $('#main-tab2').tabs('getSelected');
				tab.panel('refresh', url);
			}else{
				$('#main-tab2').tabs('add',{
					id:'1',
					title: txt,
					href: url,
					closable: true
				});
			}
		}
    });
    
</script>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:false,border:true" title='<i class="fa fa-th-large" style="color:#4fb2ff;"></i>&nbsp;&nbsp;功能' style="width:220px;">  
         <div class="easyui-accordion" data-options="fit:true,border:false">  
             <div title='<i class="fa fa-car" style="color:#4fb2ff;"></i>&nbsp;&nbsp;车辆信息'> 
             	<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="客运班车"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;客运班车</li>
				  <li class="list-group-item" url="${ctx}/yuan/audit/prj/search?_m=init" txt="客运包车"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;客运包车</li>
				  <li class="list-group-item" url="${ctx}/bn/project/expert/prj/search?_m=init" txt="客运旅游"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;客运旅游</li>
				  <li class="list-group-item" url="${ctx}/expert/audit/prj/search?_m=init" txt="货运车辆"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;货运车辆</li>
				  <li class="list-group-item" url="${ctx}/expert/audit/prj/search?_m=init" txt="环卫车辆"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;环卫车辆</li>
				</ul>
			</div>
			<div title='<i class="fa fa-users" style="color:#4fb2ff;"></i>&nbsp;&nbsp;驾驶员信息'> 
             	<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="驾驶员"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;驾驶员</li>
				</ul>
			</div>
			<div title='<i class="fa fa-square-o" style="color:#4fb2ff;"></i>&nbsp;&nbsp;运营线路信息'> 
             	<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="运营线路"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;运营线路</li>
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="线路停靠点"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;线路停靠点</li>
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="线路停靠点"><i class="fa fa-angle-right" style="color:#4fb2ff;font-size:16px;"></i>&nbsp;&nbsp;线路区域</li>
				</ul>
			</div>
         </div>  
    </div>  
	<div data-options="region:'center',border:false">
		<div class="easyui-tabs main-tab2" data-options="height:'100%',width:'100%'"  id="main-tab2">
			<div title="欢迎页面" id="homepage">
				<div class="welcome">欢迎使用统计分析管理功能</div>
			</div>
		</div>
	</div>
</div>
