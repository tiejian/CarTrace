/**
 * 根据json对象返回状态信息，进行统一控制
 * @param json
 * @returns {Boolean}
 */
statusMsg = function(json) {
	var code = json.errcode;
	var msg = json.errmsg;
	var content = "错误信息：" + msg ;
	content = "<font style='color:red;'>"+content+"</font>";
	if(code=="0"){//成功
		return true;
	}else if(code=="997"){//登录超时
		//window.parent.sessionDialog(content);
		return false;
	}else if(code=="998"){//没有相关权限
		//try { 
		//	window.parent.powerDialog(content);
			//} catch (e) { 
			//	window.parent.powerDialog(content);
			//} 
		return false;
	}else{
		$.messager.alert("提示",content,"error");
		return false;
	}
	return false;
};