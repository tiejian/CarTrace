<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	var $st_user_lock = jQuery.namespace('client.stuser.lock');
	
	$(function() {
		$st_user_lock.save = function($dialog){
	        $('#st_user_lock_form').form('submit',{
                onSubmit:function(){
                    return $(this).form('enableValidation').form('validate');
                },
                success:function(data){
                	$st_user_lock_form = $("#st_user_lock_form");
					$.post('${ctx}/st/user/locklogin?_m=exec&r='+Math.random(), $st_user_lock_form.formSerialize(), function(json) {
			        	if(statusMsg(json)){
			        		 window.top.location='${ctx}/index';
			        	}
			        }, "JSON");
			    }
            });
		};
	});
</script>
<div class="form-box">
	<div class="form-body">
		<form role="form" name="st_user_lock_form" id="st_user_lock_form" class="easyui-form form form-horizontal" data-options="novalidate:true">
			<input type="hidden" name="username" value="${username }" />
			<div class="form-group">
				<label class="col-sm-3 control-label">
					<p class="label-txt">登录密码：</p>
				</label>
				<div class="col-sm-7">
					<input type="password" id="passwd" name="passwd" value="" class="form-control easyui-validatebox" style="width:100%;height:34px;line-height:34px;"  required="required">
				</div>
			</div>
		</form>
	</div>
</div>
