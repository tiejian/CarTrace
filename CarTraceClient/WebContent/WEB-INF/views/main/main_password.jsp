<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	var $st_user = jQuery.namespace('client.stuser');
	
	$.extend($.fn.validatebox.defaults.rules, {
	    equals: {
			validator: function(value,param){
				return value == $(param[0]).val();
			},
			message: '输入内容不等于新密码内容.'
	    }
	});
	
	$(function() {
	
		$st_user.save = function($dialog){
			$('#st_user_form').form('submit',{
                onSubmit:function(){
                    return $(this).form('enableValidation').form('validate');
                },
                success:function(data){
                	var $st_user_form = $('#st_user_form');   
			        $.post('${ctx}/st/user/update?_m=exec&r='+Math.random(), $st_user_form.formSerialize(), function(json) {
			        	if(statusMsg(json)){
			        		$.messager.alert("提示","保存成功！","info");
			        		$dialog.dialog('close');
			        	}
			        }, "JSON");
			    }
            });
		};
		
		$('#passwd').keyup(function () { 
			var strongRegex = new RegExp("^(?=.{8,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$", "g"); 
			var mediumRegex = new RegExp("^(?=.{7,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$", "g"); 
			var enoughRegex = new RegExp("(?=.{6,}).*", "g"); 
		
			if (false == enoughRegex.test($(this).val())) { 
				$('#level').removeClass('pw-weak'); 
				$('#level').removeClass('pw-medium'); 
				$('#level').removeClass('pw-strong'); 
				$('#level').addClass(' pw-defule'); 
				//密码小于六位的时候，密码强度图片都为灰色 
				
			} 
			else if (strongRegex.test($(this).val())) { 
				$('#level').removeClass('pw-weak'); 
				$('#level').removeClass('pw-medium'); 
				$('#level').removeClass('pw-strong'); 
				$('#level').addClass(' pw-strong'); 
				//密码为八位及以上并且字母数字特殊字符三项都包括,强度最强 
				
			} 
			else if (mediumRegex.test($(this).val())) { 
				$('#level').removeClass('pw-weak'); 
				$('#level').removeClass('pw-medium'); 
				$('#level').removeClass('pw-strong'); 
				$('#level').addClass(' pw-medium'); 
				//密码为七位及以上并且字母、数字、特殊字符三项中有两项，强度是中等 
				
			} 
			else { 
				$('#level').removeClass('pw-weak'); 
				$('#level').removeClass('pw-medium'); 
				$('#level').removeClass('pw-strong'); 
				$('#level').addClass('pw-weak'); 
				//如果密码为6为及以下，就算字母、数字、特殊字符三项都包括，强度也是弱的 
				
			} 
			return true; 
		}); 
	});
</script>
<div class="form-box">
	<div class="form-body">
		<form role="form" name="st_user_form" id="st_user_form" class="easyui-form form form-horizontal" data-options="novalidate:true">
			<div class="seg-title">
				<div class="seg-title-box">
		    		 <ul>
			    		<li class="seg-title-box-icon" ><img src="${ctx }/assets/images/communication.gif"/></li>
			    		<li class="seg-title-box-text">基本信息</li>
			    	</ul>
		    	</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">
					<p class="label-txt">旧密码：</p>
					<p class="star">*</p>
				</label>
				<div class="col-sm-7">
					<input type="password" id="oldpasswd" name="oldpasswd" value="" class="form-control easyui-validatebox" style="width:100%;height:34px;line-height:34px;" title="旧密码" required="required" validType="length[6,20]" >
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" >
					<p class="label-txt">新密码：</p>
					<p class="star">*</p>
				</label>
				<div class="col-sm-7">
					<input type="password" id="passwd" name="passwd" value="" class="form-control easyui-validatebox" style="width:100%;height:34px;line-height:34px;" title="新密码" data-options="required:true,validType:'length[6,20]'">
					<div id="level" class="pw-strength">           	
					<div class="pw-bar"></div>
						<div class="pw-bar-on"></div>
						<div class="pw-txt">
						<span>弱</span>
						<span>中</span>
						<span>强</span>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">
					<p class="label-txt">确认密码：</p>
					<p class="star">*</p>
				</label>
				<div class="col-sm-7">
					<input type="password" id="passwdre" name="passwdre" value="" class="form-control easyui-validatebox" style="width:100%;height:34px;line-height:34px;" title="确认密码" data-options="required:true" validType="equals['#passwd']">
				</div>
			</div>
		</form>
	</div>
</div>
