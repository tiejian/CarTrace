<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap-ext.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/theme-jacqui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/styles.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/font-awesome-4.5.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.ext.css">
	
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.namespace.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/ext/datagrid-filter.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/lib/layer/layer.js"></script>
    
    <script type="text/javascript" src="${ctx}/assets/js/echarts/echarts.min.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/common.js"></script>
    
    <script type="text/javascript">
	    $(function() {
	    	setTabs();
	    	homepage();
	    	$("#main-navbar").bind("click",function(){
	    		$('.mainpage').layout('collapse','west');
	    	});
	    	$(".list-group-item").click(function(){
	    		$(".list-group-item").siblings().removeClass("on");
	    		$(this).addClass("on");
	    		var url=$(this).attr("url");
	    		var text=$(this).attr("txt");
	    		mainload(url,text);
	    	});
	    	
	    })
	</script>
	<style type="text/css">
	.nav-right-menu-toggle{cursor: pointer;}
	.nav-right-menu-toggle:HOVER{background: #0568b4;}
	.menus-user{color:#FFFFFF;height:105px;line-height: 105px;}
	.menus-user a{color:#FFFFFF;text-decoration: none;}
	.menus-user li{height:35px;line-height: 35px;padding-left:10px;cursor: pointer;}
	.menus-user li:HOVER{height:35px;line-height: 35px;background:#1caf9a}
	
	.nav-left-menu-toggle{cursor: pointer;}
	.nav-left-menu-toggle:HOVER{background: #0568b4;}
	.nav-left-menu-toggle-active{background: #0568b4;}
    </style>
</head>
<body class="easyui-layout mainpage">
    <!--[if lte IE 7]>
        <div id="errorie"><div>您还在使用老掉牙的IE，正常使用系统前请升级您的浏览器到 IE8以上版本 <a target="_blank" href="http://windows.microsoft.com/zh-cn/internet-explorer/ie-8-worldwide-languages">点击升级</a>&nbsp;&nbsp;强烈建议您更改换浏览器：<a href="http://down.tech.sina.com.cn/content/40975.html" target="_blank">谷歌 Chrome</a></div></div>
    <![endif]-->
    <div data-options="region:'north',border:false,minHeight:85" class="navbar">
    	<div style="width:25%;height:70px;line-height:70px;float: left;" id="logo"></div>
    	<div style="width:50%;height:70px;line-height:70px;float: left;" id="nav">
    		<div style="float:left;margin-left:30px;">
    			<ul class="nav navbars-left">
	    			<li onclick="mainload1('${ctx}/index.jsp');" class="nav-left-menu-toggle nav-left-menu-toggle-active" style="float: left;text-align:center;width:85px;height:85px;margin-left:10px;text-align: ">
	    				<div style="width:85px;height: 60px;line-height:60px;"><img src="${ctx}/assets/images/nav-sy.png"/></div>
	    				<div style="width:85px;height: 20px;line-height:20px;color:#FFFFFF;">首页</div>
	    			</li>
	    			<li onclick="mainload1('${ctx}/index1.jsp');" class="nav-left-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;margin-left:10px;text-align: ">
	    				<div style="width:85px;height: 60px;line-height:60px;"><img src="${ctx}/assets/images/nav-ssjk.png"/></div>
	    				<div style="width:85px;height: 20px;line-height:20px;color:#FFFFFF;">实时监控</div>
	    			</li>
	    			<li onclick="mainload1('${ctx}/index2.jsp');" class="nav-left-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;margin-left:10px;text-align: ">
	    				<div style="width:85px;height: 60px;line-height:60px;"><img src="${ctx}/assets/images/nav-jcxx.png"/></div>
	    				<div style="width:85px;height: 20px;line-height:20px;color:#FFFFFF;">基础信息</div>
	    			</li>
	    			<li onclick="mainload1('${ctx}/demo/form');" class="nav-left-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;margin-left:10px;text-align: ">
	    				<div style="width:85px;height: 60px;line-height:60px;"><img src="${ctx}/assets/images/nav-tjfx.png"/></div>
	    				<div style="width:85px;height: 20px;line-height:20px;color:#FFFFFF;">统计分析</div>
	    			</li>
	    			<li class="nav-left-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;margin-left:10px;text-align: ">
	    				<div style="width:85px;height: 60px;line-height:60px;"><img src="${ctx}/assets/images/nav-xtsz.png"/></div>
	    				<div style="width:85px;height: 20px;line-height:20px;color:#FFFFFF;">系统设置</div>
	    			</li>
	    		</ul>
    		</div>
    	</div>
    	<div style="width:25%;height:85px;line-height:85px;float: right;" id="out">
    		<div style="float:right;margin-right:10px;">
	    		<ul class="nav navbars-right">
	    			<li id="userinfo" class="nav-right-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;line-height: 85px;margin-left:15px;"><img src="${ctx}/assets/images/avatar-1-small.jpg" class="img-circle" style="width:40px;">&nbsp;<i class="fa fa-caret-down" style="color:#FFFFFF;"></i></li>
	    			<li style="float: left;height:85px;line-height: 85px;"><div style="border-left: 1px solid #6ab3ea;height:65px;line-height: 65px;margin-top:10px;"></div></li>
	    			<li id="help" onclick="help();" class="nav-right-menu-toggle" style="float: left;text-align:center;width:80px;height:85px;line-height: 85px;"><i class="fa fa-question-circle fa-3x" style="color:#FFFFFF;margin-top:25px;"></i></li>
	    			<li style="float: left;height:85px;line-height: 85px;"><div style="border-left: 1px solid #6ab3ea;height:65px;line-height: 65px;margin-top:10px;"></div></li>
	    			<li id="loginout" onclick="loginout();" class="nav-right-menu-toggle" style="float: left;text-align:center;width:80px;height:85px;line-height: 85px;"><i class="fa fa-power-off fa-3x" style="color:#ff6868;margin-top:25px;"></i></li>
	    		</ul>
    		</div>
    	</div>
    </div>
	<div data-options="region:'south',border:false" style="height:24px;background:#EBF7FD;">
		<div style="height:24px;line-height:24px;text-align:center;background: #efefef;">Copyright (c) 2015-* ******* Co. Ltd. * All right reserved.</div>
	</div>
	<div data-options="region:'center',border:false">
		<div class="easyui-tabs main-tab" data-options="height:'100%',width:'100%'"  id="main-tab">
			
		</div>
	</div>
	<div id="userinfo-temp" style="display: none;">
		<div style="padding:0px;margin:0px;background:#2b3034;width:130px;height:105px;">
			<ul class="menus-user">
				<li onclick="updateInfoView()">
					<a href="javascript:void(0)" >
						<i class="fa fa-user fa-lg" style="color:#43d1ff;"></i><span style="padding-left:10px;padding-bottom:3px;">个人信息</span>
					</a>
				</li>
				<li onclick="updatePswdView()">
					<a href="#">
						<i class="fa fa-key fa-lg" style="color:#ff8a00;"></i><span style="padding-left:6px;padding-bottom:3px;">密码修改</span>
					</a>
				</li>
				<li onclick="lockSystemView()">
					<a href="#">
						<i class="fa fa-lock fa-lg" style="color:red;"></i><span style="padding-left:10px;padding-bottom:3px;">锁定系统</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div id="dailogView"></div>
    <script type="text/javascript">
		function setTabs(){
			$('#main-tab').tabs({
				plain:true,
				narrow: true,
				border:false,
			});
			
			$('#userinfo').tooltip({
				content: $('#userinfo-temp').html(),
				showEvent: 'click',
				onShow: function(){
					var t = $(this);
					t.tooltip('tip').css({"background":"#2b3034","border":"1px solid #000000"});
					t.tooltip('tip').unbind().bind('mouseenter', function(){
						t.tooltip('show');
					}).bind('mouseleave', function(){
						t.tooltip('hide');
					});
				}
			});
			
			$('#help').tooltip({
				position: 'bottom',
				content: '<div style="color:#fff;width:40px;text-align:center;">帮 助</div>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#666',
						borderColor: '#666'
					});
				}
			});
			
			$('#loginout').tooltip({
				position: 'bottom',
				content: '<div style="color:#fff;width:40px;text-align:center;">退 出</div>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#666',
						borderColor: '#666'
					});
				}
			});
			
			$(".navbars-left li").click(function(){
				$(".navbars-left li").removeClass("nav-left-menu-toggle-active");
				$(this).addClass("nav-left-menu-toggle-active");
			});
			
			mainload("${ctx}/index.jsp");
			
		}
		function mainload(url){
			$(".tabs").hide();
			$(".tabs").css({"height":"0px;"});
			//$(".main-tab").css({"height":($(".main-tab").height())+"px"});
			$('#main-tab').tabs('add',{
					id:'1',
					href: url,
					closable: true
			});
			//$(".tab-panels").css({"height":($(".tab-panels").height()+26)+"px"});
		}
		function mainload1(url){
			var tab = $('#main-tab').tabs('getSelected');
			tab.panel('refresh', url);
			$(".tabs-first").hide();
			$(".tabs").hide();
		}
		function homepage(){
			//var tab = $('#main-tab').tabs('getSelected');
			//tab.panel('refresh', "${ctx}/homepage");
		}
		
		//添加初始化
		var viewDialog;
		updateInfoView = function(){
			viewDialog = $('#dailogView').dialog({
			    title: '个人信息',
		        href: '${ctx}/2.jsp?r='+Math.random(),
		        width: 550,
		        height: 420,
		        modal: true,
		        buttons: [{
	                text: '保 存',
	                width:'70px',
	                iconCls: 'icon-ok',
	                handler: function(){
	                	
	                }
		       },{
	                text: '关 闭',
	                width:'70px',
	                iconCls: 'icon-cancel',
	                handler: function(){
	                	viewDialog.dialog('close');
	                }
		       }]
			});
		};
		updatePswdView = function(){
			viewDialog = $('#dailogView').dialog({
			    title: '密码修改',
		        href: '${ctx}/st/user/password?_m=init&r='+Math.random(),
		        width: 600,
		        height: 400,
		        modal: true,
		        buttons: [{
	                text: '保 存',
	                width:'70px',
	                iconCls: 'icon-ok',
	                handler: function(){
	                	$st_user.save(viewDialog);
	                }
		       },{
	                text: '关 闭',
	                width:'70px',
	                iconCls: 'icon-cancel',
	                handler: function(){
	                	viewDialog.dialog('close');
	                }
		       }]
			});
		};
		
		lockSystemView = function(){
			$.messager.confirm('提示', '确定要锁定系统吗？', function (flag) {
                 if (flag) {
                     viewDialog = $('#dailogView').dialog({
						closable: false,
					    title: '锁定系统',
				        href: '${ctx}/st/user/lock?_m=init&r='+Math.random(),
				        width: 600,
				        height: 150,
				        modal: true,
				        buttons: [{
			                text: '登 录',
			                width:'70px',
			                handler: function(){
			                	$st_user_lock.save(viewDialog);
			                }
				       }]
					});
                 }
            });
		};
		
		loginout = function(){
			$.messager.confirm('提示', '确定要退出吗？', function (flag) {
                 if (flag) {
                     window.top.location='${ctx}/loginout?&r='+Math.random();
                 }
             });
	    };
	    
	    help = function(){
	    	window.open('http://www.baidu.com');
	    };
	</script>
	
</body>
</html>
