<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<div id="cc" class="easyui-layout" style="width: 100%; height: 100%;">
	<div data-options="region:'east',title:'East',bodyCls:'panel-body-homepage',noheader:true,border:false" style="width: 400px; ">
		<ul class="easyui-datalist" title="<i class='fa fa-volume-up'></i><span class='panel-head-title-text'>已发送通知</span>" style="width:370px;height:350px">
		    <li value="AL">Alabama</li>
		    <li value="AK">Alaska</li>
		    <li value="AZ">Arizona</li>
		    <li value="AR">Arkansas</li>
		    <li value="CA">California</li>
		    <li value="CO">Colorado</li>
		      <li value="AZ">Arizona</li>
		    <li value="AR">Arkansas</li>
		    <li value="CA">California</li>
		    <li value="CO">Colorado</li>
		      <li value="AZ">Arizona</li>
		    <li value="AR">Arkansas</li>
		    <li value="CA">California</li>
		    <li value="CO">Colorado</li>
		      <li value="AZ">Arizona</li>
		    <li value="AR">Arkansas</li>
		    <li value="CA">California</li>
		    <li value="CO">Colorado</li>
		      <li value="AZ">Arizona</li>
		    <li value="AR">Arkansas</li>
		    <li value="CA">California</li>
		    <li value="CO">Colorado</li>
		</ul>
		<div style="margin:20px 0;"></div>
		<div id="p" class="easyui-panel" title="<i class='fa fa-commenting-o'></i><span class='panel-head-title-text'>留言</span>" style="width:370px;height:350px">
	        <div class="liuyan-list-box">
		        <ul class="liuyan-list">
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		            <li class="liuyan-list-item">
		            	<div class="liuyan-list-item-container">
			            	<a href="" class="liuyan-list-item-l">
			               		<img src="http://himg.bdimg.com/sys/portrait/item/e1e7e5919ce5919ce4b88de4baa6e4b990e4b98ec618" alt="">
			                </a>
			                <div class="liuyan-list-item-r">
			               		<div class="liuyan-list-item-title-container">
			               			<a class="liuyan-list-item-title" target="_blank" href="http://koubei.baidu.com/t/29062?fr=ibaidu">广金所恶意冒用利巨人品牌做竞价欺骗网络投资人，要谨慎投资</a>
			               		</div>
			               		<div class="liuyan-list-item-detail-container">
			               		    <span class="liuyan-list-item-sq">[江东社区]</span>
			               			<a href=";" target="_blank" class="liuyan-list-item-username">李美人</a>
			               			<span class="liuyan-list-item-postdate">2016-06-06</span>
			               		</div>
			                </div>
		            	</div>
		            </li>
		        </ul>
	        </div>
	    </div>
	</div>
	<div data-options="region:'center',title:'center title',noheader:true,bodyCls:'panel-body-homepage',border:false" >
		<div class="wel-container">
		<div class="wel-top-menu">
			<div class="gWel-avatar">
				<a href="javascript:void(0);"
					class="gWel-avatar-inner nui-faceEdit nui-faceEdit-100"> <img
					src="${ctx}/images/noface.gif" width="100" height="100"> <span
					class="nui-faceEdit-tips">点击编辑头像</span>
				</a>
			</div>
			<div class="gWel-greet">
				最美的一天，从登录开始
			</div>
			<div class="gWel-mailInfo">
			<ul class="gWel-mailInfo-list">
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-unread">
			<div class="gWel-mailInfo-ico">
			<b class="nui-ico gWel-ico gWel-ico-unread-top"></b>
			<b class="nui-ico gWel-ico gWel-ico-unread-letter"></b>
			<b class="nui-ico gWel-ico gWel-ico-unread-bottom"></b>
			</div>
			<div class="gWel-mailInfo-txt">未读信息<span class="gWel-mailInfo-status">9</span></div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<b class="nui-ico gWel-ico gWel-ico-todo"></b>
					<b class="nui-ico gWel-ico gWel-ico-todo-lhand"></b>
					<b class="nui-ico gWel-ico gWel-ico-todo-shand"></b>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">待办事项</div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-user"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">个人信息</div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-cog"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">单位信息</div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-map"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">单位管辖</div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-volume-up"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">发布通知</div>
			</li>
			<!-- 
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-map-o"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">我的管辖</div>
			</li>
			<li class="js-component-component gWel-mailInfo-item gWel-mailInfo-todo" >
				<div class="gWel-mailInfo-ico">
					<i class="gWel-icon fa fa-cny"></i>
				</div>
				<div id="1466736031236_dvTodoMailInfo" class="gWel-mailInfo-txt">缴费信息</div>
			</li>
			 -->
			</div>
		</div>
		<div class="wel-main">
			<div id="p" class="easyui-panel" data-options="cls:'wel-main-box',width:'340px',height:'400px'" title="<i class='fa fa-map-marker'></i><span class='panel-head-title-text'>我的管辖</span>" >
			</div>
			<div id="p" class="easyui-panel" data-options="cls:'wel-main-box',width:'340px',height:'400px'" title="<i class='fa fa-cny'></i><span class='panel-head-title-text'>居民缴费信息</span>" >
			</div>
			<div id="p" class="easyui-panel" data-options="cls:'wel-main-box r-no-margin',width:'340px',height:'400px'" title="<i class='fa fa-phone'></i><span class='panel-head-title-text'>电话记录</span>" >
			</div>
			<div class="clear"></div>
			<div class="tg-box">
				<div class="tg">
				</div>
			</div>
		</div>
	    </div>
	</div>
</div>