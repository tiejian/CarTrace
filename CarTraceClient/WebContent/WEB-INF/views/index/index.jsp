<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap-ext.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/theme-jacqui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/font-awesome-4.5.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.ext.css">
	
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/ext/datagrid-filter.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/lib/layer/layer.js"></script>
    
    <script type="text/javascript" src="${ctx}/assets/js/echarts/echarts.min.js"></script>
    <script type="text/javascript">
	    $(function() {
	    	
	    	$('#userinfo-1').tooltip({
				position: 'right',
				content: '<div style="color:#fff;width:70px;text-align:center;">个人信息</div>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#666',
						borderColor: '#666'
					});
				}
			});
	    })
	</script>
</head>
<body>
   <div class="easyui-layout" data-options="fit:true">
				<div id="main-left-menu" data-options="region:'west',split:true,border:false,title:'==功能列表==',headerCls:'user-profile',bodyCls:'unit-admin-menu-body',cls:'unit-admin-menu',header:'#left-menu-header'" style="width:220px;">
					<div id="left-menu-header" class="user-profile">
						<div class="u-logo">
							<a id="userinfo-1" href="#"><img alt="" src="${ctx}/assets/images/avatar-1.jpg"></a>
						</div>
						<div class="u-info">
							<span class="u-name"><h5 > 管理员 </h5></span>
							<span class="last-login-time"><h6> 2016-02-25 08:08:06 </h6></span>
						</div>
					</div>
					<div class="easyui-accordion main-left-menu-body" style="width:100%;" data-options="multiple:true,border:false">
						<div style="height:240px;border:0px solid red;">
							<div class="easyui-calendar" style="width:100%;height:100%;border-right:0px;border-top:0px;"></div>
						</div>
						<iframe width="220" scrolling="no" height="55" frameborder="0" allowtransparency="true" src="http://i.tianqi.com/index.php?c=code&id=12&color=%23FFFFFF&bgc=%230070C0&icon=1&py=jilin&num=1"></iframe>
					</div>
				</div>
				<div data-options="region:'center',border:false">
					<div class="easyui-tabs main-tab" data-options="tabHeight:30,tabWidth:130,height:'100%',width:'100%'"  id="main-tab">
						<div title="首页" id="homepage">
							<div style="height:300px;text-align: center;">
							<div style="height:300px;width:40%;float:left;padding-top:10px;">
								<div id="main1" style="width: 80%;height:280px;padding-left:30px;"></div>
								<script type="text/javascript">
							        // 基于准备好的dom，初始化echarts实例
							        var myChart1 = echarts.init(document.getElementById('main1'));
							        // 指定图表的配置项和数据
			
									var option1 = {
									    title : {
									        text: '车辆状态饼状图',
									        x:'center'
									    },
									    tooltip : {
									        trigger: 'item',
									        formatter: "{a} <br/>{b} : {c} ({d}%)"
									    },
									    legend: {
									        orient: 'vertical',
									        left: 'left',
									        data: ['在线数量','离线数量']
									    },
									    series : [
									        {
									            name: '访问来源',
									            type: 'pie',
									            radius : '55%',
									            center: ['50%', '60%'],
									            data:[
									                {value:335, name:'在线数量'},
									                {value:310, name:'离线数量'}
									            ],
									            itemStyle: {
									                emphasis: {
									                    shadowBlur: 10,
									                    shadowOffsetX: 0,
									                    shadowColor: 'rgba(0, 0, 0, 0.5)'
									                }
									            }
									        }
									    ]
									};
							        // 使用刚指定的配置项和数据显示图表。
							        myChart1.setOption(option1);
							    </script>
							</div>
							<div style="height:300px;width:40%;float:left;padding-top:10px;">
								<div id="main2" style="width: 80%;height:280px;padding-left:30px;"></div>
								<script type="text/javascript">
							        // 基于准备好的dom，初始化echarts实例
							        var myChart2 = echarts.init(document.getElementById('main2'));
							        // 指定图表的配置项和数据
			
									var option2 = {
										title: {
							                text: '车辆报警状态柱状图'
							            },
									    color: ['#3398DB'],
									    tooltip : {
									        trigger: 'axis',
									        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
									            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
									        }
									    },
									    grid: {
									        left: '3%',
									        right: '4%',
									        bottom: '3%',
									        containLabel: true
									    },
									    xAxis : [
									        {
									            type : 'category',
									            data : ['疲劳驾驶', '超速次数', '超速车数', '未定位'],
									            axisTick: {
									                alignWithLabel: true
									            }
									        }
									    ],
									    yAxis : [
									        {
									            type : 'value'
									        }
									    ],
									    series : [
									        {
									            name:'直接访问',
									            type:'bar',
									            barWidth: '60%',
									            data:[10, 52, 200, 334]
									        }
									    ]
									};
			
							        // 使用刚指定的配置项和数据显示图表。
							        myChart2.setOption(option2);
							    </script>
							</div>
						</div>
<div style="width:100%;height:100%;border-top:1px solid #dddddd;">
<script>
var toolbar = [{
    text:'<i class="fa fa-search" aria-hidden="true"></i>刷新',
    handler:function(){alert('add')}
}];
	$(function(){
		var prjDatagrid = $('#prjDatagrid').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true,
			onLoadSuccess: function(data){
				//全部字段
				//var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field] div.datagrid-cell:not(:empty) a');
				//特定字段
				var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field="status"] div.datagrid-cell:not(:empty) a');
				cells.tooltip({
					 content: $('<div></div>'),
				     onUpdate: function(content){
					    var prjid = $(this).attr("prjid");
					    content.panel({
							width: 400,
							border: false,
							title: '项目审核记录',
							href: '${ctx}/bn/project/findpage?_m=exec&prjid='+prjid
						});
					 },
					 showEvent:'click',
					 onShow: function(){
						var t = $(this);
						t.tooltip('tip').unbind().bind('mouseenter', function(){
							t.tooltip('show');
						}).bind('mouseleave', function(){
							t.tooltip('hide');
						});
					 }
				 });
			}
		});
		prjDatagrid.datagrid('enableFilter', [
		   {
			field:'prjName',
			type:'label',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'prjStart',
			type:'datebox',
			op:['greaterorequal']
		   },
		   {
			field:'prjEnd',
			type:'datebox',
			op:['lessorequal']
		   },
		   {
			field:'status',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '草稿',value: '1'},
					{label: '待审',value: '2'},
					{label: '院审通过并提交专家审核',value: '3'},
					{label: '院审未通过',value: '4'},
					{label: '院部终止',value: '5'},
					{label: '专家审核通过',value: '6'},
					{label: '专家审核未通过',value: '7'},
					{label: '专家终止',value: '8'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btnprjadd").bind("click",function(){
			var tab = $('#main-tab').tabs('getSelected');
			tab.panel('refresh', "${ctx}/bn/project/bn_project_info/insert?_m=init");
		});
    })
    function transstatus(id,value){
		//状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
		var processing="";
		switch(value)
		{
		case 1:
			processing= "草稿";
		    break;
		case 2:
			processing= "待审";
		    break;
		case 3:
			processing= "院审通过并提交专家审核";
		    break;
		case 4:
			processing= "院审未通过";
		    break;
		case 5:
			processing= "院部终止";
		    break;
		case 6:
			processing= "专家审核通过";
		    break;
		case 7:
			processing= "专家审核未通过";
		    break;
		case 8:
			processing= "专家终止";
		    break;
		default:
			processing= "删除";		
		}
		//return "<a class='a-btn easyui-tooltip' href='javascript:void(0);' onclick='showprocessing(this,\""+id+"\")'>"+processing+"</a>";
		return "<a class='a-btn easyui-tooltip' href='javascript:void(0);' prjid='"+id+"'>"+processing+"</a>";
	}
	function toupdate(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/project/bn_project_info/update?_m=init&id="+id);
	}
	function operator(id,status,file){
		var arr = new Array();
		if(status=='1'||status=='4'||status=='7'){
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\""+id+"\");'>编辑</a>");
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\'"+id+"');'>删除</a>");
		}
		arr.push("<a class='a-btn' target='_blank' href='${ctx}/file/"+file+"'>下载</a>");
		return arr.join("");
	}
</script>
<table id="prjDatagrid"  style="width:100%;height:100%"
		data-options="border:false,rownumbers:true,autoRowHeight:false,singleSelect:true,pagination:true,toolbar:toolbar,rowStyler: function(index,row){return {style:'height:30px'};},url:'${ctx}/bn/project/bn_project_info/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'prjNo',width:100">车牌号</th>
			<th data-options="field:'prjName',width:200">车辆状态</th>
			<th data-options="field:'prjStart',width:200,align:'center'">平台最后接收时间</th>
			<th data-options="field:'prjEnd',width:100,align:'center'">速度</th>
			<th data-options="field:'parta',width:100">方向</th>
			<th data-options="field:'partb',width:395,align:'center'">位置描述</th>
		</tr>
	</thead>
</table>
							</div>
						</div>
					</div>
				</div>
		</div>
</body>
</html>
