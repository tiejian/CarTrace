<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script>
	var $rptgrid;
	var rptGroupCols;
	$(function(){
		//$("#mainresearchtable").tabledit({initnum:1,addtool:true});
		//$("#mainresearchtable").tabledit('add',$("#mainresearchtable"), 1);
    })
</script>
<div class="easyui-layout" style="width:100%;height:100%;">
	<div data-options="region:'center',title:'项目申报'">
		<div style="width:80%;margin:auto;">
			<div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
			    项目基本信息
			</div>
			<form  id="j_custom_form" class="easyui-form" method="post" data-options="novalidate:true">
			<table class="table table-condensed table-hover easyui-table" width="100%" >
	            <tbody>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjNo" class="control-label x150">项目编号</label>
	                        <input class="easyui-textbox easyui-table-input"  style="width:100%;height:32px"  type="text" name="bnProjectInfo.prjNo" id="bnProjectInfo.prjNo" value="${bnProjectInfo.prjNo}"  data-options="required:true" ></input>
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjName" class="control-label x150">项目名称</label>
	                        <input class="easyui-textbox easyui-table-input"   style="width:100%;height:32px"  type="text" name="bnProjectInfo.prjName" id="bnProjectInfo.prjName" value="${bnProjectInfo.prjName}"  data-options="required:true" ></input>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjStart" class="control-label x150">项目起时间</label>
	                        <input class="easyui-datebox" style="width:100%;height:32px" value="${bnProjectInfo.prjStart}" type="text" name="bnProjectInfo.prjStart" id="bnProjectInfo.prjStart" value="1980-08-08"  />
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjEnd" class="control-label x150">项目止时间</label>
	                        <input class="easyui-datebox" style="width:100%;height:32px" value="${bnProjectInfo.prjEnd}" type="text" name="bnProjectInfo.prjEnd" id="bnProjectInfo.prjEnd" value="1980-08-08"  />
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.parta" class="control-label x150">委托单位（甲方）</label>
	                        <input class="easyui-textbox" style="width:100%;height:32px"  type="text" name="bnProjectInfo.parta" id="bnProjectInfo.parta" value="${bnProjectInfo.parta}" >
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.partb" class="control-label x150">承担单位（乙方）</label>
	                        <input class="easyui-textbox"  style="width:100%;height:32px"  type="text" name="bnProjectInfo.partb" id="bnProjectInfo.partb" value="${bnProjectInfo.partb}"  >
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjGoal" class="control-label-multiline x150">项目建设目标</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjGoal" name="bnProjectInfo.prjGoal" data-options="multiline:true" value="${bnProjectInfo.prjGoal}" style="width:100%;height:100px"></input>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjContent" class="control-label-multiline x150">项目主要内容</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjContent" name="bnProjectInfo.prjContent" data-options="multiline:true" value="${bnProjectInfo.prjContent}" style="width:100%;height:100px"></input>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjSpecial" class="control-label-multiline x150">项目的特色与创新</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjSpecial" name="bnProjectInfo.prjSpecial" data-options="multiline:true" value="${bnProjectInfo.prjSpecial}"  style="width:100%;height:100px"></input>
	                        <input type="hidden" id="bnProjectInfo.id" name="bnProjectInfo.id" value="${bnProjectInfo.id}" />
	                    </td>
	                </tr>
	                <!-- <tr>
	                    <td colspan="4">
	                        <div style="display: inline-block; vertical-align: middle;width:100%;text-align: center;">
	                           <button type="button" class="btn btn-orange btn-lg">保存项目信息</button>
	                        </div>
	                    </td>
	                </tr> -->
	            </tbody>
	        </table>
		     <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    项目负责人简况
			  </div>
	          <table class="table table-condensed table-hover easyui-table" width="80%">
	            <tbody>
	                <tr>
	                    <td class="per25 x90">
	                        <label for="bnProjectAdmin.name" class="control-label x100">姓名</label>
	                        <input type="text" name="bnProjectAdmin.name" id="bnProjectAdmin.name" value="${bnProjectAdmin.name}"  class="easyui-textbox"   style="width:100%;height:32px">
	                    </td>
	                    <td class="per25 x90">
	                        <label for="bnProjectAdmin.gender" class="control-label x100">性别</label>
	                        <input type="radio" name="bnProjectAdmin.gender"  data-toggle="icheck" value="1" checked data-label="男">男
	                        <input type="radio" name="bnProjectAdmin.gender" data-toggle="icheck" value="0" data-label="女">女
	                    </td>
	                     <td class="per25 x90">
	                        <label for="bnProjectAdmin.age" class="control-label x100">年龄</label>
	                        <input type="text" name="bnProjectAdmin.age" id="bnProjectAdmin.age" value="${bnProjectAdmin.age}"  class="easyui-textbox"  style="width:100%;height:32px">
	                    </td>
	                     <td class="per25 x90">
	                        <label for="bnProjectAdmin.degree" class="control-label x100">最终学位</label>
	                        <select name="bnProjectAdmin.degree" id="bnProjectAdmin.degree" data-toggle="selectpicker">
			                    <option value="a">高中</option>
			                    <option value="b">专科</option>
			                    <option value="c">本科</option>
			                    <option value="c">硕士</option>
			                    <option value="c">博士</option>
			                    <option value="c">博士后</option>
			                </select>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per25 x90">
	                        <label for="j_custom_name" class="control-label x100">职称</label>
	                        <input type="text" name="bnProjectAdmin.jobTitle" id="bnProjectAdmin.jobTitle" value="${bnProjectAdmin.jobTitle}" class="easyui-textbox"  style="width:100%;height:32px">
	                    </td>
	                    <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">职务</label>
	                        <input type="text" name="bnProjectAdmin.jobPost" id="bnProjectAdmin.jobPost" value="${bnProjectAdmin.jobPost}"  class="easyui-textbox" style="width:100%;height:32px">
	                    </td>
	                     <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">从事专业</label>
	                        <input type="text" name="bnProjectAdmin.jobSubject" id="bnProjectAdmin.jobSubject" value="${bnProjectAdmin.jobSubject}"   class="easyui-textbox"     style="width:100%;height:32px">
	                    </td>
	                     <td class="per25 x90">
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per25 x90">
	                        <label for="j_custom_name" class="control-label x100">所在学校名称</label>
	                        <input type="text" name="bnProjectAdmin.college" id="bnProjectAdmin.college" value="${bnProjectAdmin.college}"  class="easyui-textbox"  style="width:100%;height:32px">
	                    </td>
	                    <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">所属院系</label>
	                        <input class="easyui-combobox" name="bnProjectAdmin.branch" id="bnProjectAdmin.branch" value="${bnProjectAdmin.branch}" data-options="valueField:'dm',textField:'mc',url:'${ctx}/dt/department/find?_m=exec'" style="width:100%;height:32px">
	                    </td>
	                     <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">主要研究方向</label>
	                        <input type="text" name="bnProjectAdmin.mainResearch" id="bnProjectAdmin.mainResearch" value="${bnProjectAdmin.mainResearch}"  class="easyui-textbox"  style="width:100%;height:32px">
	                    </td>
	                    <td class="per25 x90">
	                    </td>
	                </tr>
	                
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectAdmin.mainResult" class="control-label-multiline x500">项目负责人近三年来主要研究成果简介（发表文章、承担的项目等）</label>
	                        <input class="easyui-textbox" id="bnProjectAdmin.mainResult" name="bnProjectAdmin.mainResult" data-options="multiline:true"  value="${bnProjectAdmin.mainResult}"  style="width:100%;height:100px"></input>
	                        <input type="hidden" id="bnProjectAdmin.id" name="bnProjectAdmin.id" value="${bnProjectAdmin.id}" />
	                    </td>
	                </tr>
	                <!-- <tr>
	                    <td colspan="4">
	                        <div style="display: inline-block; vertical-align: middle;width:100%;text-align: center;">
	                           <button type="button" class="btn btn-orange btn-lg">保存项目信息</button>
	                        </div>
	                    </td>
	                </tr> -->
	            </tbody>
	        </table>
		      <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    参加研究项目的主要人员情况
			  </div>
			<!-- <table id="dg" class="easyui-datagrid" title="Row Editing in DataGrid" style="width:100%;height:auto"
		            data-options="
		                toolbar: '#tb'
		            ">
		        <thead>
		            <tr>
		                <th data-options="field:'researches[#index#].name',width:80,editor:'textbox'">姓名</th>
		                <th data-options="field:'researches[#index#].gender',width:100,editor:'textbox'">性别</th>
		                <th data-options="field:'researches[#index#].age',width:80,align:'right',editor:{type:'numberbox',options:{precision:1}}">年龄</th>
		                <th data-options="field:'researches[#index#].degree',width:80,align:'right',editor:'numberbox'">学位</th>
		                <th data-options="field:'researches[#index#].jobTitle',width:250,editor:'textbox'">职称</th>
		                <th data-options="field:'researches[#index#].jobTitle',width:60,align:'center',editor:{type:'checkbox',options:{on:'P',off:''}}">从事专业</th>
		                <th data-options="field:'researches[#index#].mainTask',width:60,align:'center',editor:{type:'checkbox',options:{on:'P',off:''}}">在项目中的主要任务</th>
		            </tr>
		        </thead>
		    </table> -->
		    <table id="dg" class="easyui-datagrid easyui-table" style="width:100%;height:auto;margin-bottom:30px;" data-options="toolbar: '#tb',singleSelect:true">
			    <thead>
			        <tr>
			            <th data-options="field:'name',width:100">姓名</th>
		                <th data-options="field:'gender',width:50,formatter:function(value,row,index){if(value==1) return '男'; else return '女';}">性别</th>
		                <th data-options="field:'age',width:50,align:'right'">年龄</th>
		                <th data-options="field:'degree',width:100">学位</th>
		                <th data-options="field:'jobTitle',width:250">职称</th>
		                <th data-options="field:'jobSubject',width:200">从事专业</th>
		                <th data-options="field:'mainTask',width:400">在项目中的主要任务</th>
			        </tr>
			    </thead>
			    <tbody>
			    <c:forEach var="result" items="${researcheres}" varStatus="status">
				    <tr>                           
						<td>${result.name}</td>            
						<td>${result.gender}</td>            
						<td>${result.age}</td>            
						<td>${result.degree}</td>            
						<td>${result.jobTitle}</td>            
						<td>${result.jobSubject}</td>     
						<td>${result.mainTask}</td>       
					</tr> 
					</c:forEach>
			    </tbody>
			</table>

		    <div id="tb" style="height:auto">
		        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="append()">Append</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="removeit()">Remove</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-save',plain:true" onclick="accept()">Accept</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-undo',plain:true" onclick="reject()">Reject</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-search',plain:true" onclick="getChanges()">GetChanges</a>
		    </div>
		    <script type="text/javascript">
			        var editIndex = undefined;
			        function endEditing(){
			            if (editIndex == undefined){return true}
			            if ($('#dg').datagrid('validateRow', editIndex)){
			                $('#dg').datagrid('endEdit', editIndex);
			                editIndex = undefined;
			                return true;
			            } else {
			                return false;
			            }
			        }
			        function onClickCell(index, field){
			            if (editIndex != index){
			                if (endEditing()){
			                    $('#dg').datagrid('selectRow', index)
			                            .datagrid('beginEdit', index);
			                    var ed = $('#dg').datagrid('getEditor', {index:index,field:field});
			                    if (ed){
			                        ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
			                    }
			                    editIndex = index;
			                } else {
			                    setTimeout(function(){
			                        $('#dg').datagrid('selectRow', editIndex);
			                    },0);
			                }
			            }
			        }
			        function onEndEdit(index, row){
			            var ed = $(this).datagrid('getEditor', {
			                index: index,
			                field: 'productid'
			            });
			            row.productname = $(ed.target).combobox('getText');
			        }
			        function append(){
			        	/* layer.open({
			        	    skin: 'layui-layer-lan'
			        	    ,type: 1
			        	    ,area: '600px'
			        	    ,content: $("#f_mainresearcher")
			        	    ,success:function(layero,index){
			        	    	$("#f_mainresearcher").html("");
			        	    	$("#f_mainresearcher").load("${ctx}/bn/project/bn_project_info/insert/researcher?_m=init");
			        	    }
			        	  }); */
			        	  $('<div></div>').dialog({
				        	    title: '增加主要人员',
				        	    width: 600,
				        	    height:400,
				        	    closed: false,
				        	    cache: false,
				        	    href: '${ctx}/bn/project/bn_project_info/insert/researcher?_m=init',
				        	    modal: true,
				        	    onClose : function() {
				                    $(this).dialog('destroy');
				                }
				        	});
				        	//$('#f_mainresearcher').dialog('refresh', '${ctx}/bn/project/bn_project_info/insert/researcher?_m=init');
			        	/* if (endEditing()){
							$('#dg').datagrid('appendRow',{status:'P'});
							editIndex = $('#dg').datagrid('getRows').length-1;
							$('#dg').datagrid('selectRow', editIndex)
									.datagrid('beginEdit', editIndex);
						} */
			        }
			        function removeit(){
			            if (editIndex == undefined){return}
			            $('#dg').datagrid('cancelEdit', editIndex)
			                    .datagrid('deleteRow', editIndex);
			            editIndex = undefined;
			        }
			        function accept(){
			            if (endEditing()){
			                $('#dg').datagrid('acceptChanges');
			            }
			        }
			        function reject(){
			            /* $('#dg').datagrid('rejectChanges');
			            editIndex = undefined; */
			        	console.log($('#dg').datagrid('getRows'));
			        	console.log($('#dg').datagrid('getData'));
			        	console.log(JSON.stringify($('#dg').datagrid('getRows')));
			        }
			        function getChanges(){
			            var rows = $('#dg').datagrid('getChanges');
			            alert(rows.length+' rows are changed!');
			        }
			    </script>
			    <input type="hidden" id="researcheres" name="researcheres" />
	        </form>
	       <div style="padding:20px;text-align:center;">
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="save()">暂存</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="audit()">提交审核</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="backlist()">返回列表</a>
		    </div>
		</div>
	</div>
</div>
<div id="f_mainresearcher" style="width:500px;z-index: 9999;display:none;"></div>
<script>
	function save(){
		$('#j_custom_form').form('submit',{
			url:"${ctx}/bn/project/bn_project_info/update?_m=draft",
			onSubmit:function(){
				if($("#j_custom_form").form('enableValidation').form('validate')){
				    console.log($('#dg').datagrid('getRows'));
					var researcher = JSON.stringify($('#dg').datagrid('getRows'));
					if(researcher=="[]"){
						alert("至少一个参与人员");
						return false;
					}else{
						$("#researcheres").val(researcher);
						return true;
					}
				}else{
					return false;
				}
			},
		    success:function(data){
		    	var data = eval('(' + data + ')');
				if(data.errcode!="0"){
					$.messager.alert('提示信息','保存失败!','error');
				}else{
					$.messager.alert('提示信息','保存成功!','info',backlist());
				}
		    }
		});
	}
	function audit(){
		$('#j_custom_form').form('submit',{
			url:"${ctx}/bn/project/bn_project_info/update?_m=exec",
			onSubmit:function(){
				if($("#j_custom_form").form('enableValidation').form('validate')){
				    console.log($('#dg').datagrid('getRows'));
					var researcher = JSON.stringify($('#dg').datagrid('getRows'));
					if(researcher=="[]"){
						alert("至少一个参与人员");
						return false;
					}else{
						$("#researcheres").val(researcher);
						return true;
					}
				}else{
					return false;
				}
			},
		    success:function(data){
		    	var data = eval('(' + data + ')');
				if(data.errcode!="0"){
					$.messager.alert('提示信息','保存失败!','error');
				}else{
					$.messager.alert('提示信息','保存成功!','info',backlist());
				}
		    }
		});
	}
	function backlist(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/project/bn_project_info/search?_m=init");
	}
</script>