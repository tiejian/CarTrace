<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
var toolbar = [{
    text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加',
    handler:function(){alert('add')}
},{
    text:'<i class="fa fa-cut" aria-hidden="true"></i>删除',
    handler:function(){alert('cut')}
},'-',{
    text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
    handler:function(){alert('save')}
},{
    text:'<i class="fa fa-edit"></i>修改',
    handler:function(){alert('save')}
}];
	$(function(){
		var prjDatagrid = $('#prjDatagrid').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true,
			onLoadSuccess: function(data){
				//全部字段
				//var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field] div.datagrid-cell:not(:empty) a');
				//特定字段
				var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field="status"] div.datagrid-cell:not(:empty) a');
				cells.tooltip({
					 content: $('<div></div>'),
				     onUpdate: function(content){
					    var prjid = $(this).attr("prjid");
					    content.panel({
							width: 400,
							border: false,
							title: '项目审核记录',
							href: '${ctx}/bn/project/findpage?_m=exec&prjid='+prjid
						});
					 },
					 showEvent:'click',
					 onShow: function(){
						var t = $(this);
						t.tooltip('tip').unbind().bind('mouseenter', function(){
							t.tooltip('show');
						}).bind('mouseleave', function(){
							t.tooltip('hide');
						});
					 }
				 });
			}
		});
		prjDatagrid.datagrid('enableFilter', [
		   {
			field:'prjName',
			type:'label',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'prjStart',
			type:'datebox',
			op:['greaterorequal']
		   },
		   {
			field:'prjEnd',
			type:'datebox',
			op:['lessorequal']
		   },
		   {
			field:'status',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '草稿',value: '1'},
					{label: '待审',value: '2'},
					{label: '院审通过并提交专家审核',value: '3'},
					{label: '院审未通过',value: '4'},
					{label: '院部终止',value: '5'},
					{label: '专家审核通过',value: '6'},
					{label: '专家审核未通过',value: '7'},
					{label: '专家终止',value: '8'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btnprjadd").bind("click",function(){
			var tab = $('#main-tab').tabs('getSelected');
			tab.panel('refresh', "${ctx}/bn/project/bn_project_info/insert?_m=init");
		});
    })
    function transstatus(id,value){
		//状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
		var processing="";
		switch(value)
		{
		case 1:
			processing= "草稿";
		    break;
		case 2:
			processing= "待审";
		    break;
		case 3:
			processing= "院审通过并提交专家审核";
		    break;
		case 4:
			processing= "院审未通过";
		    break;
		case 5:
			processing= "院部终止";
		    break;
		case 6:
			processing= "专家审核通过";
		    break;
		case 7:
			processing= "专家审核未通过";
		    break;
		case 8:
			processing= "专家终止";
		    break;
		default:
			processing= "删除";		
		}
		//return "<a class='a-btn easyui-tooltip' href='javascript:void(0);' onclick='showprocessing(this,\""+id+"\")'>"+processing+"</a>";
		return "<a class='a-btn easyui-tooltip' href='javascript:void(0);' prjid='"+id+"'>"+processing+"</a>";
	}
	function toupdate(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/project/bn_project_info/update?_m=init&id="+id);
	}
	function operator(id,status,file){
		var arr = new Array();
		if(status=='1'||status=='4'||status=='7'){
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\""+id+"\");'>编辑</a>");
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\'"+id+"');'>删除</a>");
		}
		arr.push("<a class='a-btn' target='_blank' href='${ctx}/file/"+file+"'>下载</a>");
		return arr.join("");
	}
</script>
<table id="prjDatagrid"  style="width:100%;height:100%"
		data-options="border:false,rownumbers:true,autoRowHeight:false,singleSelect:true,pagination:true,rowStyler: function(index,row){return {style:'height:30px'};},url:'${ctx}/bn/project/bn_project_info/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'prjNo',width:100">项目编号</th>
			<th data-options="field:'prjName',width:200">项目名称</th>
			<th data-options="field:'prjStart',width:140,align:'center'">项目起时间</th>
			<th data-options="field:'prjEnd',width:140,align:'center'">项目止时间</th>
			<th data-options="field:'parta',width:250">委托单位</th>
			<th data-options="field:'partb',width:250,align:'center'">承担单位/负责人</th>
			<th data-options="field:'status',width:200,align:'center',formatter:function(value,row,index){return transstatus(row.id,value);}">项目进度</th>
			<th data-options="field:'t',width:150,align:'center',formatter:function(value,row,index){return operator(row.id,row.status,row.prjFile)}"></th>
		</tr>
	</thead>
</table>
<!-- <div id="tb"  class="globa-search-box" >
	<div class="search-container">
		<ul>
			<li class="search-container-item"><label class="search-container-item-label">项目名称</label><input type="input" id="" name=""  class="easyui-textbox" data-options="height:30"/></li>
			<li class="search-container-item"><label class="search-container-item-label">项目起时间</label><input type="input" id="" name="" class="easyui-datebox" data-options="height:30"//></li>
			<li class="search-container-item"><label class="search-container-item-label">项目止时间</label><input type="input" id="" name="" class="easyui-datebox" data-options="height:30"//></li>
			<li class="search-container-item"><a id="btn" href="#" class="btn btn-primary" data-options=""><i class='fa fa-search'></i>查询</a></li>
		</ul>
	</div>
	<div class="search-container-tool">
		<a href="#" class="btn btn-info" id="btnprjadd">项目立项申报</a>
		<a href="#" class="btn btn-primary" ><i class='fa fa-save'></i>保存</a>
		<a href="#" class="btn btn-success" ><i class="fa fa-plus-square"></i>增加</a>
		<a href="#" class="btn btn-warning" ><i class="fa fa-minus-square"></i>删除</a>
	</div>
</div> -->
<script type="text/javascript">
      
  </script>
