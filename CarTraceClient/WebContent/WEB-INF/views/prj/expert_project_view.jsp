<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script>
	var $rptgrid;
	var rptGroupCols;
	$(function(){
		//$("#mainresearchtable").tabledit({initnum:1,addtool:true});
		//$("#mainresearchtable").tabledit('add',$("#mainresearchtable"), 1);
    })
</script>
<div class="easyui-layout" style="width:100%;height:100%;">
	<div data-options="region:'center',title:'项目申报'">
		<div style="width:80%;margin:auto;">
			<div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
			    项目基本信息
			</div>
			
			<table class="table table-condensed table-hover easyui-table" width="100%" >
	            <tbody>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjNo" class="control-label x150">项目编号</label>
	                        <div class="form-view-field"  >${bnProjectInfo.prjNo}</div>
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjName" class="control-label x150">项目名称</label>
	                        <div class="form-view-field"  >${bnProjectInfo.prjName}</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjStart" class="control-label x150">项目起时间</label>
	                        <div class="form-view-field"  >${bnProjectInfo.prjStart}</div>
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.prjEnd" class="control-label x150">项目止时间</label>
	                        <div class="form-view-field"  >${bnProjectInfo.prjEnd}</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.parta" class="control-label x150">委托单位（甲方）</label>
	                        <div class="form-view-field"  >${bnProjectInfo.parta}</div>
	                    </td>
	                    <td class="per50 x150">
	                        <label for="bnProjectInfo.partb" class="control-label x150">承担单位（乙方）</label>
	                        <div class="form-view-field"  >${bnProjectInfo.partb}</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjGoal" class="control-label-multiline x150">项目建设目标</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjGoal" name="bnProjectInfo.prjGoal" data-options="multiline:true" style="width:100%;height:100px" value="${bnProjectInfo.prjGoal}"></input>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjContent" class="control-label-multiline x150">项目主要内容</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjContent" name="bnProjectInfo.prjContent" data-options="multiline:true" value="${bnProjectInfo.prjContent}" style="width:100%;height:100px"></input>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectInfo.prjSpecial" class="control-label-multiline x150">项目的特色与创新</label>
	                        <input class="easyui-textbox" id="bnProjectInfo.prjSpecial" name="bnProjectInfo.prjSpecial" data-options="multiline:true" value="${bnProjectInfo.prjSpecial}" style="width:100%;height:100px"></input>
	                    </td>
	                </tr>
	                <!-- <tr>
	                    <td colspan="4">
	                        <div style="display: inline-block; vertical-align: middle;width:100%;text-align: center;">
	                           <button type="button" class="btn btn-orange btn-lg">保存项目信息</button>
	                        </div>
	                    </td>
	                </tr> -->
	            </tbody>
	        </table>
		     <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    项目负责人简况
			  </div>
	          <table class="table table-condensed table-hover easyui-table" width="80%">
	            <tbody>
	                <tr>
	                    <td class="per25 x90">
	                        <label for="bnProjectAdmin.name" class="control-label x100">姓名</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.name}</div>
	                    </td>
	                    <td class="per25 x90">
	                        <label for="bnProjectAdmin.gender" class="control-label x100">性别</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.gender}</div>
	                    </td>
	                     <td class="per25 x90">
	                        <label for="bnProjectAdmin.age" class="control-label x100">年龄</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.age}</div>
	                    </td>
	                     <td class="per25 x90">
	                        <label for="bnProjectAdmin.degree" class="control-label x100">最终学位</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.degree}</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td class="per25 x90">
	                        <label for="j_custom_name" class="control-label x100">职称</label>
	                         <div class="form-view-field"  >${bnProjectAdmin.jobTitle}</div>
	                    </td>
	                    <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">职务</label>
	                         <div class="form-view-field"  >${bnProjectAdmin.jobPost}</div>
	                    </td>
	                     <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">所在学科名称</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.jobSubject}</div>
	                    </td>
	                     <td class="per25 x90">
	                        <label for="j_custom_fname" class="control-label x100">主要研究方向</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.mainResearch}</div>
	                    </td>
	                </tr>
	                <tr>
	                    <td colspan="4">
	                        <label for="bnProjectAdmin.mainResult" class="control-label-multiline x500">项目负责人近三年来主要研究成果简介（发表文章、承担的项目等）</label>
	                        <div class="form-view-field"  >${bnProjectAdmin.mainResult}</div>
	                    </td>
	                </tr>
	                <!-- <tr>
	                    <td colspan="4">
	                        <div style="display: inline-block; vertical-align: middle;width:100%;text-align: center;">
	                           <button type="button" class="btn btn-orange btn-lg">保存项目信息</button>
	                        </div>
	                    </td>
	                </tr> -->
	            </tbody>
	        </table>
		      <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    参加研究项目的主要人员情况
			  </div>
			<!-- <table id="dg" class="easyui-datagrid" title="Row Editing in DataGrid" style="width:100%;height:auto"
		            data-options="
		                toolbar: '#tb'
		            ">
		        <thead>
		            <tr>
		                <th data-options="field:'researches[#index#].name',width:80,editor:'textbox'">姓名</th>
		                <th data-options="field:'researches[#index#].gender',width:100,editor:'textbox'">性别</th>
		                <th data-options="field:'researches[#index#].age',width:80,align:'right',editor:{type:'numberbox',options:{precision:1}}">年龄</th>
		                <th data-options="field:'researches[#index#].degree',width:80,align:'right',editor:'numberbox'">学位</th>
		                <th data-options="field:'researches[#index#].jobTitle',width:250,editor:'textbox'">职称</th>
		                <th data-options="field:'researches[#index#].jobTitle',width:60,align:'center',editor:{type:'checkbox',options:{on:'P',off:''}}">从事专业</th>
		                <th data-options="field:'researches[#index#].mainTask',width:60,align:'center',editor:{type:'checkbox',options:{on:'P',off:''}}">在项目中的主要任务</th>
		            </tr>
		        </thead>
		    </table> -->
		    <table id="dg" class="easyui-datagrid easyui-table" style="width:100%;height:auto;margin-bottom:30px;" data-options="singleSelect:true">
			    <thead>
			        <tr>
			            <th data-options="field:'name',width:100">姓名</th>
		                <th data-options="field:'gender',width:50,formatter:function(value,row,index){if(value==1) return '男'; else return '女';}">性别</th>
		                <th data-options="field:'age',width:50,align:'right'">年龄</th>
		                <th data-options="field:'degree',width:100">学位</th>
		                <th data-options="field:'jobTitle',width:250">职称</th>
		                <th data-options="field:'jobSubject',width:200">从事专业</th>
		                <th data-options="field:'mainTask',width:400">在项目中的主要任务</th>
			        </tr>
			    </thead>
			    <tbody>
			       <c:forEach var="result" items="${researcheres}" varStatus="status">
				    <tr>                           
						<td>${result.name}</td>            
						<td>${result.gender}</td>            
						<td>${result.age}</td>            
						<td>${result.degree}</td>            
						<td>${result.jobTitle}</td>            
						<td>${result.jobSubject}</td>     
						<td>${result.mainTask}</td>       
					</tr> 
					</c:forEach>
			    </tbody>
			</table>
			  <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    院部审核
			  </div>
			  <table  class="table table-condensed table-hover easyui-table" width="100%">
			    <tr>
                    <td colspan="4">
                        <label for="bnProjectAdmin.mainResult" class="control-label-multiline x500">院部审核意见</label>
                        <div class="form-view-field"  >通过,${yuanAudit.auditSuggest}</div>
                    </td>
                </tr>
	          </table>
	          <div class="sb-step"  style="padding:6px 10px;border-bottom:2px solid #B03631;font-size:14px;">
				    专家审核
			  </div>
			  <form id="j_custom_form" class="easyui-form" method="post" data-options="novalidate:true">
			  <table  class="table table-condensed table-hover easyui-table" width="100%">
			    <tr>
                    <td colspan="4">
                        <label for="bnProjectAdmin.mainResult" class="control-label-multiline x500">专家审核意见</label>
                        <input class="easyui-textbox" id="auditSuggest" name="auditSuggest" style="width:100%;height:100px" data-options="multiline:true,required:true" ></input>
                    </td>
                </tr>
	          </table>
	          <input type="hidden" id="prjId" name="prjId" value="${bnProjectInfo.id}" />
	        </form>
	       <div style="padding:20px;text-align:center;">
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="auditok()">审核通过</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="auditnotok()">审核未通过</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="prjcancel()">项目终止</a>
		        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="backlist()">返回列表</a>
		    </div>
		</div>
	</div>
</div>
<div id="f_mainresearcher" style="width:500px;z-index: 9999;display:none;"></div>
<script>
	function auditok(){
		$('#j_custom_form').form('submit',{
			url:"${ctx}/expert/audit/pass?_m=exec",
			onSubmit:function(){
				return $("#j_custom_form").form('enableValidation').form('validate');
			},
		    success:function(data){
		    	var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','保存成功!','info',backlist());
				}else{
					$.messager.alert('提示信息','保存失败!','error');
				}
		    }
		});
	}
	function auditnotok(){
		$('#j_custom_form').form('submit',{
			url:"${ctx}/expert/audit/nopass?_m=exec",
			onSubmit:function(){
				return $(this).form('enableValidation').form('validate');
			},
		    success:function(data){
		    	var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','保存成功!','info',backlist());
				}else{
					$.messager.alert('提示信息','保存失败!','error');
				}
		    }
		});
	}
	function prjcancel(){
		$('#j_custom_form').form('submit',{
			url:"${ctx}/expert/audit/cancel?_m=exec",
			onSubmit:function(){
				return $("#j_custom_form").form('enableValidation').form('validate');
			},
		    success:function(data){
		    	var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','保存成功!','info',backlist());
				}else{
					$.messager.alert('提示信息','保存失败!','error');
				}
		    }
		});
	}
	function backlist(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/expert/audit/prj/search?_m=init");
	}
</script>