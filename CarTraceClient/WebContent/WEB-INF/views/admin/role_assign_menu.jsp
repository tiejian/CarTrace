<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script>
	var $rptgrid;
	var rptGroupCols;
	$(function(){
		
    })
    function submitForm(){
		
		$('#role_add_form').form('submit',{
			url:"${ctx}/st/role/update?_m=exec",
			onSubmit:function(){
				return $(this).form('enableValidation').form('validate');
			},
			success:function(data){
				var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','保存成功!','info',refresh());
				}else{
					$.messager.alert('提示信息','保存失败!','error');
				}
			}
		})

		
	}
	function clearForm(){
		$('#role_add_form').form('clear');
	}
	function assignmenu(id){
		$.ajax({
			url : '${ctx}/st/role/assign/menu?_m=exec&code=${code}&menucode=' +id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','分配成功!','info',refresh());
					$('#enablegrid').datalist('reload');
					$('#disablegrid').datalist('reload');
				}else{
					$.messager.alert('提示信息','分配失败!','error');
				}
			}
		});
	}
	function unassignmenu(id){
		$.ajax({
			url : '${ctx}/st/role/unassign/menu?_m=exec&code=${code}&menucode=' +id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','取消分配成功!','info',refresh());
					$('#enablegrid').datalist('reload');
					$('#disablegrid').datalist('reload');
				}else{
					$.messager.alert('提示信息','分配失败!','error');
				}
			}
		});
	}
</script>
<style>
</style>
    <div style="width:40%;float:left;height:420px;margin-left:40px;margin-top:20px;margin-bottom:20px;">
	    <div id="enablegrid" class="easyui-datalist" title="可分配功能" style="width:100%;float:left;height:420px;" data-options="
	            url: '${ctx}/st/menu/findenable?_m=exec&code=${code}',
	            method: 'get',
	            groupField: 'rootCode',
	            valueField:'code',
	            textField:'menuLabel',
	            onDblClickRow:function(index,row){assignmenu(row.code);}
	            ">
	    </div>
	</div>
	<div style="width:40%;float:right;height:420px;margin-right:40px;margin-top:20px;margin-bottom:20px;">
	    <div id="disablegrid" class="easyui-datalist" title="已分配功能" style="width:100%;float:left;height:420px;" data-options="
	            url: '${ctx}/st/menu/finddisable?_m=exec&code=${code}',
	            method: 'get',
	            groupField: 'rootCode',
	            valueField:'code',
	            textField:'menuLabel',
	            onDblClickRow:function(index,row){unassignmenu(row.code);}
	            ">
	    </div>
	</div>
	<input type="hidden" id="code" name="code" value="${code}" />
