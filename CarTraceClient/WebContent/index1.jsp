<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	var oindex= 0;
	var bjarray=[
   		"${ctx}/bn/project/bn_project_info/search?_m=init",
   		"${ctx}/bn/project/bn_project_info/search?_m=init"
   	];
    $(function() {
    	$('#location').tooltip({
			position: 'top',
			content: '<div style="color:#fff;width:40px;text-align:center;">定位</div>',
			onShow: function(){
				$(this).tooltip('tip').css({
					backgroundColor: '#666',
					borderColor: '#666'
				});
			}
		});
    	$('#message').tooltip({
			position: 'top',
			content: '<div style="color:#fff;width:40px;text-align:center;">短信</div>',
			onShow: function(){
				$(this).tooltip('tip').css({
					backgroundColor: '#666',
					borderColor: '#666'
				});
			}
		});
		
		$('#tabs-alert').tabs({
		    border:false,
		    onSelect:function(title,index){
		    	$.get(bjarray[index], function(result){
		    		$('#tabs-alert').find(".tabs-panels").find("div").find(".panel-body").eq(oindex).html("");
					var o = $('#tabs-alert').find(".tabs-panels").find("div").find(".panel-body").eq(index);
					o.html(result);
					oindex = index;
				});
		    }
		});
		
    });
    
    function getAlert(index){
		$.get(bjarray[index], function(result){
    		$('#tabs-alert').find(".tabs-panels").find("div").find(".panel-body").eq(oindex).html("");
			var o = $('#tabs-alert').find(".tabs-panels").find("div").find(".panel-body").eq(index);
			o.html(result);
			oindex = index;
		});
	}
</script>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:false,border:false,cls:'unit-admin-menu'" style="width:220px;">
		<div style="height:45px;padding-top:5px;background: #FFFFFF;">
			<div style="float:left;width:80px;">
				<select name="sex" id="sex" class="form-control" title="性别" style="border-right:0px;width:80px;border-radius: 20px 0 0 20px;">
					<option value="">全部</option>
					<option value="1">在线</option>
					<option value="2">离线</option>
					<option value="2">行驶</option>
				</select>
			</div>
			<div style="float:left;width:138px;">
				<div class="input-group">
					<input type="text" class="form-control" placeholder="车牌号" style="border-left:0px;width:99px">
					<div class="btn btn-red" style="border-radius: 0 20px 20px 0;">
						<i class="fa fa-search"></i>
					</div>
				</div>
			</div>
		</div>
		<div style="height:35px;line-height:35px;text-align:center;background: #79bff6;color:#FFFFFF;font-size:16px;">
			<i class="fa fa-car" style="color:#FFFFFF;"></i>&nbsp;&nbsp;车辆列表
		</div>
		
		<div style="height:300px;color:#FFFFFF;font-size:16px;">
		
		</div>
		<div style="position:fixed;bottom:0;left:0;width:220px;height:90px;background: #d5e7f7;padding-top:10px;">
			<div id="location" class="btn btn-warning" style="float:left;margin-left:50px;padding-top:5px;width:50px;height:50px;line-height:50px;text-align:center;border-radius: 25px 25px 25px 25px;cursor: pointer;">
				<i class="fa fa-map-marker  fa-3x" style="color:#FFFFFF;"></i>
			</div>
			<div id="message" class="btn btn-warning" style="float:left;margin-left:20px;padding-top:3px;width:50px;height:50px;line-height:50px;text-align:center;border-radius: 25px 25px 25px 25px;cursor: pointer;">
				<i class="fa fa-envelope-o fa-2x" style="color:#FFFFFF;"></i>
			</div>
		</div>
	</div>
	<div data-options="region:'center',border:false">
		<iframe style="border:0px;" height="400" width="100%" src="${ctx }/1.jsp"></iframe>
		<div id="tabs-alert" class="easyui-tabs" style="height:400px;width:98.5%">
			<div title="超速报警" data-options="tools:'#p-tools'" >
			</div>
			<div title="怠速报警">
			</div>
			<div title="紧急报警">
			</div>
			<div title="设备故障报警">
			</div>
			<div title="偏离路线报警">
			</div>
			<div title="区域报警">
			</div>
			<div title="疲劳驾驶报警">
			</div>
			<div title="其他报警">
			</div>
		</div>
		<div id="p-tools">
			<a href="javascript:void(0)" class="icon-mini-refresh" onclick="getAlert('0')"></a>
		</div>
	</div>
</div>
