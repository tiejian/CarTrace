package com.jacqui.main.service;



import java.text.ParseException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.google.common.collect.Maps;
import com.jacqui.main.dao.StUserMapper;
import com.jacqui.main.domain.StUser;
import com.jacqui.service.BaseService;

/**
 * 
 * @ClassName: StUserService   
 * @Description: 系统用户 
 * @author 白展堂
 
 */
@Service
public class StUserService extends BaseService{

   private static Logger logger = LoggerFactory.getLogger(StUserService.class);
	
	@Autowired
	public StUserMapper stUserMapper;
	
    /**
	 * 
	 * @Title: findBy 
	 * @Description: 根据主键查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
    public StUser findUniqueByStaffCode(String staffCode){
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("staffCode",staffCode);
		return stUserMapper.findUniqueBy(parameters);
	}
	/**
     * @Title: findUniqueById 
     * @Description: 根据属性查询
     * @param id
     * @return DsjSkrkxx    返回类型 
     */
    public StUser findUniqueBy(Map<String, Object> parameters){
		return stUserMapper.findUniqueBy(parameters);
	}
    /**
     * @Title: update 
     * @Description: 更新操作 
     * @param dsjSkrkxx
     */
    public void update(StUser stUser){
    	stUserMapper.update(stUser);
	}
    
	/**
	 * @Title: getHttpServletRequest
     * @Description: 可以在service层获取request,仅限地不使用dubbo的情况。使用dubbo此方法无用
     * @return void    返回类型 
	 */
	private static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}
	
}
