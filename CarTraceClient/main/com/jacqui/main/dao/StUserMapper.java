package com.jacqui.main.dao;


import java.util.Map;

import com.jacqui.core.mybatis.MyBatisRepository;
import com.jacqui.main.domain.StUser;
/**
 * 
 * @ClassName: StUserMapper  
 * @Description: 系统用户
 * @author 张三丰
 *
 */
@MyBatisRepository
public interface StUserMapper {
    
    /**
     * @Description 根据条件查询并返回唯一值
     * @param parameters
     * @return BnExpert
     */
	StUser findUniqueBy(Map<String, Object> parameters);
    /**
     * @Description 更新记录
     * @param StUser
     * @return 返回更新影响的行数
     */
    int update(StUser stUser);
}