package com.jacqui.main.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.web.BaseController;
import com.jacqui.main.domain.StUser;
import com.jacqui.main.service.StUserService;
/**
 * 
 * @ClassName: StUserController 
 * @Description: 系统用户
 * @author 张三丰
 */
@Controller
@RequestMapping(value = "/st/user")
public class StUserController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(StUserController.class);
	
	@Autowired
	public StUserService stUserService;
	
	/**
	 * @description 初始化密码修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "password",params="_m=init")
	public String passwordInit(Model model,ServletRequest request) {
		return "main/main_password";
	} 
	
	/**
	 * @description 修改
	 * @param model
	 * @param stUser
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,StUser stUser,ServletRequest request){
		JsonReturn json = new JsonReturn();
		try{
			String oldPasswd = request.getParameter("oldpasswd");
			StUser stuser = getSession(request);
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("staffId", stuser.getStaffId());
			map.put("passwd", DigestUtils.md5Hex(oldPasswd));
			StUser user = stUserService.findUniqueBy(map);
			if(user==null){
				json.setErrcode("1");
				json.setErrmsg("当前密码不正确");
				return json;
			}
			stUser.setStaffId(stuser.getStaffId());
			stUser.setPasswd(DigestUtils.md5Hex(stUser.getPasswd()));
			stUser.setUpdateBy(stuser.getUpdateBy());
			stUserService.update(stUser);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	/**
	 * 系统锁定初始化
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "lock",params="_m=init")
	public String lockInit(Model model,ServletRequest request) {
		StUser stuser = getSession(request);
		request.setAttribute("username", stuser.getStaffId());
		return "main/main_lock";
	} 
	
	/**
	 * 锁定系统用户登录
	 * @param model
	 * @param stUser
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "locklogin",params = "_m=exec")
	@ResponseBody
	public Object locklogin(Model model,StUser stUser,ServletRequest request){
		JsonReturn json = new JsonReturn();
		try{
			String username = request.getParameter("username");
			String passwd = request.getParameter("passwd");
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("staffId", username);
			map.put("passwd", DigestUtils.md5Hex(passwd));
			StUser user = stUserService.findUniqueBy(map);
			if(user==null){
				json.setErrcode("1");
				json.setErrmsg("密码不正确");
				return json;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
}
