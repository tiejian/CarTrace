package com.jacqui.main.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @ClassName: StUser
 * @Description: 系统用户
 * @author 张三丰
 * 
 */
public class StUser implements Serializable {

	private static final long serialVersionUID = 1L;

	private String staffId; // 员工ID
	private String passwd; // 登录密码
	private int passwdLevel; // 密码级别
	private String companyId; // 所属企业
	private int status; // 状态0无效1有效
	private int ifAdmin; // 是否超级用户0否1是
	private Date lockTime; // 锁定时间
	private int loginFailureCount; // 登录失败次数
	private Date createAt; // 创建时间
	private String createBy; // 创建人
	private Date updateAt; // 修改时间
	private String updateBy; // 修改人

	public String getStaffId() {
		return staffId;
	}

	public void setStaffId(String staffId) {
		this.staffId = staffId;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}

	public int getPasswdLevel() {
		return passwdLevel;
	}

	public void setPasswdLevel(int passwdLevel) {
		this.passwdLevel = passwdLevel;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getIfAdmin() {
		return ifAdmin;
	}

	public void setIfAdmin(int ifAdmin) {
		this.ifAdmin = ifAdmin;
	}

	public Date getLockTime() {
		return lockTime;
	}

	public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	}

	public int getLoginFailureCount() {
		return loginFailureCount;
	}

	public void setLoginFailureCount(int loginFailureCount) {
		this.loginFailureCount = loginFailureCount;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateAt() {
		return updateAt;
	}

	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

}