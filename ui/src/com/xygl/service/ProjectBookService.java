package com.xygl.service;


import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xygl.core.exception.CustomException;
import com.xygl.core.service.BaseService;
import com.xygl.core.utils.UuidExtUtil;
import com.xygl.dao.BnProjectBookDaoI;
import com.xygl.dao.BnProjectConcreteContentDaoI;
import com.xygl.dao.BnProjectConcreteContentListDaoI;
import com.xygl.dao.BnProjectPersonnelDaoI;
import com.xygl.dao.BnProjectPrincipalDaoI;
import com.xygl.domain.Admin;
import com.xygl.domain.BnProjectBook;
import com.xygl.domain.BnProjectConcreteContent;
import com.xygl.domain.BnProjectConcreteContentList;
import com.xygl.domain.BnProjectPersonnel;
import com.xygl.domain.BnProjectPrincipal;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.service.ProjectBookService.java
 *  @Class Name    : ProjectBookService
 *  @Description   : 项目立项书申报
 *  @Author        : ljx
 *  @Creation Date : 2016-5-21 下午1:52:35 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-21       TODO
 */
@Service
public class ProjectBookService extends BaseService<BnProjectBook, String>{

	@Autowired
	private BnProjectBookDaoI bnProjectBookDaoI;
	@Autowired
	private BnProjectPrincipalDaoI bnProjectPrincipalDaoI;
	@Autowired
	private BnProjectConcreteContentDaoI bnProjectConcreteContentDaoI;
	@Autowired
	private BnProjectPersonnelDaoI bnProjectPersonnelDaoI;
	@Autowired
	private BnProjectConcreteContentListDaoI bnProjectConcreteContentListDaoI;
    
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class,readOnly=false)
	public void insertProjectBook(BnProjectBook bnProjectBook, BnProjectPrincipal bnProjectPrincipal,
			BnProjectConcreteContent bnProjectConcreteContent,Admin admin,String zyryjson,String jtmxjson)throws Exception{
		String id = UuidExtUtil.getUUID();
		bnProjectBook.setId(id);
		bnProjectBook.setAuditStatus("1");
		bnProjectBook.setCreateBy(admin.getCreateBy());
		bnProjectBook.setCreateName(admin.getCreateName());
		bnProjectBook.setDeleteSgin("0");
		
		bnProjectPrincipal.setId(UuidExtUtil.getUUID());
		bnProjectPrincipal.setPbid(bnProjectBook.getId());
		bnProjectPrincipal.setCreateBy(admin.getCreateBy());
		bnProjectPrincipal.setCreateName(admin.getCreateName());
		bnProjectPrincipal.setDeleteSgin("0");
		
		bnProjectConcreteContent.setId(UuidExtUtil.getUUID());
		bnProjectConcreteContent.setPbid(bnProjectBook.getId());
		bnProjectConcreteContent.setCreateBy(admin.getCreateBy());
		bnProjectConcreteContent.setCreateName(admin.getCreateName());
		bnProjectConcreteContent.setDeleteSgin("0");
		
		JSONArray zyryJson = JSONArray.parseArray(zyryjson);
		if(zyryJson!=null && zyryJson.size()>0){
			BnProjectPersonnel p = new BnProjectPersonnel();
			for (Object object : zyryJson) {
				JSONObject json = (JSONObject)object;
				p.setId(UuidExtUtil.getUUID());
				p.setPbid(bnProjectBook.getId());
				p.setPpid(bnProjectPrincipal.getId());
				p.setName(json.getString("ryqkxm"));
				p.setAge(json.getInteger("ryqknl"));
				p.setSex(json.getString("ryqkxb"));
				p.setDegree(json.getString("ryqkxw"));
				p.setSpecialty(json.getString("ryqkcszy"));
				p.setJobTitle(json.getString("ryqkzc"));
				p.setPrincipalJob(json.getString("ryqkzyrw"));
				p.setCreateBy(admin.getCreateBy());
				p.setCreateName(admin.getCreateName());
				p.setDeleteSgin("0");
				
				bnProjectPersonnelDaoI.insert(p);
			}
		}else{
			throw new CustomException("参加研究项目的主要人员情况至少有一行！");
		}
		if(StringUtils.isNotBlank(jtmxjson)){
			JSONArray jtmxJson = JSONArray.parseArray(jtmxjson);
			if(jtmxJson!=null && jtmxJson.size()>0){
				BnProjectConcreteContentList c = new BnProjectConcreteContentList();
				for (Object object : jtmxJson) {
					JSONObject json = (JSONObject)object;
					c.setId(UuidExtUtil.getUUID());
					c.setPbid(bnProjectBook.getId());
					c.setPccid(bnProjectConcreteContent.getId());
					c.setProjectName(json.getString("jtmxxmmc"));
					c.setContent(json.getString("jtmxjsnr"));
					c.setShowform(json.getString("jtmxzyzxxs"));
					c.setBudgetWorkload(json.getString("jtmxysgzl"));
					c.setPrincipal(json.getString("jtmxfzr"));
					c.setCreateBy(admin.getCreateBy());
					c.setCreateName(admin.getCreateName());
					c.setDeleteSgin("0");
					
					bnProjectConcreteContentListDaoI.insert(c);
				}
			}
		}
		
		bnProjectBookDaoI.insert(bnProjectBook);
		bnProjectPrincipalDaoI.insert(bnProjectPrincipal);
		bnProjectConcreteContentDaoI.insert(bnProjectConcreteContent);
	}
	
	public boolean findUniqueIsExist(String projectNo)throws Exception{
		long count = bnProjectBookDaoI.findUniqueIsExist(projectNo);
		if(count > 0){
			return false;
		}
		return true;
	}
	
	public List<BnProjectConcreteContentList> findAllBnProjectConcreteContentListByPbid(String pbid){
		return bnProjectConcreteContentListDaoI.findAllBnProjectConcreteContentListByPbid(pbid);
	}
	
	public List<BnProjectPersonnel> findAllBnProjectPersonnelByPbid(String pbid){
		return bnProjectPersonnelDaoI.findAllBnProjectPersonnelByPbid(pbid);
	}
	
}
