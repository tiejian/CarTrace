package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.BnProjectPrincipalDaoI;
import com.xygl.domain.BnProjectPrincipal;

@Service
public class BnProjectPrincipalService extends BaseService<BnProjectPrincipal, String>{

	@Autowired
	private BnProjectPrincipalDaoI bnProjectPrincipalDaoI;
	
	public BnProjectPrincipal getBnProjectPrincipalByPbid(String pbid){
		return bnProjectPrincipalDaoI.getBnProjectPrincipalByPbid(pbid);
	}
}
