package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.BnProjectBookDaoI;
import com.xygl.domain.BnProjectBook;

@Service
public class BnProjectBookService extends BaseService<BnProjectBook, String>{

	@Autowired
	private BnProjectBookDaoI bnProjectBookDaoI;
	
}
