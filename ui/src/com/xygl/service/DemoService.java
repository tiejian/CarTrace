package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.DemoDaoI;
import com.xygl.domain.Demo;


@Service
public class DemoService extends BaseService<Demo, String>{

	@Autowired
	private DemoDaoI demoDaoI;
    
}
