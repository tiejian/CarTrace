package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.BnProjectConcreteContentDaoI;
import com.xygl.domain.BnProjectConcreteContent;

@Service
public class BnProjectConcreteContentService extends BaseService<BnProjectConcreteContent, String>{

	@Autowired
	private BnProjectConcreteContentDaoI bnProjectConcreteContentDaoI;
	
	public BnProjectConcreteContent getBnProjectConcreteContentByPbid(String pbid){
		return bnProjectConcreteContentDaoI.getBnProjectConcreteContentByPbid(pbid);
	}
	
}
