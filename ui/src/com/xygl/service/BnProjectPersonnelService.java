package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.BnProjectPersonnelDaoI;
import com.xygl.domain.BnProjectPersonnel;

@Service
public class BnProjectPersonnelService extends BaseService<BnProjectPersonnel, String>{

	@Autowired
	private BnProjectPersonnelDaoI bnProjectPersonnelDaoI;
	
}
