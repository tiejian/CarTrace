package com.xygl.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xygl.core.service.BaseService;
import com.xygl.dao.BnProjectConcreteContentListDaoI;
import com.xygl.domain.BnProjectConcreteContentList;

@Service
public class BnProjectConcreteContentListService extends BaseService<BnProjectConcreteContentList, String>{

	@Autowired
	private BnProjectConcreteContentListDaoI bnProjectConcreteContentListDaoI;
	
}
