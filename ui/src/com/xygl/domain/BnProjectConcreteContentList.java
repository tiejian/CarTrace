package com.xygl.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect
@SuppressWarnings("serial")
public class BnProjectConcreteContentList implements java.io.Serializable {

	private String id;// 主键
	private String pbid;// 项目立项书主键
	private String pccid;// 项目建设目标与内容主键
	private String projectName;// 项目名称
	private String content;// 建设内容
	private String showform;// 资源展现形式
	private String budgetWorkload;// 预算/工作量
	private String principal;// 负责人
	private Date createAt;// 创建时间
	private String createBy;// 创建人
	private String createName;// 创建人名称
	private String deleteSgin;// 删除标记（1删除，0未删除）

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPbid() {
		return pbid;
	}

	public void setPbid(String pbid) {
		this.pbid = pbid;
	}

	public String getPccid() {
		return pccid;
	}

	public void setPccid(String pccid) {
		this.pccid = pccid;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getShowform() {
		return showform;
	}

	public void setShowform(String showform) {
		this.showform = showform;
	}

	public String getBudgetWorkload() {
		return budgetWorkload;
	}

	public void setBudgetWorkload(String budgetWorkload) {
		this.budgetWorkload = budgetWorkload;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8") 
	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getDeleteSgin() {
		return deleteSgin;
	}

	public void setDeleteSgin(String deleteSgin) {
		this.deleteSgin = deleteSgin;
	}

}
