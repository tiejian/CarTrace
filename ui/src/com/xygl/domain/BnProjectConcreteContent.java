package com.xygl.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect
@SuppressWarnings("serial")
public class BnProjectConcreteContent implements java.io.Serializable {

	private String id;// 主键
	private String pbid;// 项目立项书主键
	private String projectBeginTime;// 项目起时间
	private String projectEndTime;// 项目止时间
	private String projectGoal;// 项目建设目标
	private String projectContent;// 项目建设内容
	private String projectFeatureInnovation;// 项目特色与创新
	private Date createAt;// 创建时间
	private String createBy;// 创建人
	private String createName;// 创建人名称
	private String deleteSgin;// 删除标记（1删除，0未删除）

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPbid() {
		return pbid;
	}

	public void setPbid(String pbid) {
		this.pbid = pbid;
	}

	public String getProjectBeginTime() {
		return projectBeginTime;
	}

	public void setProjectBeginTime(String projectBeginTime) {
		this.projectBeginTime = projectBeginTime;
	}

	public String getProjectEndTime() {
		return projectEndTime;
	}

	public void setProjectEndTime(String projectEndTime) {
		this.projectEndTime = projectEndTime;
	}

	public String getProjectGoal() {
		return projectGoal;
	}

	public void setProjectGoal(String projectGoal) {
		this.projectGoal = projectGoal;
	}

	public String getProjectContent() {
		return projectContent;
	}

	public void setProjectContent(String projectContent) {
		this.projectContent = projectContent;
	}

	public String getProjectFeatureInnovation() {
		return projectFeatureInnovation;
	}

	public void setProjectFeatureInnovation(String projectFeatureInnovation) {
		this.projectFeatureInnovation = projectFeatureInnovation;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8") 
	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getDeleteSgin() {
		return deleteSgin;
	}

	public void setDeleteSgin(String deleteSgin) {
		this.deleteSgin = deleteSgin;
	}

}
