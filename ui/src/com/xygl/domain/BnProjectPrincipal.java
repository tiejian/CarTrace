package com.xygl.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect
@SuppressWarnings("serial")
public class BnProjectPrincipal implements java.io.Serializable {

	private String id;// 主键
	private String pbid;// 项目立项书主键
	private String name;// 姓名
	private int age;// 年龄
	private String sex;// 1男2女
	private String jobTitle;// 职称
	private String degree;// 最终学位
	private String position;// 职务
	private String sciencename;// 所在学科名称
	private String researchDirection;// 主要研究方向
	private String achievement;// 研究成果
	private Date createAt;// 创建时间
	private String createBy;// 创建人
	private String createName;// 创建人名称
	private String deleteSgin;// 删除标记（1删除，0未删除）

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPbid() {
		return pbid;
	}

	public void setPbid(String pbid) {
		this.pbid = pbid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getSciencename() {
		return sciencename;
	}

	public void setSciencename(String sciencename) {
		this.sciencename = sciencename;
	}

	public String getResearchDirection() {
		return researchDirection;
	}

	public void setResearchDirection(String researchDirection) {
		this.researchDirection = researchDirection;
	}

	public String getAchievement() {
		return achievement;
	}

	public void setAchievement(String achievement) {
		this.achievement = achievement;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8") 
	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getDeleteSgin() {
		return deleteSgin;
	}

	public void setDeleteSgin(String deleteSgin) {
		this.deleteSgin = deleteSgin;
	}

}
