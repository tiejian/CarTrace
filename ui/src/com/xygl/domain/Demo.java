package com.xygl.domain;



import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.domain.Demo.java
 *  @Class Name    : Demo
 *  @Description   : demo
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:52:00 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
@JsonAutoDetect
@SuppressWarnings("serial")
public class Demo implements java.io.Serializable {

	private String id;
	private String name;
	private String xb;
	private String xbmc;
	private String tjr;
	private String tjrName;
	private Date addTime;
	private Date csrq;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getXbmc() {
		return xbmc;
	}

	public void setXbmc(String xbmc) {
		this.xbmc = xbmc;
	}

	public String getXb() {
		return xb;
	}

	public void setXb(String xb) {
		this.xb = xb;
	}

	public String getTjr() {
		return tjr;
	}

	public void setTjr(String tjr) {
		this.tjr = tjr;
	}

	public String getTjrName() {
		return tjrName;
	}

	public void setTjrName(String tjrName) {
		this.tjrName = tjrName;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8") 
	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}
	
	@JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
	public Date getCsrq() {
		return csrq;
	}

	public void setCsrq(Date csrq) {
		this.csrq = csrq;
	}
	
}
