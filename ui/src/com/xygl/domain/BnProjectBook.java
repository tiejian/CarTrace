package com.xygl.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;

@JsonAutoDetect
@SuppressWarnings("serial")
public class BnProjectBook implements java.io.Serializable {

	private String id;// 主键
	private String projectName;// 项目名称
	private String projectNo;// 项目编号
	private String projectBeginTime;// 项目起时间
	private String projectEndTime;// 项目止时间
	private String entrustingCompany;// 委托单位（甲方）
	private String assumeComPany;// 承担单位/负责人（乙方）
	private String auditStatus;// 1院部评审中，2专家评审中，3申请修改，4审批通过，5审批不通过
	private String auditStatusName;// 1院部评审中，2专家评审中，3申请修改，4审批通过，5审批不通过
	private Date createAt;// 创建时间
	private String createBy;// 创建人
	private String createName;// 创建人名称
	private String deleteSgin;// 删除标记（1删除，0未删除）

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(String projectNo) {
		this.projectNo = projectNo;
	}

	public String getProjectBeginTime() {
		return projectBeginTime;
	}

	public void setProjectBeginTime(String projectBeginTime) {
		this.projectBeginTime = projectBeginTime;
	}

	public String getProjectEndTime() {
		return projectEndTime;
	}

	public void setProjectEndTime(String projectEndTime) {
		this.projectEndTime = projectEndTime;
	}

	public String getEntrustingCompany() {
		return entrustingCompany;
	}

	public void setEntrustingCompany(String entrustingCompany) {
		this.entrustingCompany = entrustingCompany;
	}

	public String getAssumeComPany() {
		return assumeComPany;
	}

	public void setAssumeComPany(String assumeComPany) {
		this.assumeComPany = assumeComPany;
	}

	public String getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8") 
	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getCreateName() {
		return createName;
	}

	public void setCreateName(String createName) {
		this.createName = createName;
	}

	public String getDeleteSgin() {
		return deleteSgin;
	}

	public void setDeleteSgin(String deleteSgin) {
		this.deleteSgin = deleteSgin;
	}

	public String getAuditStatusName() {
		return auditStatusName;
	}

	public void setAuditStatusName(String auditStatusName) {
		this.auditStatusName = auditStatusName;
	}

}
