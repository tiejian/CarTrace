package com.xygl.domain;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.domain.Admin.java
 *  @Class Name    : Admin
 *  @Description   : 管理员登录信息实体类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:32:46 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class Admin {
	private String id;// 主键
	private String createBy;// 帐号
	private String createName;// 名称
	private String glsf;// 1超级管理员,2系统管理员,3系统操作员
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public String getCreateName() {
		return createName;
	}
	public void setCreateName(String createName) {
		this.createName = createName;
	}
	public String getGlsf() {
		return glsf;
	}
	public void setGlsf(String glsf) {
		this.glsf = glsf;
	}
	
	
}
