package com.xygl.dao;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.BnProjectBook;

@MyBatisRepository
public interface BnProjectBookDaoI extends BaseDaoI<BnProjectBook, String>{
	
	public long findUniqueIsExist(String projectNo);

}
