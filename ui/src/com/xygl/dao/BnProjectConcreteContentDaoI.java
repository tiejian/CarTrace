package com.xygl.dao;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.BnProjectConcreteContent;

@MyBatisRepository
public interface BnProjectConcreteContentDaoI extends BaseDaoI<BnProjectConcreteContent, String>{
	
	public BnProjectConcreteContent getBnProjectConcreteContentByPbid(String pbid);
	
}
