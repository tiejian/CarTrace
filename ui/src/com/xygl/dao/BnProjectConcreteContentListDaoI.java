package com.xygl.dao;

import java.util.List;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.BnProjectConcreteContentList;

@MyBatisRepository
public interface BnProjectConcreteContentListDaoI extends BaseDaoI<BnProjectConcreteContentList, String>{
	
	public List<BnProjectConcreteContentList> findAllBnProjectConcreteContentListByPbid(String pbid);
}
