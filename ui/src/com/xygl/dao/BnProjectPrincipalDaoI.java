package com.xygl.dao;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.BnProjectPrincipal;

@MyBatisRepository
public interface BnProjectPrincipalDaoI extends BaseDaoI<BnProjectPrincipal, String>{
	
	public BnProjectPrincipal getBnProjectPrincipalByPbid(String pbid);
	
}
