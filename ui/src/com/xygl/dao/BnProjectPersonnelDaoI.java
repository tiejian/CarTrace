package com.xygl.dao;

import java.util.List;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.BnProjectPersonnel;

@MyBatisRepository
public interface BnProjectPersonnelDaoI extends BaseDaoI<BnProjectPersonnel, String>{
	
	public List<BnProjectPersonnel> findAllBnProjectPersonnelByPbid(String pbid);
}
