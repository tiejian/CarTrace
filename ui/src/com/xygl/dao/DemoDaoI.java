package com.xygl.dao;

import com.xygl.core.dao.BaseDaoI;
import com.xygl.core.mybatis.MyBatisRepository;
import com.xygl.domain.Demo;

@MyBatisRepository
public interface DemoDaoI extends BaseDaoI<Demo, String>{
	
}
