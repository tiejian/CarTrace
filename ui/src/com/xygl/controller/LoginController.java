package com.xygl.controller;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.xygl.core.constant.Constant;
import com.xygl.core.constant.ConstantCode;
import com.xygl.core.utils.Json;
import com.xygl.core.utils.SysLogUtil;
import com.xygl.domain.Admin;

/**
 * 
 * @Project : aitew-web-admin
 * @Program Name : com.ccbs.login.controllor.LoginController.java
 * @Class Name : LoginController
 * @Description : 管理员登录视图控制层
 * @Author : ljx
 * @Creation Date : 2015-5-25 下午6:43:30
 * 
 */
@RequestMapping(value = "/login")
@Controller
public class LoginController {
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	public Json login(Model model, HttpServletRequest request) {
		Json json = null;
		try {
			json = new Json();
			HttpSession session = request.getSession(false);
			String createBy = request.getParameter("createby");
			String pswd = request.getParameter("password");
			String c = (String)session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
			String parm = (String) request.getParameter("checkcode");
			if(!createBy.equals("superadmin")){
				json.setCode(ConstantCode.CODE_ERR_994);
				json.setJsonobj("登录帐户不存在或密码不正确！");
				return json;
			}else if(!pswd.equals("123456")){
				json.setCode(ConstantCode.CODE_ERR_994);
				json.setJsonobj("登录帐户不存在或密码不正确！");
				return json;
			}else if(!parm.equals(c)){
				json.setCode(ConstantCode.CODE_ERR_991);
				json.setJsonobj("验证码不正确！");
				return json;
			}
//			map = tSysAdminService.getLoginAdminBy(zh, MD5.getMD5Code(pswd));
//			if(map == null){
//				json.setCode(ConstantCode.CODE_ERR_994);
//				//json.setJsonobj("登录帐户不存在或密码不正确！");
//				return json;
//			}else if(((String)map.get("sdzt")).equals("2")){
//				json.setCode(ConstantCode.CODE_ERR_993);
//				//json.setJsonobj("登录帐号已锁定，请联系统管理员！");
//				SysLogUtil.error( map.get("zh") + "登录帐号已锁定，请联系统管理员！");
//				return json;
//			}
			Admin admin = new Admin();
			admin.setId("1");
			admin.setCreateBy(createBy);
			admin.setCreateName("管理员");
			admin.setGlsf("1");
			
			String sessionid = request.getSession().getId();
			WebUtils.setSessionAttribute((HttpServletRequest) request,Constant.SESSION_ADMIN_KEY, admin);
			WebUtils.setSessionAttribute((HttpServletRequest) request,Constant.SESSION_ID_KEY, sessionid);
			WebUtils.setSessionAttribute((HttpServletRequest) request,Constant.SESSION_ADMIN_ID_KEY, admin.getId());
			json.setJsonobj(sessionid);
		} catch (Exception e) {
			SysLogUtil.error("LoginController login时出错！", e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setJsonobj("系统异常！");
		}
		return json;
	}
	
	/**
	 * 
	 * @Description : 管理员退出
	 * @Method_Name : logout
	 * @param request
	 * @param response
	 * @return : void
	 * @Creation Date : 2015-4-6 下午6:44:23
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logout(HttpServletRequest request, HttpServletResponse response) {
		try {
			HttpSession session = request.getSession(false);
			if (session != null) {
				session.removeAttribute(Constant.SESSION_ADMIN_KEY);
				session.removeAttribute(Constant.SESSION_ID_KEY);
				session.removeAttribute(Constant.SESSION_ADMIN_ID_KEY);
				session.removeAttribute(Constant.SESSION_MENU_KEY);
				session.invalidate();
			}
			response.sendRedirect(request.getContextPath()+"/index.jsp");
		} catch (Exception e) {
			SysLogUtil.error("LoginController logout时出错！", e);
		}
	}

}
