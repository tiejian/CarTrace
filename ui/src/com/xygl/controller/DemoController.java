package com.xygl.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.at.page.Criteria;
import com.at.page.Criterion;
import com.at.page.DataGrid;
import com.at.page.Page;
import com.at.page.PagingQueryFilter;
import com.xygl.core.constant.ConstantCode;
import com.xygl.core.controllor.BaseController;
import com.xygl.core.utils.Json;
import com.xygl.core.utils.SysLogUtil;
import com.xygl.domain.Demo;
import com.xygl.service.DemoService;


@RequestMapping("/demo")
@Controller
public class DemoController extends BaseController {

	@Autowired
	public DemoService demoService;

	/**
	 * 
	 * @Description : demo查询页面初始化
	 * @Method_Name : init
	 * @return
	 * @return : String
	 * @Creation Date : 2015-4-6 上午11:46:22
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/init" , method = RequestMethod.GET)
	public String init() {
		return "demo/demo-query";
	}

	/**
	 * 
	 * @Description : demo查询分页
	 * @Method_Name : query
	 * @return
	 * @return : DataGrid
	 * @Creation Date : 2015-4-6 上午11:46:22
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	@ResponseBody
	public DataGrid query(@RequestParam("page") int page,@RequestParam("rows") int pageSize,
			HttpServletRequest request)throws Exception {
		List<Criterion> criterions = PagingQueryFilter
				.bySearchFilter(PagingQueryFilter
						.buildFromHttpRequest(PagingQueryFilter
								.getParametersStartingWith(request,
										"filter")));
		Criteria criteria = new Criteria(criterions, new Page(page,
				pageSize, PagingQueryFilter.getPagingQueryOrder(request)));
		return demoService.paging(criteria);
	}

	/**
	 * 
	 * @Description : demo添加页面初始化
	 * @Method_Name : toinsert
	 * @return
	 * @return : String
	 * @Creation Date : 2015-4-6 下午4:20:38
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/toinsert", method = RequestMethod.GET)
	public String toinsert() {
		return "demo/demo-insert";
	}

	/**
	 * 
	 * @Description : demo添加
	 * @Method_Name : insert
	 * @param demo	demo对象
	 * @param request
	 * @return
	 * @throws Exception
	 * @return : Json
	 * @Creation Date : 2015-4-6 下午4:31:18
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/doinsert", method = RequestMethod.POST)
	@ResponseBody
	public Json doinsert(Demo demo, HttpServletRequest request) throws Exception {
		Json json = new Json();
		try {
//			Admin admin = SessionUtil.getAdmin(request);
//			demo.setId(UuidExtUtil.getUUID());
//			demo.setTjr(admin.getAccount());
//			demo.setTjrName(admin.getName());
			demoService.insert(demo);
		} catch (Exception e) {
			SysLogUtil.error("DemoController doinsert时出错！" + ConstantCode.CODE_ERR_999, e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setMsg("保存失败！");
		}
		return json;
	}

	/**
	 * 
	 * @Description : 获取单一demo实体
	 * @Method_Name : toedit
	 * @param id
	 *            主键
	 * @param model
	 * @return
	 * @throws Exception
	 * @return : String
	 * @Creation Date : 2015-4-6 下午7:26:52
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/toedit", method = RequestMethod.GET)
	public String toedit(@RequestParam("id") String id, Model model)
			throws Exception {
		try {
			Demo demo = demoService.findUniqueBy(id);
			model.addAttribute("demo", demo);
		} catch (Exception e) {
			SysLogUtil.error("DemoController toedit时出错！" + ConstantCode.CODE_ERR_999, e);
		}
		return "demo/demo-edit";
	}

	/**
	 * 
	 * @Description : demo信息删除
	 * @Method_Name : del
	 * @param id
	 *            主键
	 * @param request
	 * @return
	 * @throws Exception
	 * @return : Json
	 * @Creation Date : 2015-4-6 下午7:23:43
	 * @version : v1.00
	 * @Author : ljx
	 */
	@RequestMapping(value = "/dodel", method = RequestMethod.GET)
	@ResponseBody
	public Json dodel(@RequestParam("id") String id) throws Exception {
		Json json = new Json();
		try {
			demoService.delete(id);
		} catch (Exception e) {
			SysLogUtil.error("DemoController dodel时出错！" + ConstantCode.CODE_ERR_999, e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setMsg("删除失败！");
		}
		return json;
	}
	
	/**
	 * 
	 *  @Description    : demo更新
	 *  @Method_Name    : doupdate
	 *  @param demo	demo对象
	 *  @param request
	 *  @return
	 *  @throws Exception 
	 *  @return         : Json
	 *  @Creation Date  : 2015-5-19 下午2:37:22 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@RequestMapping(value = "/doupdate", method = RequestMethod.POST)
	@ResponseBody
	public Json doupdate(Demo demo, HttpServletRequest request) throws Exception {
		Json json = new Json();
		try {
//			Admin admin = SessionUtil.getAdmin(request);
//			demo.setTjr(admin.getAccount());
//			demo.setTjrName(admin.getName());
			demoService.update(demo);
		} catch (Exception e) {
			SysLogUtil.error("DemoController doupdate时出错！" + ConstantCode.CODE_ERR_999, e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setMsg("保存失败！");
		}
		return json;
	}
	
	public static void main(String[] args) {
		System.out.println("20150526211822243");
	}
	
}
