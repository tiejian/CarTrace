package com.xygl.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.at.page.Criteria;
import com.at.page.Criterion;
import com.at.page.DataGrid;
import com.at.page.Page;
import com.at.page.PagingQueryFilter;
import com.xygl.core.constant.ConstantCode;
import com.xygl.core.controllor.BaseController;
import com.xygl.core.exception.CustomException;
import com.xygl.core.utils.DateUtil;
import com.xygl.core.utils.Json;
import com.xygl.core.utils.SessionUtil;
import com.xygl.core.utils.SysLogUtil;
import com.xygl.domain.Admin;
import com.xygl.domain.BnProjectBook;
import com.xygl.domain.BnProjectConcreteContent;
import com.xygl.domain.BnProjectConcreteContentList;
import com.xygl.domain.BnProjectPersonnel;
import com.xygl.domain.BnProjectPrincipal;
import com.xygl.service.BnProjectBookService;
import com.xygl.service.BnProjectConcreteContentListService;
import com.xygl.service.BnProjectConcreteContentService;
import com.xygl.service.BnProjectPersonnelService;
import com.xygl.service.BnProjectPrincipalService;
import com.xygl.service.ProjectBookService;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.controller.ProjectBookController.java
 *  @Class Name    : ProjectBookController
 *  @Description   : 项目立项书控制层
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午1:08:03 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
@RequestMapping("/projectbook")
@Controller
public class ProjectBookController extends BaseController {

	@Autowired
	public ProjectBookService projectBookService;
	@Autowired
	public BnProjectBookService bnProjectBookService;
	@Autowired
	public BnProjectConcreteContentListService bnProjectConcreteContentListService;
	@Autowired
	public BnProjectConcreteContentService bnProjectConcreteContentService;
	@Autowired
	public BnProjectPersonnelService bnProjectPersonnelService;
	@Autowired
	public BnProjectPrincipalService bnProjectPrincipalService;
	

	
	@RequestMapping(value = "/init" , method = RequestMethod.GET)
	public String init() {
		return "projectbook/projectbook-query";
	}

	@RequestMapping(value = "/query", method = RequestMethod.POST)
	@ResponseBody
	public DataGrid query(@RequestParam("page") int page,@RequestParam("pagesize") int pagesize,
			HttpServletRequest request)throws Exception {
		List<Criterion> criterions = PagingQueryFilter
				.bySearchFilter(PagingQueryFilter
						.buildFromHttpRequest(PagingQueryFilter
								.getParametersStartingWith(request,
										"filter")));
		Criteria criteria = new Criteria(criterions, new Page(page,
				pagesize, PagingQueryFilter.getPagingQueryOrder(request)));
		return projectBookService.paging(criteria);
	}

	@RequestMapping(value = "/toinsert", method = RequestMethod.GET)
	public String toinsert() {
		return "projectbook/projectbook-insert";
	}

	@RequestMapping(value = "/doinsert", method = RequestMethod.POST)
	@ResponseBody
	public Json doinsert(BnProjectBook bnProjectBook, BnProjectPrincipal bnProjectPrincipal,
			BnProjectConcreteContent bnProjectConcreteContent, HttpServletRequest request) throws Exception {
		Json json = new Json();
		try {
			Admin admin = SessionUtil.getSessionAdmin(request);
			String zyryjson = request.getParameter("zyryjson");
			String jtmxjson = request.getParameter("jtmxjson");
			boolean flag = projectBookService.findUniqueIsExist(bnProjectBook.getProjectNo());
			if(!flag){
				json.setCode(ConstantCode.CODE_ERR_995);
				json.setMsg("项目编号已存在，重新输入！");
				return json;
			}
			projectBookService.insertProjectBook(bnProjectBook,bnProjectPrincipal,bnProjectConcreteContent,admin,zyryjson,jtmxjson);
		
		} catch (CustomException e) {
			SysLogUtil.error("ProjectBookController doinsert时出错！" + ConstantCode.CODE_ERR_999, e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setMsg(e.getMessage());
		} catch (Exception e) {
			SysLogUtil.error("ProjectBookController doinsert时出错！" + ConstantCode.CODE_ERR_999, e);
			json.setCode(ConstantCode.CODE_ERR_999);
			json.setMsg("保存失败！");
		}
		return json;
	}
	
	@RequestMapping(value = "/tozyryinsert", method = RequestMethod.GET)
	public String tozyryinsert() {
		return "projectbook/projectbook-zyry-insert";
	}
	
	@RequestMapping(value = "/tojtmxinsert", method = RequestMethod.GET)
	public String tojtmxinsert() {
		return "projectbook/projectbook-jtmx-insert";
	}

	@RequestMapping(value = "/tolook", method = RequestMethod.GET)
	public String tolook(@RequestParam("id") String id, Model model)
			throws Exception {
		try {
			BnProjectBook bnProjectBook = bnProjectBookService.findUniqueBy(id);
			BnProjectConcreteContent bnProjectConcreteContent = bnProjectConcreteContentService.getBnProjectConcreteContentByPbid(id);
			BnProjectPrincipal bnProjectPrincipal= bnProjectPrincipalService.getBnProjectPrincipalByPbid(id);
			List<BnProjectConcreteContentList> bnProjectConcreteContentList = projectBookService.findAllBnProjectConcreteContentListByPbid(id);
			List<BnProjectPersonnel> bnProjectPersonnelList = projectBookService.findAllBnProjectPersonnelByPbid(id);
			
			
			
			model.addAttribute("bnProjectBook", bnProjectBook);
			model.addAttribute("bnProjectConcreteContent", bnProjectConcreteContent);
			model.addAttribute("bnProjectPrincipal", bnProjectPrincipal);
			model.addAttribute("bnProjectConcreteContentList", bnProjectConcreteContentList);
			model.addAttribute("bnProjectPersonnelList", bnProjectPersonnelList);
			model.addAttribute("beginDate", DateUtil.convertStringToDate(DateUtil.datePattern, bnProjectBook.getProjectBeginTime()));
			model.addAttribute("endDate", DateUtil.convertStringToDate(DateUtil.datePattern, bnProjectBook.getProjectEndTime()));
		
		} catch (Exception e) {
			SysLogUtil.error("ProjectBookController toedit时出错！" + ConstantCode.CODE_ERR_999, e);
		}
		return "projectbook/projectbook-look";
	}

}
