package com.xygl.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.xygl.core.constant.ConstantCode;
import com.xygl.core.utils.SysLogUtil;


@RequestMapping(value = "/mainframe")
@Controller
public class MainFrameController {
	
	@RequestMapping(value="/mf" , method = RequestMethod.GET)
	public String mainframe(HttpServletRequest request, HttpServletResponse response) {
		try{
			//HttpSession session = request.getSession(false);
		}catch(Exception e){
			SysLogUtil.error("MainFrameController mainframe时出错！" + ConstantCode.CODE_ERR_999, e);
		}
		return "system/mainframe";
	}
	
}
