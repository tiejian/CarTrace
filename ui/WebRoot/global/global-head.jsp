<%@ page language="java" pageEncoding="UTF-8"%>
<%@page import="java.util.Date"%>    
<%@page import="java.text.SimpleDateFormat"%>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false" %> 
<% 
SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
String date = String.valueOf(new Date().getTime());  
%> 
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="ver" value="<%=date %>" />
