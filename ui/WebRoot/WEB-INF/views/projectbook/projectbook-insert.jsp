<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<%@include file="/global/global-other.jsp" %>
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<!--  <link href='http://fonts.useso.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>-->
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="${ctx }/assets/plugins/weather-icons/css/weather-icons.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/nvd3/nv.d3.min.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="${ctx }/assets/css/styles.css">
		<link rel="stylesheet" href="${ctx }/assets/css/styles-responsive.css">
		<link rel="stylesheet" href="${ctx }/assets/css/plugins.css">
		<link rel="stylesheet" href="${ctx }/assets/css/themes/theme-default.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="${ctx }/assets/css/print.css" type="text/css" media="print"/>
		<!-- ligerUi CSS -->
		<link  href="${ctx }/assets/plugins/ligerUI/skins/style1/css/ligerui-all.css?v=20150426" rel="stylesheet" type="text/css" />
		<link  href="${ctx }/assets/plugins/ligerUI/skins/ligerui-icons.css?v=20150426" rel="stylesheet" type="text/css" />
		<link  href="${ctx }/assets/css/css.css?v=20150426" rel="stylesheet" type="text/css" />
		<!-- end: CORE CSS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body >
		<div class="row">
			<form role="form" name="form1" id="form1" class="form-horizontal">
						<div class="nav">
							<div style="float: left;width:200px;">
					    		 <ul>
						    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
						    		<li style="float:left;padding-left: 5px;">项目立项书书面信息</li>
						    	</ul>
					    	</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">项目名称：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="projectName" name="projectName" value="" class="form-control" title="项目名称">
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">项目编号：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="projectNo" name="projectNo" value="" class="form-control" title="项目编号">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">委托单位（甲方）：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-8">
								<input type="text" id="entrustingCompany" name="entrustingCompany" value="" class="form-control" title="委托单位（甲方）">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">承担单位/负责人（乙方）：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-8">
								<input type="text" id="assumeComPany" name="assumeComPany" value="" class="form-control" title="承担单位/负责人（乙方）">
							</div>
						</div>
						<div class="nav">
							<div style="float: left;width:200px;">
					    		 <ul>
						    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
						    		<li style="float:left;padding-left: 5px;">项目负责人简况信息</li>
						    	</ul>
					    	</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">姓    名：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="name" name="name" value="" class="form-control" title="姓名">
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">年    龄：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="age" name="age" value="" class="form-control" title="年龄">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">性    别：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<select name="sex" id="sex" class="form-control" title="性别" >
									<option value="">--请选择--</option>
									<option value="1">男</option>
									<option value="2">女</option>
								</select>
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">最终学位：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<select name="degree" id="degree" class="form-control" title="最终学位">
									<option value="">--请选择--</option>
									<option value="1">荣誉博士学位</option>
									<option value="2">博士学位</option>
									<option value="3">硕士学位</option>
									<option value="4">学士学位</option>
									<option value="0">无</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">职    称：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<select name="jobTitle" id="jobTitle" class="form-control" title="职称">
									<option value="">--请选择--</option>
									<option value="11">教授</option>
									<option value="12">副教授</option>
									<option value="81">高级工程师</option>
									<option value="83">工程师</option>
									<option value="85">技术员</option>
									<option value="165">技术设计员</option>
								</select>
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">职    务：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<select name="position" id="position" class="form-control" title="职务">
									<option value="">--请选择--</option>
									<option value="001A">书记</option>
									<option value="001B">副书记</option>
									<option value="002A">委员</option>
									<option value="004A">主任</option>
									<option value="004B">副主任</option>
									<option value="083Q">总工程师</option>
									<option value="216A">局长</option>
									<option value="216B">副局长</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">所在学科名称：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-8">
								<input type="text" id="sciencename" name="sciencename" value="" class="form-control" title="所在学科名称">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">主要研究方向：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-8">
								<input type="text" id="researchDirection" name="researchDirection" value="" class="form-control" title="主要研究方向">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-7 control-label" for="form-field-1" style="text-align:left;padding-left:75px;">
								<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
								<p style="float:left">项目负责人近三年来主要研究成果简介（发表文章、承担的项目等）：</p>
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="form-field-1">
								
							</label>
							<div class="col-sm-10">
								<textarea id="achievement" name="achievement" rows="8" class="form-control" title="主要研究成果"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label" for="form-field-1" style="text-align:left;padding-left:75px;">
								<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
								<p style="float:left">参加研究项目的主要人员情况：</p>
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="form-field-1">
								
							</label>
							<div class="col-sm-10">
								<div>
									<div id="zyryqkToolbar"></div>
									<div id="zyryqkGrid"></div> 
								</div>
							</div>
						</div>
						<div class="nav">
							<div style="float: left;width:300px;">
					    		 <ul>
						    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
						    		<li style="float:left;padding-left: 5px;">项目建设目标与建设内容信息</li>
						    	</ul>
					    	</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								项目名称：
							</label>
							<div class="col-sm-8">
								<input type="text" id="projectNameTemp" name="projectNameTemp" value="" class="form-control" disabled="disabled">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">项目起止时间：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-4">
								<input type="text" id="projectBeginTime" name="projectBeginTime" value="" readonly="readonly" class="form-control" onclick="WdatePicker();" title="项目起时间">
							</div>
							<div class="col-sm-4">
								<input type="text" id="projectEndTime" name="projectEndTime" value="" readonly="readonly" class="form-control" onclick="WdatePicker();" title="项目止时间">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
								<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
								<p style="float:left">项目建设目标：</p>
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" >
								
							</label>
							<div class="col-sm-10">
								<textarea id="projectGoal" name="projectGoal" rows="8" class="form-control" title="项目建设目标"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
								<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
								<p style="float:left">项目主要内容：</p>
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" >
								
							</label>
							<div class="col-sm-10">
								<textarea id="projectContent" name="projectContent" rows="8" class="form-control" title="项目主要内容"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
								<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
								<p style="float:left">项目的特色与创新：</p>
							</label>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" >
								
							</label>
							<div class="col-sm-10">
								<textarea id="projectFeatureInnovation" name="projectFeatureInnovation" rows="8" class="form-control" title="项目的特色与创新"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-1 control-label" for="form-field-1">
								
							</label>
							<div class="col-sm-10">
								<div>
									<div id="jtmxToolbar"></div>
									<div id="jtmxGrid"></div> 
								</div>
							</div>
						</div>
						
						<div class="form-group last">
						</div>
				</form>
			</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="${ctx }/assets/plugins/respond.min.js"></script>
		<script src="${ctx }/assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="${ctx }/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="${ctx }/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="${ctx }/assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="${ctx }/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="${ctx }/assets/plugins/moment/min/moment.min.js"></script>
		<script src="${ctx }/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="${ctx }/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="${ctx }/assets/plugins/bootbox/bootbox.min.js"></script>
		<script src="${ctx }/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="${ctx }/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="${ctx }/assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="${ctx }/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="${ctx }/assets/plugins/velocity/jquery.velocity.min.js"></script>
		<script src="${ctx }/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
		<script src="${ctx }/assets/plugins/toastr/toastr.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="${ctx }/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="${ctx }/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="${ctx }/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="${ctx }/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="${ctx }/assets/plugins/truncate/jquery.truncate.js"></script>
		<script src="${ctx }/assets/plugins/summernote/dist/summernote.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="${ctx }/assets/js/main.js"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/core/base.js?v=20150426" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins_bak/ligerDialog.js?sdfsd" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
		<script src="${ctx }/assets/js/jquery.form.js" type="text/javascript"></script>
		<script src="${ctx }/assets/js/common.js?v=2016-05-21" type="text/javascript"></script>
		<script>
			var $zyryqkGrid;
			var $zyryqkToolbar;
			var zyryqkData = [];
			var $jtmxGrid;
			var $jtmxToolbar;
			var jtmxData = [];
			jQuery(document).ready(function() {
	    		
	    		$zyryqkGrid = $("#zyryqkGrid").ligerGrid({
					columns:[
				         {display:"姓名",name:"ryqkxm",width:60, editor: { type: 'text' }},
				         {display:"性别",name:"ryqkxbmc",width:60, editor: { type: 'text' }},
				         {display:"年龄",name:"ryqknl",width:60, editor: { type: 'text' }},
				         {display:"学位",name:"ryqkxwmc",width:80, editor: { type: 'text' }},
				         {display:"职称",name:"ryqkzcmc",width:80, editor: { type: 'text' }},
				         {display:"从事专业",name:"ryqkcszy",width:100, editor: { type: 'text' }},
				         {display:"在项目中的主要任务",name:"ryqkzyrw",width:165, editor: { type: 'text' }}
					],
					rownumbers:true,
					height:120,
					data :{Rows:zyryqkData},                          
					usePager:false,
					isScroll:true
				});
				
				$zyryqkToolbar=$("#zyryqkToolbar").ligerToolBar({ 
					items: [
			             { id:'jgbmxj',text: '新建', click: createZyryqk, icon:'add',icon:'nadd'},
			             { line:true },
			             { id:'jgbmzx',text: '删除', click: deleteZyryqk, icon:'delete',icon:'ndel'}
			         ]
				});
				
				
				$jtmxGrid = $("#jtmxGrid").ligerGrid({
					columns:[
				         {display:"项目名称",name:"jtmxxmmc",width:150},
				         {display:"建设内容",name:"jtmxjsnr",width:170},
				         {display:"资源展现形式",name:"jtmxzyzxxs",width:100},
				         {display:"预算/工作量",name:"jtmxysgzl",width:100},
				         {display:"负责人",name:"jtmxfzr",width:80}
					],
					rownumbers:true,
					height:120,
					data :{Rows:jtmxData},                          
					usePager:false,
					isScroll:true,
					enabledEdit: true, 
					clickToEdit: false
				});
				
				$jtmxToolbar=$("#jtmxToolbar").ligerToolBar({ 
					items: [
			             { id:'jgbmxj',text: '新建', click: createJtmx, icon:'add',icon:'nadd'},
			             { line:true },
			             { id:'jgbmzx',text: '删除', click: deleteJtmx, icon:'delete',icon:'ndel'}
			         ]
				});
				
				$("#projectName").blur(function(){
					$("#projectNameTemp").attr("value",$(this).val());
				});
				
			});
			
			createZyryqk = function(){
    			window.parent.$.ligerDialog.open({ title:'主要人员',url:"${ctx}/projectbook/tozyryinsert?r="+Math.random(),height: 420,width: 800, 
    			buttons: [ 
    				{ text: '确定', onclick: function (item, dialog) { 
    						//提交子窗口事件
    						dialog['jiframe'].get(0).contentWindow.save(dialog, $zyryqkGrid,createZyryqkFun);
    					} ,cls:'l-dialog-btn-inner-save' 
    				}, 
    				{ text: '取消', onclick: function (item, dialog) { dialog.close(); },cls:'l-dialog-btn-inner-no'  } ] 
    			});
    		
    		};
    		
    		createJtmx = function(){
    			if(zyryqkData.length == 0){
    				errorMsg("参加研究项目的主要人员情况至少有一行！");
    				return;
    			}
    		
    			window.parent.$.ligerDialog.open({ title:'具体内容',url:"${ctx}/projectbook/tojtmxinsert?r="+Math.random(),height: 280,width: 800, data:zyryqkData,
    			buttons: [ 
    				{ text: '确定', onclick: function (item, dialog) { 
    						//提交子窗口事件
    						dialog['jiframe'].get(0).contentWindow.save(dialog, $jtmxGrid,createJtmxFun);
    					} ,cls:'l-dialog-btn-inner-save' 
    				}, 
    				{ text: '取消', onclick: function (item, dialog) { dialog.close(); },cls:'l-dialog-btn-inner-no'  } ] 
    			});
    		
    		};
			
			
			var array={
	   			"#projectName":['required:true','maxlength:50'],
	   			"#projectNo":['required:true','maxlength:50'],
	   			"#entrustingCompany":['required:true','maxlength:50'],
	   			"#assumeComPany":['required:true','maxlength:50'],
	   			"#name":['required:true','maxlength:10'],
	   			"#age":['required:true','digits:true','maxlength:4'],
	   			"#sex":['required:true'],
	   			"#degree":['required:true'],
	   			"#jobTitle":['required:true'],
	   			"#position":['required:true'],
	   			"#sciencename":['required:true','maxlength:50'],
	   			"#researchDirection":['required:true','maxlength:50'],
	   			"#achievement":['required:true','maxlength:1000']
			};	
			
			var array1={
	   			"#projectBeginTime":['required:true'],
	   			"#projectEndTime":['required:true'],
	   			"#projectGoal":['required:true','maxlength:2000'],
	   			"#projectContent":['required:true','maxlength:2000'],
	   			"#projectFeatureInnovation":['required:true','maxlength:2000']
			};
			
			save = function(dialog,grid){
			
				if(!checkValidate(array)){
				  return false;
				}
				
				var zyryqkData = $zyryqkGrid.getData();
				if(zyryqkData.length==0){
					errorMsg("参加研究项目的主要人员情况至少有一行！");
					return;
				}
				
				if(!checkValidate(array1)){
				  return false;
				}
				
				parent.$.ligerDialog.waitting('正在提交中...');
				var poststr=$('#form1').formSerialize();
				poststr+="&zyryjson=" + JSON.stringify($zyryqkGrid.getData());
				poststr+="&jtmxjson=" + JSON.stringify($jtmxGrid.getData());
				$.ajax({url:'${ctx}/projectbook/doinsert?r='+Math.random(),type:"post",dataType:"json",cache:false,data:poststr,success:savecallback(grid,dialog),error:ajaxError});
			};
			
			savecallback = function (grid,dialog){
				return function(json){  
			        parent.$.ligerDialog.closeWaitting();
					if(json.code!=100 )
					{  
						errorMsg(json.msg);
					}else {
						parent.$.ligerDialog.success("保存成功！","保存提示",function(){
							dialog.close();
						});
						grid.loadData();
					}
					return;
			    };  
			};
			
			createZyryqkFun = function(row){
				zyryqkData.push(row);
				$zyryqkGrid.loadData();
			};
			
			createJtmxFun = function(row){
				jtmxData.push(row);
				$jtmxGrid.loadData();
			};
			
			deleteZyryqk = function(){
				if($zyryqkGrid==null){
					errorMsg('请选择一行数据！'); return; 
				}
				var row = $zyryqkGrid.getSelectedRow();
			    if (!row) { 
			    	errorMsg('请选择一行数据！'); return; 
			    }
			    
				parent.$.ligerDialog.confirm('是否要删除【'+row.ryqkxm+'】的记录？',function (yes) {if(yes){
					zyryqkData.splice($.inArray(row,zyryqkData),1); 
					$zyryqkGrid.deleteSelectedRow();
					
					$.each(jtmxData,function(i){
						if(jtmxData[i].jtmxfzr==row.ryqkxm){
							jtmxData.splice($.inArray(jtmxData[i],jtmxData),1); 
							$jtmxGrid.loadData();
						}
					});
					
					parent.$.ligerDialog.success("删除成功！");
				}});
			}
			deleteJtmx = function(){
				if($jtmxGrid==null){
					errorMsg('请选择一行数据！'); return; 
				}
				var row = $jtmxGrid.getSelectedRow();
			    if (!row) { 
			    	errorMsg('请选择一行数据！'); return; 
			    }
				parent.$.ligerDialog.confirm('是否要删除【'+row.jtmxxmmc+'】的记录？',function (yes) {if(yes){
					jtmxData.splice($.inArray(row,jtmxData),1); 
					$jtmxGrid.deleteSelectedRow();
					parent.$.ligerDialog.success("删除成功！");
				}});
			}
			
		</script>
	</body>
	<!-- end: BODY -->
</html>