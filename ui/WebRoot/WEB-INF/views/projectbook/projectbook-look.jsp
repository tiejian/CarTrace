<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>项目申报浏览</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<%@include file="/global/global-other.jsp" %>
	<style type="text/css">
		body{margin:0;padding:0;width:794px;}
		#container{width:756px;min-height: 1123px;margin-left:19px;}
		.xmlxs{width:756px;min-height: 1123px;margin-top:19px;}
		.xmlxs1{width:756px;min-height: 1142px;margin-top:38px;}
	</style>
	<script src="${ctx }/assets/js/dm.js?v=2016-05-21" type="text/javascript"></script>
  </head>
  <body>
   		<div id="container">
   			<div class="xmlxs">
   				<div style="margin-top:50px;">
   				<p class=MsoNormal align=left style='text-align:left;mso-pagination:widow-orphan'><span
				style='font-family:宋体;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
				mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
				Calibri;mso-hansi-theme-font:minor-latin'>附件</span><span lang=EN-US>1</span><span
				style='font-family:宋体;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
				mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
				Calibri;mso-hansi-theme-font:minor-latin'>：项目立项书</span></p>
				</div>
				<div style="text-align: right">
				<p class=MsoNormal><span
				style='font-family:宋体;mso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;
				mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:
				Calibri;mso-hansi-theme-font:minor-latin'>项目编号：${bnProjectBook.projectNo }</span></p>
				</div>
				<div>
				<p class=MsoNormal align=center style='margin-top:120pt;margin-right:0cm;
				margin-bottom:15.6pt;margin-left:0cm;mso-para-margin-top:1.0gd;mso-para-margin-right:
				0cm;mso-para-margin-bottom:1.0gd;mso-para-margin-left:0cm;text-align:center;
				line-height:200%'><b style='mso-bidi-font-weight:normal'><span
				style='font-size:16.0pt;line-height:200%;font-family:黑体'>项目立项书<span lang=EN-US><o:p></o:p></span></span></b></p>
   				</div>
   				<div style="padding-left:150px;margin-top:350px;">
   					<p class=MsoNormal style='mso-char-indent-count:4.0;'><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:Calibri;
					mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>项目名称</span></u></b><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt'> </span></u></b><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:Calibri;
					mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>：</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes;'>${bnProjectBook.projectName }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span><o:p></o:p></span></u></p>
   				</div>
   				<div style="padding-left:150px;">
   					<p class=MsoNormal style='mso-char-indent-count:4.0'><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:Calibri;
					mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>项目起至时间</span></u></b><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt'> </span></u></b><b style='mso-bidi-font-weight:normal'><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>：</span></u></b><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt'> </span></u></b><u><span lang=EN-US
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'></span></span></u><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'><fmt:formatDate value="${beginDate }" pattern="yyyy"/>年</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'></span></span></u><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'><fmt:formatDate value="${beginDate }" pattern="MM"/>月</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'>&nbsp;</span></span></u><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>至</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'>&nbsp;</span></span></u><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'><fmt:formatDate value="${endDate }" pattern="yyyy"/>年</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'></span></span></u><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'><fmt:formatDate value="${endDate }" pattern="MM"/>月</span></u><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'>&nbsp; </span><o:p></o:p></span></u></p>
   				</div>
   				<div style="padding-left:150px;">
	   				<p class=MsoNormal style='mso-char-indent-count:
					4.03'><b style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:Calibri;
					mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin'>委托单位（甲方）：</span></u></b><b
					style='mso-bidi-font-weight:normal'><u><span style='font-size:14.0pt;
					mso-bidi-font-size:11.0pt'> </span></u></b><u><span lang=EN-US
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'>${bnProjectBook.entrustingCompany }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><o:p></o:p></span></u></p>
   				</div>
   				<div style="padding-left:150px;">
   					<p class=MsoNormal><b
					style='mso-bidi-font-weight:normal'><u><span
					style='font-size:14.0pt;mso-bidi-font-size:11.0pt;font-family:宋体;mso-ascii-font-family:
					Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					minor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;' >承担单位/负责人（乙方）：</span></u></b><u><span
					lang=EN-US style='font-size:14.0pt;mso-bidi-font-size:11.0pt'><span
					style='mso-spacerun:yes'>${bnProjectBook.assumeComPany }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</span><o:p></o:p></span></u></p>
   				</div>
   			</div>
   			<span style="page-break-after : auto;"></span>
   			<div class="xmlxs1">
   				<p class=MsoNormal><b><span style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:
					宋体;mso-fareast-theme-font:minor-fareast'>一、项目负责人简况 <span lang=EN-US><o:p></o:p></span></span></b></p>
   				<div align=center>
					<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
					 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
					 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
					 0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-insidev:
					 .5pt solid windowtext'>
					 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>姓名<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  ${bnProjectPrincipal.name }
					  </td>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>年龄<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=3 valign=top style='border:solid windowtext 1.0pt;
					  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					 	${bnProjectPrincipal.age }
					  </td>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>职<span
					  lang=EN-US><span style='mso-spacerun:yes'>&nbsp; </span></span>称<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <script type="text/javascript">
					  	document.write(getzc('${bnProjectPrincipal.jobTitle }'));
					  </script>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:1'>
					  <td width=126 colspan=2 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>最终学位<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=2 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <script type="text/javascript">
					  	document.write(getxw('${bnProjectPrincipal.degree }'));
					  </script>
					  </td>
					  <td width=126 colspan=2 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>性别<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=3 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <script type="text/javascript">
					  	document.write(getxb('${bnProjectPrincipal.sex }'));
					  </script>
					  </td>
					  <td width=126 colspan=2 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>职<span
					  lang=EN-US><span style='mso-spacerun:yes'>&nbsp; </span></span>务<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=126 colspan=2 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  <script type="text/javascript">
					  	document.write(getzw('${bnProjectPrincipal.position }'));
					  </script>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:2;page-break-inside:avoid'>
					  <td width=252 colspan=4 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>所在学科名称<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=504 colspan=9 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					 ${bnProjectPrincipal.sciencename }
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:3;page-break-inside:avoid'>
					  <td width=252 colspan=4 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>主要研究方向<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=504 colspan=9 valign=top style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
					  ${bnProjectPrincipal.researchDirection }
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:4;height:24.6pt;mso-row-margin-right:.6pt'>
					  <td width=756 colspan=13 valign="middle" style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;height:24.6pt'>
					  <p class=MsoNormal><b><span style='font-size:14.0pt;font-family:宋体;
					  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>项目负责人近三年来主要研究成果简介（发表文章、承担的项目等）</span></b><b
					  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:14.0pt;
					  font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></b></p>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:5;height: 400px;mso-row-margin-right:.6pt'>
					  <td width=756 colspan=13 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;min-height: 400px'>
					  ${bnProjectPrincipal.achievement }
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:4;height:24.6pt;mso-row-margin-right:.6pt'>
					  <td width=756 colspan=13 valign="middle" style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;height:24.6pt'>
					  <p class=MsoNormal><b><span style='font-size:14.0pt;font-family:宋体;
					  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>参加研究项目的主要人员情况</span></b><b
					  style='mso-bidi-font-weight:normal'><span lang=EN-US style='font-size:14.0pt;
					  font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></b></p>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:7;height:25.35pt;mso-row-margin-right:.6pt;'>
					  <td width=70 valign=middle style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>姓名<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=80 valign=middle style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>性别<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=80 valign=middle style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>年龄<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=70 valign=middle style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>学位<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=70 valign=middle style='border-top:none;border-left:
					  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>职称<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=100 colspan="2" valign=middle style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>从事专业<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					  <td width=300 colspan=7 valign=middle style='border-top:none;
					  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
					  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
					  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt'>
					  <p class=MsoNormal align=center style='text-align:center'><b
					  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;font-family:
					  宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>在项目中的主要任务<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  </td>
					 </tr>
					 <c:if test="${bnProjectPersonnelList!=null}">
					 	<c:forEach items="${bnProjectPersonnelList}" var="li">
						 <tr style='mso-yfti-irow:7;height:25.35pt;mso-row-margin-right:.6pt;'>
						  <td width=70 valign=middle style='border:solid windowtext 1.0pt;
						  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
						  padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						  ${li.name }
						  </td>
						  <td width=80 valign=middle style='border-top:none;
						  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						  	<script type="text/javascript">
					  		document.write(getxb('${li.sex }'));
					  		</script>
						  </td>
						  <td width=80 valign=middle style='border-top:none;
						  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						 	${li.age }
						  </td>
						  <td width=70 valign=middle style='border-top:none;
						  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						 	<script type="text/javascript">
					  		document.write(getxw('${li.degree }'));
					  		</script>
						  </td>
						  <td width=70 valign=middle style='border-top:none;border-left:
						  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						  	<script type="text/javascript">
					  		document.write(getzc('${li.jobTitle }'));
					  		</script>
						  </td>
						  <td width=100 colspan="2" valign=middle style='border-top:none;
						  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						  	${li.specialty }
						  </td>
						  <td width=300 colspan=7 valign=middle style='border-top:none;
						  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
						  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
						  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:25.35pt;text-align: center'>
						  	${li.specialty }
						  </td>
						 </tr>
						 </c:forEach>
					 </c:if>
					 
					 
					</table>
				</div>
				<p class=MsoNormal style='line-height:200%'><b style='mso-bidi-font-weight:
				normal'><span style='font-size:14.0pt;line-height:200%;font-family:宋体;
				mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>二、项目建设目标与建设内容<span
				lang=EN-US><o:p></o:p></span></span></b></p>
				<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
				 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
				 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
				 0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-insidev:
				 .5pt solid windowtext'>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
				  <td width=126 valign=top style='width:102.5pt;border:solid windowtext 1.0pt;
				  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%'><b style='mso-bidi-font-weight:
				  normal'><span style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>项目名称</span></b><span
				  lang=EN-US style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
				  </td>
				  <td width=630 colspan="12" valign=top style='width:338.0pt;border:solid windowtext 1.0pt;
				  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%'><span lang=EN-US
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast'><o:p>${bnProjectBook.projectName }</o:p></span></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
				  <td width=126 valign=top style='width:102.5pt;border:solid windowtext 1.0pt;
				  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%'><b style='mso-bidi-font-weight:
				  normal'><span style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>项目起止时间</span></b><span
				  lang=EN-US style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
				  </td>
				  <td width=630 colspan="12" valign=top style='width:338.0pt;border:solid windowtext 1.0pt;
				  border-left:none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%'><span lang=EN-US
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast'><o:p><fmt:formatDate value="${beginDate }" pattern="yyyy"/>年<fmt:formatDate value="${beginDate }" pattern="MM"/>月&nbsp;至&nbsp;<fmt:formatDate value="${endDate }" pattern="yyyy"/>年<fmt:formatDate value="${endDate }" pattern="MM"/>月</o:p></span></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:2'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%;tab-stops:36.0pt'><b
				  style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;line-height:
				  200%;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;
				  color:black'>项目建设目标：<span lang=EN-US><o:p></o:p></span></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:3;height:248.3pt'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt;height:248.3pt'>
				  <p class=MsoNormal style='line-height:200%'><span lang=EN-US
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast'><o:p>${bnProjectConcreteContent.projectGoal }</o:p></span></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:4'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal style='line-height:200%'><b style='mso-bidi-font-weight:
				  normal'><span style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;color:black'>项目主要内容：<span
				  lang=EN-US><o:p></o:p></span></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:5;height:265.8pt'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt;height:265.8pt'>
				  <p class=MsoNormal style='line-height:200%'><span lang=EN-US
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast'><o:p>${bnProjectConcreteContent.projectContent }</o:p></span></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:6;height:30.4pt'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt;height:30.4pt'>
				  <p class=MsoNormal style='line-height:200%'><b style='mso-bidi-font-weight:
				  normal'><span style='font-size:14.0pt;line-height:200%;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;color:black'>项目的特色</span></b><b><span
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-family:宋体;color:black;
				  mso-font-kerning:0pt'>与创新</span></b><b style='mso-bidi-font-weight:normal'><span
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast;color:black'>：<span lang=EN-US><o:p></o:p></span></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:7;mso-yfti-lastrow:yes;height:641.1pt'>
				  <td width=587 colspan=2 valign=top style='width:440.5pt;border:solid windowtext 1.0pt;
				  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt;height:641.1pt'>
				  <p class=MsoNormal style='line-height:200%'><span lang=EN-US
				  style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
				  宋体;mso-fareast-theme-font:minor-fareast'><o:p>${bnProjectConcreteContent.projectFeatureInnovation }</o:p></span></p>
				  </td>
				 </tr>
				</table>
				<span lang=EN-US style='font-size:12.0pt;mso-bidi-font-size:10.0pt;line-height:
				150%;font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-family:
				"Times New Roman";mso-bidi-theme-font:minor-bidi;color:black;mso-font-kerning:
				1.0pt;mso-ansi-language:EN-US;mso-fareast-language:ZH-CN;mso-bidi-language:
				AR-SA'><br clear=all style='page-break-before:always;mso-break-type:section-break'>
				</span>
				<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
				 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
				 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
				 0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-insidev:
				 .5pt solid windowtext'>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;font-weight: bold;'>
				  <td width=80 style='border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>序号<span lang=EN-US><o:p></o:p></span></span></p>
				  </td>
				  <td width=180 style='border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>项目名称<span lang=EN-US><o:p></o:p></span></span></p>
				  </td>
				  <td width=140 style='border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>建设内容<span lang=EN-US><o:p></o:p></span></span></p>
				  </td>
				  <td width=140 style='border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>资源展现形式<span lang=EN-US><o:p></o:p></span></span></p>
				  </td>
				  <td width=160 style='border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>预算/工作量</span></span></p>
				  </td>
				  <td width=76 style='border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:18.0pt;
				  mso-line-height-rule:exactly'><span style='font-size:14.0pt;font-family:宋体;
				  mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast;mso-bidi-font-weight:
				  bold'>负责人<span lang=EN-US><o:p></o:p></span></span></p>
				  </td>
				 </tr>
				 <c:if test="${bnProjectConcreteContentList!=null }">
				 	<c:forEach var="li" items="${bnProjectConcreteContentList}" varStatus="i">
				 	<tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
					  <td width=80 style='border:solid windowtext 1.0pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					  ${i.index+1 }
					  </td>
					  <td width=180 style='border:solid windowtext 1.0pt;border-left:
					  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					  ${li.projectName }
					  </td>
					  <td width=140 style='border:solid windowtext 1.0pt;border-left:
					  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					  ${li.content }
					  </td>
					  <td width=130 style='border:solid windowtext 1.0pt;border-left:
					  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					  ${li.showform }
					  </td>
					  <td width=160 style='border:solid windowtext 1.0pt;border-left:
					  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					  ${li.budgetWorkload }
					  </td>
					  <td width=76 style='border:solid windowtext 1.0pt;border-left:
					  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;text-align: center'>
					   ${li.principal }
					  </td>
					 </tr>
				 	</c:forEach>
				 </c:if>
				 </table>
				 
				 <p class=MsoNormal style='margin-top:7.8pt;margin-right:0cm;margin-bottom:7.8pt;
					margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;mso-para-margin-bottom:
					.5gd;mso-para-margin-left:0cm;line-height:200%;tab-stops:36.0pt'><b
					style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;line-height:
					200%;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>三、项目申报与评审</span></b><span
					lang=EN-US style='font-size:14.0pt;line-height:200%;font-family:宋体;mso-fareast-font-family:
					宋体;mso-fareast-theme-font:minor-fareast'><o:p></o:p></span></p>
					
					<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=756
					 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
					 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
					 0cm 5.4pt 0cm 5.4pt;mso-border-insideh:.5pt solid windowtext;mso-border-insidev:
					 .5pt solid windowtext'>
					 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:23.85pt'>
					  <td width=756 style='border:solid windowtext 1.0pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:23.85pt'>
					  <p class=MsoNormal align=left style='margin-top:2.0pt;margin-right:-5.4pt;
					  margin-bottom:2.0pt;margin-left:0cm;text-align:left'><b style='mso-bidi-font-weight:
					  normal'><span style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:
					  宋体;mso-fareast-theme-font:minor-fareast'>申报项目所在院部意见</span></b><span
					  lang=EN-US style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:
					  宋体;mso-fareast-theme-font:minor-fareast;letter-spacing:.5pt'><o:p></o:p></span></p>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:1;height:174.2pt'>
					  <td width=756 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;height:174.2pt'>
					  <p class=MsoNormal style='padding-top:130px;padding-left:450px;margin-top:2.0pt;margin-right:0cm;
					  margin-bottom:2.0pt;margin-left:0cm;mso-line-height-alt:
					  0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;
					  font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>负责人（签章）<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  <p class=MsoNormal align=right style='margin-top:30px;margin-right:0cm;
					  margin-bottom:2.0pt;margin-left:0cm;text-align:right;mso-line-height-alt:
					  0pt'><b style='mso-bidi-font-weight:normal'><span lang=EN-US
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'><span
					  style='mso-spacerun:yes'>
					  </span></span></b><b style='mso-bidi-font-weight:normal'><span
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'>年<span lang=EN-US><span
					  style='mso-spacerun:yes'>&nbsp;&nbsp;</span></span>月<span lang=EN-US><span
					  style='mso-spacerun:yes'>&nbsp;&nbsp;</span></span>日</span></b><span
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:23.85pt'>
					  <td width=756 style='border:solid windowtext 1.0pt;mso-border-alt:
					  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:23.85pt'>
					  <p class=MsoNormal align=left style='margin-top:2.0pt;margin-right:-5.4pt;
					  margin-bottom:2.0pt;margin-left:0cm;text-align:left'><b style='mso-bidi-font-weight:
					  normal'><span style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:
					  宋体;mso-fareast-theme-font:minor-fareast'>专家组立项评审意见</span></b><span
					  lang=EN-US style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:
					  宋体;mso-fareast-theme-font:minor-fareast;letter-spacing:.5pt'><o:p></o:p></span></p>
					  </td>
					 </tr>
					 <tr style='mso-yfti-irow:1;height:174.2pt'>
					  <td width=756 valign=top style='border:solid windowtext 1.0pt;
					  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
					  padding:0cm 5.4pt 0cm 5.4pt;height:174.2pt'>
					  <p class=MsoNormal style='padding-top:130px;padding-left:450px;margin-top:2.0pt;margin-right:0cm;
					  margin-bottom:2.0pt;margin-left:0cm;mso-line-height-alt:
					  0pt'><b style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;
					  font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>专家组成员签字<span
					  lang=EN-US><o:p></o:p></span></span></b></p>
					  <p class=MsoNormal align=right style='margin-top:30px;margin-right:0cm;
					  margin-bottom:2.0pt;margin-left:0cm;text-align:right;mso-line-height-alt:
					  0pt'><b style='mso-bidi-font-weight:normal'><span lang=EN-US
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'><span
					  style='mso-spacerun:yes'>
					  </span></span></b><b style='mso-bidi-font-weight:normal'><span
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'>年<span lang=EN-US><span
					  style='mso-spacerun:yes'>&nbsp;&nbsp;</span></span>月<span lang=EN-US><span
					  style='mso-spacerun:yes'>&nbsp;&nbsp;</span></span>日</span></b><span
					  style='font-size:14.0pt;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:
					  minor-fareast'> <span lang=EN-US><o:p></o:p></span></span></p>
					  </td>
					 </tr>
				</table>
				<p class=MsoNormal style='margin-top:7.8pt;margin-right:0cm;margin-bottom:7.8pt;
				margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;mso-para-margin-bottom:
				.5gd;mso-para-margin-left:0cm;line-height:200%;tab-stops:36.0pt'><b
				style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;line-height:
				200%;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>四、项目评审及费用拨付办法<span
				lang=EN-US><o:p></o:p></span></span></b></p>
				<p class=MsoNormal style='text-indent:31.5pt;mso-char-indent-count:2.25;
				line-height:150%'><span style='font-size:14.0pt;line-height:150%;font-family:
				宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>项目任务书签订生效后，项目负责人牵头组织制订实施方案以及制定相应保障措施，组织项目实施，确保任务保质保量按时完成，项目运行后，按<span
				style='color:black'>照</span>提交成果可以进行阶段性验收<span lang=EN-US>,</span><span
				style='color:black'>验收合格后可分阶段兑现项目经费。对</span>提交成果<span style='color:black'>验收不合格的项目，学校暂缓兑现项目经费。
				</span><span style='mso-spacerun:yes'>&nbsp;</span><span lang=EN-US><o:p></o:p></span></span></p>
   				<div style="height:70px;"></div>
   				<p class=MsoNormal align=left style='text-align:left;line-height:150%'><b
				style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;line-height:
				150%;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>甲方：<span
				lang=EN-US><span
				style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span>乙方： <span lang=EN-US><o:p></o:p></span></span></b></p>
   				<p class=MsoNormal align=left style='text-align:left;line-height:150%'><b
				style='mso-bidi-font-weight:normal'><span style='font-size:14.0pt;line-height:
				150%;font-family:宋体;mso-fareast-font-family:宋体;mso-fareast-theme-font:minor-fareast'>签字：<span
				lang=EN-US><span
				style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span>签字： <span lang=EN-US><o:p></o:p></span></span></b></p>
				<p class=MsoNormal style='text-indent:150pt;mso-char-indent-count:5.0;
				line-height:150%'><b style='mso-bidi-font-weight:normal'><span
				style='font-size:14.0pt;line-height:150%;font-family:宋体;mso-fareast-font-family:
				宋体;mso-fareast-theme-font:minor-fareast'>年<span lang=EN-US><span
				style='mso-spacerun:yes'>&nbsp;</span></span>月<span
				lang=EN-US><span style='mso-spacerun:yes'>&nbsp; </span></span>日<span
				lang=EN-US><span
				style='mso-spacerun:yes'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</span></span>年<span lang=EN-US><span
				style='mso-spacerun:yes'>&nbsp;</span></span>月<span
				lang=EN-US><span style='mso-spacerun:yes'>&nbsp;</span></span>日<span
				lang=EN-US><o:p></o:p></span></span></b></p>
   				<div style="height:70px;"></div>
   				<h2><span style='font-family:宋体;mso-ascii-font-family:"Calibri Light";
				mso-ascii-theme-font:major-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
				major-fareast;mso-hansi-font-family:"Calibri Light";mso-hansi-theme-font:major-latin'>附件</span><span
				lang=EN-US>2</span><span style='font-family:宋体;mso-ascii-font-family:"Calibri Light";
				mso-ascii-theme-font:major-latin;mso-fareast-font-family:宋体;mso-fareast-theme-font:
				major-fareast;mso-hansi-font-family:"Calibri Light";mso-hansi-theme-font:major-latin'>：项目立项书清单</span></h2>
   				<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 width=756
				 style='border-collapse:collapse;mso-table-layout-alt:fixed;border:none;
				 mso-border-alt:solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:
				 0cm 5.4pt 0cm 5.4pt'>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>项目编号</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>项目名称</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>项目起至时间</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>项目负责人</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>所在学科</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>主要研究方向</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'>项目进度</span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:35px;'>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;mso-border-alt:
				  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=71 style='width:53.45pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=103 style='width:77.15pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=104 style='width:77.95pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=94 style='width:70.85pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=113 style='width:85.1pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				  <td width=85 style='width:63.75pt;border:solid windowtext 1.0pt;border-left:
				  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
				  padding:0cm 5.4pt 0cm 5.4pt'>
				  <p class=MsoNormal align=center style='text-align:center;line-height:150%'><b
				  style='mso-bidi-font-weight:normal'><span style='font-family:宋体;mso-ascii-font-family:
				  Calibri;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:宋体;
				  mso-fareast-theme-font:minor-fareast;mso-hansi-font-family:Calibri;
				  mso-hansi-theme-font:minor-latin'></span><span lang=EN-US><o:p></o:p></span></b></p>
				  </td>
				 </tr>
				 </table>
				 <div style="height:70px;"></div>
   			</div>
   		</div>
  </body>
</html>
