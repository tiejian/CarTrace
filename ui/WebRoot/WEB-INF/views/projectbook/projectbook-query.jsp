<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html>
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<meta charset="utf-8" />
		<%@include file="/global/global-other.jsp" %>
	</head>
	<body>
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content" style="margin:0px;padding:0px;">
				<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				<div class="container">
					<!-- start: TOOLBAR -->
					<div class="toolbar row">
						<div class="col-sm-12 hidden-xs">
							<div class="page-header">
								<i class="fa fa-home fa-lg" style="color:red;"></i> 当前位置：综合管理   <i class="fa fa-angle-right fa-lg"></i> 项目管理  <i class="fa fa-angle-right fa-lg"></i> 项目申报  
							</div>
						</div>
					</div>
					<div id="slidingbar-area">
						<div id="slidingbar">
							<div class="row">
								<div class="col-sm-12">
									<form id="form2" role="form" class="form-horizontal">
										<div class="form-group">
											<label class="col-xs-4 control-label" for="form-field-1" style="font-size:14px;text-align: right;width:150px;">
												项目起止时间：
											</label>
											<div class="col-sm-1">
												<input type="text" placeholder="项目起时间" id="form-field-9" class="form-control" onclick="WdatePicker();">
											</div>
											<label class="col-xs-1 control-label" for="form-field-1" style="font-size:14px;text-align: right;width:20px;">
												至
											</label>
											<div class="col-sm-1">
												<input type="text" placeholder="项目止时间" id="form-field-9" class="form-control" onclick="WdatePicker();">
											</div>
											<div class="col-sm-3">
												<button type="button" id="searchAdvanced" class="btn btn-dark-orange btn-squared" style="width:80px;">
													<i class="fa fa-search"></i> 查    询
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="row">
								<br/>
								<!-- start: SLIDING BAR TOGGLE BUTTON -->
								<div class="col-md-11 text-center">
									<a href="#" class="sb_toggle"><i class="fa fa-chevron-up" style="color:blue;"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="condition_simple row">
						<div class="col-sm-12 condition_simple_pl15">
							<form id="form1" role="form" class="form-horizontal">
								<div class="col-lg-2" style="margin: 0px;text-align:right;width:150px;">
									<select id="searchCondition" class="selectpicker" data-width="110px">
									  <option value="1">项目名称</option>
									  <option value="2">项目编号</option>
									  <option value="3">委托单位（甲方）</option>
									  <option value="4">承担单位/负责人（乙方）</option>
									</select>
								</div>
								<div class="col-sm-4 condition_simple_pt15">
									<input type="text" id="searchContent" name="searchContent" placeholder="查找内容..." id="form-field-9" class="form-control">
								</div>
								<div class="col-sm-3">
									<button id="search" type="button" class="btn btn-red btn-squared" style="width:80px;">
										<i class="fa fa-search"></i> 查    询
									</button>
									&nbsp;
									<button type="button" class="btn btn-red btn-squared sb_toggle" style="width:100px;display: none">
										高级查询 <i class="fa fa-angle-double-up fa-lg"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div style="padding-left:20px;padding-right:1px;">
					<div id="toptoolbar" style="border-left:0px"></div>
				</div>
				<div style="padding-left:20px;padding-right:10px;">
					<div id="maingrid" style="border-left:0px;"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<script>
			var $xygl_projectbook = jQuery.namespace('xygl.projectbook');
	    	$xygl_projectbook.$grid = null;
	    	$xygl_projectbook.$bar = null;
	    	$(function (){
	    		Main.subview();
	    	
	    		$xygl_projectbook.$bar = $("#toptoolbar").ligerToolBar({
	    			items: [
		               	{ text: '新建',click:function(){$xygl_projectbook.save();},icon:'nadd'},
		                { line:true }, 
		                { text: '浏览',click:function(){$xygl_projectbook.get();}, icon:'nsearch'}
		            ]
	            });
	    		$(".l-toolbar").css({"border-bottom":"0px"});
	    		
	    		$xygl_projectbook.ready=function(){
	    			$xygl_projectbook.$grid = $("#maingrid").ligerGrid({
		                columns: [
		                ,{ display: '项目名称', name: 'projectName', width: 150 }
		                ,{ display: '项目编号', name: 'projectNo', width: 100 }
		                ,{ display: '项目起时间', name: 'projectBeginTime', width: 100}
		                ,{ display: '项目止时间', name: 'projectEndTime', width: 100}
		                ,{ display: '委托单位（甲方）', name: 'entrustingCompany', width: 150 }
		                ,{ display: '承担单位/负责人（乙方）', name: 'assumeComPany', width: 150 }
		                ,{ display: '评审状态', name: 'auditStatusName', width: 80 }
		                ,{ display: '创建时间', name: 'createAt', width: 150}
		                ,{ display: '创建人', name: 'createName', width: 80}
		                ], 
		                delayLoad: true,
		                url:"${ctx}/projectbook/query",
		                width: '99.8%',  
		                height: $(document).height()-220,
						pageSize: 20,
						rownumbers:true,
						root :'rows',                          
						record:'total',                        
						pageParmName :'page',               
						pagesizeParmName:'pagesize',
						sortName:'CREATE_AT',
						sortOrder:'DESC',
						onSuccess:function(data){
							//成功返回回调函数 
						},
						onError:function(){
							//错误返回回调函数 
							ajaxError();
						},
						onDblClickRow: function(row){
							//行双击事件
							//get();
						}
		            });
	    		};
	    		
	    		//初始化查询table
	    		$xygl_projectbook.ready();
	    		
	    		//添加页打开
	    		$xygl_projectbook.save = function(){
	    			//$.ligerDialog.open({id:'add',title:'demo新建窗口',url:"insert1.html?_m=toinsert&r="+Math.random(),height: 600,width: 1000, showMax: true, showToggle: false, showMin: false, isResize: true});
	    			$.ligerDialog.open({ title:'项目申报',url:"${ctx}/projectbook/toinsert?_m=toinsert&r="+Math.random(),height: 500,width: 800, 
	    			buttons: [ 
	    				{ text: '保存', onclick: function (item, dialog) { 
	    						//提交子窗口事件
	    						dialog['jiframe'].get(0).contentWindow.save(dialog, $xygl_projectbook.$grid);
	    					},cls:'l-dialog-btn-inner-save'
	    				}, 
	    				{ text: '取消', onclick: function (item, dialog) { dialog.close(); } ,cls:'l-dialog-btn-inner-no' } ] 
	    			});
	    		};
	    		
	    		//编辑页打开
	    		$xygl_projectbook.get = function(){
	    			if($xygl_projectbook.$grid==null){
		        		$.ligerErrorMsg('请选择一行数据！'); return; 
		        	}
		        	var row = $xygl_projectbook.$grid.getSelectedRow();
		            if (!row) { 
		            	$.ligerErrorMsg('请选择一行数据！'); return; 
		            }
		            //window.top.location = "${ctx}/projectbook/tolook?id="+row.id+"&r="+Math.random();
	    		
	    			var w = ($(window).width()/2)-820/2;
	    			window.open ("${ctx}/projectbook/tolook?id="+row.id+"&r="+Math.random(), 'a4', 'height=680, width=820, top=0, left='+w+', toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, status=no'); //
	    		
	    		};
	    		
	    		//删除功能
	    		$xygl_projectbook.del = function(){
	    			if($xygl_projectbook.$grid==null){
		        		$.ligerErrorMsg('请选择一行数据！'); return; 
		        	}
		        	var row = $xygl_projectbook.$grid.getSelectedRow();
		            if (!row) { 
		            	$.ligerErrorMsg('请选择一行数据！'); return; 
		            }
		            $.ligerDialog.confirm('是否要删除【'+row.name+'】的信息？','注销提示',function (yes) {if(yes){
		            	$.ajax({url:'${ctx}/demo?_m=del&id='+row.id+'&r='+Math.random(),type:"get",dataType:"json",cache:false,error:ajaxError,success:function(json){
		            		if(json.errcode!="0" )
		    				{  
		    					$.ligerErrorMsg(json.errmsg);
		    				}else {
		    					$.ligerDialog.success("删除成功！");
		    					$xygl_projectbook.query();
		    				}
		    				return;
		            	}});
		            }});
	    		};
	    		
	    		//查询功能
	    		$xygl_projectbook.query = function(){
	    			$xygl_projectbook.$grid.set({ delayLoad: false }); //设置数据参数
		        	$xygl_projectbook.$grid.set({ newPage: 1 });
		        	var parms = null;
		        	
		        	if($("#searchCondition").val()=="1"){
		        		parms = {
		       				"filter_LIKES_A_PROJECT_NAME":$("#searchContent").val()
		        		};
		        	}else if($("#searchCondition").val()=="2"){
		        		parms = {
		       				"filter_LIKES_A_PROJECT_NO":$("#searchContent").val()
		        		};
		        	}else if($("#searchCondition").val()=="3"){
		        		parms = {
		       				"filter_LIKES_A_ENTRUSTING_COMPANY":$("#searchContent").val()
		        		};
		        	}else if($("#searchCondition").val()=="4"){
		        		parms = {
		       				"filter_LIKES_A_ASSUME_COMPANY":$("#searchContent").val()
		        		};
		        	}
		        	
		        	$xygl_projectbook.$grid.set({ parms: parms });
		            $xygl_projectbook.$grid.loadData();//加载数据
	    		};
	    		
	    		$("#search").click(function(){
	    			$xygl_projectbook.query();
	    		});
	    	});
	    </script>
	</body>
	<!-- end: BODY -->
</html>