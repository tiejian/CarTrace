<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<%@include file="/global/global-other.jsp" %>
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<!--  <link href='http://fonts.useso.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>-->
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="${ctx }/assets/plugins/weather-icons/css/weather-icons.min.css">
		<link rel="stylesheet" href="${ctx }/assets/plugins/nvd3/nv.d3.min.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="${ctx }/assets/css/styles.css">
		<link rel="stylesheet" href="${ctx }/assets/css/styles-responsive.css">
		<link rel="stylesheet" href="${ctx }/assets/css/plugins.css">
		<link rel="stylesheet" href="${ctx }/assets/css/themes/theme-default.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="${ctx }/assets/css/print.css" type="text/css" media="print"/>
		<!-- ligerUi CSS -->
		<link  href="${ctx }/assets/plugins/ligerUI/skins/style1/css/ligerui-all.css?v=20150426" rel="stylesheet" type="text/css" />
		<link  href="${ctx }/assets/plugins/ligerUI/skins/ligerui-icons.css?v=20150426" rel="stylesheet" type="text/css" />
		<link  href="${ctx }/assets/css/css.css?v=20150426" rel="stylesheet" type="text/css" />
		<!-- end: CORE CSS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body >
		<div class="row">
			<form role="form" name="form1" id="form1" class="form-horizontal">
						<div class="nav">
							<div style="float: left;width:200px;">
					    		 <ul>
						    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
						    		<li style="float:left;padding-left: 5px;">基本信息</li>
						    	</ul>
					    	</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">项目名称：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="xmmc" name="xmmc" value="" class="form-control" title="项目名称">
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">负责人：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<select name="fzr" id="fzr" class="form-control" title="负责人" >
									<option value="">--请选择--</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="form-field-1">
								<p style="float:right">建设内容：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-8">
								<input type="text" id="jsnr" name="jsnr" value="" class="form-control" title="建设内容">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">
								<p style="float:right">资源展现形式：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="zyzxxs" name="zyzxxs" value="" class="form-control" title="资源展现形式">
							</div>
							<label class="col-sm-2 control-label">
								<p style="float:right">预算/工作量：</p>
								<p style="color:red;padding-right:5px;font-size:16px;float:right">*</p>
							</label>
							<div class="col-sm-3">
								<input type="text" id="ysgzl" name="ysgzl" value="" class="form-control" title="预算/工作量">
							</div>
						</div>
						
						<div class="form-group last">
					</div>
			</form>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="${ctx }/assets/plugins/respond.min.js"></script>
		<script src="${ctx }/assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="${ctx }/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="${ctx }/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="${ctx }/assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="${ctx }/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="${ctx }/assets/plugins/moment/min/moment.min.js"></script>
		<script src="${ctx }/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="${ctx }/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="${ctx }/assets/plugins/bootbox/bootbox.min.js"></script>
		<script src="${ctx }/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="${ctx }/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="${ctx }/assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="${ctx }/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="${ctx }/assets/plugins/velocity/jquery.velocity.min.js"></script>
		<script src="${ctx }/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="${ctx }/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
		<script src="${ctx }/assets/plugins/toastr/toastr.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="${ctx }/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="${ctx }/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="${ctx }/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="${ctx }/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="${ctx }/assets/plugins/truncate/jquery.truncate.js"></script>
		<script src="${ctx }/assets/plugins/summernote/dist/summernote.min.js"></script>
		<script src="${ctx }/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		<script src="${ctx }/assets/js/main.js"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/core/base.js?v=20150426" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins_bak/ligerDialog.js?sdfsd" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
		<script src="${ctx }/assets/plugins/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
		<script src="${ctx }/assets/js/jquery.form.js" type="text/javascript"></script>
		<script src="${ctx }/assets/js/common.js?v=2016-05-21" type="text/javascript"></script>
		<script>
			var dialog = frameElement.dialog; //调用页面的dialog对象(ligerui对象)
			var zyryData = null;
			$(function(){
				zyryData = dialog.get('data');//获取data参数
				$.each(zyryData,function(i){
					$("#fzr").append('<option value="'+zyryData[i].ryqkxm+'">'+zyryData[i].ryqkxm+'</option>');
				});
			});
			
			var array={
	   			"#xmmc":['required:true','maxlength:50'],
	   			"#fzr":['required:true'],
	   			"#jsnr":['required:true','maxlength:50'],
	   			"#zyzxxs":['required:true','maxlength:50'],
	   			"#ysgzl":['required:true','maxlength:50']
			};
			
			save = function(dialog,grid,createJtmxFun){
				if(!checkValidate(array)){
				  return false;
				}
				
				var jtmx = {
					jtmxxmmc : $("#xmmc").val(),
					jtmxfzr : $("#fzr").val(),
					jtmxjsnr : $("#jsnr").val(),
					jtmxzyzxxs : $("#zyzxxs").val(),
					jtmxysgzl : $("#ysgzl").val()
				};
				
				createJtmxFun(jtmx);
				
				parent.$.ligerDialog.success("保存成功！","保存提示",function(){
					dialog.close();
				});
			};
			
		</script>
	</body>
	<!-- end: BODY -->
</html>