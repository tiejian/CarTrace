<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>艾特智能车载wifi系统统一管理平台</title>
<%@include file="/global/global-other.jsp" %>
<script type="text/javascript">
submitForm = function($dialog,$grid){
	var $demo_form = $('#demo_int_form');
    if ($demo_form.form('validate')) {
        $.post('${ctx}/demo/doinsert', $demo_form.formSerialize(), function(json) {
        	if(statusMsg(json)){
        		$grid.datagrid('reload');
        		parent.$.messager.alert("提示","保存成功！","info");
            	$dialog.close();
        	}
        }, "JSON");
    }
}
</script>
</head>
<body>
<div style="width:100%">
		<div class="inputContent">
		<form id="demo_int_form" name="demo_int_form" action="#" method="post">
			<div>
			<div class="navinfo">
				<ul>
					<li class="first"></li>
					<li>基本信息</li>
				</ul>
			</div>
           	<table cellpadding="0" cellspacing="0" border="0" class="table_form">
	            <tr>
	                <td width="80" align="right" class="l-table-edit-td-l"><span class="notnull">*</span>姓名：</td>
	                <td align="left" class="l-table-edit-td-r">
						<input name="name" id="name" value="" class="easyui-validatebox validatebox-text" required="required" data-options="validType:['minLength[6]']" type="text">
					</td>
	           		<td width="80" align="right" class="l-table-edit-td-l">部门：</td>
	                <td align="left" class="l-table-edit-td-r">
	                	<select name="bm" id="bm" class="easyui-combobox" required="required" panelHeight="auto" validType="selectValueRequired['#bm']" style="height:26px;width:153px;">
							<option value="">--请选择--</option>
							<option value="1">主席</option>
							<option value="2">研发中心</option>
							<option value="3">销售部</option>
							<option value="4">市场部</option>
							<option value="5">顾问组</option>
						</select>
					</td>
	            </tr>
	            <tr>
	                <td width="80" align="right" class="l-table-edit-td-l"><span class="notnull">*</span>性别：</td>
	                <td align="left" class="l-table-edit-td-r">
	                	<at:dm_select name="xb" id="xb" cssClass="easyui-combobox" code="getTDmXb" first="--请选择--" style="width: 153px;height:26px;"
							zdyContent=" panelHeight=\"auto\" required=\"required\" validType=\"selectValueRequired['#xb']\" data-options=\"valueField: 'dm',textField: 'mc'\" ">
						</at:dm_select>
	                </td>
	           		<td width="80" align="right" class="l-table-edit-td-l">部门：</td>
	                <td align="left" class="l-table-edit-td-r">
	                	
	                </td>
	            </tr>
	            <tr>
	                <td width="80" align="right" class="l-table-edit-td-l"><span class="notnull">*</span>爱好：</td>
	                <td align="left" class="l-table-edit-td-r">
	                	 <input id="CheckBoxList1_0" type="checkbox" name="CheckBoxList1$0" checked="checked" /><label for="CheckBoxList1_0">篮球</label>
						 <input id="CheckBoxList1_1" type="checkbox" name="CheckBoxList1$1" /><label for="CheckBoxList1_1">网球</label>
						 <input id="CheckBox1" type="checkbox" name="CheckBoxList1$1" /><label for="CheckBoxList1_1">足球</label>      
	                </td>
	           		<td width="80" align="right" class="l-table-edit-td-l">出生日期：</td>
	                <td align="left" class="l-table-edit-td-r">
	                	<input type="text" name="csrq" id="csrq" value="<fmt:formatDate value="${demo.csrq }" pattern="yyyy-MM-dd"/>" class="Wdate" onClick="WdatePicker();"  style="width:150px;"/>
	                </td>
	            </tr>
	            <tr>
	                <td width="80" align="right" class="l-table-edit-td-l"><span class="notnull">*</span>描述：</td>
	                <td align="left" class="l-table-edit-td-r" colspan="3">
						
	                </td>
	            </tr>
            </table>
			</div>
      		</form>
      	</div>
	</div>
</body>
</html>
