<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>艾特智能车载wifi系统统一管理平台</title>
<%@include file="/global/global-other.jsp" %>
<script type="text/javascript">
var viewDialog;
$(function() {
	var $grid = $('#demo_datagrid');
	// 定义命名空间  
    var $aiter_demo = jQuery.namespace( 'aiterw.demo');
	//初始化
    $aiter_demo.ready = function() {  
    	$grid.datagrid({	
			url : '${ctx}/demo/query',
			fit : true,
			fitColumns : true,
			border : false,
			pageSize : 20,
			pagination : true,
			rownumbers : true,
			idField : 'id',
			sortName : 'add_time',
			sortOrder : 'desc',
			queryParams: serializeObject($("#demo_query_form").serializeArray()),
			singleSelect:true,
			nowrap : false,
			columns : [ [
				{field : 'id', title : '主键',width : 250,align : 'left',sortable : false},
				{field : 'name', title : '姓名',width : 80,align : 'left',sortable : false},
				{field : 'xb', title : '性别',width : 80,align : 'left',sortable : false},
				{field : 'csrq', title : '出生日期',width : 100,align : 'left',sortable : false},
				{field : 'tjrName', title : '添加人',width : 80,align : 'left',sortable : false},
				{field : 'addTime', title : '添加时间',width : 150,align : 'left',sortable : false},
				{field : '', title : '',align : 'left',sortable : false}
			] ],
			toolbar : [ {
				text : '查询',
				iconCls : 'icon-search',
				handler : function() {
					$aiter_demo.query();
				}
			}, '-',{
				text : '添加',
				iconCls : 'icon-add',
				handler : function() {
					$aiter_demo.addView();
				}
			}, '-'
				,{
					text : '编辑',
					iconCls : 'icon-edit',
					handler : function() {
						$aiter_demo.editView();
					}
				}, '-',{
					text : '删除',
					iconCls : 'icon-remove',	
					handler : function() {
						$aiter_demo.remove();
					}
				}
			]
		});
	};
	//初始化
	$aiter_demo.ready();
	
	//添加初始化
	$aiter_demo.addView = function(){
		viewDialog=parent.$.dialog({
	        title: '新增用户',
	        href: '${ctx}/demo/toinsert?r='+Math.random(),
	        width: 550,
	        height: 350,
	        //bodyStyle: {overflow: 'auto'},
	        buttons: [{
                text: '保 存',
                width:'70px',
                iconCls: 'icon-ok',
                handler: function(){
                	viewDialog.find('iframe').get(0).contentWindow.submitForm(viewDialog, $grid);
                }
	       },{
               text: '关 闭',
               width:'70px',
               iconCls: 'icon-close',
               handler: function(){
               		viewDialog.close();
               }
	       }]
	    });
	};
	//编辑初始化
	$aiter_demo.editView = function(){
		var row = $grid.datagrid("getSelected"); 
		if(row == null){
			parent.$.messager.alert("提示","请选择一行数据！","error");
			return;
		}
		viewDialog=parent.$.dialog({
	        title: '编辑用户',
	        href: '${ctx}/demo/toedit?id=' + row.id + '&r='+Math.random(),
	        width: 550,
	        height: 350,
	        buttons: [{
                text: '保 存',
                width:'70px',
                iconCls: 'icon-ok',
                handler: function(){
                	viewDialog.find('iframe').get(0).contentWindow.submitForm(viewDialog, $grid);
                }
	       },{
               text: '关 闭',
               width:'70px',
               iconCls: 'icon-close',
               handler: function(){
               		viewDialog.close();
               }
	       }]
	    });
	};
	
	//删除
	$aiter_demo.remove = function(){
		var row = $grid.datagrid("getSelected"); 
		if(row == null){
			parent.$.messager.alert("提示","请选择一行数据！","error");
			return;
		}
		 if (row) {
             parent.$.messager.confirm('提示', '确定要删除吗？', function (flag) {
                 if (flag) {
                     $.post('${ctx}/demo/dodel', { id: row.id }, function (result) {
                    	 if (statusMsg(result)) {
	                    	 $grid.datagrid('reload');
	                         parent.$.messager.alert("提示","删除成功！","info");
                    	 }
                     }, 'json');
                 }
             });
         }
	};
	//查询
	$aiter_demo.query = function() {  
		$grid.datagrid('load', serializeObject($("#demo_query_form").serializeArray()));
	};
});
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:true"  style="height:50px;border-left: 0px;border-top: 0px;border-right: 0px;">
      <div class="condition">
			<form id="demo_query_form" name="demo_query_form">
				<input type="hidden" name="filter_EQS_A_GLDQNO" value="${session_admin_key.ssdqno }"/>
				<input type="hidden" name="filter_EQS_A_GLJMSNO" value="${session_admin_key.jmsno }"/>
				<table cellpadding="0" cellspacing="0">
		            <tr>
		                <td width="50" align="right" class="l-table-edit-td">姓名：</td>
		                <td align="left" class="l-table-edit-td"><input name="filter_LIKES_A_NAME" type="text" id="filter_LIKES_A_NAME" class="inp" /></td>
		                <td width="50" align="right" class="l-table-edit-td">性别：</td>
		                <td align="left" class="l-table-edit-td">
							<at:dm_select name="filter_EQS_A_XB" id="filter_EQS_A_XB" cssClass="easyui-combobox" code="getTDmXb" first="--全部--" style="height:26px;"
								zdyContent=" panelHeight=\"auto\" data-options=\"valueField: 'dm',textField: 'mc'\" ">
							</at:dm_select>
						</td>
		           		<td width="80" align="right" class="l-table-edit-td">出生日期起：</td>
		                <td align="left" class="l-table-edit-td"><input name="filter_GTED_A_CSRQ" type="text" id="filter_GTED_A_CSRQ" ltype="text" class="Wdate" onclick="WdatePicker();"/></td>
		           		<td width="80" align="right" class="l-table-edit-td">出生日期止：</td>
		                <td align="left" class="l-table-edit-td"><input name="filter_LTED_A_CSRQ" type="text" id="filter_LTED_A_CSRQ" ltype="text" class="Wdate" onclick="WdatePicker();"/></td>
		            </tr>
		     	</table>
			</form>
		</div>  
</div>
<div data-options="region:'center',border:false" height="100%">
	<table id="demo_datagrid"></table>
</div>
</body>
</html>