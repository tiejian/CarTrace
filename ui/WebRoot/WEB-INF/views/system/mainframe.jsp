<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<!-- start: HEAD -->
	<head>
		<title>校园项目管理系统</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<%@include file="/global/global-other.jsp" %>
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="${ctx}/assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/animate.css/animate.min.css">
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
		<link rel="stylesheet" href="${ctx}/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/summernote/dist/summernote.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/toastr/toastr.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/bootstrap-select/bootstrap-select.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/DataTables/media/css/DT_bootstrap.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
		<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<link rel="stylesheet" href="${ctx}/assets/plugins/weather-icons/css/weather-icons.min.css">
		<link rel="stylesheet" href="${ctx}/assets/plugins/nvd3/nv.d3.min.css">
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- start: CORE CSS -->
		<link rel="stylesheet" href="${ctx}/assets/css/styles.css">
		<link rel="stylesheet" href="${ctx}/assets/css/styles-responsive.css">
		<link rel="stylesheet" href="${ctx}/assets/css/plugins.css">
		<link rel="stylesheet" href="${ctx}/assets/css/themes/theme-default.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="${ctx}/assets/css/print.css" type="text/css" media="print"/>
		<!-- ligerUi CSS -->
		<link  href="${ctx}/assets/plugins/ligerUI/skins/style1/css/ligerui-all.css?v=20150426" rel="stylesheet" type="text/css" />
		<link  href="${ctx}/assets/plugins/ligerUI/skins/ligerui-icons.css?v=20150426" rel="stylesheet" type="text/css" />
		<!-- end: CORE CSS -->
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<header class="topbar navbar navbar-inverse navbar-fixed-top inner">
				<!-- start: TOPBAR CONTAINER -->
				<div class="container">
					<div class="navbar-header">
						<!-- start: LOGO -->
						<a class="navbar-brand" href="#">
							<span style="font-weight: bold;font-size:24px;padding-left:20px;">校园项目管理系统</span>
						</a>
						<!-- end: LOGO -->
					</div>
					<div class="topbar-tools">
						<!-- start: TOP NAVIGATION MENU -->
						<ul class="nav navbar-right">
							<!-- start: USER DROPDOWN -->
							<li class="right-menu-toggle">
								<a href="#" onclick="initview('welcome.html')" style="padding-top:8px;" data-original-title="首  页" data-placement="bottom" class="tooltips">
									<i class="fa fa fa-home fa-2x"></i>
								</a>
							</li>
							<li class="dropdown current-user left-menu-toggle">
								<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
									<img src="${ctx}/assets/images/avatar-1-small.jpg" class="img-circle" alt=""><i class="fa fa-caret-down "></i>
								</a>
								<ul class="dropdown-menu dropdown-dark">
									<li>
										<a href="#">
											<i class="fa fa-user fa-lg" style="color:#43d1ff;"></i><span style="padding-left:10px;padding-bottom:3px;">个人信息</span>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-key fa-lg" style="color:#ff8a00;"></i><span style="padding-left:6px;padding-bottom:3px;">密码修改</span>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-lock fa-lg" style="color:red;"></i><span style="padding-left:10px;padding-bottom:3px;">锁定系统</span>
										</a>
									</li>
								</ul>
							</li>
							<!-- end: USER DROPDOWN -->
							<li class="right-menu-toggle">
								<a href="#" style="padding-top:8px;" data-original-title="帮  助" data-placement="bottom" class="tooltips">
									<i class="fa fa-question-circle fa-2x"></i>
								</a>
							</li>
							<!-- end: USER DROPDOWN -->
							<li class="right-menu-toggle" onclick="loginout();">
								<a href="#" style="padding-top:8px;" data-original-title="退  出" data-placement="bottom" class="tooltips">
									<i class="fa fa-power-off fa-2x" style="color:#ff6868;"></i>
								</a>
							</li>
						</ul>
						<!-- end: TOP NAVIGATION MENU -->
					</div>
				</div>
				<!-- end: TOPBAR CONTAINER -->
			</header>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<a class="closedbar inner hidden-sm hidden-xs" href="#">
			</a>
			<nav id="pageslide-left" class="pageslide inner">
				<div class="navbar-content">
					<!-- start: SIDEBAR -->
					<div class="main-navigation left-wrapper transition-left">
						<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block" style="float: left;">
								<div data-original-title="头  像" data-placement="right" class="tooltips">
									<a href="#"><img src="${ctx}/assets/images/avatar-1.jpg" alt=""></a>
								</div>
							</div>
							<div class="inline-block" style="float: left;line-height: 60px;height:60px;margin-top:18px;">
								<div style="padding-top:3px;padding-bottom:12px;"><h4 class="no-margin" style="color:#ff6f06;"> 管理员 </h4></div>
								<div style="padding-bottom:2px;"><h6 class="no-margin"> 2016-02-25 08:08:06 </h6></div>
							</div>
							<div class="inline-block" style="float: left;">
								<div class="navigation-toggler hidden-sm hidden-xs">
									<a href="#main-navbar" class="btn user-options sb-toggle-left"></a>
									<a href="#main-navbar" class="btn user-options sb-toggle-left"></a>
									<a href="#main-navbar" class="btn user-options sb-toggle-left"></a>
								</div>
							</div>
						</div>
						<!-- start: MAIN NAVIGATION MENU -->
						<ul class="main-navigation-menu">
							<li>
								<a href="javascript:;" class="active">
									<i class="fa fa-th-large" style="color:red;"></i> <span class="title">综合管理</span> <i class="icon-arrow" style="color:#00c36e;font-weight: bold;"></i>
								</a>
								<ul class="sub-menu">
									<li>
										<a href="javascript:;">
											<i class="fa fa-sitemap" style="color:#f58a5c;"></i>	项目管理 <i class="icon-arrow" style="color:#00c36e;font-weight: bold;"></i>
										</a>
										<ul class="sub-menu">
											<li>
												<a href="#" onclick="initview('${ctx}/projectbook/init');">
													<i class="fa fa-angle-double-right" style="color:#43d1ff;font-size:14px;"></i>项目申报
												</a>
											</li>
										</ul>
									</li>
								</ul>
							</li>
						</ul>
						<!-- end: MAIN NAVIGATION MENU -->
					</div>
					<!-- end: SIDEBAR -->
				</div>
			</nav>
			<div id="layout1">
	            <div position="center">
	            	<div id="centerFrame">
	            	</div>
        		</div>
	        	<div position="bottom">
		        	<div style="height:25px;line-height:25px;text-align:center;background: #efefef;">Copyright (c) 2015-* ******* Co. Ltd. * All right reserved.</div>
		        </div>
        	</div> 
			<!-- end: MAIN CONTAINER -->
		</div>
		
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="${ctx}/assets/plugins/respond.min.js"></script>
		<script src="${ctx}/assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="${ctx}/assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="${ctx}/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="${ctx}/assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="${ctx}/assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="${ctx}/assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="${ctx}/assets/plugins/moment/min/moment.min.js"></script>
		<script src="${ctx}/assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="${ctx}/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="${ctx}/assets/plugins/bootbox/bootbox.min.js"></script>
		<script src="${ctx}/assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
		<script src="${ctx}/assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
		<script src="${ctx}/assets/plugins/jquery.appear/jquery.appear.js"></script>
		<script src="${ctx}/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="${ctx}/assets/plugins/velocity/jquery.velocity.min.js"></script>
		<script src="${ctx}/assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
		<script src="${ctx}/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
		<script src="${ctx}/assets/plugins/toastr/toastr.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
		<script src="${ctx}/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
		<script src="${ctx}/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		<script src="${ctx}/assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
		<script src="${ctx}/assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
		<script src="${ctx}/assets/plugins/truncate/jquery.truncate.js"></script>
		<script src="${ctx}/assets/plugins/summernote/dist/summernote.min.js"></script>
		<script src="${ctx}/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
		
		<script src="${ctx}/assets/js/main.js"></script>
		
		<script src="${ctx}/assets/plugins/ligerUI/js/core/base.js?v=20150426" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins_bak/ligerGrid.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins_bak/ligerDialog.js?v=20150426" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/ligerUI/js/plugins/ligerToolBar.js?v=20150426" type="text/javascript"></script>
		<script src="${ctx}/assets/plugins/My97DatePicker/WdatePicker.js?v=20150426" type="text/javascript"></script>
		<!-- end: CORE JAVASCRIPTS  -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				$("#layout1").ligerLayout();
				initview(_CTX_+"/welcome.html");
			});
			
			loginout = function(){
		    	$.ligerDialog.confirm('是否要退出？','退出提示',function (yes) {if(yes){
	            	window.top.location='${ctx}/login/logout?&r='+Math.random();
	            }});
		    };
	    </script>
	</body>
	<!-- end: BODY -->
</html>