<%@ page language="java" pageEncoding="UTF-8"%>
<%@include file="/global/global-head.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="title" content="校园项目管理系统">
<title>校园项目管理系统</title>
<link rel="stylesheet" type="text/css" href="${ctx }/assets/css/login.css">
</head>
<body style="height:100%">
  <div class="mailLoginBox">
    <div class="mailLogin">
        <div class="headerBox">
            <div class="header clearfix">
                <h1>校园项目管理系统</h1>
            </div>
        </div>
        <!--背景通过改变class,bg1为荷花背景，bg2为小象背景-->
        <div class="mainBox bg2" data-sudaclick="bg2">            
            <div class="main" style="background:url(${ctx}/assets/images/img5.jpg)">
                <div class="positionBox clearfix">
                    <div class="loginBox" data-sudaclick="loginBox" style="cursor:default">
                    	<form action="#" id="form1" name="form1">
                        <div class="tit" style="">
                        	系统登录入口
                        </div>
                        <div class="freeMailbox">
                            <!--不显示visibility="hidden",若错误visibility=visible-->
                            <div class="freeError" style="visibility:visble;">
                                <span class="loginError tip1" style="display:none">您输入的帐号或密码不正确</span>
                                <span class="loginError tip2" style="display:none">验证码不正确</span>
                            </div>
                            <div class="usernameBox">
                                <input id="createby" name="createby" type="text" class="username focus" value="superadmin" placeholder="输入帐号"/>
                            </div>
                            <div class="passwordBox">
                                <input id="password" name="password" type="password" class="password" value="123456" placeholder="输入密码"/>
                            </div>
                            <div class="checkcodeBox">
                                <div class="clearfix">
                                    <input id="checkcode" name="checkcode" type="text" value="" placeholder="验证码"/>
                                    <img id="code" src="Kaptcha.jpg"/>
                                </div>
                                <p class="clearfix" style="margin-top:5px;"><a href="#" onclick="refreshCode();">看不清？换一个</a>按右图填写，字母小写</p>
                            </div>
                            <div class="loginmainBox">
                               <input type="button" id="loginbtn" name="loginbtn" class="btn btn-login" value="登  录"/>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerBox">
            <div class="footer clearfix">
                <p class="copy">版权所有&nbsp;&nbsp;&nbsp;&nbsp;Copyright&nbsp;©&nbsp;2016-&nbsp;&nbsp;Corporation,&nbsp;All&nbsp;Rights&nbsp;Reserved</p>
                <a target="_blank" href="#">校园官网</a><span>|</span>                
                <a target="_blank" href="#">校园微博</a><span>|</span>                
                <a target="_blank" href="#">意见反馈</a><span>|</span>
                <a target="_blank" href="http://www.12321.cn">不良信息举报</a>
            </div>
        </div>
    </div>
 </div>
</body>
<script src="${ctx}/assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
	$(function(){
		$("#loginbtn").click(function(){
			if($("#createby").val()==""){
				alert("帐号不能为空，请输入！");
				$("#createby").focus();
				return;
			}else if($("#password").val()==""){
				alert("密码不能为空，请输入！");
				$("#password").focus();
				return;
			}else if($("#checkcode").val()==""){
				alert("验证码不能为空，请输入！");
				$("#checkcode").focus();
				return;
			}
			var p = { createby: $("#createby").val(), password: $("#password").val(),checkcode: $("#checkcode").val()};
			$.post('${ctx}/login', p, function (result) {
	           	 if(result.code==991){
	           	 	$(".tip2").show();
	           	 	$(".tip1").hide();
	           	 }else if(result.code==994){
	           	 	$(".tip1").show();
	           	 	$(".tip2").hide();
	           	 }else if(result.code==100){
	           	 	$(".loginError").hide();
	           	 	$("#loginbtn").attr("value","正在登录...");
	           	 	$("#loginbtn").attr("disabled","disabled");
	           	 	window.top.location="${ctx}/mainframe/mf";
	           	 }
            }, 'json');
		});	
		
		refreshCode = function(){
			$("#code").attr("src","${ctx}/Kaptcha.jpg?r="+Math.random());
		};
		
		
		
	});
</script>
</html>