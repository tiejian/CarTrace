var xb = {
   1:'男',
   2:'女'
};
var xw = {
		   1:'荣誉博士学位',
		   2:'博士学位',
		   3:'硕士学位',
		   4:'学士学位',
		   0:'无'
		};
var zc = {
		   11:'教授',
		   12:'副教授',
		   81:'高级工程师',
		   83:'工程师',
		   85:'技术员',
		   165:'技术设计员'
		};
var zw = {
		'001A':'教授',
		'001B':'副教授',
		'002A':'委员',
		'004A':'主任',
		'004B':'副主任',
		'083Q':'总工程师',
		'216A':'局长',
		'216B':'副局长'
};

function getxb(str){
	return xb[str];
}
function getxw(str){
	return xw[str];
}
function getzc(str){
	return zc[str];
}
function getzw(str){
	return zw[str];
}
