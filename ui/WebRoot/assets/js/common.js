﻿//鼠标屏蔽
//document.oncontextmenu=new Function("event.returnValue=false;");
//document.onselectstart=new Function("event.returnValue=false;");
//
var alertDialog=parent.$.ligerDialog;
var message="数据验证失败：";
var content="<div style='color:red;'>$message$</div>";
            
function closeWin(type){
	if(type==1){
		if(window.confirm("是否关闭窗口？")){
			this.window.opener=null;
			window.close();
		}	
	}else{
		this.window.opener=null;
		window.close();
	}
}

function checkValidate(array){
	var flag=true;
	$.each(array,function(i){
		var id=i;
		var validates=array[i];
		$.each(validates,function(v){
			var vv=validates[v].split(":");
			if(vv[0]=="required"){
				if(!requiredValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="email"){
				if(!emailValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="url"){
				if(!urlValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="maxlength"){
				if(!maxlengthValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="maxsize"){
				if(!maxsizeValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="minlength"){
				if(!minlengthValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="minsize"){
				if(!minsizeValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="date"){
				if(!dateValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="number"){
				if(!numberValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="digits"){
				if(!digitsValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="equalTo"){
				if(!equalToValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="tel"){
				if(!telValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="num"){
				if(!numValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="judgeDate"){
				if(!judgeDateValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="idCard"){
				if(!idCardValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="fh"){
				if(!fhValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="tfqzh"){
				if(!tfqzhValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="money"){
				if(!moneyValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else if(vv[0]=="percent"){
				if(!percentValidate(id,vv[1])){
					flag=false;
					return false;
				}
			}else{
				flag=true;
			}
		});
		if(!flag){
			return flag;
		}
	});
	return flag;
}
function requiredValidate(id,value){
	if(value){
		var msg="字段不能为空！";
		var obj=$(id);
		if(obj.val()==""){
			//alert(obj.attr("title")+msg);
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function emailValidate(id,value){
	if(value){
		var msg="请输入正确格式的电子邮件！";
		var obj=$(id);
		if(obj.val().length>0){
			if(!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function urlValidate(id,value){
	if(value){
		var msg="请输入合法的网址！";
		var obj=$(id);
		if(obj.val().length>0){
			if(!/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function maxlengthValidate(id,value){
	var msg="只能输入最大长度为"+value+"的字符串！";
	var obj=$(id);
	if(obj.val().length>0){
		if(getByteLen(obj.val())>parseInt(value)){
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function maxsizeValidate(id,value){
	var msg="只能输入最大长度为"+value+"的字符串！";
	var obj=$(id);
	if(obj.val().length>0){
		if(obj.val().length>parseInt(value)){
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function minlengthValidate(id,value){
	var msg="只能输入最小长度为"+value+"的字符串！";
	var obj=$(id);
	if(obj.val().length>0){
		if(parseInt(value)>getByteLen(obj.val())){
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function minsizeValidate(id,value){
	var msg="只能输入最小长度为"+value+"的字符串！";
	var obj=$(id);
	if(obj.val().length>0){
		if(parseInt(value)>obj.val().length){
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function getByteLen(val) { 
	var len = 0; 
	var a = val.split("");
	for (var i = 0; i < val.length; i++) {
	if (a[i].match(/[^\x00-\xff]/ig) != null) //全角 
	len += 2; 
	else 
	len += 1; 
	} 
	return len; 
} 
function dateValidate(id,value){
	var msg="请输入合法的日期格式，格式："+value;
	var obj=$(id);
	if(obj.val().length>0){
		if(!dateCheck(obj.val(),value)){
			commonAlert(id,"【"+obj.attr("title")+"】"+msg);
			return false;
		}
	}
	return true;
}
function numberValidate(id,value){
	if(value){
		var msg="请输入合法的数字！";
		var obj=$(id);
		if(obj.val().length>0){
			if(!/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function digitsValidate(id,value){
	if(value){
		var msg="只能输入整数！";
		var obj=$(id);
		if(obj.val().length>0){
			if(!/^\d+$/.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function numValidate(id,value){
    if(value){
		var msg="请输入数字！";
		var obj=$(id);
		if(obj.val().length>0){
			if(isNaN(obj.val())){
	          	commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
	        }
		}
	}
	return true;
 }

function equalToValidate(id,value){
	if(value){
		var msg="值必须相同！";
		var obj=$(id);
		var obj1=$(value);
		if(obj.val().length>0||obj1.val().length>0){
			if(obj.val()!=obj1.val()){
				commonAlert(id,"【"+obj1.attr("title")+"】与【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function fhValidate(id,value){
	if(value){
		var msg="规则不正确，1-6位有效数字，7位罗马字符，8-19位有效数字！";
		var obj=$("#"+id).val();
		if(obj.length>0){
			if(!/^(\d{6})([\u2160-\u216b]{1})(\d{12})?$/.test(value)){
	          	//commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
	        }
		}
	}
	return true;
}
function tfqzhValidate(id,value){
	if(value){
		var msg="规则不正确，1-6位有效数字，7位罗马字符，8-12位有效数字！";
		var obj=$("#"+id).val();
		if(obj.length>0){
			if(!/^(\d{6})([\u2160-\u216b]{1})(\d{5})?$/.test(value)){
	          	//commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
	        }
		}
	}
	return true;
}
function tfqzhPfValidate(id,value){
	if(value){
		var msg="规则不正确，1-6位有效数字，7位罗马字符或(A,B,C,D)，8-12位有效数字！";
		var obj=$("#"+id).val();
		if(obj.length>0){
			if(!/^(\d{9})([A-Z]{1})(\d{2})?$/.test(value)){
	          	//commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
	        }
		}
	}
	return true;
}
function fhPfValidate(id,value){
	if(value){
		var msg="规则不正确，1-6位有效数字，7位罗马字符或(A,B,C,D)，8-19位有效数字！";
		var obj=$("#"+id).val();
		if(obj.length>0){
			if(!/^(\d{9})([A-Z]{1})(\d{9})?$/.test(value)){
	          	//commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
	        }
		}
	}
	return true;
}
function dateCheck(str,dateformat){
	var reg1="^([0-9]{4})[.-]{1}([0-9]{1,2})[.-]{1}([0-9]{1,2})$";
	var reg2="^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})$";
	var reg3="^([0-9]{4})$";
	var re;
	if(dateformat=="YYYY-MM-DD"){
		re = new RegExp(reg1);
	}else if(dateformat=="YYYY/MM/DD"){
		re = new RegExp(reg2);
	}else if(dateformat=="YYYY"){
		re = new RegExp(reg3);
	}
	
    var ar;
    var res = true;
    
    if ((ar = re.exec(str)) != null){
        var i;
        i = parseFloat(ar[2]);
        // verify mm
        if (i <= 0 || i > 12){
            res = false;
        }
        i = parseFloat(ar[3]);
        // verify dd
        if (i <= 0 || i > 31){
            res = false;
        }
    }else{
        res = false;
    }
    if (!res){
        return false;
    }
    return res;
}
function telValidate(id,value){
	var pattern =/^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
	if(value){
		var msg="请输入正确的电话号码，格式为:0000-00000000！";
		var obj=$(id);
		var s =obj.val(); 
		if(s.length > 0){
			if(!pattern.exec(s)){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
		        return false;
		    }
		}
	}
	return true;
}
function judgeDateValidate(id,value){  
	var obj1=$(id);
	var obj2=$(value);
	if(obj1.val()==""&&obj2.val()==""){
		return true;
	}
	//if(obj1.val()==""||obj2.val()==""){
	//	commonAlert(id,"比较2个日期【"+obj1.attr("title")+"和"+obj2.attr("title")+"】"+"不能有空值！");
	//	return false;
	//}
	if(obj2.val()!=""){
		if(obj1.val()==""){
			commonAlert(id,"【"+obj2.attr("title")+"存在时"+obj1.attr("title")+"】不能为空！");
			return false;
		}
	}
	var date1 = obj1.val().replace(/-/g, "/");   
	var date2 = obj2.val().replace(/-/g, "/");   
	var d1 = new Date(date1);   
	var d2 = new Date(date2);   
	if(Date.parse(d1) - Date.parse(d2)> 0){   
	    commonAlert(id,"【"+obj1.attr("title")+"不能大于"+obj2.attr("title")+"】");
	    return false;
	}
	return true;
}
function idCardValidate(id,value){  
	if(value){
		var msg="请输入正确的身份证号码！";
		var obj=$(id);
		var s =obj.val(); 
		if(s.length==15) 
			s = idCard15To18(s);
	    var sigma = 0;    
	    var a = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2 );    
	    var w = new Array("1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2");         
	    for (var i = 0; i < 17; i++) {    
	        var ai = parseInt(s.substring(i, i + 1));    
	        var wi = a[i];    
	        sigma += ai * wi;             
	    }     
	    var number = sigma % 11;              
	    var check_number = w[number];     
	    if (s.substring(17) != check_number) {    
	    	commonAlert(id,"【"+obj.attr("title")+"】"+msg);
	        return false;    
	    }
	}
	return true;  
} 
function idCard15To18(id){  
    var W = new Array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1);  
    var A = new Array("1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2");  
    var i,j,s=0;  
    var newid;  
    newid = id;  
    newid = newid.substring(0,6)+"19"+newid.substring(6,id.length);  
    for(i=0;i<newid.length;i++ ){  
      j= parseInt(newid.substring(i,i+1))*W[i];  
      s=s+j;  
    }  
    s = s % 11;  
    newid=newid+A[s];   
    return newid;  
}
function moneyValidate(id,value){
	if(value){
		var msg="请输入合法金额,如:12或12.00！";
		var obj=$(id);
		if(obj.val().length>0){
			if(!/^(([1-9]{1}\d*)|([0]{1}))(\.(\d){1,2})?$/.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}
function percentValidate(id,value){
	if(value){
		var msg="请输入合法百分数！";
		var obj=$(id);
		if(obj.val().length>0){
			var reg = /^((\d+\.?\d*)|(\d*\.\d+))\%$/;
			if(!reg.test(obj.val())){
				commonAlert(id,"【"+obj.attr("title")+"】"+msg);
				return false;
			}
		}
	}
	return true;
}

function errorMsg(msg){
	alertDialog.error("<div style='color:red;'>错误信息：<br/>"+msg+"</div>", "错误提示");
}

function commonAlert(id,content){
	var obj=$(id);
	alertDialog.error("<div style='color:red;'>"+message+"<br/>"+content+"</div>", "错误提示", function(){
		obj.focus();
		return true;
	});
}

Array.max = function(array){  
	return Math.max.apply(Math,array);
};
Array.min = function(array){  
	return Math.max.apply(Math,array);
};

Date.prototype.format = function (format) {  
    var o = {  
        "M+": this.getMonth() + 1,  
        "d+": this.getDate(),  
        "h+": this.getHours(),  
        "m+": this.getMinutes(),  
        "s+": this.getSeconds(),  
        "q+": Math.floor((this.getMonth() + 3) / 3),  
        "S": this.getMilliseconds()  
    }  
    if (/(y+)/.test(format)) {  
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));  
    }  
    for (var k in o) {  
        if (new RegExp("(" + k + ")").test(format)) {  
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));  
        }  
    }  
    return format;  
}  
function getSmpFormatDate(date, isFull) {  
    var pattern = "";  
    if (isFull == true || isFull == undefined) {  
        pattern = "yyyy-MM-dd hh:mm:ss";  
    } else {  
        pattern = "yyyy-MM-dd";  
    }  
    return getFormatDate(date, pattern);  
}  

function getSmpFormatNowDate(isFull) {  
    return getSmpFormatDate(new Date(), isFull);  
}  

function getSmpFormatDateByLong(l, isFull) {  
    return getSmpFormatDate(new Date(l), isFull);  
}  

function getFormatDateByLong(l, pattern) {  
    return getFormatDate(new Date(l), pattern);  
}  
function getFormatDate(date, pattern) {  
    if (date == undefined) {  
        date = new Date();  
    }  
    if (pattern == undefined) {  
        pattern = "yyyy-MM-dd hh:mm:ss";  
    }  
    return date.format(pattern);  
}  
function openGisNode(msg,value,url){
	if(value==""){
		alertDialog.error(msg); return; 
	}
	url  = url.replace("$var2$",value);
	window.open(url);
}
jQuery.fn.getFileName = function(){
	var v =$(this).val().split("\\");
	return v[v.length-1];
};

String.prototype.trim=function() {    return this.replace(/(^\s*)|(\s*$)/g,'');}

function getSessionId(){
	var c_name = 'JSESSIONID';
	if(document.cookie.length>0){
	  c_start=document.cookie.indexOf(c_name + "=")
	  if(c_start!=-1){ 
	    c_start=c_start + c_name.length+1 
	    c_end=document.cookie.indexOf(";",c_start)
	    if(c_end==-1) c_end=document.cookie.length
	    return unescape(document.cookie.substring(c_start,c_end));
	  }
	 }
}
