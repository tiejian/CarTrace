package com.xygl.core.utils;


import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class MainCode {
	public static void main(String[] args) throws IOException,
			XMLParserException, InvalidConfigurationException, SQLException,
			InterruptedException {
		String filePath = MainCode.class.getResource("/generatorConfig.xml").getPath();
		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		File configFile = new File(filePath);
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(configFile);
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGeneratro = new MyBatisGenerator(config,
				callback, warnings);
		try{
			myBatisGeneratro.generate(null);
		}catch(Exception e){
			
		}
		for (String w : warnings) {
			System.out.println(w);
		}
		System.err.println("code success!");
	}
}
