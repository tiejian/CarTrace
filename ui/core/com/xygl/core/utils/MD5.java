package com.xygl.core.utils;




import java.security.MessageDigest;


public class MD5 {
    
    // 全局数组
    private final static String[] strDigits = { "0", "1", "2", "3", "4", "5",
            "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    public MD5() {
    
    }

    /**
     * 
     *  @Description    : 返回形式为数字跟字符串
     *  @Method_Name    : byteToArrayString
     *  @param bByte
     *  @return 
     *  @return         : String
     *  @Creation Date  : 2015-6-1 下午10:30:58 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        // System.out.println("iRet="+iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }

    /**
     *  
     *  @Description    : 转换字节数组为16进制字串
     *  @Method_Name    : byteToString
     *  @param bByte
     *  @return 
     *  @return         : String
     *  @Creation Date  : 2015-6-1 下午10:31:16 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    /**
     * 
     *  @Description    : 获取加密内容
     *  @Method_Name    : getMD5Code
     *  @param strObj
     *  @return 
     *  @return         : String
     *  @Creation Date  : 2015-6-1 下午10:31:21 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    public static String getMD5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes("UTF-8")));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultString.toUpperCase();
    }

    public static void main(String[] args) {
		System.out.println(MD5.getMD5Code("wechat.congcongbus.string.key").toLowerCase());
	}
}