package com.xygl.core.utils;


import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.WebUtil.java
 *  @Class Name    : WebUtil
 *  @Description   : WebUtil工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:23:50 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class WebUtil {
	
	/**
	 * 
	 *  @Description    : 获取当前request
	 *  @Method_Name    : getHttpServletRequest
	 *  @return 
	 *  @return         : HttpServletRequest
	 *  @Creation Date  : 2015-6-7 下午3:54:23 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}
}
