﻿package com.xygl.core.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.MenuTreeNode.java
 *  @Class Name    : MenuTreeNode
 *  @Description   : 菜单实体
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:28:52 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class MenuTreeNode {

	private String id;//菜单id
	private String text;//菜单名称
	private String href;//href地址
	private String pid;//父菜单pid
	private String iconCls;//菜单图标
	private String isLeaf;//是否子节点，0否，1是
	private String help;//是否有帮助
	private Map<String,String> attributes;
	
	private List<MenuTreeNode> children;
	
	public void addChild(MenuTreeNode obj) {
		if(children==null){
			children = new ArrayList<MenuTreeNode>();
		}
		children.add(obj);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public List<MenuTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<MenuTreeNode> children) {
		this.children = children;
	}

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public String getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(String isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
	

}
