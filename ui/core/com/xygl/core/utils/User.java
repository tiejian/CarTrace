package com.xygl.core.utils;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.User.java
 *  @Class Name    : User
 *  @Description   : 用户登录实体类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:25:38 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class User {
	
	private String uid;// 用户id
	private String umc;// 用户名称
	private String sjhm;// 手机号码
	private String sjhgsd;//手机号归属地
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getSjhm() {
		return sjhm;
	}
	public void setSjhm(String sjhm) {
		this.sjhm = sjhm;
	}
	public String getUmc() {
		return umc;
	}
	public void setUmc(String umc) {
		this.umc = umc;
	}
	public String getSjhgsd() {
		return sjhgsd;
	}
	public void setSjhgsd(String sjhgsd) {
		this.sjhgsd = sjhgsd;
	}
	
}
