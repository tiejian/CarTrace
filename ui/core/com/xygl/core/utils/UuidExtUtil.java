package com.xygl.core.utils;

import java.util.UUID;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.UuidExtUtil.java
 *  @Class Name    : UuidExtUtil
 *  @Description   : 获取uuid工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:25:17 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class UuidExtUtil {
	public static String getUUID() {
		String uuid = UUID.randomUUID().toString();
		return uuid.replaceAll("-", "");
	}
}
