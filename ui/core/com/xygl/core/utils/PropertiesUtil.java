
package com.xygl.core.utils;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import com.xygl.core.exception.CustomException;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.PropertiesUtil.java
 *  @Class Name    : PropertiesUtil
 *  @Description   : 获取资源文件工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:28:17 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class PropertiesUtil {

	private static Resource resource = null;

	static {
		resource = new ClassPathResource("/config.properties");
	}

	/**
	 * 
	 *  @Description    : 获取资源文件对象
	 *  @Method_Name    : getProperties
	 *  @return
	 *  @throws IOException 
	 *  @return         : Properties
	 *  @Creation Date  : 2015-6-2 上午10:44:44 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static Properties getProperties() throws IOException {
		return PropertiesLoaderUtils.loadProperties(resource);
	}
	
	/**
	 * 
	 *  @Description    : 获取资源属性值
	 *  @Method_Name    : getProperties
	 *  @param key
	 *  @return
	 *  @throws Exception 
	 *  @return         : String
	 *  @Creation Date  : 2015-6-2 上午10:44:57 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static String getProperties(String key) throws Exception{
		Properties properties = null;
		try{
			properties = PropertiesLoaderUtils.loadProperties(resource);
		}catch(Exception e){
			SysLogUtil.error("PropertiesUtil getProperties 获取资源文件失败！", e);
			throw new CustomException("PropertiesUtil getProperties 获取资源文件失败！");
		}
		return properties.getProperty(key);
	}
	
}
