﻿package com.xygl.core.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.BeanUtil.java
 *  @Class Name    : BeanUtil
 *  @Description   : spring容器获取bean工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:31:52 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class BeanUtil implements ApplicationContextAware {

	private static ApplicationContext context;

	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		context = arg0;
	}

	public static Object getBean(String beanid) {
		return context.getBean(beanid);
	}
}
