package com.xygl.core.utils;

import com.xygl.core.constant.ConstantCode;


/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.Json.java
 *  @Class Name    : Json
 *  @Description   : 封装json对象用于返回给ajax请求
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:29:28 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class Json {
	private String code = ConstantCode.CODE_SUCC_100;//默认成功代码，其余均为错误代码
	private String msg;//消息
	private Object jsonobj;//其他object对象

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg
	 *            the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the jsonobj
	 */
	public Object getJsonobj() {
		return jsonobj;
	}

	/**
	 * @param jsonobj
	 *            the jsonobj to set
	 */
	public void setJsonobj(Object jsonobj) {
		this.jsonobj = jsonobj;
	}

}
