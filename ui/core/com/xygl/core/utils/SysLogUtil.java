﻿package com.xygl.core.utils;

import java.util.Properties;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.SysLogUtil.java
 *  @Class Name    : SysLogUtil
 *  @Description   : 系统日志工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:24:57 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
@SuppressWarnings("all")
public class SysLogUtil {
	
    public static final int DEBUG = 0;

    public static final int INFO = 1;

    public static final int WARN = 2;

    public static final int ERROR = 3;

    public static final int FATAL = 4;

    private static boolean inited = false;

    private static Priority[] priority = { Priority.DEBUG, Priority.INFO, Priority.WARN,
                    Priority.ERROR, Priority.FATAL };

    private static Logger logger = Logger.getLogger("ccbs-wx-web");

    protected SysLogUtil() {
    }

    public static void init(Properties p) {
        if (!inited) {
            PropertyConfigurator.configure(p);
            inited = true;
        }
    }

    public static void initXml(String fileName) {
        if (!inited) {
            DOMConfigurator.configure(fileName);
            inited = true;
        }
    }

    public static void init() {
        if (!inited) {
            BasicConfigurator.configure();
            inited = true;
        }
    }

    public static void debug(Object message) {
        //   if (inited)
        {
            logger.debug(message);
        }
    }

    public static void debug(Object message, Throwable t) {
        //   if (inited)
        {
            logger.debug(message, t);
        }
    }

    public static void error(Object message) {
        //  if (inited)
        {
            logger.error(message);
        }
    }
    public static void error(Throwable t) {
        //  if (inited)
        {
            logger.error(t.getMessage(), t);
        }
    }

    public static void error(Object message, Throwable t) {
        //  if (inited)
        {
            logger.error(message, t);
        }
    }

    public static void fatal(Object message) {
        //  if (inited)
        {
            logger.fatal(message);
        }
    }

    public static void fatal(Throwable t) {
        //  if (inited)
        {
            logger.fatal(t.getMessage(), t);
        }
    }
    
    public static void fatal(Object message, Throwable t) {
        //   if (inited)
        {
            logger.fatal(message, t);
        }
    }

    public static void info(Object message) {
        // if (inited)
        {
            logger.info(message);
        }
    }

    public static void info(Object message, Throwable t) {
        //  if (inited)
        {
            logger.info(message, t);
        }
    }

    public static void log(int level, Object message, Throwable t) {
        // if (inited)
        {
            logger.log(priority[level], message, t);
        }
    }

    public static void log(int level, Object message) {
        //  if (inited)
        {
            logger.log(priority[level], message);
        }
    }

    public static void warn(Object message) {
        // if (inited)
        {
            logger.warn(message);
        }
    }
    public static void warn(Throwable t) {
        //  if (inited)
        {
            logger.warn(t.getMessage(), t);
        }
    }

    public static void warn(Object message, Throwable t) {
        // if (inited)
        {
            logger.warn(message, t);
        }
    }

    public static boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }

    public static boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    public static boolean isWarnEnabled() {
        return logger.isEnabledFor(Priority.WARN);
    }

    public static boolean isErrorEnabled() {
        return logger.isEnabledFor(Priority.ERROR);
    }

    public static boolean isFatalEnabled() {
        return logger.isEnabledFor(Priority.FATAL);
    }
}
