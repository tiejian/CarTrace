﻿package com.xygl.core.utils;



import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateEditor extends PropertyEditorSupport {

	public String getAsText() {
		Date value = (Date) getValue();
		if (null == value) {
			value = new Date();
		}
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(value);
	}

	public void setAsText(String text) throws IllegalArgumentException {
		Date value = null;
		if (null != text && !text.equals("")) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try {
				value = df.parse(text);
			} catch (Exception e) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					value = sdf.parse(text);
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}
		}
		setValue(value);
	}
}
