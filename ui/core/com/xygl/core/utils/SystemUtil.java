﻿package com.xygl.core.utils;


import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.SystemUtil.java
 *  @Class Name    : SystemUtil
 *  @Description   : 系统工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:25:29 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class SystemUtil {

	/**
	 * 
	 *  @Description    : 根据流水号和字符长度格式化流水号，超过固定长度放回当前流水号
	 *  @Method_Name    : getPolishingLsh
	 *  @param lsh
	 *  @param length
	 *  @return 
	 *  @return         : String
	 *  @Creation Date  : 2015-6-2 下午1:01:13 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static String getPolishingLsh(int lsh, int length) {
		StringBuffer format = new StringBuffer();
		int len = String.valueOf(lsh).length();
		for (int i = 0; i < length - len; i++) {
			format.append("0");
		}
		format.append(lsh);
		return format.toString();
	}

	/**
	 * 
	 *  @Description    : 创建文件夹
	 *  @Method_Name    : createFolder
	 *  @param path 
	 *  @return         : void
	 *  @Creation Date  : 2015-6-2 下午1:02:05 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static void createFolder(String path) {
		//windows
//		StringTokenizer st = new StringTokenizer(path, "\\");
//		String path1 = st.nextToken() + "\\";
//		String path2 = path1;
//		while (st.hasMoreTokens()) {
//			path1 = st.nextToken() + "\\";
//			path2 += path1;
//			File inbox = new File(path2);
//			if (!inbox.exists()){
//				inbox.mkdir();
//			}
//		}
		//linux
		File f=new File(path);
		f.setWritable(true, false);
		f.mkdirs();
	}
	
	/**
	 * 
	 *  @Description    : 获取订单流水号
	 *  @Method_Name    : getOrderLsh
	 *  @param prefix D,T 订单，退单
	 *  @return 
	 *  @return         : String
	 *  @Creation Date  : 2015-11-2 上午10:17:33 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	public static String getOrderLsh(String prefix) {
		//D+8位随机数+20151011SSSSS+6位随机数
		//T+8位随机数+20151011SSSSS+6位随机数
		StringBuffer sbf = new StringBuffer();
		sbf.append(prefix);
		sbf.append(getFixLenthString(8));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		sbf.append(sdf.format(new Date()));
		sbf.append(getFixLenthString(6));
		return sbf.toString();
	}
	
	/**
	 * 
	 *  @Description    : 获取指定长度随机数
	 *  @Method_Name    : getFixLenthString
	 *  @param strLength
	 *  @return 
	 *  @return         : String
	 *  @Creation Date  : 2015-11-2 上午10:15:00 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
    public static String getFixLenthString(int strLength) {
    	int a[] = new int[strLength];
    	StringBuffer sbf = new StringBuffer(strLength);
        for(int i=0;i<a.length;i++ ) {
            a[i] = (int)(10*(Math.random()));
            sbf.append(a[i]);
        }
        return sbf.toString();
    }
    
	public static void main(String[] args) {
		System.out.println(getOrderLsh("D"));
	}
}
