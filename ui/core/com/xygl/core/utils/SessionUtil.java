﻿package com.xygl.core.utils;


import javax.servlet.http.HttpServletRequest;

import org.springframework.web.util.WebUtils;

import com.xygl.core.constant.Constant;
import com.xygl.domain.Admin;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.utils.SessionUtil.java
 *  @Class Name    : SessionUtil
 *  @Description   : session信息工具类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:26:20 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class SessionUtil {

	/**
	 * 
	 * @Description : 获取session中当前链接sessionId
	 * @Method_Name : getSessionId
	 * @param request
	 * @return
	 * @throws Exception
	 * @return : String
	 * @Creation Date : 2015-4-5 下午7:44:50
	 * @version : v1.00
	 * @Author : ljx
	 */
	public static String getSessionId(HttpServletRequest request)
			throws Exception {
		try {
			return (String) WebUtils.getSessionAttribute(request,
					Constant.SESSION_ID_KEY);
		} catch (Exception e) {
			SysLogUtil.error("获取session中当前链接sessionId时出错！", e);
		}
		return null;
	}
	
	public static Admin getSessionAdmin(HttpServletRequest request)
			throws Exception {
		try {
			return (Admin) WebUtils.getSessionAttribute(request,
					Constant.SESSION_ADMIN_KEY);
		} catch (Exception e) {
			SysLogUtil.error("获取session中当前链接admin时出错！", e);
		}
		return null;
	}
	
}
