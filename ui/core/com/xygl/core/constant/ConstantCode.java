package com.xygl.core.constant;


/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.constant.ConstantCode.java
 *  @Class Name    : ConstantCode
 *  @Description   : 状态码常量类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:21:43 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class ConstantCode {
	public static String CODE_SUCC_100 = "100";// 成功
	public static String CODE_ERR_999 = "999";// 系统错误
	public static String CODE_ERR_998 = "998";// 权限错误
	public static String CODE_ERR_997 = "997";// 登录超时
	public static String CODE_ERR_996 = "996";// 帐号错误
	public static String CODE_ERR_995 = "995";// 业务错误
	public static String CODE_ERR_994 = "994";// 登录帐号错误
	public static String CODE_ERR_993 = "993";// 登录帐号锁定错误
	public static String CODE_ERR_992 = "992";// 登录帐号登录错误
	public static String CODE_ERR_991 = "991";// 验证码错误
	
}
