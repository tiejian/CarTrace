package com.xygl.core.constant;


/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.constant.Constant.java
 *  @Class Name    : Constant
 *  @Description   : 系统常量类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:20:57 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class Constant {
	// 管理员session
	public static String SESSION_ADMIN_KEY = "session_admin_key";
	// 管理员id session
	public static String SESSION_ADMIN_ID_KEY = "session_admin_id_key";
	// session id
	public static String SESSION_ID_KEY = "session_id_key";
	// 菜单 session
	public static String SESSION_MENU_KEY = "session_menu_key";
	
}
