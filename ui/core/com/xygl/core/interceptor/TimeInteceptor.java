package com.xygl.core.interceptor;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.interceptor.TimeInteceptor.java
 *  @Class Name    : TimeInteceptor
 *  @Description   : 执行时间拦截器
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:36:15 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class TimeInteceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = Logger.getLogger(TimeInteceptor.class);
	
	@Override
	public void afterCompletion(HttpServletRequest request,	HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request,	HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,	ModelAndView modelAndView)throws Exception {
		long startTime = (Long) request.getAttribute("startTime");
		long endTime = System.currentTimeMillis();
		long executeTime = endTime - startTime;
		//modelAndView.addObject("executeTime", executeTime);
		logger.debug("[" + handler + "] executeTime : " + (executeTime/1000)+ "s");    
	}

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		long startTime = System.currentTimeMillis();
		request.setAttribute("startTime", startTime);
		return true;	
	}

}