package com.xygl.core.interceptor;


import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSONObject;
import com.xygl.core.constant.ConstantCode;
import com.xygl.core.utils.Json;
import com.xygl.core.utils.SessionUtil;
import com.xygl.core.utils.SysLogUtil;
import com.xygl.domain.Admin;
public class SessionInterceptor extends HandlerInterceptorAdapter{
	
	
	@Override
	public void afterCompletion(HttpServletRequest request,	HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request,	HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,	ModelAndView modelAndView)throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
		Admin admin = SessionUtil.getSessionAdmin(request);
		if(null == admin){
			String uri = request.getRequestURI();
			SysLogUtil.debug("uri " + uri);
			if(uri.toLowerCase().indexOf("/init")>-1){
				return super.preHandle(request, response, handler);	
			}
			String requestType = request.getHeader("X-Requested-With");   
			String accept = request.getHeader("accept");
			//request.setAttribute("code", ConstantCode.CODE_ERR_997);
			if("XMLHttpRequest".equals(requestType)){
				if(accept.indexOf("application/json")>-1){//返回json
					//response.setStatus(Integer.valueOf(ConstantCode.CODE_ERR_997));
					Json json=new Json();
					json.setCode(ConstantCode.CODE_ERR_997);
					json.setMsg("当前登录已超时，请重新登录！");
					PrintWriter pw=response.getWriter();
					pw.write(JSONObject.toJSONString(json));
					pw.flush();
					pw.close();
					SysLogUtil.debug("当前登录已超时，请重新登录！");
				}else{
					response.setContentType("text/html; charset=utf-8" );
					response.sendRedirect(request.getContextPath() + "/index.jsp?r="+Math.random());
					SysLogUtil.debug("当前登录已超时，请重新登录！");
				}
			}else{
				response.sendRedirect(request.getContextPath() + "/index.jsp?r="+Math.random());
				SysLogUtil.debug("正常访问资源请求！");
			}
			return false;
        }else{        	
        	return super.preHandle(request, response, handler);	
        }
	}
}
