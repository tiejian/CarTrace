package com.xygl.core.controllor;


import java.util.Date;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.xygl.core.utils.DateEditor;


/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.controllor.BaseController.java
 *  @Class Name    : BaseController
 *  @Description   : controller父类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:22:36 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class BaseController {
	
	@InitBinder
	public void initBinder(WebDataBinder binder) throws Exception {
		binder.registerCustomEditor(Date.class, new DateEditor());
	}
	
	public String pageJsonError() throws Exception {
		return "{\"Rows\":[],\"Total\":\"0\"}";
	}
	
}
