package com.xygl.core.listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.listener.ActiveUserListener.java
 *  @Class Name    : ActiveUserListener
 *  @Description   : 使用用户监听，同时存储用户sessionId
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:34:57 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class ActiveUserListener implements HttpSessionListener {

	private static int sessionCount = 0;

	private static Map<String,Object> sessionMaps = new HashMap<String,Object>(); // 存放session的集合类

	public void sessionCreated(HttpSessionEvent arg0) {
		HttpSession session = arg0.getSession();
		String sessionId = session.getId();
		sessionMaps.put(sessionId, session);
		sessionCount++;
	}

	public void sessionDestroyed(HttpSessionEvent arg0) {
		sessionCount--;
		String sessionId = arg0.getSession().getId();
		sessionMaps.remove(sessionId);
	}

	public static int getSessionCount() {
		return sessionCount;
	}

	public static Map<String,Object> getSessionMaps() {
		return sessionMaps;
	}
}
