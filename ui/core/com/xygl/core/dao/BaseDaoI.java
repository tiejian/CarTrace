package com.xygl.core.dao;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.at.page.Criteria;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.dao.BaseDaoI.java
 *  @Class Name    : BaseDaoI
 *  @Description   : 父dao基类
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:37:04 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
@Repository
public interface BaseDaoI <T extends Serializable, PK extends Serializable> {
	
	/**
	 * 
	 *  @Description    : 添加对象
	 *  @Method_Name    : insert
	 *  @param o
	 *  @return 
	 *  @return         : int
	 *  @Creation Date  : 2015-4-5 下午8:23:43 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	int insert(Object o);
    
	/**
	 * 
	 *  @Description    : 更新对象
	 *  @Method_Name    : update
	 *  @param o
	 *  @return 
	 *  @return         : int
	 *  @Creation Date  : 2015-4-5 下午8:23:30 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
    int update(Object o);
    
    /**
     * 
     *  @Description    : 单一主键物理删除
     *  @Method_Name    : delete
     *  @param o
     *  @return 
     *  @return         : int
     *  @Creation Date  : 2015-4-5 下午8:22:55 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    int delete(Object o);
    
    /**
     * 
     *  @Description    : 联合主键物理删除
     *  @Method_Name    : deleteByMap
     *  @param parameters
     *  @return 
     *  @return         : int
     *  @Creation Date  : 2015-4-5 下午8:27:07 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    int deleteByMap(Map<String,Object> parameters);
    
    /**
     * 
     *  @Description    : 单一主键逻辑删除
     *  @Method_Name    : logicDelete
     *  @param parameters
     *  @return 
     *  @return         : int
     *  @Creation Date  : 2015-4-5 下午8:28:45 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    int logicDelete(Object o);
    
    /**
     * 
     *  @Description    : 联合主键逻辑删除
     *  @Method_Name    : logicDeleteByMap
     *  @param parameters
     *  @return 
     *  @return         : int
     *  @Creation Date  : 2015-4-5 下午8:32:40 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    int logicDeleteByMap(Map<String,Object> parameters);
    
    /**
     * 
     *  @Description    : 单一主键对象查询
     *  @Method_Name    : findUniqueBy
     *  @param value
     *  @return 
     *  @return         : T
     *  @Creation Date  : 2015-4-5 下午8:23:56 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    @SuppressWarnings("hiding")
	<T> T findUniqueBy(Object value);
    
    /**
     * 
     *  @Description    : 联合主键对象查询
     *  @Method_Name    : findUniqueByMap
     *  @param parameters
     *  @return 
     *  @return         : T
     *  @Creation Date  : 2015-4-5 下午8:24:59 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    @SuppressWarnings("hiding")
   	<T> T findUniqueByMap(Map<String,Object> parameters);
    
    /**
     * 
     *  @Description    : 获取分页数据
     *  @Method_Name    : paging
     *  @param criteria
     *  @return 
     *  @return         : List<T>
     *  @Creation Date  : 2015-4-6 下午9:36:50 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    List<T> paging(Criteria criteria);
    
    /**
     * 
     *  @Description    : 获取总数
     *  @Method_Name    : pagingCount
     *  @param criteria
     *  @return 
     *  @return         : int
     *  @Creation Date  : 2015-4-6 下午9:38:50 
     *  @version        : v1.00
     *  @Author         : ljx
     */
    long pagingCount(Criteria criteria);
    
}
