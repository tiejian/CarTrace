﻿package com.xygl.core.exception;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.exception.CustomException.java
 *  @Class Name    : CustomException
 *  @Description   : 自定义异常
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:28:02 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
public class CustomException extends Exception {
	
	private static final long serialVersionUID = -7245324240254194102L;

	public CustomException() {
		super();
	}
	
	public CustomException(String msg) {
		super(msg);
	}
	
}
