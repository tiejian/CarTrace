package com.xygl.core.service;


import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.at.page.Criteria;
import com.at.page.DataGrid;
import com.xygl.core.dao.BaseDaoI;

/**
 * 
 * @Project : ccbs-wx-web
 * @Program Name : com.ccbs.core.service.BaseService.java
 * @Class Name : BaseService
 * @Description : service业务层接口父类
 * @Author : ljx
 * @Creation Date : 2015-4-18 下午2:36:23
 * 
 */
@Service
public class BaseService<T extends Serializable, PK extends Serializable> {
	
	@Autowired
	protected  BaseDaoI<T,PK> baseDaoI;

	/**
	 * 
	 *  @Description    : 单一主键对象查询
	 *  @Method_Name    : findUniqueBy
	 *  @param value
	 *  @return
	 *  @throws Exception 
	 *  @return         : T
	 *  @Creation Date  : 2015-5-27 下午4:39:36 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	@Transactional(readOnly = true)
	public <T> T findUniqueBy(Object value) throws Exception {
		return (T) baseDaoI.findUniqueBy(value);
	}

	/**
	 * 
	 *  @Description    : 添加对象
	 *  @Method_Name    : insert
	 *  @param value
	 *  @throws Exception 
	 *  @return         : void
	 *  @Creation Date  : 2015-5-27 下午4:41:09 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
	public void insert(Object value) throws Exception {
		baseDaoI.insert(value);
	}

	/**
	 * 
	 *  @Description    : 更新对象
	 *  @Method_Name    : update
	 *  @param value
	 *  @throws Exception 
	 *  @return         : void
	 *  @Creation Date  : 2015-5-27 下午4:41:50 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
	public void update(Object value) throws Exception {
		baseDaoI.update(value);
	}

	/**
	 * 
	 *  @Description    : 物理删除
	 *  @Method_Name    : delete
	 *  @param value
	 *  @throws Exception 
	 *  @return         : void
	 *  @Creation Date  : 2015-5-27 下午4:42:02 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
	public void delete(Object value) throws Exception {
		baseDaoI.delete(value);
	}

	/**
	 * 
	 *  @Description    : 逻辑删除
	 *  @Method_Name    : logicDelete
	 *  @param value
	 *  @throws Exception 
	 *  @return         : void
	 *  @Creation Date  : 2015-5-27 下午4:42:34 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class, readOnly = false)
	public void logicDelete(Object value) throws Exception {
		baseDaoI.logicDelete(value);
	}

	/**
	 * 
	 *  @Description    : 获取分页数据
	 *  @Method_Name    : paging
	 *  @param criteria
	 *  @return
	 *  @throws Exception 
	 *  @return         : DataGrid
	 *  @Creation Date  : 2015-5-27 下午4:43:08 
	 *  @version        : v1.00
	 *  @Author         : ljx
	 */
	@Transactional(readOnly = true)
	public DataGrid paging(Criteria criteria) throws Exception {
		DataGrid dg = new DataGrid();
		List<T> list = baseDaoI.paging(criteria);
		long count = baseDaoI.pagingCount(criteria);
		dg.setTotal(count);
		dg.setRows(list);
		return dg;
	}

}
