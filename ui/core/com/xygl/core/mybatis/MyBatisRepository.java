package com.xygl.core.mybatis;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

/**
 * 
 *  @Project       : xygl_pro
 *  @Program Name  : com.xygl.core.mybatis.MyBatisRepository.java
 *  @Class Name    : MyBatisRepository
 *  @Description   : 标识MyBatis的DAO,方便{@link org.mybatis.spring.mapper.MapperScannerConfigurer}的扫描
 *  @Author        : ljx
 *  @Creation Date : 2016-5-18 下午12:34:40 
 *  @ModificationHistory  
 *  Who        When          What 
 *  --------   ----------    -----------------------------------
 *  username   2016-5-18       TODO
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Component
public @interface MyBatisRepository {
	String value() default "";
}
