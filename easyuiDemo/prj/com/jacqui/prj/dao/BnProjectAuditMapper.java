package com.jacqui.prj.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.jacqui.prj.domain.BnProjectAudit;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.mybatis.MyBatisRepository;
/**
 * 
 * @ClassName: BnProjectAuditMapper  
 * @Description: 项目信息审核 
 * @author 白展堂
 *
 */
@MyBatisRepository
public interface BnProjectAuditMapper {
    /**
     * @Description 动态表名的分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<BnProjectAudit>
     */
    List<BnProjectAudit> paging4DynamicTblName(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 动态表名的分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount4DynamicTblName(Criteria criteria);
    /**
     * @Description 分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<BnProjectAudit>
     */
    List<BnProjectAudit> paging(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 用于全文检索的分页，需要指定查询字段
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<BnProjectAudit>
     */
    List<BnProjectAudit> paging4elastic(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 获取分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount(Criteria criteria);
    /**
     * @Description 根据条件查询
     * @param parameters
     * @return List<BnProjectAudit>
     */
    List<BnProjectAudit> findBy(Map<String, Object> parameters);
    /**
     * @Description 根据条件查询并返回唯一值
     * @param parameters
     * @return BnProjectAudit
     */
    BnProjectAudit findUniqueBy(Map<String, Object> parameters);
    
    /**
     * 查找最后一次院审通过记录
     * @param parameters
     * @return
     */
    BnProjectAudit findLastYuan(Map<String, Object> parameters);
    /**
     * @Description 插入记录
     * @param stUser
     * @return 返回插入影响的行数
     */
    int insert(BnProjectAudit bnProjectAudit);
    /**
     * @Description 更新记录
     * @param BnProjectAudit
     * @return 返回更新影响的行数
     */
    int update(BnProjectAudit bnProjectAudit);
    /**
     * @Description 根据主键删除
     * @param id
     */
    void deleteById(String id);
    /**
     * @Description 批量删除
     * @param parameters 批量删除条件组合
     */
    void deleteBy(Map<String, Object> parameters);
}