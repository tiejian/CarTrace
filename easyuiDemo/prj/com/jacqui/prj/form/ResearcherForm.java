package com.jacqui.prj.form;

public class ResearcherForm {
	
	private String reName;
	private String reGender;
    private String reAge;
    private String reDegree;
    private String reJobTitle;
    private String reJobSubject;
    private String mainTask;
	public String getReName() {
		return reName;
	}
	public void setReName(String reName) {
		this.reName = reName;
	}
	public String getReGender() {
		return reGender;
	}
	public void setReGender(String reGender) {
		this.reGender = reGender;
	}
	public String getReAge() {
		return reAge;
	}
	public void setReAge(String reAge) {
		this.reAge = reAge;
	}
	public String getReDegree() {
		return reDegree;
	}
	public void setReDegree(String reDegree) {
		this.reDegree = reDegree;
	}
	public String getReJobTitle() {
		return reJobTitle;
	}
	public void setReJobTitle(String reJobTitle) {
		this.reJobTitle = reJobTitle;
	}
	public String getReJobSubject() {
		return reJobSubject;
	}
	public void setReJobSubject(String reJobSubject) {
		this.reJobSubject = reJobSubject;
	}
	public String getMainTask() {
		return mainTask;
	}
	public void setMainTask(String mainTask) {
		this.mainTask = mainTask;
	}

}
