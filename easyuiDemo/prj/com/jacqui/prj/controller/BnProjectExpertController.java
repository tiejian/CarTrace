package com.jacqui.prj.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.admin.service.DtDepartmentService;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.prj.domain.RlProjectExpert;
import com.jacqui.prj.service.BnExpertService;
import com.jacqui.prj.service.BnProjectInfoService;
import com.jacqui.prj.service.RlProjectExpertService;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping(value = "/bn/project/expert")
public class BnProjectExpertController {
	
	@Autowired
	public BnProjectInfoService bnProjectInfoService;
	
	@Autowired
	public BnExpertService bnExpertService;
	
	@Autowired
	public DtDepartmentService dtDepartmentService;
	
	@Autowired
	public RlProjectExpertService rlProjectExpertService;
	
	@RequestMapping(value = "prj/search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "prj/project_expert_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "prj/search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<BnProjectInfo> retObject = new PageResult<>();
		retObject = bnProjectInfoService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		List<BnProjectInfo> list= retObject.getList();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", retObject.getList());
		map.put("total", retObject.getTotalCount());
		map.put("pageCurrent", page);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	@RequestMapping(value = "assign",params="_m=init")
	public String asignInit(@RequestParam String prjid,Model model,ServletRequest request) {
		model.addAttribute("prjid", prjid);
		return "prj/project_assign_expert";
	} 
	@RequestMapping(value = "assign",params="_m=exec")
	@ResponseBody
	public Object asignExec(@RequestParam String prjid,@RequestParam String userid,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("prjid", prjid);
			RlProjectExpert rlProjectExpert = new RlProjectExpert();
			rlProjectExpert.setPrjId(prjid);
			rlProjectExpert.setExpertId(userid);
			rlProjectExpertService.insert(rlProjectExpert);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	@RequestMapping(value = "unassign",params="_m=exec")
	@ResponseBody
	public Object unasign(@RequestParam String prjid,@RequestParam String userid,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("prjid", prjid);
			parameters.put("staffId", userid);
			rlProjectExpertService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	
	

}
