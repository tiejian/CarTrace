package com.jacqui.prj.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Maps;
import com.jacqui.admin.domain.StRole;
/**
 * 
 * @ClassName: BnExpert  
 * @Description: 专家信息 
 * @author 白展堂
 * 
 */
public class BnExpert implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //id
    
    private String name; //姓名
    
    private String idcard; //身份证号
    
    private String degree; //学位
    
    private String jobTitle; //职称
    
    private String jobPost; //职务
    
    private int expertType; //专家类型
    
    private String expertTypeDesc;
    
    private String unitName; //企业（学校）名称
    
    private int userType;
    
    private String userTypeDesc;
    
    private String jobSubject;
    
    private String passwd;
    
    private String department;
    
    private List<StRole> roles;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getIdcard(){
    	return idcard;
    }
    public void setIdcard(String idcard){
        this.idcard = idcard;
    }
    public String getDegree(){
    	return degree;
    }
    public void setDegree(String degree){
        this.degree = degree;
    }
    public String getJobTitle(){
    	return jobTitle;
    }
    public void setJobTitle(String jobTitle){
        this.jobTitle = jobTitle;
    }
    public String getJobPost(){
    	return jobPost;
    }
    public void setJobPost(String jobPost){
        this.jobPost = jobPost;
    }
    public int getExpertType(){
    	return expertType;
    }
    public void setExpertType(int expertType){
        this.expertType = expertType;
    }
    public String getUnitName(){
    	return unitName;
    }
    public void setUnitName(String unitName){
        this.unitName = unitName;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "id");
		labels.put("name", "姓名");
		labels.put("idcard", "身份证号");
		labels.put("degree", "学位");
		labels.put("jobTitle", "职称");
		labels.put("jobPost", "职务");
		labels.put("expertType", "专家类型");
		labels.put("unitName", "企业（学校）名称");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public String getJobSubject() {
		return jobSubject;
	}
	public void setJobSubject(String jobSubject) {
		this.jobSubject = jobSubject;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
//		this.passwd = passwd;
		this.passwd = StringUtils.isNotBlank(passwd)?DigestUtils.md5Hex(passwd):passwd;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getExpertTypeDesc() {
		if(expertType==1){
			return "企业";
		}else{
			return "教职人员";
		}
	}
	public void setExpertTypeDesc(String expertTypeDesc) {
		this.expertTypeDesc = expertTypeDesc;
	}
	public String getUserTypeDesc() {
		if(userType==1){
			return "专家";
		}else{
			return "非专家";
		}
	}
	public void setUserTypeDesc(String userTypeDesc) {
		this.userTypeDesc = userTypeDesc;
	}
	public List<StRole> getRoles() {
		return roles;
	}
	public void setRoles(List<StRole> roles) {
		this.roles = roles;
	}
}