package com.jacqui.prj.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnResearcher  
 * @Description: 项目主要人员 
 * @author 白展堂
 * 
 */
public class BnResearcher implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //id
    
    private String name; //姓名
    
    private String idcard; //身份证号
    
    private int gender;
    
    private int age;
    
    private String degree; //学位
    
    private String jobTitle; //职称
    
    private String jobPost; //职务
    
    private String jobSubject; //从事专业
    
    private String mainTask; //从事专业
    
    private String college; //学校名称
    
    private String branch; //所在院部
    
    private String prjId;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getIdcard(){
    	return idcard;
    }
    public void setIdcard(String idcard){
        this.idcard = idcard;
    }
    public String getDegree(){
    	return degree;
    }
    public void setDegree(String degree){
        this.degree = degree;
    }
    public String getJobTitle(){
    	return jobTitle;
    }
    public void setJobTitle(String jobTitle){
        this.jobTitle = jobTitle;
    }
    public String getJobPost(){
    	return jobPost;
    }
    public void setJobPost(String jobPost){
        this.jobPost = jobPost;
    }
    public String getJobSubject(){
    	return jobSubject;
    }
    public void setJobSubject(String jobSubject){
        this.jobSubject = jobSubject;
    }
    public String getCollege(){
    	return college;
    }
    public void setCollege(String college){
        this.college = college;
    }
    public String getBranch(){
    	return branch;
    }
    public void setBranch(String branch){
        this.branch = branch;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "id");
		labels.put("name", "姓名");
		labels.put("idcard", "身份证号");
		labels.put("degree", "学位");
		labels.put("jobTitle", "职称");
		labels.put("jobPost", "职务");
		labels.put("jobSubject", "从事专业");
		labels.put("college", "学校名称");
		labels.put("branch", "所在院部");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getMainTask() {
		return mainTask;
	}
	public void setMainTask(String mainTask) {
		this.mainTask = mainTask;
	}
	public String getPrjId() {
		return prjId;
	}
	public void setPrjId(String prjId) {
		this.prjId = prjId;
	}
}