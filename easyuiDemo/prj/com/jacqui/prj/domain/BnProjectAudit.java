package com.jacqui.prj.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnProjectAudit  
 * @Description: 项目信息审核 
 * @author 白展堂
 * 
 */
public class BnProjectAudit implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //id
    
    private String prjId; //项目编号
    
    private String auditSuggest; //审核意见
    
    private Date createAt; //审核时间
    
    private String createAtStr;//审核时间-对应的字符型日期
    private String createBy; //审核人
    
    private int status; //状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
    
    private BnExpert bnExpert;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getPrjId(){
    	return prjId;
    }
    public void setPrjId(String prjId){
        this.prjId = prjId;
    }
    public String getAuditSuggest(){
    	return auditSuggest;
    }
    public void setAuditSuggest(String auditSuggest){
        this.auditSuggest = auditSuggest;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public int getStatus(){
    	return status;
    }
    public void setStatus(int status){
        this.status = status;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "id");
		labels.put("prjId", "项目编号");
		labels.put("auditSuggest", "审核意见");
		labels.put("createAt", "审核时间");
		labels.put("createBy", "审核人");
		labels.put("status", "状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public BnExpert getBnExpert() {
		return bnExpert;
	}
	public void setBnExpert(BnExpert bnExpert) {
		this.bnExpert = bnExpert;
	}
}