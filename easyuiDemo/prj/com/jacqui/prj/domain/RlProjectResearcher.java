package com.jacqui.prj.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: RlProjectResearcher  
 * @Description: 项目主要人员关系 
 * @author 白展堂
 * 
 */
public class RlProjectResearcher implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //id
    
    private String prjNo; //项目编号
    
    private String researcherId; //
    
    private String mainTask; //
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getPrjNo(){
    	return prjNo;
    }
    public void setPrjNo(String prjNo){
        this.prjNo = prjNo;
    }
    public String getResearcherId(){
    	return researcherId;
    }
    public void setResearcherId(String researcherId){
        this.researcherId = researcherId;
    }
    public String getMainTask(){
    	return mainTask;
    }
    public void setMainTask(String mainTask){
        this.mainTask = mainTask;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "id");
		labels.put("prjNo", "项目编号");
		labels.put("researcherId", "");
		labels.put("mainTask", "");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}