package com.jacqui.prj.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: RlProjectExpert  
 * @Description: 项目分配专家 
 * @author 白展堂
 * 
 */
public class RlProjectExpert implements Serializable{

    private static final long serialVersionUID = 1L;

    private String prjId; //项目ID
    
    private String expertId; //专家ID
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getPrjId(){
    	return prjId;
    }
    public void setPrjId(String prjId){
        this.prjId = prjId;
    }
    public String getExpertId(){
    	return expertId;
    }
    public void setExpertId(String expertId){
        this.expertId = expertId;
    }
	public Map<String, String> getLabels() {
		labels.put("prjId", "项目ID");
		labels.put("expertId", "专家ID");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}