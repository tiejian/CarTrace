package com.jacqui.admin.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.admin.domain.RlRoleMenu;
import com.jacqui.admin.domain.RlRoleUser;
import com.jacqui.admin.domain.StRole;
import com.jacqui.admin.service.RlRoleMenuService;
import com.jacqui.admin.service.RlRoleUserService;
import com.jacqui.admin.service.StRoleService;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.web.BaseController;
/**
 * 
 * @ClassName: StRoleController 
 * @Description: 系统角色信息表 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/st/role")
public class StRoleController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(StRoleController.class);
	
	@Autowired
	public StRoleService stRoleService;
	
	@Autowired
	public RlRoleUserService rlRoleUserService;
	
	@Autowired
	public RlRoleMenuService rlRoleMenuService;
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "admin/role_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<StRole> retObject = new PageResult<>();
		retObject = stRoleService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		List<StRole> list= retObject.getList();
		return super.paging(list, retObject.getTotalCount(), page);
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<StRole> list = Lists.newArrayList();
		list = stRoleService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		StRole stRole = stRoleService.findUniqueBy(parameters);
		return stRole;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "admin/role_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,StRole stRole,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			stRoleService.insert(stRole);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(@RequestParam String code,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("code", code);
		StRole role = stRoleService.findUniqueBy(parameters);
		model.addAttribute("role", role);
		return "admin/role_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,StRole stRole,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			stRoleService.update(stRole);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	@RequestMapping(value = "assign/user",params="_m=init")
	public String asignUserInit(@RequestParam String code,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("code", code);
		model.addAttribute("code", code);
		return "admin/role_assign_user";
	} 
	@RequestMapping(value = "assign/user",params="_m=exec")
	@ResponseBody
	public Object asignUser(@RequestParam String code,@RequestParam String userid,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("code", code);
			RlRoleUser rlRoleUser = new RlRoleUser();
			rlRoleUser.setRoleCode(code);
			rlRoleUser.setStaffId(userid);
			rlRoleUserService.insert(rlRoleUser);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	@RequestMapping(value = "unassign/user",params="_m=exec")
	@ResponseBody
	public Object unasignUser(@RequestParam String code,@RequestParam String userid,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("roleCode", code);
			parameters.put("staffId", userid);
			rlRoleUserService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	@RequestMapping(value = "assign/menu",params="_m=init")
	public String asignMenuInit(@RequestParam String code,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("code", code);
		model.addAttribute("code", code);
		return "admin/role_assign_menu";
	} 
	@RequestMapping(value = "assign/menu",params="_m=exec")
	@ResponseBody
	public Object asignMenu(@RequestParam String code,@RequestParam String menucode,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("code", code);
			RlRoleMenu rlRoleMenu = new RlRoleMenu();
			rlRoleMenu.setRoleCode(code);
			rlRoleMenu.setMenuCode(menucode);
			rlRoleMenuService.insert(rlRoleMenu);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	@RequestMapping(value = "unassign/menu",params="_m=exec")
	@ResponseBody
	public Object unasignMenu(@RequestParam String code,@RequestParam String menucode,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			parameters.put("roleCode", code);
			parameters.put("menuCode", menucode);
			rlRoleMenuService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json; 
	} 
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,StRole stRole,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			stRoleService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			stRoleService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
