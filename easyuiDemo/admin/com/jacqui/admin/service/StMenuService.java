package com.jacqui.admin.service;



import java.text.ParseException;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import com.jacqui.admin.dao.StMenuMapper;
import com.jacqui.admin.domain.StMenu;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.persistence.Criterion;
import com.jacqui.core.persistence.DynamicSpecifications;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.form.PageResult;
import com.jacqui.service.BaseService;

import com.google.common.collect.Maps;

/**
 * 
 * @ClassName: StMenuService   
 * @Description: 系统菜单信息表 
 * @author 白展堂
 
 */
@Service
public class StMenuService extends BaseService{

   private static Logger logger = LoggerFactory.getLogger(StMenuService.class);
	
	@Autowired
	public StMenuMapper stMenuMapper;
	
	
	/**
	 * 
	 * @Title: paging4DynamicTblName 
	 * @Description: 表名为动态参数的分页查询
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<StMenu> paging4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities,Integer page,Integer limit) {
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,tblName);
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return stMenuMapper.paging4DynamicTblName(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount4DynamicTblName 
	 * @Description: <p>表名为动态参数的总记录数查询，当数据量大的时候原有分页带有的总记录数方法会很消耗性能</p>
	 * <p>可通过此方法单独计算总记录数</p>
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities) {
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,tblName);
		return stMenuMapper.pagingCount4DynamicTblName(criteria);
	}
	/**
	 * 
	 * @Title: paging 
	 * @Description: 表名固定的分页方法 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<StMenu> paging(List<SearchFilter> filters,Integer page,Integer limit) {
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"st_menu");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return stMenuMapper.paging(criteria,pageBounds);
	}
	/**
	 * @Title: pagingExt 
	 * @Description: 表名固定的分页方法，直接返回总记录数, 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型
	 * @throws ParseException
	 */
	public PageResult pagingWithTotal(List<SearchFilter> filters,Integer page,Integer limit){
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"st_menu");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		List<StMenu> list = stMenuMapper.paging(criteria,pageBounds);
		PageList<StMenu> pageList = (PageList)list;
		PageResult result = new PageResult();
		result.setList(pageList);
		result.setTotalCount(pageList.getPaginator().getTotalCount());
		result.setTotalPage(pageList.getPaginator().getTotalPages());
		return result;
	}
	/**
	 * 
	 * @Title: paging4elastic 
	 * @Description: 全文检索的分页方法
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<StMenu> paging4elastic(List<SearchFilter> filters,Integer page,Integer limit) throws ParseException{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"st_menu");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return stMenuMapper.paging4elastic(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount 
	 * @Description: 表名固定的记录总数方法
	 * @param filters
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount(List<SearchFilter> filters) {
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"st_menu");
		return stMenuMapper.pagingCount(criteria);
	}
	/**
	 * 
	 * @Title: findBy 
	 * @Description: 根据属性查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<StMenu> findBy(Map<String, Object> parameters) {
		return stMenuMapper.findBy(parameters);
	}
	public List<StMenu> findenable(Map<String, Object> parameters) {
		return stMenuMapper.findenable(parameters);
	}
	public List<StMenu> finddisable(Map<String, Object> parameters) {
		return stMenuMapper.finddisable(parameters);
	}
    /**
	 * 
	 * @Title: findBy 
	 * @Description: 根据主键查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
    public StMenu findUniqueById(String id){
        HttpServletRequest request = getHttpServletRequest();
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("id",id);
        //parameters.put("__user__",user.getCertificateNum());
        //parameters.put("__tblName__","st_menu");
        //parameters.put("xzq",user.getDistrictCode());
		return stMenuMapper.findUniqueBy(parameters);
	}
	/**
     * @Title: findUniqueById 
     * @Description: 根据属性查询
     * @param id
     * @return DsjSkrkxx    返回类型 
     */
    public StMenu findUniqueBy(Map<String, Object> parameters){
		return stMenuMapper.findUniqueBy(parameters);
	}
    /**
     * @Title: insert 
     * @Description: 插入操作
     * @param dsjSkrkxx
     * @return String    返回类型 
     */
    public String insert(StMenu stMenu){
    	stMenuMapper.insert(stMenu);
		return  String.valueOf(stMenu.getId());
	}
    /**
     * @Title: update 
     * @Description: 更新操作 
     * @param dsjSkrkxx
     */
    public void update(StMenu stMenu){
		stMenuMapper.update(stMenu);
	}
    /**
     * @Title: deleteById 
     * @Description: 根据主键删除
     * @param id
     */
    public void deleteById(String id){
		stMenuMapper.deleteById(id);
	}
    /**
     * @Title: deleteBy
     * @Description: 根据属性删除
     * @param id
     */
    public void deleteBy(Map<String, Object> parameters){
		stMenuMapper.deleteBy(parameters);
	}
	/**
	 * @Title: getHttpServletRequest
     * @Description: 可以在service层获取request,仅限地不使用dubbo的情况。使用dubbo此方法无用
     * @return void    返回类型 
	 */
	private static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}
}
