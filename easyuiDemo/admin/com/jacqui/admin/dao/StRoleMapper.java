package com.jacqui.admin.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.jacqui.admin.domain.StRole;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.mybatis.MyBatisRepository;
/**
 * 
 * @ClassName: StRoleMapper  
 * @Description: 系统角色信息表 
 * @author 白展堂
 *
 */
@MyBatisRepository
public interface StRoleMapper {
    /**
     * @Description 动态表名的分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<StRole>
     */
    List<StRole> paging4DynamicTblName(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 动态表名的分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount4DynamicTblName(Criteria criteria);
    /**
     * @Description 分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<StRole>
     */
    List<StRole> paging(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 用于全文检索的分页，需要指定查询字段
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<StRole>
     */
    List<StRole> paging4elastic(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 获取分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount(Criteria criteria);
    /**
     * @Description 根据条件查询
     * @param parameters
     * @return List<StRole>
     */
    List<StRole> findBy(Map<String, Object> parameters);
    /**
     * @Description 根据条件查询并返回唯一值
     * @param parameters
     * @return StRole
     */
    StRole findUniqueBy(Map<String, Object> parameters);
    /**
     * @Description 插入记录
     * @param stUser
     * @return 返回插入影响的行数
     */
    int insert(StRole stRole);
    /**
     * @Description 更新记录
     * @param StRole
     * @return 返回更新影响的行数
     */
    int update(StRole stRole);
    /**
     * @Description 根据主键删除
     * @param id
     */
    void deleteById(String id);
    /**
     * @Description 批量删除
     * @param parameters 批量删除条件组合
     */
    void deleteBy(Map<String, Object> parameters);
}