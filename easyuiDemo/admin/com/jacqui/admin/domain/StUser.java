package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: StUser  
 * @Description: 系统用户 
 * @author 白展堂
 * 
 */
public class StUser implements Serializable{

    private static final long serialVersionUID = 1L;

    private String staffCode; //员工ID
    
    private String passwd; //登录密码
    
    private int passwdLevel; //密码级别
    
    private int status; //状态0无效1有效
    
    private Date lockTime; //锁定时间
    
    private String lockTimeStr;//锁定时间-对应的字符型日期
    private int loginFailureCount; //登录失败次数
    
    private Date createAt; //创建时间
    
    private String createAtStr;//创建时间-对应的字符型日期
    private String createBy; //创建人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private String updateBy; //修改人
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getStaffCode(){
    	return staffCode;
    }
    public void setStaffCode(String staffCode){
        this.staffCode = staffCode;
    }
    public String getPasswd(){
    	return passwd;
    }
    public void setPasswd(String passwd){
        this.passwd = passwd;
    }
    public int getPasswdLevel(){
    	return passwdLevel;
    }
    public void setPasswdLevel(int passwdLevel){
        this.passwdLevel = passwdLevel;
    }
    public int getStatus(){
    	return status;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public Date getLockTime(){
    	return lockTime;
    }
    public void setLockTime(Date lockTime){
        this.lockTime = lockTime;
    }
    public String getLockTimeStr(){
    	return lockTimeStr;
    }
    public void setLockTimeStr(String lockTimeStr){
        this.lockTimeStr = lockTimeStr;
    }	
    public int getLoginFailureCount(){
    	return loginFailureCount;
    }
    public void setLoginFailureCount(int loginFailureCount){
        this.loginFailureCount = loginFailureCount;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
	public Map<String, String> getLabels() {
		labels.put("staffCode", "员工ID");
		labels.put("passwd", "登录密码");
		labels.put("passwdLevel", "密码级别");
		labels.put("status", "状态0无效1有效");
		labels.put("lockTime", "锁定时间");
		labels.put("loginFailureCount", "登录失败次数");
		labels.put("createAt", "创建时间");
		labels.put("createBy", "创建人");
		labels.put("updateAt", "修改时间");
		labels.put("updateBy", "修改人");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}