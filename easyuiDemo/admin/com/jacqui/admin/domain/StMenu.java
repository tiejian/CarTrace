package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: StMenu  
 * @Description: 系统菜单信息表 
 * @author 白展堂
 * 
 */
public class StMenu implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //主键
    
    private String name; //菜单名称
    
    private String code; //菜单代码
    
    private String menuLabel; //菜单标签
    
    private String menuAction; //动作
    
    private String rootCode; //根
    
    private String parentCode; //父级
    
    private int displayOrder; //显示顺序
    
    private int isDisplay; //是否显示0不显示1显示
    
    private String picUrl; //图片路径
    
    private int menuLevel; //级别
    
    private int isLeaf; //是否叶子节点0否1是
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getMenuLabel(){
    	return menuLabel;
    }
    public void setMenuLabel(String menuLabel){
        this.menuLabel = menuLabel;
    }
    public String getMenuAction(){
    	return menuAction;
    }
    public void setMenuAction(String menuAction){
        this.menuAction = menuAction;
    }
    public String getRootCode(){
    	return rootCode;
    }
    public void setRootCode(String rootCode){
        this.rootCode = rootCode;
    }
    public String getParentCode(){
    	return parentCode;
    }
    public void setParentCode(String parentCode){
        this.parentCode = parentCode;
    }
    public int getDisplayOrder(){
    	return displayOrder;
    }
    public void setDisplayOrder(int displayOrder){
        this.displayOrder = displayOrder;
    }
    public int getIsDisplay(){
    	return isDisplay;
    }
    public void setIsDisplay(int isDisplay){
        this.isDisplay = isDisplay;
    }
    public String getPicUrl(){
    	return picUrl;
    }
    public void setPicUrl(String picUrl){
        this.picUrl = picUrl;
    }
    public int getMenuLevel(){
    	return menuLevel;
    }
    public void setMenuLevel(int menuLevel){
        this.menuLevel = menuLevel;
    }
    public int getIsLeaf(){
    	return isLeaf;
    }
    public void setIsLeaf(int isLeaf){
        this.isLeaf = isLeaf;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "主键");
		labels.put("name", "菜单名称");
		labels.put("code", "菜单代码");
		labels.put("menuLabel", "菜单标签");
		labels.put("menuAction", "动作");
		labels.put("rootCode", "根");
		labels.put("parentCode", "父级");
		labels.put("displayOrder", "显示顺序");
		labels.put("isDisplay", "是否显示0不显示1显示");
		labels.put("picUrl", "图片路径");
		labels.put("menuLevel", "级别");
		labels.put("isLeaf", "是否叶子节点0否1是");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}