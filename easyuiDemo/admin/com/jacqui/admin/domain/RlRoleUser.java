package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: RlRoleUser  
 * @Description: 角色人员分配信息 
 * @author 白展堂
 * 
 */
public class RlRoleUser implements Serializable{

    private static final long serialVersionUID = 1L;

    private String roleCode; //角色代码
    
    private String staffId; //人员工号
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getRoleCode(){
    	return roleCode;
    }
    public void setRoleCode(String roleCode){
        this.roleCode = roleCode;
    }
    public String getStaffId(){
    	return staffId;
    }
    public void setStaffId(String staffId){
        this.staffId = staffId;
    }
	public Map<String, String> getLabels() {
		labels.put("roleCode", "角色代码");
		labels.put("staffId", "人员工号");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}