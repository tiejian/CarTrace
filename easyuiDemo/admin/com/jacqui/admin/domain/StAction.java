package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: StAction  
 * @Description: 系统行为列表 
 * @author 白展堂
 * 
 */
public class StAction implements Serializable{

    private static final long serialVersionUID = 1L;

    private String code; //代码
    
    private String name; //名称
    
    private String url; //动作
    
    private String descript; //描述
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getUrl(){
    	return url;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public String getDescript(){
    	return descript;
    }
    public void setDescript(String descript){
        this.descript = descript;
    }
	public Map<String, String> getLabels() {
		labels.put("code", "代码");
		labels.put("name", "名称");
		labels.put("url", "动作");
		labels.put("descript", "描述");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}