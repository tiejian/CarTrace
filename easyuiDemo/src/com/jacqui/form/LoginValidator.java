/*
 * 项目名：游戏监管信息服务平台
 * 包     名：com.reawin.system.form
 * 文件名：LoginValidator.java
 * 日     期：2011-4-25-上午09:08:39
 * CopyRight © 2010-2011 吉林睿网
 */
package com.jacqui.form;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * @author 孙飞
 *
 */
@Component
public class LoginValidator implements Validator{
	
	
//	@Autowired
//	public UserServiceProvider userServiceProvider;

	/* (non-Javadoc)
	 * @see org.springframework.validation.Validator#supports(java.lang.Class)
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		return LoginForm.class.isAssignableFrom(clazz);
	}

	/* (non-Javadoc)
	 * @see org.springframework.validation.Validator#validate(java.lang.Object, org.springframework.validation.Errors)
	 */
	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "username.required","请输入身份证号");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "password.required","请输入密码");
		LoginForm form=(LoginForm)target;
		if(StringUtils.isBlank(form.getViewtimes()))
			form.setViewtimes("1");
//		if(Integer.valueOf(form.getViewtimes())>1){
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "verCode", "verCode.required","请输入验证码");
//			if(null==form.getRandCode())
//				errors.rejectValue("verCode", "","验证码过期，请刷新后重新输入");
//			if(StringUtils.isNotBlank(form.getVerCode()) && !form.getVerCode().toUpperCase().equalsIgnoreCase(form.getRandCode().toUpperCase()))
//				errors.rejectValue("verCode","", "验证码错误，请重新输入");
//		}
		if(null!=form.getUsername()&&form.getUsername().length()>18)
			errors.rejectValue("username", "username.max","最大18个字符");
		if(null!=form.getPassword()&&form.getPassword().length()>64)
			errors.rejectValue("password", "password.max","最大64个字符");
//		if(StringUtils.isNotBlank(form.getIdCard()) && StringUtils.isNotBlank(form.getUserTypePwd())){
//			User user = userServiceProvider.getUserByNamePwd(form.getIdCard(), form.getUserTypePwd());
//			if(null==user){
//				errors.rejectValue("userTypePwd", "","用户名或密码错误");
//			}
//		}
		
	}

}
