package com.jacqui.form;

public class PasswordForm {
	
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;
	/** 
	 * @return oldPassword 
	 */
	public String getOldPassword() {
		return oldPassword;
	}
	/** 
	 * @param oldPassword 要设置的 oldPassword 
	 */
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	/** 
	 * @return newPassword 
	 */
	public String getNewPassword() {
		return newPassword;
	}
	/** 
	 * @param newPassword 要设置的 newPassword 
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	/** 
	 * @return confirmPassword 
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}
	/** 
	 * @param confirmPassword 要设置的 confirmPassword 
	 */
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
	

}
