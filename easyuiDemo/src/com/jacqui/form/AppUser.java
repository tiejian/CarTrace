package com.jacqui.form;

import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @ClassName: User 
 * @Description: 组织机构人员 
 * @author koudong
 * @date 2014-12-1 下午3:55:34 
 *
 */
public class AppUser implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 名字
	 */
	private String name;
	/**
	 * 登录名
	 */
	private String userName;
	/**
	 * 主岗部门
	 */
	private String orgDepartmentCode;
	/**
	 * 主岗单位
	 */
	private String orgAccountCode;
	/**
	 * 主岗单位名称
	 */
	private String orgAccountName;
	/**
	 * 主岗部门名称
	 */
	private String orgDepartmentName;
    /**
     * 数据范围	
     */
	private String xzq;
	/**
	 * 数据范围
	 */
	private String dataRange;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getOrgDepartmentCode() {
		return orgDepartmentCode;
	}
	public void setOrgDepartmentCode(String orgDepartmentCode) {
		this.orgDepartmentCode = orgDepartmentCode;
	}
	public String getOrgAccountCode() {
		return orgAccountCode;
	}
	public void setOrgAccountCode(String orgAccountCode) {
		this.orgAccountCode = orgAccountCode;
	}
	public String getOrgAccountName() {
		return orgAccountName;
	}
	public void setOrgAccountName(String orgAccountName) {
		this.orgAccountName = orgAccountName;
	}
	public String getOrgDepartmentName() {
		return orgDepartmentName;
	}
	public void setOrgDepartmentName(String orgDepartmentName) {
		this.orgDepartmentName = orgDepartmentName;
	}
	public String getXzq() {
		return xzq;
	}
	public void setXzq(String xzq) {
		this.xzq = xzq;
	}
	public String getDataRange() {
		return dataRange;
	}
	public void setDataRange(String dataRange) {
		this.dataRange = dataRange;
	}
	
	
	

}
