package com.jacqui.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * 
 * @author matt
 *
 */
public class AuthHandlerInterceptor extends HandlerInterceptorAdapter{

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
//		if(!isMobileDevice(request)){
//			return super.preHandle(request, response, handler);
//		}else{
//			response.getWriter().write("adfafafa");
//			return false;
//		}
		return super.preHandle(request, response, handler);
		
	}
	
	private boolean isMobileDevice(HttpServletRequest request){
		String requestHeader = request.getHeader("user-agent");
		String[] deviceArray = new String[]{"android","iphone","ipod","ipad","windows phone","mqqbrower","mac os"};
		requestHeader = requestHeader.toLowerCase();
		for(int i=0;i<deviceArray.length;i++){
			if(requestHeader.indexOf(deviceArray[i])>0){
				return true;
			}
		}
		return false;
	}
	
}
