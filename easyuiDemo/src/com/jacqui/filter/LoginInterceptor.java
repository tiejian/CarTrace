/*
 * 项目名：游戏监管信息服务平台
 * 包     名：com.reawin.web.interceptor
 * 文件名：LoginInterceptor.java
 * 日     期：2011-3-8-上午09:21:11
 * CopyRight © 2010-2011 吉林睿网
 */
package com.jacqui.filter;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

import com.jacqui.constant.Constant;
import com.jacqui.prj.domain.BnExpert;


/**
 * @author 孙飞
 *
 */
public class LoginInterceptor extends HandlerInterceptorAdapter{

	private static Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);
	@Override
	public void afterCompletion(HttpServletRequest request,HttpServletResponse response, Object handler, Exception ex)throws Exception {
//		logger.info("I am in LoginInterceptor afterCompletion");
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void postHandle(HttpServletRequest request,HttpServletResponse response, Object handler,ModelAndView modelAndView) throws Exception {
		logger.info(request.getRequestURI());
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request,HttpServletResponse response, Object handler) throws Exception {
//        logger.info("I am in LoginInterceptor preHandle");
//        logger.info(request.getContextPath());
//        logger.info(request.getRequestURI());
//        logger.info(request.getPathInfo());
//        logger.info(handler.getClass().getName());
        
        HttpServletRequest requestWrapper =null;
        String cur_contextPath = request.getContextPath();
		String cur_requestInfo = request.getRequestURI();
		String tmpUrl = cur_requestInfo.substring(cur_contextPath.length());
		BnExpert bnExpert = (BnExpert) WebUtils.getSessionAttribute(request,Constant.SESSION_USER_KEY);
		/**使用uploadify上传图片时，因为flash上传需要单独开一个session，所以上传是单独线程，需要根据jsessionid取得当前的session*/
		if(null==bnExpert){
			String jsessionid=request.getParameter("jsessionid");
			if(StringUtils.isNotBlank(jsessionid)){
				Map activeSessions = ActiveUserListener.getSessionMaps();
				HttpSession mySession = (HttpSession)activeSessions.get(jsessionid);
				bnExpert = (BnExpert) mySession.getAttribute(Constant.SESSION_USER_KEY);
			}
		}
        
        //对静态资源不进行拦截
        if(cur_requestInfo.indexOf("file/")>0||cur_requestInfo.indexOf("css/")>0||cur_requestInfo.indexOf("js/")>0||cur_requestInfo.indexOf("images/")>0||cur_requestInfo.indexOf("img/")>0)
        	return super.preHandle(requestWrapper, response, handler);    
        
        if(cur_requestInfo.equals(cur_contextPath+"/redirect/login"))
        	return super.preHandle(requestWrapper, response, handler);  
        
        if(cur_requestInfo.equals(cur_contextPath+"/login"))
        	return super.preHandle(requestWrapper, response, handler);  
        
        if(null==bnExpert){
        	ModelAndView modelAndView = new ModelAndView();
        	modelAndView.setViewName("welcome");
//        	modelAndView.addObject("siteLoginForm",new SiteLoginForm());
        	throw new ModelAndViewDefiningException(modelAndView);
        }else{        	
        	return super.preHandle(requestWrapper, response, handler);	
        }
	}
	
}
