package com.jacqui.controller;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Maps;
import com.jacqui.admin.domain.StMenu;
import com.jacqui.admin.domain.StRole;
import com.jacqui.constant.Constant;
import com.jacqui.form.LoginForm;
import com.jacqui.form.LoginValidator;
import com.jacqui.prj.domain.BnExpert;
import com.jacqui.prj.service.BnExpertService;

@Controller
public class LoginController {
	
	@Autowired
	public BnExpertService bnExpertService;
	
	
	@Autowired
	@Qualifier("loginValidator")
    protected LoginValidator loginValidator;
	
	
	private String sysCode="zhzs_yun";
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.loginValidator);
	}
	
	@RequestMapping(value = "redirect/login", method = RequestMethod.GET)
	public String redirect_to_login_page(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("loginForm", new LoginForm());
		model.addAttribute("key", "none");
		model.addAttribute("errorCount", 0);
		return "login";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String redirect_to_login_page(LoginForm loginForm,Model model,BindingResult result,HttpServletRequest request, HttpServletResponse response){
		String username = loginForm.getUsername();
		String password = loginForm.getPassword();
		
		loginValidator.validate(loginForm, result);
		
		if(result.hasErrors()){
			return "login";
		}
		
		BnExpert bnExpert= null;
		Map<String, Object> parameters = Maps.newHashMap();
		if(loginForm.getUsername().equals("admin")){
			parameters.put("idcard", username);
			bnExpert= bnExpertService.findUniqueBy(parameters);
		}else{
			parameters.put("idcard", username);
			parameters.put("passwd", password);
			bnExpert= bnExpertService.findUniqueBy(parameters);
		}
		if(null==bnExpert){
			result.rejectValue("BnExpertname", "","用户名或密码错误");
			return "login";
		}	
		////此位置在登录时判断session中是否有BnExpert信息，发生这种情况的登录后，刷新主页会出现重复提交的问题
		////为防止重复提交时被记录日志在此做逻辑处理，在登录时如里Constant.SESSION_ID_KEY不为空则是二次登录
		////此时不记录日志
		List<StRole> setRole = bnExpert.getRoles();
		Map<String,StMenu> mapMenu = new HashMap<String,StMenu>();
		for(StRole role:setRole){
			System.out.println(role.getName());
			List<StMenu> menus = role.getMenus();
			for(StMenu m:menus){
				mapMenu.put(m.getCode(),m);
			}
		}
		WebUtils.setSessionAttribute(request,  Constant.SESSION_MENU_KEY, mapMenu);
		BnExpert lastBnExpert = (BnExpert)WebUtils.getSessionAttribute(request, Constant.SESSION_USER_KEY);
		if(null == lastBnExpert){
			WebUtils.setSessionAttribute(request,  Constant.SESSION_USER_KEY, bnExpert)	;	
		}else{
			request.setAttribute(Constant.SESSION_ID_KEY,request.getSession().getId());
		}
		Collection<StMenu> menus=null;
		if(bnExpert.getName().equals("superAdmin")){
//			menus=menuManager.getMenuByBnExpert(BnExpert.getBnExpertName());
		}else{
			menus=new TreeSet<StMenu>(new Comparator<StMenu>(){
				public int compare(StMenu o1, StMenu o2) {
					return Integer.valueOf(o1.getDisplayOrder()).compareTo(Integer.valueOf(o2.getDisplayOrder()));
				}});
			for(StRole role : setRole){
				System.out.println(role.getName());
				List<StMenu> menusSet=role.getMenus();
				for(StMenu menu:menusSet){
					menus.add(menu);
				}
			}
		}
		
//		model.addAttribute("menus", menus);
	    return "redirect:/index";  
	}
	
	
	@RequestMapping(value="log/in",method=RequestMethod.POST)
	public String redirect_to_login_in(LoginForm loginForm,BindingResult result,Model model,HttpServletRequest request, HttpServletResponse response){
		return "redirect:/index"; 
	}

}
