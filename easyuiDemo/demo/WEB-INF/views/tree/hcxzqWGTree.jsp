<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
    var hytree;
	var setting = {
		async: {
			enable: true,
			url:"${ctx}/tree/hcxzq/load",
			autoParam:["code"]
		},
		check: {
			enable: true,
			chkStyle: "checkbox",
			chkboxType: { "Y": "s", "N": "p" }
		}
	};


	$(document).ready(function(){
		hytree = $.fn.zTree.init($("#hcXzqTree"), setting);
		$("#hybtn").bind("click",function(){
			var wgdm = $("#wgdm").val();
			var wgmc = $("#wgmc").val();
			var xzqvalue=new Array();
			var sNodes = hytree.getCheckedNodes(true);
			for (var i=0, l=sNodes.length; i<l; i++) {
				xzqvalue.push(sNodes[i].code);
			}
			setJgfwValue(xzqvalue.join(","),wgdm,wgmc);
		});

	});

	function setJgfwValue(xzq,wgdm,wgmc){
		$.ajax({
			url: '${ctx}/mgr/cell/jgfwWG/set?_m=exec&xzq='+xzq+'&wgdm='+wgdm+'&wgmc='+encodeURIComponent(escape(wgmc)),
			type:'get',
			success: function(data, status, xhr){
				if(data.errcode==0){
					layer.alert('操作成功', {
						icon: 1,
						zIndex:99999999,
						skin: 'layer-ext-moon',
						success:function(){
							initLoadJgfw1();
							initLoadJgfwView();
							
						}
					});
				}else{
					layer.alert(data.errmsg, {
						icon:5,
						zIndex:99999999,
						skin: 'layer-ext-moon' //该皮肤由layer.seaning.com友情扩展。关于皮肤的扩展规则，去这里查阅
					})
				}
			}
		});
	}
</script>
<div style="padding-left:10px; padding-top:10px;">
<span>网格代码:</span> <input style="width:300px" type='text' id='wgdm' value="${wgdm}" readonly='readonly'/>
</div>
<div style="padding-left:10px; padding-top:10px;">
<span>网格名称:</span> <input style="width:300px" type='text' id='wgmc' value="${wgmc}"/>
</div>
<div class="zTreeBackground ztreeleft">
	<ul id="hcXzqTree" class="ztree"></ul>
</div>
<div class="treebtn"><button class="btn btn-default onebtn" type="button" id="hybtn">确定</button></div>