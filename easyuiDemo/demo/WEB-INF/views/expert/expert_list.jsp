<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
		
		var expertlist = $('#expertlist').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		expertlist.datagrid('enableFilter', [
		   {
			field:'name',
			type:'text',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'expertType',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '企业',value: '1'},
					{label: '教职人员',value: '2'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btnexpertadd").bind("click",function(){
			$('<div></div>').dialog({
        	    title: '增加专家信息',
        	    width: 600,
        	    height:400,
        	    closed: false,
        	    cache: false,
        	    href: '${ctx}/bn/expert/insert?_m=init',
        	    modal: true,
        	    onClose : function() {
                    $(this).dialog('destroy');
                }
        	});
		});
    })
    function getsummary(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/expert/audit/prj/get?_m=exec&id="+id);
	}
	
    function transtype(value){
		switch(value)
		{
		case "1":
		    return "企业";
		    break;
		default:
			return "教职人员";		
		}
	}
    function transagefromidcard(value){
    	var date = new Date();
    	var year = date.getFullYear(); 
    	var birthday_year = parseInt(value.substr(6,4));
    	var userage= year - birthday_year;
    	return userage;
	}
    function transgenderfromidcard(value){
    	if (parseInt(value.substr(16, 1)) % 2 == 1) { 
   		   return "男";
   		} else { 
   			return "女";
   		} 
	}
	function updateexpert(id){
		$('<div></div>').dialog({
    	    title: '修改专家信息',
    	    width: 600,
    	    height:400,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/bn/expert/update?_m=init&id='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function delexpert(id){
		$.ajax({
			url : '${ctx}/bn/expert/delete?_m=exec&id=' + id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','已删除!','info',refresh());
				}else{
					$.messager.alert('提示信息','删除失败!','error');
				}
			}
		});
	}
	function refresh(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/expert/search?_m=init");
	}
</script>
<table id="expertlist"  style="width:100%;height:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,toolbar:'#experttb',url:'${ctx}/bn/expert/search?_m=load',method:'get',queryParams:{filter_EQ_user_type:'1'}">
	<thead>
		<tr>
			<th data-options="field:'name',width:100">名称</th>
			<th data-options="field:'idcard',width:200">身份证号</th>
			<th data-options="field:'xb',width:140,align:'center',formatter:function(value,row,index){return transgenderfromidcard(row.idcard);}">性别</th>
			<th data-options="field:'age',width:140,align:'center',formatter:function(value,row,index){return transagefromidcard(row.idcard);}">年龄</th>
			<th data-options="field:'degree',width:140,align:'center'">学位</th>
			<th data-options="field:'jobTitle',width:140,align:'center'">职称</th>
			<th data-options="field:'jobPost',width:140,align:'center'">职务</th>
			<th data-options="field:'expertType',width:200,formatter:function(value,row,index){return transtype(value);}">专家类型</th>
			<th data-options="field:'unitName',width:250,align:'center'">企业（学校）名称</th>
			<th data-options="field:'t',width:150,formatter:function(value,row,index){return '<a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:updateexpert(\''+row.id+'\');&quot;>修改</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:delexpert(\''+row.id+'\');&quot;>删除</a>'}"></th>
		</tr>
	</thead>
</table>
<div id="experttb" style="padding:2px 5px;">
	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" id="btnexpertadd">增加</a>
</div>
<div id="f_expertopera" style="width:500px;z-index: 9999;display:none;"></div>