<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/material/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/material/easyui-form-table.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/icon.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/css/font-awesome-4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/sq.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/css/form/formplate1.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.ext.css">
	<link rel="stylesheet" type="text/css" href="../demo.css">
	<script type="text/javascript" src="${ctx}/lib/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/lib/easyui/ext/datagrid-filter.js"></script>
    <script type="text/javascript" src="${ctx}/lib/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/lib/form/form.js"></script>
    <script type="text/javascript" src="${ctx}/lib/layer/layer.js"></script>
    
    <script type="text/javascript">
	    $(function() {
	    	setTabs();
	    	homepage();
	    })
		
	</script>
</head>
<body class="easyui-layout mainpage">
    <!--[if lte IE 7]>
        <div id="errorie"><div>您还在使用老掉牙的IE，正常使用系统前请升级您的浏览器到 IE8以上版本 <a target="_blank" href="http://windows.microsoft.com/zh-cn/internet-explorer/ie-8-worldwide-languages">点击升级</a>&nbsp;&nbsp;强烈建议您更改换浏览器：<a href="http://down.tech.sina.com.cn/content/40975.html" target="_blank">谷歌 Chrome</a></div></div>
    <![endif]-->
    <div data-options="region:'north',border:false,maxHeight:50" class="maintop">XXXXXX系统</div>
	<div data-options="region:'west',split:true,border:false,title:'==功能列表==',headerCls:'main-head-left',bodyCls:'unit-admin-menu-body',cls:'unit-admin-menu'" style="width:220px;padding:0px;text-align:center;">
		<div id="cc" class="easyui-calendar" style="width:100%;height:180px;border:0;"></div>
		<div class="easyui-accordion left-menu role-unit-admin-leftmenu" style="width:100%;" data-options="multiple:true">
			<div title="项目管理" data-options="iconCls:'icon-project',selected:true,collapsed:true,collapsible:true" style="">
			    <ul class="easyui-tree" data-options="onClick:function(node){mainload(node);}">
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/bn/project/bn_project_info/search?_m=init'}" style="padding:10px;">项目申报</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/yuan/audit/prj/search?_m=init'}" style="padding:10px;">院部评审</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/bn/project/expert/prj/search?_m=init'}" style="padding:10px;">指定评审专家</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/expert/audit/prj/search?_m=init'}" style="padding:10px;">专家评审</li>
				</ul>
			</div>
			<div title="系统管理" data-options="iconCls:'icon-project',selected:true,collapsed:false,collapsible:false" style="">
			    <ul class="easyui-tree" data-options="onClick:function(node){mainload(node);}">
			        <li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/bn/expert/search?_m=init'}" style="padding:10px;">专家库管理</li>
			        <li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/dt/department/search?_m=init'}" style="padding:10px;">院系管理</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/bn/customer/search?_m=init'}" style="padding:10px;">用户管理</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/st/role/search?_m=init'}" style="padding:10px;">角色管理</li>
				</ul>
			</div> 
			<div title="例子" data-options="iconCls:'icon-project',collapsed:false,collapsible:false" style="">
			    <ul class="easyui-tree" data-options="onClick:function(node){mainload(node);}">
			        <li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/demo/list'}" style="padding:10px;">表格</li>
			        <li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/demo/form'}" style="padding:10px;">表单</li>
			        <li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/homepage?_m=init'}" style="padding:10px;">服务单位首页</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/bn/customer/search?_m=init'}" style="padding:10px;">用户管理</li>
					<li data-options="iconCls:'icon-right-blue-arrow',attributes:{url:'${ctx}/st/role/search?_m=init'}" style="padding:10px;">角色管理</li>
				</ul>
			</div>
			
		</div>
	</div>
	<div data-options="region:'south',border:false" style="height:24px;background:#EBF7FD;padding:10px;">south region</div>
	<div data-options="region:'center',border:false,">
		<div class="easyui-tabs main-tab"   style="width:100%;height:100%;paddig:10px;" id="main-tab">
			<div title="我的主页"  data-options="height:27" id="homepage">
			</div>
		</div>
	</div>
    <script type="text/javascript">
		function setTabs(){
			$('#main-tab').tabs({
				plain:true,
				narrow: true,
				border:false,
				tabHeight:26
			})
		}
		function mainload(node){
			if($('#main-tab').tabs('exists',node.text)){
				$('#main-tab').tabs('select',node.text);
				var tab = $('#main-tab').tabs('getSelected');
				tab.panel('refresh', node.attributes['url']);
			}else{
				$('#main-tab').tabs('add',{
					id:'1',
					title: node.text,
					href: node.attributes['url'],
					closable: true
				});
			}
		}
		function homepage(){
			var tab = $('#main-tab').tabs('getSelected');
			tab.panel('refresh', "${ctx}/homepage");
		}
		
	</script>
</body>
</html>
