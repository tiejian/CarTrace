<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
		
		var deptlist = $('#deptlist').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		deptlist.datagrid('enableFilter', [
		   {
			field:'name',
			type:'text',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'deptType',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '企业',value: '1'},
					{label: '教职人员',value: '2'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btndeptadd").bind("click",function(){
			$('<div></div>').dialog({
        	    title: '增加院系信息',
        	    width: 600,
        	    height:300,
        	    closed: false,
        	    cache: false,
        	    href: '${ctx}/dt/department/insert?_m=init',
        	    modal: true,
        	    onClose : function() {
                    $(this).dialog('destroy');
                }
        	});
		});
    })
    function getsummary(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/dept/audit/prj/get?_m=exec&id="+id);
	}
	
    function transtype(value){
		switch(value)
		{
		case "1":
		    return "企业";
		    break;
		default:
			return "教职人员";		
		}
	}
    function transagefromidcard(value){
    	var date = new Date();
    	var year = date.getFullYear(); 
    	var birthday_year = parseInt(value.substr(6,4));
    	var userage= year - birthday_year;
    	return userage;
	}
    function transgenderfromidcard(value){
    	if (parseInt(value.substr(16, 1)) % 2 == 1) { 
   		   return "男";
   		} else { 
   			return "女";
   		} 
	}
	function updatedept(id){
		$('<div></div>').dialog({
    	    title: '修改院系信息',
    	    width: 600,
    	    height:300,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/dt/department/update?_m=init&dm='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function deldept(id){
		$.ajax({
			url : '${ctx}/dt/department/delete?_m=exec&dm=' + id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','已删除!','info',refresh());
				}else{
					$.messager.alert('提示信息','删除失败!','error');
				}
			}
		});
	}
	function refresh(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/dt/department/search?_m=init");
	}
</script>
<table id="deptlist"  style="width:100%;height:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,toolbar:'#depttb',url:'${ctx}/dt/department/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'dm',width:120">代码</th>
			<th data-options="field:'mc',width:300">名称</th>
			<th data-options="field:'t',width:150,formatter:function(value,row,index){return '<a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:updatedept(\''+row.dm+'\');&quot;>修改</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:deldept(\''+row.dm+'\');&quot;>删除</a>'}"></th>
		</tr>
	</thead>
</table>
<div id="depttb" style="padding:2px 5px;">
	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" id="btndeptadd">增加</a>
</div>
<div id="f_deptopera" style="width:500px;z-index: 9999;display:none;"></div>