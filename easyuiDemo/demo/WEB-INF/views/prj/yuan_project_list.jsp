<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
		var yuanprjlist = $('#yuanprjlist').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true,
			onLoadSuccess: function(data){
				//全部字段
				//var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field] div.datagrid-cell:not(:empty) a');
				//特定字段
				var cells = $(this).datagrid('getPanel').find('div.datagrid-body td[field="status"] div.datagrid-cell:not(:empty) a');
				cells.tooltip({
					 content: $('<div></div>'),
				     onUpdate: function(content){
					    var prjid = $(this).attr("prjid");
					    content.panel({
							width: 400,
							border: false,
							title: '项目审核记录',
							href: '${ctx}/bn/project/findpage?_m=exec&prjid='+prjid
						});
					 },
					 showEvent:'click',
					 onShow: function(){
						var t = $(this);
						t.tooltip('tip').unbind().bind('mouseenter', function(){
							t.tooltip('show');
						}).bind('mouseleave', function(){
							t.tooltip('hide');
						});
					 }
				 });
			}
		});
		yuanprjlist.datagrid('enableFilter', [
		   {
			field:'prjName',
			type:'text',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'prjStart',
			type:'datebox',
			op:['greaterorequal']
		   },
		   {
			field:'prjEnd',
			type:'datebox',
			op:['lessorequal']
		   },
		   {
			field:'status',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '待审',value: '1'},
					{label: '院审通过并提交专家审核',value: '2'},
					{label: '院审未通过',value: '3'},
					{label: '院部终止',value: '4'},
					{label: '专家审核通过',value: '5'},
					{label: '专家审核未通过',value: '6'},
					{label: '专家终止',value: '7'}
				]
		    },
			op:['equal']
		   }
		   
		 ]);
    })
    function getsummary(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/yuan/audit/prj/get?_m=exec&id="+id);
	}
	
    function transstatus(id,value){
		//状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
    	var processing="";
		switch(value)
		{
		case 1:
			processing= "草稿";
		    break;
		case 2:
			processing= "待审";
		    break;
		case 3:
			processing= "院审通过并提交专家审核";
		    break;
		case 4:
			processing= "院审未通过";
		    break;
		case 5:
			processing= "院部终止";
		    break;
		case 6:
			processing= "专家审核通过";
		    break;
		case 7:
			processing= "专家审核未通过";
		    break;
		case 8:
			processing= "专家终止";
		    break;
		default:
			processing= "删除";		
		}
		return "<a class='a-btn easyui-tooltip' href='javascript:void(0);' prjid='"+id+"'>"+processing+"</a>";
	}
	
</script>
<table id="yuanprjlist"   style="width:100%;height:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,toolbar:'#tb',url:'${ctx}/yuan/audit/prj/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'prjNo',width:100">项目编号</th>
			<th data-options="field:'prjName',width:200">项目名称</th>
			<th data-options="field:'prjStart',width:140,align:'center'">项目起时间</th>
			<th data-options="field:'prjEnd',width:140,align:'center'">项目止时间</th>
			<th data-options="field:'parta',width:250">委托单位</th>
			<th data-options="field:'partb',width:250,align:'center'">承担单位/负责人</th>
			<th data-options="field:'status',width:200,align:'center',formatter:function(value,row,index){return transstatus(row.id,value);}">项目进度</th>
			<th data-options="field:'t',width:150,align:'center',formatter:function(value,row,index){return '<a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:getsummary(\''+row.id+'\');&quot;>详细</a><a class=&quot;a-btn&quot; href=&quot;${ctx}/file/'+row.prjFile+'&quot;>下载</a>'}"></th>
		</tr>
	</thead>
</table>
