<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script>
	var $rptgrid;
	var rptGroupCols;
	$(function(){
		//$("#mainresearchtable").tabledit({initnum:1,addtool:true});
		//$("#mainresearchtable").tabledit('add',$("#mainresearchtable"), 1);
    })
    function submitForm(){
		
		if($("#ff").form('enableValidation').form('validate')){
			$.messager.alert('Info', "asas", 'info');
			alert($(".gender").val())
			$('#dg').datagrid('appendRow',{
				name: $("#name").val(),
				gender: $(".gender").val(),
				age: $("#age").val(),
				degree: $("#degree").val(),
				jobTitle: $("#jobTitle").val(),
				jobSubject: $("#jobSubject").val(),
				mainTask: $("#mainTask").val()
			});
		}
		/* $('#ff').form('submit',{
			onSubmit:function(){
				return $(this).form('enableValidation').form('validate');
			},
			success:function(data){
				$.messager.alert('Info', data, 'info');
			}
		}); */
	}
	function clearForm(){
		$('#ff').form('clear');
	}
</script>
<div id="f_mainresearcher1" style="z-index: 9999;">
	<form id="ff" method="post"  class="easyui-form" method="post" data-options="novalidate:true">
	   	<table class="table table-condensed table-hover easyui-table" width="100%" >
	   		<tbody>
		   		<tr>
			   	    <td class="x150">
		                <label for="name" class="control-label x150">姓名</label>
		                <input class="easyui-textbox easyui-table-input"  style="width:100%;height:28px"  type="text" name="name" id="name"  data-options="required:true,validType:['length[2,10]']" ></input>
		            </td>
	            </tr>
	            <tr>
			   	    <td class="x150">
		                <label for="gender" class="control-label x150">性别</label>
		                <input type="radio" name="gender" class="gender"  data-toggle="icheck" checked value="1" data-label="男">男
	                    <input type="radio" name="gender" class="gender"  data-toggle="icheck" value="0" data-label="女">女
		            </td>
	            </tr>
	            <tr>
			   	    <td class="x150">
		                <label for=age class="control-label x150">年龄</label>
		                <input class="easyui-numberbox easyui-table-input"  style="width:100%;height:28px"  type="text" name="age" id="age"  data-options="min:20,precision:0,max:90" ></input>
		            </td>
	            </tr>
	            <tr>
			   	    <td class="x150">
		                <label for="degree" class="control-label x150">学位</label>
		                <select name="degree" id="degree" data-toggle="selectpicker">
		                    <option value="1">高中</option>
		                    <option value="2">专科</option>
		                    <option value="3">本科</option>
		                    <option value="4">硕士</option>
		                    <option value="5">博士</option>
		                    <option value="6">博士后</option>
		                </select>
		            </td>
	            </tr>
	            <tr>
			   	    <td class="x150">
		                <label for="jobTitle" class="control-label x150">职称</label>
		                <input class="easyui-textbox easyui-table-input"  style="width:100%;height:28px"  type="text" name="jobTitle" id="jobTitle"  data-options="required:true,validType:['length[2,30]']" ></input>
		            </td>
	            </tr>
	            <tr>
			   	    <td class="x150">
		                <label for="jobSubject" class="control-label x150">从事专业</label>
		                <input class="easyui-textbox easyui-table-input"  style="width:100%;height:28px"  type="text" name="jobSubject" id="jobSubject"  data-options="required:true,validType:['length[2,30]']" ></input>
		            </td>
	            </tr>
	             <tr>
			   	    <td class="x150">
		                <label for="mainTask" class="control-label x150">在项目中的主要任务</label>
		                <input class="easyui-textbox easyui-table-input"  style="width:100%;height:28px"  type="text" name="mainTask" id="mainTask"  data-options="required:true,validType:['length[2,256]']" ></input>
		            </td>
	            </tr>
	   		</tbody>
	    </table>
	    <div style="padding:0 0 20px 0;text-align:center;">
	        <a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">确定</a>
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">关闭</a>
	    </div>
	</form>
</div>
