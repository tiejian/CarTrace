<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
    })
    function transstatus(id,value){
		//状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
		var processing="";
		switch(value)
		{
		case 1:
			processing= "草稿";
		    break;
		case 2:
			processing= "待审";
		    break;
		case 3:
			processing= "院审通过并提交专家审核";
		    break;
		case 4:
			processing= "院审未通过";
		    break;
		case 5:
			processing= "院部终止";
		    break;
		case 6:
			processing= "专家审核通过";
		    break;
		case 7:
			processing= "专家审核未通过";
		    break;
		case 8:
			processing= "专家终止";
		    break;
		default:
			processing= "删除";		
		}
		return "<a class='a-btn' href='javascript:void(0);' onclick='showprocessing(this,\""+id+"\")'>"+processing+"</a>";
	}
</script>
<ul>
	 <c:forEach var="audit" items="${list}">
	 <li><span><fmt:formatDate value="${audit.createAt}" pattern="yyyy-MM-dd HH:mm:ss " /></span> <span>${audit.bnExpert.name}</span> <span><c:choose> 
  <c:when test="${audit.status==1}">   
     草稿  
  </c:when> 
  <c:when test="${audit.status==2}">   
  待审
  </c:when>
  <c:when test="${audit.status==3}">   
  院审通过并提交专家审核
  </c:when>
  <c:when test="${audit.status==4}">   
  院审未通过 
  </c:when>
  <c:when test="${audit.status==5}">   
 院部终止 
  </c:when>
  <c:when test="${audit.status==6}">   
 专家审核通过
  </c:when>
  <c:when test="${audit.status==7}">   
  专家审核未通过 
  </c:when>
  <c:when test="${audit.status==8}">   
  专家终止
  </c:when>
  <c:otherwise>   
  删除
  </c:otherwise> 
</c:choose> </span></li>
	</c:forEach>
</ul>
