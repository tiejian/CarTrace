<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$('.menu-item').click(function() {
			$(this).addClass('on').siblings().removeClass('on');
			//$('#ppp').panel('open').panel('refresh','${ctx}/demo/list');
		})
	});
</script>
<div class="pass-portrait-content pass-portrait-clearfix">
	<div class="pass-portrait-left">
		<div class="pass-portrait-tabcontent" id="portraitCustom">
			<p class="pass-portrain-commonp">方法一：选择本地照片，上传编辑自己的头像</p>
			<div class="pass-portrait-openimg">
				<input type="button" class="pass-portrait-filebtn" name="openImgBtn"
					id="openImgBtn" value="选择图片"><input type="file"
					class="pass-portrait-file" name="file" id="fileImg"><span
					class="pass-portrait-msg">支持jpg、jpeg、gif、png、bmp格式的图片</span>
			</div>
			<p class="pass-portrain-commonp">方法二：选择推荐头像，快速上传优质头像</p>
			<ul class="pass-portrait-recommend pass-portrait-clearfix"
				id="passPortraitRecommend" data-hotpor="undefined">
				<li id="recom-wildkid-1" class=""><img
					src="https://ss0.bdstatic.com/7Ls0a8Sm1A5BphGlnYG/sys/portrait/hotitem/wildkid/1"
					data-serie="wildkid" data-num="1"><span class="recommendSpan"></span></li>
				<li id="recom-wildkid-2" class=""><img
					src="https://ss0.bdstatic.com/7Ls0a8Sm1A5BphGlnYG/sys/portrait/hotitem/wildkid/2"
					data-serie="wildkid" data-num="2"><span class="recommendSpan"></span></li>
			</ul>
		</div>
		<div class="pass-form-hidden">
			<input type="hidden" name="isCrop" id="isCrop"><input
				type="hidden" name="serie" id="serie"><input type="hidden"
				name="num" id="num"><input type="hidden" name="picId"
				id="picId"><input type="hidden" name="isHistory"
				id="isHistory"><input type="hidden" name="historyId"
				id="historyId">
		</div>
		<div class="pass-portrait-save ">
			<input type="button" value="保存头像"
				class="pass-portrait-savebtn pass-portrait-disabled" disabled="true"
				id="savePortrait">&nbsp;<input type="button" value="重新选择"
				class="pass-portrait-cancelbtn pass-portrait-hide"
				id="passPortraitRechoicebtn"><input type="file"
				class="pass-portrait-refile pass-portrait-hide" name="file"
				id="fileReImg"><span
				class="pass-portrait-status pass-portrait-hide"
				id="passPortraitPrompt">成功更新头像</span>
		</div>
	</div>
	<div class="pass-portrait-right">
		<p class="pass-portrain-commonp">头像预览</p>
		<p id="previewBoxBig" class="pass-portrait-previewboxbig">
			<img
				src="${ctx}/images/plogo.jpg"
				class="pass-portrait-previewbig" id="previewImgBig" style="">
		</p>
		<p class="pass-portrain-commonp pass-portrain-previewp">
			<span>大头像100*100</span>
		</p>
		<p id="previewBoxSmall" class="pass-portrait-previewboxbigsmall">
			<img
				src="${ctx}/images/plogo.jpg"
				class="pass-portrait-previewsmall" id="previewImgSmall" style="">
		</p>
		<p class="pass-portrain-commonp pass-portrain-previewp">
			<span>小头像55*55</span>
		</p>
	</div>
</div>