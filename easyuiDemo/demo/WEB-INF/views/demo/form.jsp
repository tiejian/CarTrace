<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$('.menu-item').click(function(){
		    $(this).addClass('on').siblings().removeClass('on');
		    $('#ppp').panel('open').panel('refresh',$(this).attr("url"));
		})
		//$('#ppp').panel('open').panel('refresh',"${ctx}/demo/form/base");
	});
</script>
<div class="info-box">
	<div id="ccc" class="easyui-layout" style="width: 100%; height: 100%;">
		<div data-options="region:'north',border:false,noheader:true"	style="height: 50px; background: #F8F8F8;"></div>
		<div
			data-options="region:'west',title:'East',bodyCls:'',border:false,noheader:true"
			style="width: 200px; background: #F8F8F8;">
			<div class="setting-menu">
				<div class="menu-title menu-profile active">
					<h3>个人资料</h3>
				</div>
				<ul class="menu-list">
					<li class="menu-item basic-link on" url="${ctx}/demo/form/base"><a href="javascript:void(0);">基本资料</a></li>
					<li class="menu-split"></li>
					<li class="menu-item details-link"><a href="javascript:void(0);">补充资料</a></li>
					<li class="menu-split"></li>
					<li class="menu-item portrait-link" url="${ctx}/demo/form/logo"><a href="javascript:void(0);">头像设置</a></li>
				</ul>
				<!-- <div class="menu-title menu-privacy-current active"
					id="settingPrivacy">
					<h3>隐私设置</h3>
				</div>
				<ul class="menu-list">
					<li class="menu-item tieba-link" id="settingPrivacyTieba"><a
						href="/p/setting/privacy/tieba">我在贴吧</a></li>
					<li class="menu-split"></li>
					<li class="menu-item zhidao-link" id="settingPrivacyZhidao"><a
						href="/p/setting/privacy/zhidao">我在知道</a></li>
					<li class="menu-split"></li>
					<li class="menu-item wenku-link" id="settingPrivacyWenku"><a
						href="/p/setting/privacy/wenku">我在文库</a></li>
					<li class="menu-split"></li>
					<li class="menu-item baike-link " id="settingPrivacyBaike"><a
						href="/p/setting/privacy/baike">我在百科</a></li>
					<li class="menu-split"></li>
				</ul> -->
			</div>
		</div>
		<div data-options="region:'center',title:'center title',bodyCls:'',noheader:true,border:false">
			<div id="ppp" class="easyui-panel"	style="width: 100%; height: 100%; padding: 10px; background: #fff;"	data-options="border:false"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
//$('#ppp').panel('open').panel('refresh','${ctx}/demo/form/base');
</script>