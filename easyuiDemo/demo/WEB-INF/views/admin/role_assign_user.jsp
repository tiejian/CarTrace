<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<script>
	var $rptgrid;
	var rptGroupCols;
	$(function(){
		
    })
    function submitForm(){
		
		$('#role_add_form').form('submit',{
			url:"${ctx}/st/role/update?_m=exec",
			onSubmit:function(){
				return $(this).form('enableValidation').form('validate');
			},
			success:function(data){
				var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','保存成功!','info',refresh());
				}else{
					$.messager.alert('提示信息','保存失败!','error');
				}
			}
		})

		
	}
	function clearForm(){
		$('#role_add_form').form('clear');
	}
	function assignusr(id){
		$.ajax({
			url : '${ctx}/st/role/assign/user?_m=exec&code=${code}&userid=' +id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','分配成功!','info',refresh());
					$('#enablegrid').datalist('reload');
					$('#disablegrid').datalist('reload');
				}else{
					$.messager.alert('提示信息','分配失败!','error');
				}
			}
		});
	}
	function unassignusr(id){
		$.ajax({
			url : '${ctx}/st/role/unassign/user?_m=exec&code=${code}&userid=' +id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','取消分配成功!','info',refresh());
					$('#enablegrid').datalist('reload');
					$('#disablegrid').datalist('reload');
				}else{
					$.messager.alert('提示信息','分配失败!','error');
				}
			}
		});
	}
</script>
<style>
</style>
    <div style="width:40%;float:left;height:420px;margin-left:40px;margin-top:20px;margin-bottom:20px;">
	    <div id="enablegrid" class="easyui-datalist" title="可分配用户" style="width:100%;float:left;height:420px;" data-options="
	            url: '${ctx}/bn/expert/findenable?_m=exec&code=${code}',
	            method: 'get',
	            groupField: 'department',
	            valueField:'id',
	            textField:'name',
	            onDblClickRow:function(index,row){assignusr(row.id);}
	            ">
	    </div>
	</div>
	<div style="width:40%;float:right;height:420px;margin-right:40px;margin-top:20px;margin-bottom:20px;">
	    <div id="disablegrid" class="easyui-datalist" title="已分配用户" style="width:100%;float:left;height:420px;" data-options="
	            url: '${ctx}/bn/expert/finddisable?_m=exec&code=${code}',
	            method: 'get',
	            groupField: 'department',
	            valueField:'id',
	            textField:'name',
	            onDblClickRow:function(index,row){unassignusr(row.id);}
	            ">
	    </div>
	</div>
	<input type="hidden" id="code" name="code" value="${code}" />
