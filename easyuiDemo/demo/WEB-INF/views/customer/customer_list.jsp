<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
		
		var customerlist = $('#customerlist').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		customerlist.datagrid('enableFilter', [
		   {
			field:'name',
			type:'text',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'customerType',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '企业',value: '1'},
					{label: '教职人员',value: '2'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btncustomeradd").bind("click",function(){
			$('<div></div>').dialog({
        	    title: '增加用户信息',
        	    width: 600,
        	    height:450,
        	    closed: false,
        	    cache: false,
        	    href: '${ctx}/bn/customer/insert?_m=init',
        	    modal: true,
        	    onClose : function() {
                    $(this).dialog('destroy');
                }
        	});
		});
    })
    function getsummary(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/customer/audit/prj/get?_m=exec&id="+id);
	}
	
    function transtype(value){
		switch(value)
		{
		case "1":
		    return "企业";
		    break;
		default:
			return "教职人员";		
		}
	}
    function transagefromidcard(value){
    	var date = new Date();
    	var year = date.getFullYear(); 
    	var birthday_year = parseInt(value.substr(6,4));
    	var userage= year - birthday_year;
    	return userage;
	}
    function transgenderfromidcard(value){
    	if (parseInt(value.substr(16, 1)) % 2 == 1) { 
   		   return "男";
   		} else { 
   			return "女";
   		} 
	}
	function updatecustomer(id){
		$('<div></div>').dialog({
    	    title: '修改用户信息',
    	    width: 600,
    	    height:450,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/bn/customer/update?_m=init&id='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function delcustomer(id){
		$.ajax({
			url : '${ctx}/bn/customer/delete?_m=exec&id=' + id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','已删除!','info',refresh());
				}else{
					$.messager.alert('提示信息','删除失败!','error');
				}
			}
		});
	}
	function refresh(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/customer/search?_m=init");
	}
</script>
<table id="customerlist"  style="width:100%;height:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,toolbar:'#customertb',url:'${ctx}/bn/customer/search?_m=load',method:'get',queryParams:{filter_EQ_user_type:'2'}">
	<thead>
		<tr>
			<th data-options="field:'name',width:100">名称</th>
			<th data-options="field:'idcard',width:200">身份证号</th>
			<th data-options="field:'xb',width:80,align:'center',formatter:function(value,row,index){return transgenderfromidcard(row.idcard);}">性别</th>
			<th data-options="field:'age',width:80,align:'center',formatter:function(value,row,index){return transagefromidcard(row.idcard);}">年龄</th>
			<th data-options="field:'degree',width:100,align:'center'">学位</th>
			<th data-options="field:'jobTitle',width:140,align:'center'">职称</th>
			<th data-options="field:'jobPost',width:100,align:'center'">职务</th>
			<th data-options="field:'jobSubject',width:200">从事专业</th>
			<th data-options="field:'unitName',width:250,align:'center'">学校名称</th>
			<th data-options="field:'department',width:200,align:'center'">所在院系</th>
			<th data-options="field:'t',width:150,formatter:function(value,row,index){return '<a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:updatecustomer(\''+row.id+'\');&quot;>修改</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:delcustomer(\''+row.id+'\');&quot;>删除</a>'}"></th>
		</tr>
	</thead>
</table>
<div id="customertb" style="padding:2px 5px;">
	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" id="btncustomeradd">增加</a>
</div>
<div id="f_customeropera" style="width:500px;z-index: 9999;display:none;"></div>