package com.jacqui.core.security;

import org.junit.Test;
/**
 * 
 * @author 小老头
 * @version 1.0
 * @since 1.0
 */
public class AESCoderTest {
	@Test 
	public void testAES(){
//		String content = "datashare56";   
		String content = "111111";   
		String password = "ilaotou@qq.com";   
		//加密    
		System.out.println("加密前：" + content);   
		byte[] encryptResult = AESCoder.encrypt(content, password);   
		String encryptResultStr = AESCoder.parseByte2HexStr(encryptResult);   
		System.out.println("加密后：" + encryptResultStr);   
		//解密    
		byte[] decryptFrom = AESCoder.parseHexStr2Byte(encryptResultStr);   
		byte[] decryptResult = AESCoder.decrypt(decryptFrom,password);   
		System.out.println("解密后：" + new String(decryptResult));  

	}

}
