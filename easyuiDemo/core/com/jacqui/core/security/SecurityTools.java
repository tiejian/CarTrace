package com.jacqui.core.security;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.digest.DigestUtils;

public class SecurityTools {
	/**
	 * 取得密钥对的KEY
	 * @return
	 */
	public static String getkey(){
		return SecurityConstant.getKey();
	}
	/**
	 * 加密过程
	 * @param key 取得密钥对的KEY
	 * @param content 要加密的内容
	 * @return 加密后的内容
	 */
	public static String encrypt(String key,String content){
//		System.out.println("content：" + content);  
		String cryptkey=SecurityConstant.getKeyPairs(key);
		byte[] encryptResult = AESCoder.encrypt(content, cryptkey);   
		String encryptResultStr = AESCoder.parseByte2HexStr(encryptResult);   
		return encryptResultStr;
	}
	/**
	 * 解密过程
	 * @param key 取得密钥对的KEY
	 * @param content 要解密的内容
	 * @return 解密后的内容
	 * @throws UnsupportedEncodingException 
	 */
	public static String decrypt(String key,String content) throws UnsupportedEncodingException{
//		System.out.println("content：" + content);
		String cryptkey=SecurityConstant.getKeyPairs(key);
		byte[] decryptFrom = AESCoder.parseHexStr2Byte(content);   
		byte[] decryptResult = AESCoder.decrypt(decryptFrom,cryptkey);   
//		System.out.println("return：" + new String(decryptResult,"utf-8"));
		return new String(new String(decryptResult,"utf-8"));
	}
	/**
	 * 对给定的内容进行MD5
	 * @param content
	 * @return
	 */
	public static String md5Hex(String content){
		return DigestUtils.md5Hex(content);
	}
	public static void main(String[] args) throws UnsupportedEncodingException{
		String content="要解密的内容的*&1311313,，";
		String key = getkey();
		String a=encrypt(key,content);
		System.out.println("加密后：" + a);  
		System.out.println("加密后MD5：" + md5Hex(a));  
		String b=decrypt(key,"ddd");
		System.out.println("解密后：" + b);
	}
	
}
