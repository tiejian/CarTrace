package com.jacqui.core.security;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sun.security.x509.CertificateValidity;
import sun.security.x509.X509CertImpl;
import sun.security.x509.X509CertInfo;

/**
 * <p>这个 servlet 的作用是：
 * <p>当用户请求该 serlvet ，从密钥库中提取 clientCA 证书，将证书有效期修改为当前日期到下一天。
 * 也就是说，当用户客户端请求该 servlet ，即可获得一个新的证书，这个证书的有效期已经向后延了一天。
 * 我们的目的是，用户每次登录后，检查用户下载的证书，如证书已过了有效期，则请求此 servlet 即可获得一个有效的新证书。
 */
public class GetNewCert1 extends HttpServlet {

	private static final long serialVersionUID = 1L;

	// 有效期天数
	private static final int Max_Days = 1;
	// keystore 密码
	private static final char[] password = "ipcc@95598".toCharArray();
	// keystore 文件路径
	private static final String keystoreFilename = "/home/zhang/Downloads/a.keystore";
	// 证书文件名
	private static final String certFilename = "client.cer";
	// 证书别名
	private static final String alias = "clientCA";

	private KeyStore keystore;

	private String sigAlgrithm;

	// 读取 keystore
	private KeyStore loadKeystore(String keystorepath) {
		KeyStore ks = null;
		try {
			FileInputStream fIn = new FileInputStream(keystorepath);
			ks = KeyStore.getInstance("JKS");
			ks.load(fIn, password);
			fIn.close();
			return ks;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ks;
	}

	// 获得 CertInfo
	private X509CertInfo getCertInfo(Certificate c, String alias) {
		X509CertInfo certInfo = null;
		try {
			// 从待签发的证书中提取证书信息　　
			byte[] encod2 = c.getEncoded();// 获取 证书内容（经过编码的字节）
			X509CertImpl cimp2 = new X509CertImpl(encod2);// 创建 X509CertImpl 象
			sigAlgrithm = cimp2.getSigAlgName();
			// 获取 X509CertInfo 对象
			certInfo = (X509CertInfo) cimp2.get(X509CertImpl.NAME + "." + X509CertImpl.INFO);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return certInfo;
	}

	// 修改有效期
	private void updateValidity(X509CertInfo cinfo, int days) {
		// 获取当前时间
		Date d1 = new Date();
		// 有效期为当前日期后延 n 天
		Date d2 = new Date(d1.getTime() + days * 24 * 60 * 60 * 1000L);
		// 创建有效期对象
		CertificateValidity cv = new CertificateValidity(d1, d2);
		try {
			cinfo.set(X509CertInfo.VALIDITY, cv);// 设置有效期
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 存储证书
	private void saveCert(KeyStore ks, char[] storepass, String alias, PrivateKey pKey, char[] keypass, X509CertInfo cinfo, String algrithm) {
		try {
			X509CertImpl cert = new X509CertImpl(cinfo);// 新建证书
			cert.sign(pKey, algrithm); // 使用 CA 私钥对其签名
			// 获取别名对应条目的证书链
			Certificate[] chain = new Certificate[] { cert };
			// 向密钥库中添加条目 , 使用已存在别名将覆盖已存在条目
			ks.setKeyEntry(alias, pKey, keypass, chain);
			// 将 keystore 存储至文件
			FileOutputStream fOut = new FileOutputStream(keystoreFilename);
			keystore.store(fOut, password);
			fOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// 导出证书
	private void exportCert(KeyStore ks, String alias, HttpServletResponse response) {
		try {
			Certificate cert = keystore.getCertificate(alias);
			// 得到证书内容（以编码过的格式）
			byte[] buf = cert.getEncoded();
			// 写证书文件
			response.setContentType("application/x-download");
			response.addHeader("Content-Disposition", "attachment;filename=" + certFilename);
			OutputStream out = response.getOutputStream();
			out.write(buf);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 导出证书
	private void exportCert(KeyStore ks, String alias) {
		try {
			Certificate cert = keystore.getCertificate(alias);
			// 得到证书内容（以编码过的格式）
			byte[] buf = cert.getEncoded();
			// 写证书文件

			// 将 keystore 存储至文件
			FileOutputStream fOut = new FileOutputStream("/home/zhang/Downloads/a.cer");
			fOut.write(buf);
			fOut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			keystore = loadKeystore(keystoreFilename); // 读取 keystore
			Certificate c = keystore.getCertificate(alias);// 读取证书
			X509CertInfo cinfo = getCertInfo(c, alias);// 获得证书的 CertInfo
			updateValidity(cinfo, Max_Days);// 修改证书有效期

			// 从密钥库中读取 CA 的私钥
			PrivateKey pKey = (PrivateKey) keystore.getKey(alias, "123456".toCharArray());
			// 将 keystore 存储至 keystore 文件
			saveCert(keystore, password, alias, pKey, "123456".toCharArray(), cinfo, sigAlgrithm);
			exportCert(keystore, alias, response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	public void df() {
		try {
			keystore = loadKeystore(keystoreFilename); // 读取 keystore
			Certificate c = keystore.getCertificate(alias);// 读取证书
			X509CertInfo cinfo = getCertInfo(c, alias);// 获得证书的 CertInfo
			updateValidity(cinfo, Max_Days);// 修改证书有效期

			// 从密钥库中读取 CA 的私钥
			PrivateKey pKey = (PrivateKey) keystore.getKey(alias, "123456".toCharArray());
			// 将 keystore 存储至 keystore 文件
			saveCert(keystore, password, alias, pKey, "123456".toCharArray(), cinfo, sigAlgrithm);
			exportCert(keystore, alias);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		GetNewCert1 c = new GetNewCert1();
		c.df();
	}

}
