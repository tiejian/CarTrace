package com.jacqui.core.utils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import javax.servlet.http.HttpServletRequest;
/**
 * 
 * @ClassName: IpUtils 
 * @Description: IP工具
 * @author ilaotou@qq.com
 * @date 2014年11月4日 上午9:52:36 
 *
 */
public class IpUtils {
	
	/**
	 * 
	 * @Title: getIpAddr 
	 * @Description: 获取客户端IP地址
	 * @param request 请求
	 * @return 
	 * @date 2014年11月4日 上午10:17:41
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		// 多个路由时，取第一个非unknown的ip
		String[] arr = ip.split(",");
		for (final String str : arr) {
			if (!"unknown".equalsIgnoreCase(str)) {
				ip = str;
			}
			break;
		}
		return ip;
	}
	/**
	 * 
	 * @Title: getMACAddress 
	 * @Description: TODO(这里用一句话描述这个方法的作用) 
	 * @param ip
	 * @return 
	 * @date 2014年11月4日 上午10:17:29
	 */
	public static String getMACAddress(final String ip) {
		String str = "";
		String macAddress = "";
		try {
			Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
			InputStreamReader ir = new InputStreamReader(p.getInputStream());
			LineNumberReader input = new LineNumberReader(ir);
			for (int i = 1; i < 100; i++) {
				str = input.readLine();
				if (str != null) {
					if (str.indexOf("MAC Address") > 1) {
						macAddress = str.substring(
								str.indexOf("MAC Address") + 14, str.length());
						break;
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
		return macAddress;
	}
}
