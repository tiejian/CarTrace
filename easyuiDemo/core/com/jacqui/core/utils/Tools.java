package com.jacqui.core.utils;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class Tools {
	public static String transDbField2JavaField(String dbField) {
		String[] section = dbField.split("_");
		StringBuilder sb = new StringBuilder(64);
		for (String string : section) {
			sb.append(trans(string));
		}
		return transFirstLower(sb.toString());
	}

	public static String transDb2JavaName(String dbField) {
		String[] section = dbField.split("_");
		StringBuilder sb = new StringBuilder(64);
		for (String string : section) {
			sb.append(trans(string));
		}
		return sb.toString();
	}

	public static String trans(String str) {
		return str = str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
	}

	public static String transFirstUpper(String str) {
		return str = str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String transFirstLower(String str) {
		return str = str.substring(0, 1).toLowerCase() + str.substring(1);
	}

	public static String getShortVoName(String voName) {
		return StringUtils.substring(voName, 0, voName.length() - 2);
	}

	public static String convertJavaName2DbName(String s) {
		StringBuilder sb = new StringBuilder(100);
		for (int i = 0; i < s.length(); i++) {
			if (i > 0 && s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				sb.append("_");
				sb.append(s.charAt(i));
			} else {
				sb.append(s.charAt(i));
			}

		}
		return StringUtils.lowerCase(sb.toString());
	}

	public static double round(double v, int scale) {
		BigDecimal bd = new BigDecimal(Double.toString(v));
		return bd.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}


	public static final char UNDERLINE = '_';

	public static String camelToUnderline(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (Character.isUpperCase(c)) {
				sb.append(UNDERLINE);
				sb.append(Character.toLowerCase(c));
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static String underlineToCamel(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		int len = param.length();
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++) {
			char c = param.charAt(i);
			if (c == UNDERLINE) {
				if (++i < len) {
					sb.append(Character.toUpperCase(param.charAt(i)));
				}
			} else {
				sb.append(c);
			}
		}
		return sb.toString();
	}

	public static String underlineToCamel2(String param) {
		if (param == null || "".equals(param.trim())) {
			return "";
		}
		StringBuilder sb = new StringBuilder(param);
		Matcher mc = Pattern.compile("_").matcher(param);
		int i = 0;
		while (mc.find()) {
			int position = mc.end() - (i++);
			// String.valueOf(Character.toUpperCase(sb.charAt(position)));
			sb.replace(position - 1, position + 1, sb.substring(position, position + 1).toUpperCase());
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		// System.out.println(transDb2JavaName("cj_aa"));
		System.out.println(convertJavaName2DbName("prjName"));
	}


}
