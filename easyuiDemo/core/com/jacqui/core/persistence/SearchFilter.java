/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.jacqui.core.persistence;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.core.form.ConditionEntity;

public class SearchFilter {

	public enum Operator {
		EQ, NEQ, LIKE, NOTLIKE, GT, LT, GTE, LTE,NULL,IN,IS,ISN
	}

	public String fieldName;
	public Object value;
	public Operator operator;

	public SearchFilter() {
	}
	/**
	 * 
	 * <p>Title: </p> 
	 * <p>Description: 用于构造查询条件</p> 
	 * @param fieldName 字段名称
	 * @param operator  操作符
	 * @param value     字段值
	 */
	public SearchFilter(String fieldName, Operator operator, Object value) {
		this.fieldName = fieldName;
		this.value = value;
		this.operator = operator;
	}

	/**
	 * searchParams中key的格式为OPERATOR_FIELDNAME
	 */
	public static Map<String, SearchFilter> parse(Map<String, Object> searchParams) {
		Map<String, SearchFilter> filters = Maps.newHashMap();

		for (Entry<String, Object> entry : searchParams.entrySet()) {
			// 过滤掉空值
			String key = entry.getKey();
			Object value = entry.getValue();
//			if (StringUtils.isBlank((String) value)) {
//				continue;
//			}
			
			if(value instanceof String){
				if (StringUtils.isBlank((String) value)) {
					continue;
				}
			}else{
				if(null == value){
					continue;
				}
			}

			// 拆分operator与filedAttribute
			String[] names = StringUtils.split(key, "_");
			if (names.length != 2) {
				throw new IllegalArgumentException(key + " is not a valid search filter name");
			}
			String filedName = names[1];
			Operator operator = Operator.valueOf(names[0]);

			// 创建searchFilter
			SearchFilter filter = new SearchFilter(filedName, operator, value);
			filters.put(key, filter);
		}

		return filters;
	}
	public static List<SearchFilter> parseToList(Map<String, Object> searchParams) {
		List<SearchFilter> filters= Lists.newArrayList();

		for (Entry<String, Object> entry : searchParams.entrySet()) {
			// 过滤掉空值
			String key = entry.getKey();
			Object value = entry.getValue();
//			if (StringUtils.isBlank((String) value)) {
//				continue;
//			}
			
			if(value instanceof String){
				if (StringUtils.isBlank((String) value)) {
					continue;
				}
			}else{
				if(null == value){
					continue;
				}
			}

			// 拆分operator与filedAttribute
//			String[] names = StringUtils.split(key, "_");
//			if (names.length != 2) {
//				throw new IllegalArgumentException(key + " is not a valid search filter name");
//			}
			String unprefixedKey = StringUtils.substringAfter(key, "_");
			String filedName = StringUtils.substringAfter(unprefixedKey, "_");
			Operator operator = Operator.valueOf(StringUtils.substringBefore(unprefixedKey, "_"));

			// 创建searchFilter
			SearchFilter filter = new SearchFilter(filedName, operator, value);
			filters.add(filter);
		}

		return filters;
	}
	public static Map<String, Object> parseToMap(Map<String, Object> searchParams) {
		Map<String, Object> filters= Maps.newHashMap();

		for (Entry<String, Object> entry : searchParams.entrySet()) {
			// 过滤掉空值
			String key = entry.getKey();
			Object value = entry.getValue();
//			if (StringUtils.isBlank((String) value)) {
//				continue;
//			}
			if(value instanceof String){
				if (StringUtils.isBlank((String) value)) {
					continue;
				}
			}else{
				if(null == value){
					continue;
				}
			}
			filters.put(StringUtils.substring(key, 1), value);
		}
		return filters;
	}
	/**
	 * 
	 */
	public static List<SearchFilter> parse(List<ConditionEntity> conditionEntities ) {
		List<SearchFilter> list = Lists.newArrayList();

		for (ConditionEntity  entity : conditionEntities) {
			// 过滤掉空值
			Object value = entity.getValue();
//			if (StringUtils.isBlank((String) value)) {
//				continue;
//			}
			if(value instanceof String){
				if (StringUtils.isBlank((String) value)) {
					continue;
				}
			}else{
				if(null == value){
					continue;
				}
			}
			Operator operator = Operator.valueOf(entity.getOperator());
			// 创建searchFilter
			SearchFilter filter = new SearchFilter(entity.getFieldName(), operator, entity.getValue());
			list.add(filter);
		}
		return list;
	}
}
