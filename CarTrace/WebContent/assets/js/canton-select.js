var ctx = window.ctx + '/';
$(function() {
	var _mkid = $('#canton_p').combobox({
		url: ctx+'dt/xzq/find?_m=exec&level=1',
		editable: false,
		valueField: 'code',
		textField: 'name',
		onSelect: function (record) {
			$('#canton_c').combobox({
				disabled: false,
				url: ctx+'/dt/xzq/find?_m=exec&level=2&pcode='+record.code,
				valueField: 'code',
				textField: 'name',
				onSelect: function (record1) {
					$('#canton').combobox({
						disabled: false,
						url: ctx+'/dt/xzq/find?_m=exec&level=3&pcode='+record1.code,
						valueField: 'code',
						textField: 'name'
					});
				}
			});
		}
	});

});
