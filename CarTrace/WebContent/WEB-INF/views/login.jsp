<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/material/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/material/easyui-form-table.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/css/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/css/login.css">
	
	<script type="text/javascript" src="${ctx}/lib/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/js/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="${ctx}/lib/easyui/locale/easyui-lang-zh_CN.js"></script>
    
    <script type="text/javascript">
	    $(function() {
	    })
		function sub(){
		    if(!valid())
		    return false;
			//var form = document.getElementById("frlogin"); 
			//form.submit();  
			alert(1);
			
			$('#frlogin').form('submit');
		}
	    function valid(){
    	  var _idCard = $("#username").val();
    	  var _pwordType = $("#password").val();
    	  if(jQuery.trim(_idCard).length == 0){
    	    setErrorMsg("用户名不可为空");
    	    $("#username").focus();
    	    return false;
    	  }
    	  else if(jQuery.trim(_pwordType).length == 0){
    	    setErrorMsg("密码不可为空");
    	    $("#password").focus();
    	    return false;
    	  }
    	  return true;
    	}
	    function setErrorMsg(msg){
    	 $(".div_error").html(msg);
    	 window.setTimeout("clearError()",5000);
    	}
	    function clearError(){
          $(".div_error").html("");
        }

	</script>
</head>
<body class="easyui-layout">
    <!--[if lte IE 7]>
        <div id="errorie"><div>您还在使用老掉牙的IE，正常使用系统前请升级您的浏览器到 IE8以上版本 <a target="_blank" href="http://windows.microsoft.com/zh-cn/internet-explorer/ie-8-worldwide-languages">点击升级</a>&nbsp;&nbsp;强烈建议您更改换浏览器：<a href="http://down.tech.sina.com.cn/content/40975.html" target="_blank">谷歌 Chrome</a></div></div>
    <![endif]-->
	<div data-options="region:'center'">
	       <div class="main-login">

				<div class="login-content">	
					<h2>用户登录</h2>
					
				   <form:form  action="${ctx}/login" method="post" commandName="loginForm" id="frlogin" name="frlogin">	
				    <div class="login-info">
					<span class="user">&nbsp;</span>
					<input name="username" id="username" type="text"  value="220211197905173639" class="login-input"/>
					</div>
				    <div class="login-info">
					<span class="pwd">&nbsp;</span>
					<input name="password" id="password" type="password"  value="111111" class="login-input"/>
					</div>
				    <div class="login-oper">
					<div class="div_error"><form:errors path="username" /><form:errors path="password" /></div>
					</div>
				    <div class="login-oper">
					<input name="" type="submit" value="登 录" class="login-btn"/>
					<input name="" type="button" value="重 置" class="login-reset"/>
					</div>
					
				    </form:form>
			    </div>
			</div> 
	</div>
    <script type="text/javascript">
	</script>
</body>
</html>
