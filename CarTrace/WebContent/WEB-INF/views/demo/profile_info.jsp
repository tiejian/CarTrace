<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$('.menu-item').click(function() {
			$(this).addClass('on').siblings().removeClass('on');
			//$('#ppp').panel('open').panel('refresh','${ctx}/demo/list');
		})
	});
</script>
	<div class="mod-setting-profile" id="1000001" style="display: block;">
		<div class="setting-profile-title yahei">详细资料</div>
		<form id="profile" class="setting-profile-form easyui-form"
			method="post" data-options="novalidate:true">
			<table>
				<tbody>
					<tr>
						<th class="personal-detail-th">文本型:</th>
						<td class="personal-detail-td"><input
							class="easyui-textbox easyui-table-input"
							style="width: 100%; height: 32px" type="text"
							name="bnProjectInfo.prjNo" id="bnProjectInfo.prjNo"
							data-options="required:true"></input></td>
					</tr>
					<tr>
						<th class="personal-detail-th">数值型:</th>
						<td class="personal-detail-td"><input
							class="easyui-numberbox easyui-table-input"
							style="width: 100%; height: 32px" type="text"
							name="bnProjectInfo.prjNo" id="bnProjectInfo.prjNo"
							data-options="required:true"></input></td>
					</tr>
					<tr>
						<th class="personal-detail-th">日期型:</th>
						<td class="personal-detail-td"><input
							class="easyui-datebox easyui-table-input"
							style="width: 100%; height: 32px" type="text"
							name="bnProjectInfo.prjNo" id="bnProjectInfo.prjNo"
							data-options="required:true"></input></td>
					</tr>
					<tr>
						<th class="personal-detail-th">联系方式:</th>
						<td class="personal-detail-td"><input
							class="easyui-textbox easyui-table-input"
							style="width: 100%; height: 32px" type="text"
							name="bnProjectInfo.prjNo" id="bnProjectInfo.prjNo"
							data-options="required:true"></input></td>
					</tr>
					<tr>
						<th class="personal-detail-th"></th>
						<td class="personal-detail-td"><a href="#"
							class="easyui-linkbutton btn btn-primary"><i
								class='fa fa-save'></i>保存</a></td>
					</tr>
				</tbody>
			</table>
		</form>
	</div>
