<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$("#btn-save").bind("click",function(){
			$('#form1').form('submit',{
                onSubmit:function(){
                    return $(this).form('enableValidation').form('validate');
                }
            });
		});
	});
</script>
<div class="form-box">
	<div class="form-title" >
		<div class="form-toolbar">
			<a id="btn-save" href="#" class="easyui-linkbutton btn-form" ><i class="fa fa-floppy-o" aria-hidden="true"></i>保存</a>
			<a id="btn-back" href="#" class="easyui-linkbutton btn-form" ><i class="fa fa-reply" aria-hidden="true"></i>返回</a>
		</div>
	</div>
	<div class="form-body">
		<form role="form" name="form1" id="form1" class="easyui-form form form-horizontal" data-options="novalidate:true">
				<div class="seg-title">
					<div class="seg-title-box">
			    		 <ul>
				    		<li class="seg-title-box-icon" ><img src="${ctx }/assets/images/communication.gif"/></li>
				    		<li class="seg-title-box-text">项目立项书书面信息</li>
				    	</ul>
			    	</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">项目名称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="projectName" name="projectName" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">项目编号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="projectNo" name="projectNo" value="" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">委托单位（甲方）：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="entrustingCompany" name="entrustingCompany" value="" class="form-control" title="委托单位（甲方）">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">承担单位/负责人（乙方）：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="assumeComPany" name="assumeComPany" value="" class="form-control" title="承担单位/负责人（乙方）">
					</div>
				</div>
				<div class="seg-title">
					<div style="float: left;width:200px;">
			    		 <ul>
				    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
				    		<li style="float:left;padding-left: 5px;">项目负责人简况信息</li>
				    	</ul>
			    	</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">姓    名：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="name" name="name" value="" class="form-control" title="姓名">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">年    龄：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="age" name="age" value="" class="easyui-numberbox form-control" title="年龄" style="width:100%;height:34px;line-height:34px;">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">姓    名：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="name" name="name" value="" class="form-control" title="姓名">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">出生日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="age" name="age" value="" class="easyui-datebox form-control" title="年龄" style="width:100%;height:34px;line-height:34px;">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">性    别：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<select name="sex" id="sex" class="form-control" title="性别" >
							<option value="">--请选择--</option>
							<option value="1">男</option>
							<option value="2">女</option>
						</select>
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">最终学位：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<select name="degree" id="degree" class="form-control" title="最终学位">
							<option value="">--请选择--</option>
							<option value="1">荣誉博士学位</option>
							<option value="2">博士学位</option>
							<option value="3">硕士学位</option>
							<option value="4">学士学位</option>
							<option value="0">无</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">职    称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<select name="jobTitle" id="jobTitle" class="form-control" title="职称">
							<option value="">--请选择--</option>
							<option value="11">教授</option>
							<option value="12">副教授</option>
							<option value="81">高级工程师</option>
							<option value="83">工程师</option>
							<option value="85">技术员</option>
							<option value="165">技术设计员</option>
						</select>
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">职    务：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<select name="position" id="position" class="form-control" title="职务">
							<option value="">--请选择--</option>
							<option value="001A">书记</option>
							<option value="001B">副书记</option>
							<option value="002A">委员</option>
							<option value="004A">主任</option>
							<option value="004B">副主任</option>
							<option value="083Q">总工程师</option>
							<option value="216A">局长</option>
							<option value="216B">副局长</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">所在学科名称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="sciencename" name="sciencename" value="" class="form-control" title="所在学科名称">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">主要研究方向：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="researchDirection" name="researchDirection" value="" class="form-control" title="主要研究方向">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-7 control-label" for="form-field-1" style="text-align:left;padding-left:75px;">
						<p class="star" style="float:left;">*</p>
						<p class="label-txt" style="float:left;">项目负责人近三年来主要研究成果简介（发表文章、承担的项目等）：</p>
					</label>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="form-field-1">
						
					</label>
					<div class="col-sm-10">
						<textarea id="achievement" name="achievement" rows="8" class="form-control" title="主要研究成果"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="form-field-1" style="text-align:left;padding-left:75px;">
						<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
						<p style="float:left">参加研究项目的主要人员情况：</p>
					</label>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="form-field-1">
						
					</label>
					<div class="col-sm-10">
						<div>
							<div id="zyryqkToolbar"></div>
							<div id="zyryqkGrid"></div> 
						</div>
					</div>
				</div>
				<div class="seg-title">
					<div style="float: left;width:300px;">
			    		 <ul>
				    		<li style="float:left;width:21px;"><img src="${ctx }/assets/images/communication.gif"/></li>
				    		<li style="float:left;padding-left: 5px;">项目建设目标与建设内容信息</li>
				    	</ul>
			    	</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						项目名称：
					</label>
					<div class="col-sm-8">
						<input type="text" id="projectNameTemp" name="projectNameTemp" value="" class="form-control" disabled="disabled">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">项目起止时间：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="projectBeginTime" name="projectBeginTime" value="" readonly="readonly" class="form-control" onclick="WdatePicker();" title="项目起时间">
					</div>
					<div class="col-sm-4">
						<input type="text" id="projectEndTime" name="projectEndTime" value="" readonly="readonly" class="form-control" onclick="WdatePicker();" title="项目止时间">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
						<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
						<p style="float:left">项目建设目标：</p>
					</label>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" >
						
					</label>
					<div class="col-sm-10">
						<textarea id="projectGoal" name="projectGoal" rows="8" class="form-control" title="项目建设目标"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
						<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
						<p style="float:left">项目主要内容：</p>
					</label>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" >
						
					</label>
					<div class="col-sm-10">
						<textarea id="projectContent" name="projectContent" rows="8" class="form-control" title="项目主要内容"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-7 control-label" style="text-align:left;padding-left:75px;">
						<p style="color:red;padding-right:5px;font-size:16px;float:left">*</p>
						<p style="float:left">项目的特色与创新：</p>
					</label>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" >
						
					</label>
					<div class="col-sm-10">
						<textarea id="projectFeatureInnovation" name="projectFeatureInnovation" rows="8" class="form-control" title="项目的特色与创新"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="form-field-1">
						
					</label>
					<div class="col-sm-10">
						<div>
							<div id="jtmxToolbar"></div>
							<div id="jtmxGrid"></div> 
						</div>
					</div>
				</div>
				<div class="form-group last">
				</div>
		</form>
	</div>
</div>
<script type="text/javascript">
//$('#ppp').panel('open').panel('refresh','${ctx}/demo/form/base');
</script>