<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
    var companyBaseinfoDatagrid;
	var toolbar = [{
		text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加',
		handler:function(){toadd();}
	},{
		text:'<i class="fa fa-edit"></i>修改',
		handler:function(){toupdate();}
	},{
		text:'<i class="fa fa-cut" aria-hidden="true"></i>删除',
		handler:function(){todelete();}
	},'-',{
		text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
		handler:function(){alert('save')}
	}];
	$(function(){
		companyBaseinfoDatagrid = $('#companyBaseinfoDatagrid').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		companyBaseinfoDatagrid.datagrid('enableFilter', [
		   {
			field:'name',
			type:'textbox',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'prjStart',
			type:'datebox',
			op:['greaterorequal']
		   },
		   {
			field:'prjEnd',
			type:'datebox',
			op:['lessorequal']
		   },
		   {
			field:'opType',
			type:'combobox',
			options:{
				valueField: 'code',
				textField: 'name',
				url:'${ctx}/dt/yylx/find?_m=exec'
		    },
			op:['equal']
		   },
		   {
			field:'staffCount',
			type:'label'
		   },
		   {
			field:'vehicleCount',
			type:'label'
		   },
		 ]);
    })
	function toupdate(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/project/bn_project_info/update?_m=init&id="+id);
	}
	function operator(id,status,file){
		var arr = new Array();
		if(status=='1'||status=='4'||status=='7'){
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\""+id+"\");'>编辑</a>");
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\'"+id+"');'>删除</a>");
		}
		arr.push("<a class='a-btn' target='_blank' href='${ctx}/file/"+file+"'>下载</a>");
		return arr.join("");
	}
	function reload(){
		companyBaseinfoDatagrid.datagrid('reload');
	}
</script>
<table id="companyBaseinfoDatagrid"  style="width:100%;height:100%"
		data-options="border:false,rownumbers:true,autoRowHeight:false,singleSelect:true,pagination:true,toolbar:toolbar,rowStyler: function(index,row){return {style:'height:30px'};},url:'${ctx}//hrmgr/company/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'name',width:300">业户名称</th>
			<th data-options="field:'address',width:200,align:'center'">业户地址</th>
			<th data-options="field:'canton',width:140,align:'center'">所属行政区划</th>
			<th data-options="field:'opType',width:250">营运类型</th>
			<th data-options="field:'companyLevel',width:250,align:'center'">企业等级</th>
			<th data-options="field:'staffCount',width:200,align:'center'">从业人员数量</th>
			<th data-options="field:'vehicleCount',width:200,align:'center'">车辆数</th>
			<th data-options="field:'opArea',width:200,align:'center'">经营区域</th>
			<th data-options="field:'opScope',width:200,align:'center'">经营范围</th>
			<th data-options="field:'licenseNum',width:200,align:'center'">经营许可证号</th>
			<th data-options="field:'t',width:150,align:'center',formatter:function(value,row,index){return operator(row.id,row.status,row.prjFile)}"></th>
		</tr>
	</thead>
</table>
<script type="text/javascript">
      function toadd(){
			$('<div></div>').dialog({
				id:'dg-company-info-insert',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">企业信息登记<span>',
				width: 900,
				height:640,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/company/insert?_m=init',
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-companyinfo-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registCompany();}
				},{
					id:"btn-companyinfo-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-company-info-insert").dialog('destroy');}
				}]
			});
	  }
	  function toupdate(){
		var row = companyBaseinfoDatagrid.datagrid('getSelected');
		if(row==null){
			$.messager.alert({
				border:false,
				title:'提示',
				msg:'请选择一行数据',
				icon:'warn-ext'
			});
		}else{
			$('<div></div>').dialog({
				id:'dg-company-info-update',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">企业信息登记<span>',
				width: 900,
				height:640,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/company/update?_m=init&id='+row.id,
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-companyinfo-update-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registCompany();}
				},{
					id:"btn-companyinfo-update-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-company-info-insert").dialog('destroy');}
				}]
			});
		}
	  }
	  function todelete(){
		    var row = companyBaseinfoDatagrid.datagrid('getSelected');
			if(row==null){
				$.messager.alert({
					border:false,
					title:'提示',
					msg:'请选择一行数据',
					icon:'warn-ext'
				});
			}else{
				$.messager.confirm({
					border:false,
					title:'提示', 
					msg:'确定要删除?', 
					fn:function(r){
						if (r){
							$.ajax({
					             type: "GET",
					             url: "${ctx}/hrmgr/company/delete?_m=exec",
					             data: {id:row.id},
					             dataType: "json",
					             success: function(data){
				                        if(data.errcode!=1){
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'已删除',
												icon:'sucess-ext',
												fn:function(){
													reload();
												}
											});
										}else{
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'删除失败',
												icon:'fail-ext',
												fn:function(){
													reload();
												}
											});
											
										}
				                }
					         });
						 }
					}
				});
			}
	  }
</script>
