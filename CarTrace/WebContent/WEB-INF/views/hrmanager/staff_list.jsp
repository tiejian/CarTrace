<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
    var staffBaseinfoDatagrid;
	var toolbar = [{
		text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加工作人员',
		handler:function(){toaddstaff();}
	},{
		text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加驾驶员',
		handler:function(){toadddriver();}
	},{
		text:'<i class="fa fa-plus-square" aria-hidden="true"></i>增加安全员',
		handler:function(){toaddso();}
	},{
		text:'<i class="fa fa-edit"></i>修改',
		handler:function(){toupdate();}
	},{
		text:'<i class="fa fa-cut" aria-hidden="true"></i>删除',
		handler:function(){todelete();}
	},'-',{
		text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
		handler:function(){alert('save')}
	}];
	$(function(){
		staffBaseinfoDatagrid = $('#staffBaseinfoDatagrid').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
    })
	function toupdate(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/bn/project/bn_project_info/update?_m=init&id="+id);
	}
	function operator(id,status,file){
		var arr = new Array();
		if(status=='1'||status=='4'||status=='7'){
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\""+id+"\");'>编辑</a>");
			arr.push("<a class='a-btn' href='javascript:;' onclick='javascript:toupdate(\'"+id+"');'>删除</a>");
		}
		arr.push("<a class='a-btn' target='_blank' href='${ctx}/file/"+file+"'>下载</a>");
		return arr.join("");
	}
	function reload(){
		staffBaseinfoDatagrid.datagrid('reload');
	}
</script>
<table id="staffBaseinfoDatagrid"  style="width:100%;height:100%" class="easyui-datagrid"
		data-options="border:false,rownumbers:true,autoRowHeight:false,singleSelect:true,pagination:true,toolbar:toolbar,rowStyler: function(index,row){return {style:'height:30px'};},url:'${ctx}//hrmgr/staff/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'deptCode',width:300">部门（科室）名称</th>
			<th data-options="field:'name',width:200,align:'center'">姓名</th>
			<th data-options="field:'idCardNo',width:140,align:'center'">身份证号</th>
			<th data-options="field:'sex',width:250">性别</th>
			<th data-options="field:'age',width:250,align:'center'">年龄</th>
			<th data-options="field:'healthCond',width:200,align:'center'">健康状况</th>
			<th data-options="field:'education',width:200,align:'center'">学历</th>
			<th data-options="field:'phoneNum',width:200,align:'center'">联系电话</th>
			<th data-options="field:'jobPosts',width:200,align:'center'">岗位</th>
			<th data-options="field:'jobTitle',width:200,align:'center'">职务</th>
			<th data-options="field:'joinAt',width:200,align:'center'">入职时间</th>
			<th data-options="field:'t',width:150,align:'center',formatter:function(value,row,index){return operator(row.id,row.status,row.prjFile)}"></th>
		</tr>
	</thead>
</table>
<script type="text/javascript">
      function toaddstaff(){
			$('<div></div>').dialog({
				id:'dg-staff-info-insert',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">工作人员信息登记<span>',
				width: 1000,
				height:600,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/staff/insert?_m=init',
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-staffinfo-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registstaff();}
				},{
					id:"btn-staffinfo-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-staff-info-insert").dialog('destroy');}
				}]
			});
	  }
	  function toadddriver(){
			$('<div></div>').dialog({
				id:'dg-driver-info-insert',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">企业信息登记<span>',
				width: 1000,
				height:'80%',
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/driver/insert?_m=init',
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-staffinfo-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registstaff();}
				},{
					id:"btn-staffinfo-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-staff-info-insert").dialog('destroy');}
				}]
			});
	  }
	  function toaddso(){
			$('<div></div>').dialog({
				id:'dg-so-info-insert',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">企业信息登记<span>',
				width: 1000,
				height:'80%',
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/staff/insert?_m=init',
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-staffinfo-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registstaff();}
				},{
					id:"btn-staffinfo-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-staff-info-insert").dialog('destroy');}
				}]
			});
	  }
	  function toupdate(){
		var row = staffBaseinfoDatagrid.datagrid('getSelected');
		if(row==null){
			$.messager.alert({
				border:false,
				title:'提示',
				msg:'请选择一行数据',
				icon:'warn-ext'
			});
		}else{
			$('<div></div>').dialog({
				id:'dg-staff-info-update',
				title: '<i class="fa fa-windows"></i><span class="dialog-title">企业信息登记<span>',
				width: 1000,
				height:600,
				closed: false,
				cache: false,
				border:false,
				maximizable:true,
				resizable:true,
				href: '${ctx}/hrmgr/staff/update?_m=init&id='+row.staffId,
				modal: true,
				onClose : function() {
					$(this).dialog('destroy');
				},
				buttons:[{
					id:"btn-staffinfo-update-save",
					text:'<i class="fa fa-floppy-o" aria-hidden="true"></i>保存',
					handler:function(){registstaff();}
				},{
					id:"btn-staffinfo-update-back",
					text:'<i class="fa fa-reply" aria-hidden="true"></i>返回',
					handler:function(){$("#dg-staff-info-insert").dialog('destroy');}
				}]
			});
		}
	  }
	  function todelete(){
		    var row = staffBaseinfoDatagrid.datagrid('getSelected');
			if(row==null){
				$.messager.alert({
					border:false,
					title:'提示',
					msg:'请选择一行数据',
					icon:'warn-ext'
				});
			}else{
				$.messager.confirm({
					border:false,
					title:'提示', 
					msg:'确定要删除?', 
					fn:function(r){
						if (r){
							$.ajax({
					             type: "GET",
					             url: "${ctx}/hrmgr/staff/delete?_m=exec",
					             data: {id:row.id},
					             dataType: "json",
					             success: function(data){
				                        if(data.errcode!=1){
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'已删除',
												icon:'sucess-ext',
												fn:function(){
													reload();
												}
											});
										}else{
											$.messager.alert({
												border:false,
												title:'提示',
												msg:'删除失败',
												icon:'fail-ext',
												fn:function(){
													reload();
												}
											});
											
										}
				                }
					         });
						 }
					}
				});
			}
	  }
</script>
