<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		var iframe=false;
		var maxFileSize=50*1024;
		if(!$.support.leadingWhitespace){
			iframe=true;
		}
		var uploadifyId="staff_image";
		$('#staff_image').fileupload({
			url: '${ctx}/uploader?_m=file',
	        dataType: 'json',
			iframe:iframe,
			maxNumberOfFiles:1,
			maxFileSize:20,
			add:function(e,data){
				console.log(data);
				var o = data['files'][0];
                if(parseInt(o.size/1024)>maxFileSize){
					enabledFile();
					alert("上传文件不可以超过2M");
				}
				var endWith = jQuery.trim(o.name.substring(o.name.lastIndexOf(".")+1).toLowerCase());
				if(endWith!="bmp" && endWith!="jpg" && endWith!="png" && endWith!="gif" && endWith!="jpeg" ){
					enabledFile();
					alert("必须上传图片格式");
					return false;
				}
				initProessbar(uploadifyId+"_processbar",true);
				data.submit();
			},
		    error:function(jqXhr,textStatus,errThrown){
				clearProcessbar(uploadifyId+"_processbar");
				enabledFile();
				return false;
			},
	        done: function (e, data) {
				console.log(data);
                clearProcessbar(uploadifyId+"_processbar");
				enabledFile();
				var fileInfo = data.result;
				var url="${ctx}/upload/"+fileInfo.obj;
				$("#dndArea").html("");
				$("#dndArea").append('<img id="logopriview" src="'+url+'" width="260" height="270"/>');
				$("#progress").remove();
	        },
			progressall: function (e, data) {
				var progress = parseInt(data.loaded / data.total * 100, 10);
				console.log(progress);
                $(".queueList").append('<div class="progress" id="progress"><div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: '+progress+'%;">'+progress+'%</div></div>');
			}
	    });

        //$('#idCardNo').validatebox('validate',function(){alert(1);});

	});
	function enabledFile(){
		$("input[type='file']").each(function(){
			$(this).attr("disabled",false);
		});
	}
	function processbar(barId,processNum){
	}
	function initProessbar(barId,processNum){
	}
	function clearProcessbar(barId){
	}
	function registstaff(){
		$('#staff-insert-form').form('submit',{
			url:'${ctx}/hrmgr/staff/insert',
			queryParams:{_m:'exec'},
			onSubmit:function(){
				if($(this).form('enableValidation').form('validate')){
					$("#btn-staffinfo-save").linkbutton("disable");
					return true;
				}else{
					return false;
				}
			},
			success: function(data){
				$("#btn-staffinfo-save").linkbutton("enable");
				var ret =  eval("(" +data+ ")");
				console.log(ret);
				if(ret.errcode!=1){
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存成功',
						icon:'sucess-ext',
						fn:function(){
							reload();
							$("#dg-staff-info-insert").dialog('destroy');
						}
					});
				}else{
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存失败',
						icon:'fail-ext',
						fn:function(){
						}
					});
					
				}
				
			}
		});
		
	}
	function changeidcard(value){
		$("#idCardNo").textbox('enableValidation');
		if($("#idCardNo").textbox('isValid')){
			$("#age").textbox('setValue', getAgeFromIdcard(value));
			var gender=getGenderFromIdcard(value);
			$("#sexView").textbox('setValue', gender==1?'男':'女');
			$("#sex").val(gender);
		}
	}
</script>
<div class="normal-form">
	<form role="form" name="staff-insert-form" id="staff-insert-form" class="easyui-form form form-horizontal" data-options="novalidate:true" method="post">
				<div class="seg-title">
					<div class="seg-title-box">
			    		 <ul>
				    		<li class="seg-title-box-icon" ><i class="fa fa-book" aria-hidden="true"></i></li>
				    		<li class="seg-title-box-text">基本信息</li>
				    	</ul>
			    	</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">所属部门：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="deptCode" name="deptCode" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="学历"  data-options=" valueField: 'id',textField: 'name',url:'${ctx}/hrmgr/dept/find?_m=exec&compid=${sessionScope.session_user_key.companyId}',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">姓名：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">身份证号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="idCardNo" name="idCardNo" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="身份证号" data-options="required:true,validType:'idcard',onChange:changeidcard">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">性别：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="sexView" name="sexView" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="性别" data-options="required:true,editable:false">
						<input type="hidden" id="sex" name="sex" value="" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">年龄：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="age" name="age" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="年龄" data-options="required:true,editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">健康状况：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="healthCond" name="healthCond" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="健康状况" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/jkzk/find?_m=exec',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">职务：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="jobTitle" name="jobTitle" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="职务" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">学历：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="education" name="education" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="学历"  data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/education/find?_m=exec',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">联系电话：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="phoneNum" name="phoneNum" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="联系电话" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">入职时间：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="joinAt" name="joinAt" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="入职时间" data-options="required:true,editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="form-field-1">
						<p class="label-txt">住址：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-10">
						<input type="text" id="address" name="address" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="住址">
					</div>
				</div>
				<div class="staff-logo" id="uploader">
					<div class="queueList">
						<div id="dndArea" class="placeholder">
							
						</div>
					</div>
					<div class="upload-toolbar"><span class="btn btn-primary btn-upload"><i class="fa fa-file-image-o"></i>选择图片<input type="file" id="staff_image" name="staff_image" class="stafflogo"/></span></div>
				</div>
				<input type="hidden" id="photoPath" name="photoPath" value="" />
				<div class="seg-title">
					<div class="seg-title-box">
			    		 <ul>
				    		<li class="seg-title-box-icon" ><i class="fa fa-book" aria-hidden="true"></i></li>
				    		<li class="seg-title-box-text">驾驶证信息</li>
				    	</ul>
			    	</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">驾驶证号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">准驾车型：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/zjcx/find?_m=exec',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">档案编号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">发证机关：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">初次领证日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true,editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">有效起始日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true,editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">有效期限</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">从业状态：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/cyzt/find?_m=exec',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">从业资格证初领时间</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true,editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">从业资格证发证机关：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">从业资格证发放时间：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true,editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">从业资格证有效期至：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">专业类型：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/trafficsubject/find?_m=exec',editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">上次体检时间：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options="required:true,editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						<p class="label-txt">机动车驾驶证类附件名称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
						<input type="text" id="name" name="name" value="" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="姓名" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/trafficsubject/find?_m=exec',editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">上传：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-4">
					</div>
				</div>
		</form>
</div>
