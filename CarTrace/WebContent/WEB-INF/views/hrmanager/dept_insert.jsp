<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$.getScript('${ctx}/assets/js/canton-select.js');
	});
	function registdept(){
		$('#dept-insert-form').form('submit',{
			url:'${ctx}/hrmgr/dept/insert',
			queryParams:{_m:'exec'},
			onSubmit:function(){
				if($(this).form('enableValidation').form('validate')){
					$("#btn-deptinfo-save").linkbutton("disable");
					return true;
				}else{
					return false;
				}
			},
			success: function(data){
				$("#btn-deptinfo-save").linkbutton("enable");
				var ret =  eval("(" +data+ ")");
				console.log(ret);
				if(ret.errcode!=1){
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存成功',
						icon:'sucess-ext',
						fn:function(){
							reload();
							$("#dg-dept-info-insert").dialog('destroy');
						}
					});
				}else{
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存失败',
						icon:'fail-ext',
						fn:function(){
						}
					});
					
				}
				
			}
		});
		
	}
</script>
<div class="normal-form">
	<form role="form" name="dept-insert-form" id="dept-insert-form" class="easyui-form form form-horizontal" data-options="novalidate:true" method="post">
				<div class="seg-title">
					<div class="seg-title-box">
			    		 <ul>
				    		<li class="seg-title-box-icon" ><i class="fa fa-book" aria-hidden="true"></i></li>
				    		<li class="seg-title-box-text">企业部门信息</li>
				    	</ul>
			    	</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">部门（科室）标识名称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="name" name="name" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">部门标识简称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="shortName" name="shortName" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目编号" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">所属行政区划：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-9">
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton_p" name="canton_p" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="省" data-options="editable:false" />
						</div>
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton_c" name="canton_c" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="市" data-options="editable:false" />
						</div>
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton" name="canton" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="区" data-options="required:true,editable:false" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">部门（科室）标识地址：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="address" name="address" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="委托单位（甲方）">
					</div>
				</div>
				
               <div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">经营负责人：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="pic" name="pic" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">经营负责人手机号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="picPhone" name="picPhone" value="" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">应急电话：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="sosNum" name="sosNum" value="" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">传真号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="faxNum" name="faxNum" value="" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">成立日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="foundDate" name="foundDate" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="成立日期" data-options="editable:false,required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">撤销日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="cancelDate" name="cancelDate" value="" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="撤销日期" data-options="editable:false,required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">部门职能：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="deptFunc" name="deptFunc" value="" class="form-control easyui-textbox" style="width:100%;height:102px;line-height:34px;" title="部门职能" data-options="multiline:true,required:true">
					</div>
				</div>
		</form>
</div>
<script type="text/javascript">
//$('#ppp').panel('open').panel('refresh','${ctx}/demo/form/base');
</script>