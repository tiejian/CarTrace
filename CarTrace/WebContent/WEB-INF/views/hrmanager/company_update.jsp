<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script type="text/javascript">
	$(function() {
		$.getScript('${ctx}/assets/js/canton-select.js');
	});
	function registCompany(){
		$('#company-update-form').form('submit',{
			url:'${ctx}/hrmgr/company/update',
			queryParams:{_m:'exec'},
			onSubmit:function(){
				if($(this).form('enableValidation').form('validate')){
					$("#btn-companyinfo-save").linkbutton("disable");
					return true;
				}else{
					return false;
				}
			},
			success: function(data){
				$("#btn-companyinfo-update-save").linkbutton("enable");
				var ret =  eval("(" +data+ ")");
				if(ret.errcode!=1){
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存成功',
						icon:'sucess-ext',
						fn:function(){
							reload();
							$("#dg-company-info-update").dialog('destroy');
						}
					});
				}else{
					$.messager.alert({
						border:false,
						title:'提示',
						msg:'保存失败',
						icon:'fail-ext',
						fn:function(){
							reload();
							$("#dg-company-info-update").dialog('destroy');
						}
					});
					
				}
				
			}
		});
		
	}
</script>
<div class="normal-form">
	<form role="form" name="company-update-form" id="company-update-form" class="easyui-form form form-horizontal" data-options="novalidate:true" method="post">
				<div class="seg-title">
					<div class="seg-title-box">
			    		 <ul>
				    		<li class="seg-title-box-icon" ><i class="fa fa-book" aria-hidden="true"></i></li>
				    		<li class="seg-title-box-text">企业信息</li>
				    	</ul>
			    	</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">业户名称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="name" name="name" value="${bnCompanyInfo.name}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">业户简称：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="shortName" name="shortName" value="${bnCompanyInfo.shortName}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目编号" data-options="required:true">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">所在行政区：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-9">
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton_p" name="canton_p" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="省" data-options="editable:false" />
						</div>
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton_c" name="canton_c" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="市" data-options="editable:false" />
						</div>
						<div style="float:left;margin-right:2px;">
						<input type="text" id="canton" name="canton" value="" class="form-control easyui-combobox" style="width:198px;height:34px;line-height:34px;" title="区" data-options="required:true,editable:false" />
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="form-field-1">
						<p class="label-txt">业户地址：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-8">
						<input type="text" id="address" name="address" value="${bnCompanyInfo.address}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="委托单位（甲方）">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">营运类型：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="opType" name="opType" value="${bnCompanyInfo.opType}" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/yylx/find?_m=exec',editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">企业等级：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="companyLevel" name="companyLevel" value="${bnCompanyInfo.companyLevel}" class="form-control easyui-combobox" title="项目编号" style="width:100%;height:34px;line-height:34px;"  data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/qydj/find?_m=exec',editable:false">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">经营区域：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="opArea" name="opArea" value="${bnCompanyInfo.opArea}" class="form-control easyui-combobox" style="width:100%;height:34px;line-height:34px;" title="项目名称"  data-options=" valueField: 'code',textField: 'name',url:'${ctx}/dt/jyqy/find?_m=exec',editable:false">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">经营范围：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="opScope" name="opScope" value="${bnCompanyInfo.opScope}" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">从业人员数量：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="staffCount" name="staffCount" value="${bnCompanyInfo.staffCount}" class="form-control easyui-numberbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">车辆数：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="vehicleCount" name="vehicleCount" value="${bnCompanyInfo.vehicleCount}" class="form-control easyui-numberbox" style="width:100%;height:34px;line-height:34px;" title="项目编号">
					</div>
				</div>
                <div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">经营许可证号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="licenseNum" name="licenseNum" value="${bnCompanyInfo.licenseNum}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">核发日期：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="issueAt" name="issueAt" value="${bnCompanyInfo.issueAt}" class="form-control easyui-datebox"  style="width:100%;height:34px;line-height:34px;"title="项目编号" data-options="editable:false,required:true">
					</div>
				</div>
                   <div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">有效日期起：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="expiryStart" name="expiryStart" value="${bnCompanyInfo.expiryStart}" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="editable:false,required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">有效日期止：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="expiryEnd" name="expiryEnd" value="${bnCompanyInfo.expiryEnd}" class="form-control easyui-datebox" style="width:100%;height:34px;line-height:34px;"title="项目编号" data-options="editable:false,required:true">
					</div>
				</div>

               <div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">经营负责人：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="opPic" name="opPic" value="${bnCompanyInfo.opPic}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">经营负责人手机号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="opPicPhone" name="opPicPhone" value="${bnCompanyInfo.opPicPhone}" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">安全负责人：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="soPic" name="soPic" value="${bnCompanyInfo.soPic}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">安全负责人手机号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="soPicPhone" name="soPicPhone" value="${bnCompanyInfo.soPicPhone}" class="form-control" title="项目编号">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">应急电话：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="sosTel" name="sosTel" value="${bnCompanyInfo.sosTel}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="项目名称" data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">传真号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="faxTel" name="faxTel" value="${bnCompanyInfo.faxTel}" class="form-control" title="项目编号">
					</div>
					<input type="hidden" id="id" name="id" value="${bnCompanyInfo.id}" />
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">
						<p class="label-txt">单位管理账号：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id="adminAccount" name="adminAccount" value="${bnCompanyInfo.adminAccount}" class="form-control easyui-textbox" style="width:100%;height:34px;line-height:34px;" title="单位管理账号"  data-options="required:true">
					</div>
					<label class="col-sm-2 control-label">
						<p class="label-txt">单位管理账号密码：</p>
						<p class="star">*</p>
					</label>
					<div class="col-sm-3">
						<input type="text" id=adminPasswd name="adminPasswd" value="${bnCompanyInfo.adminPasswd}" class="form-control" title="单位管理账号密码">
					</div>
				</div>
		</form>
</div>
<script type="text/javascript">
//$('#company-update-form').form('load', '${ctx}/hrmgr/company/get/${id}?_m=exec');
</script>