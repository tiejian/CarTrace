<<<<<<< HEAD
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap-ext.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/theme-jacqui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/font-awesome-4.5.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/lib/layer/skin/layer.ext.css">
	
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/ext/datagrid-filter.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/easyui/locale/easyui-lang-zh_CN.js"></script>
    <script type="text/javascript" src="${ctx}/lib/layer/layer.js"></script>
    <script type="text/javascript">
	    $(function() {
	    	setTabs();
	    	homepage();
	    	$("#main-navbar").bind("click",function(){
	    		$('.mainpage').layout('collapse','west');
	    	});
	    	$(".list-group-item").click(function(){
	    		$(".list-group-item").siblings().removeClass("on");
	    		$(this).addClass("on");
	    		var url=$(this).attr("url");
	    		var text=$(this).attr("txt");
	    		mainload(url,text);
	    	});
	    	
	    })
		
	</script>
	<style type="text/css">
	.nav-right-menu-toggle{cursor: pointer;}
	.nav-right-menu-toggle:HOVER{background: #0568b4;}
	.menus-user{color:#FFFFFF;height:105px;line-height: 105px;}
	.menus-user a{color:#FFFFFF;text-decoration: none;}
	.menus-user li{height:35px;line-height: 35px;padding-left:10px;cursor: pointer;}
	.menus-user li:HOVER{height:35px;line-height: 35px;background:#1caf9a}
    </style>
</head>
<body class="easyui-layout mainpage">
    <!--[if lte IE 7]>
        <div id="errorie"><div>您还在使用老掉牙的IE，正常使用系统前请升级您的浏览器到 IE8以上版本 <a target="_blank" href="http://windows.microsoft.com/zh-cn/internet-explorer/ie-8-worldwide-languages">点击升级</a>&nbsp;&nbsp;强烈建议您更改换浏览器：<a href="http://down.tech.sina.com.cn/content/40975.html" target="_blank">谷歌 Chrome</a></div></div>
    <![endif]-->
    <div data-options="region:'north',border:false,minHeight:85" class="navbar">
    	<div style="width:25%;height:70px;line-height:70px;float: left;" id="logo"></div>
    	<div style="width:50%;height:70px;line-height:70px;float: left;" id="nav">
    	</div>
    	<div style="width:25%;height:85px;line-height:85px;float: right;" id="out">
    		<div style="float:right;margin-right:10px;">
	    		<ul class="nav navbars-right">
	    			<li id="userinfo" class="nav-right-menu-toggle" style="float: left;text-align:center;width:85px;height:85px;line-height: 85px;margin-left:15px;"><img src="${ctx}/assets/images/avatar-1-small.jpg" class="img-circle" style="width:40px;">&nbsp;<i class="fa fa-caret-down" style="color:#FFFFFF;"></i></li>
	    			<li style="float: left;height:85px;line-height: 85px;"><div style="border-left: 1px solid #6ab3ea;height:65px;line-height: 65px;margin-top:10px;"></div></li>
	    			<li id="help" class="nav-right-menu-toggle" style="float: left;text-align:center;width:80px;height:85px;line-height: 85px;"><i class="fa fa-question-circle fa-3x" style="color:#FFFFFF;margin-top:25px;"></i></li>
	    			<li style="float: left;height:85px;line-height: 85px;"><div style="border-left: 1px solid #6ab3ea;height:65px;line-height: 65px;margin-top:10px;"></div></li>
	    			<li id="loginout" class="nav-right-menu-toggle" style="float: left;text-align:center;width:80px;height:85px;line-height: 85px;"><i class="fa fa-power-off fa-3x" style="color:#ff6868;margin-top:25px;"></i></li>
	    		</ul>
    		</div>
    	</div>
    </div>
	<div id="main-left-menu" data-options="region:'west',split:true,border:false,title:'==功能列表==',headerCls:'user-profile',bodyCls:'unit-admin-menu-body',cls:'unit-admin-menu',header:'#left-menu-header'" style="width:220px;">
		<div id="left-menu-header" class="user-profile">
			<div class="u-logo">
				<a href="#"><img alt="" src="${ctx}/assets/images/avatar-1.jpg"></a>
			</div>
			<div class="u-info">
				<span class="u-name"><h5 > 管理员 </h5></span>
				<span class="last-login-time"><h6> 2016-02-25 08:08:06 </h6></span>
			</div>
			<div class="u-toggle-box">
				<a class="u-btn-ss" href="#" id="main-navbar"></a>
			</div>
		</div>
		<div class="easyui-accordion main-left-menu-body" style="width:100%;" data-options="multiple:true,border:false">
			<div title='<i class="fa fa-th-large"></i>项目管理' data-options="iconCls:'icon-project',selected:true,collapsed:true,collapsible:true" style="">
                <ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="项目申报">项目申报</li>
				  <li class="list-group-item" url="${ctx}/yuan/audit/prj/search?_m=init" txt="院部评审">院部评审</li>
				  <li class="list-group-item" url="${ctx}/bn/project/expert/prj/search?_m=init" txt="指定评审专家">指定评审专家</li>
				  <li class="list-group-item" url="${ctx}/expert/audit/prj/search?_m=init" txt="专家评审">专家评审</li>
				</ul>
			</div>
			<div title='<i class="fa fa-th-large"></i>系统管理' data-options="iconCls:'icon-project',selected:true,collapsed:false,collapsible:false" style="">
				<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/expert/search?_m=init"  txt="专家库管理">专家库管理</li>
				  <li class="list-group-item" url="${ctx}/dt/department/search?_m=init" txt="院系管理">院系管理</li>
				  <li class="list-group-item" url="${ctx}/bn/customer/search?_m=init" txt="用户管理">用户管理</li>
				  <li class="list-group-item" url="${ctx}/st/role/search?_m=init" txt="角色管理">角色管理</li>
				</ul>
			</div> 
			<div title='<i class="fa fa-th-large"></i>例子' data-options="iconCls:'icon-project',collapsed:false,collapsible:false" style="">
				<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/demo/list" txt="表格">表格</li>
				  <li class="list-group-item" url="${ctx}/demo/form" txt="表单">表单</li>
				  <li class="list-group-item" url="${ctx}/homepage?_m=init" txt="服务单位首页">服务单位首页</li>
				  <li class="list-group-item" url="${ctx}/bn/customer/search?_m=init" txt="用户管理">用户管理</li>
				  <li class="list-group-item" url="${ctx}/st/role/search?_m=init" txt="角色管理">角色管理</li>
				</ul>
			</div>
			
		</div>
	</div>
	<div data-options="region:'south',border:false" style="height:24px;background:#EBF7FD;">south region</div>
	<div data-options="region:'center',border:false">
		<div class="easyui-tabs main-tab" data-options="tabHeight:30,tabWidth:130,height:'100%',width:'100%'"  id="main-tab">
			<div title="我的主页" id="homepage">
			sdsdsdds
			</div>
		</div>
	</div>
	<div id="userinfo-temp" style="display: none;">
		<div style="padding:0px;margin:0px;background:#2b3034;width:130px;height:105px;">
			<ul class="menus-user">
				<li>
					<a href="#">
						<i class="fa fa-user fa-lg" style="color:#43d1ff;"></i><span style="padding-left:10px;padding-bottom:3px;">个人信息</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-key fa-lg" style="color:#ff8a00;"></i><span style="padding-left:6px;padding-bottom:3px;">密码修改</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-lock fa-lg" style="color:red;"></i><span style="padding-left:10px;padding-bottom:3px;">锁定系统</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
    <script type="text/javascript">
		function setTabs(){
			$('#main-tab').tabs({
				plain:true,
				narrow: true,
				border:false,
			});
			
			$('#userinfo').tooltip({
				content: $('#userinfo-temp').html(),
				showEvent: 'click',
				onShow: function(){
					var t = $(this);
					t.tooltip('tip').css({"background":"#2b3034","border":"1px solid #000000"});
					t.tooltip('tip').unbind().bind('mouseenter', function(){
						t.tooltip('show');
					}).bind('mouseleave', function(){
						t.tooltip('hide');
					});
				}
			});
			
			$('#help').tooltip({
				position: 'bottom',
				content: '<div style="color:#fff;width:40px;text-align:center;">帮 助</div>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#666',
						borderColor: '#666'
					});
				}
			});
			
			$('#loginout').tooltip({
				position: 'bottom',
				content: '<div style="color:#fff;width:40px;text-align:center;">退 出</div>',
				onShow: function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#666',
						borderColor: '#666'
					});
				}
			});
		}
		function mainload(url,txt){
			if($('#main-tab').tabs('exists',txt)){
				$('#main-tab').tabs('select',txt);
				var tab = $('#main-tab').tabs('getSelected');
				tab.panel('refresh', url);
			}else{
				$('#main-tab').tabs('add',{
					id:'1',
					title: txt,
					href: url,
					closable: true
				});
			}
		}
		function homepage(){
			//var tab = $('#main-tab').tabs('getSelected');
			//tab.panel('refresh', "${ctx}/homepage");
		}
		
	</script>
</body>
</html>
=======
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="zh">
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/bootstrap-ext.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/metro/easyui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/theme-jacqui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/jq-upload/jquery.fileupload.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/jq-upload/jquery.fileupload-ui.css">
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/font-awesome-4.5.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" type="text/css" href="${ctx}/assets/css/ztree/zTreeStyle/zTreeStyle.css">
	
	
	<script type="text/javascript" src="${ctx}/assets/js/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx}/assets/js/easyui/ext/datagrid-filter.js"></script>
    <script type="text/javascript" src="${ctx}/assets/js/easyui/locale/easyui-lang-zh_CN.js"></script>
    
    
    <script type="text/javascript" src="${ctx}/assets/js/ztree/jquery.ztree.all.min.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/distpicker.min.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/jq-upload/vendor/jquery.ui.widget.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/jq-upload/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/jq-upload/jquery.fileupload.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/jq-upload/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="${ctx}/assets/js/common.js"></script>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=BaOal2TeYs2BVijpSaA3vIMTLviXL1cO"></script>
<script type="text/javascript" src="${ctx}/assets/gis/hanlinyuan.js"></script>
    
    
    <script type="text/javascript">
        window.ctx = '${ctx}';
	    $(function() {
	    	setTabs();
	    	homepage();
	    	$("#main-navbar").bind("click",function(){
	    		$('.mainpage').layout('collapse','west');
	    	});
	    	$(".list-group-item").click(function(){
	    		$(".list-group-item").siblings().removeClass("on");
	    		$(this).addClass("on");
	    		var url=$(this).attr("url");
	    		var text=$(this).attr("txt");
	    		mainload(url,text);
	    	});
	    	
	    })
		
	</script>
</head>
<body class="easyui-layout mainpage">
    <!--[if lte IE 7]>
        <div id="errorie"><div>您还在使用老掉牙的IE，正常使用系统前请升级您的浏览器到 IE8以上版本 <a target="_blank" href="http://windows.microsoft.com/zh-cn/internet-explorer/ie-8-worldwide-languages">点击升级</a>&nbsp;&nbsp;强烈建议您更改换浏览器：<a href="http://down.tech.sina.com.cn/content/40975.html" target="_blank">谷歌 Chrome</a></div></div>
    <![endif]-->
    <div data-options="region:'north',border:false,minHeight:55" class="navbar">
    </div>
	<div id="main-left-menu" data-options="region:'west',split:true,border:false,title:'==功能列表==',headerCls:'user-profile',bodyCls:'unit-admin-menu-body',cls:'unit-admin-menu',header:'#left-menu-header'" style="width:220px;">
		<div id="left-menu-header" class="user-profile">
			<div class="u-logo">
				<a href="#"><img alt="" src="${ctx}/assets/images/avatar-1.jpg"></a>
			</div>
			<div class="u-info">
				<span class="u-name"><h5 > 管理员 </h5></span>
				<span class="last-login-time"><h6> 2016-02-25 08:08:06 </h6></span>
			</div>
			<div class="u-toggle-box">
				<a class="u-btn-ss" href="#" id="main-navbar"></a>
			</div>
		</div>
		<div class="easyui-accordion main-left-menu-body" style="width:100%;" data-options="multiple:false,border:false">
			<%-- <div title='<i class="fa fa-th-large"></i>项目管理' data-options="iconCls:'icon-project',selected:true,collapsed:true,collapsible:true" style="">
                <ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/project/bn_project_info/search?_m=init" txt="项目申报">项目申报</li>
				  <li class="list-group-item" url="${ctx}/yuan/audit/prj/search?_m=init" txt="院部评审">院部评审</li>
				  <li class="list-group-item" url="${ctx}/bn/project/expert/prj/search?_m=init" txt="指定评审专家">指定评审专家</li>
				  <li class="list-group-item" url="${ctx}/expert/audit/prj/search?_m=init" txt="专家评审">专家评审</li>
				</ul>
			</div>
			<div title='<i class="fa fa-th-large"></i>系统管理' data-options="iconCls:'icon-project',selected:true,collapsed:false,collapsible:false" style="">
				<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/bn/expert/search?_m=init"  txt="专家库管理">专家库管理</li>
				  <li class="list-group-item" url="${ctx}/dt/department/search?_m=init" txt="院系管理">院系管理</li>
				  <li class="list-group-item" url="${ctx}/bn/customer/search?_m=init" txt="用户管理">用户管理</li>
				  <li class="list-group-item" url="${ctx}/st/role/search?_m=init" txt="角色管理">角色管理</li>
				</ul>
			</div>  --%>
			<div title='<i class="fa fa-th-large"></i>人力资源管理' data-options="iconCls:'icon-project'" style="">
				<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/hrmgr/company/search?_m=init"  txt="企业信息管理">企业信息管理</li>
				  <li class="list-group-item" url="${ctx}/hrmgr/dept/search?_m=init" txt="部门信息管理">部门信息管理</li>
				  <li class="list-group-item" url="${ctx}/hrmgr/staff/search?_m=init" txt="人员信息管理">人员信息管理</li>
				</ul>
			</div> 
			<div title='<i class="fa fa-th-large"></i>例子' data-options="iconCls:'icon-project'" style="">
				<ul class="list-group main-menu-item">
				  <li class="list-group-item" url="${ctx}/demo/list" txt="表格">表格</li>
				  <li class="list-group-item" url="${ctx}/demo/form" txt="表单">表单</li>
				  <li class="list-group-item" url="${ctx}/demo/gis" txt="表单">百度地图</li>
				  <li class="list-group-item" url="${ctx}/homepage?_m=init" txt="服务单位首页">服务单位首页</li>
				  <li class="list-group-item" url="${ctx}/bn/customer/search?_m=init" txt="用户管理">用户管理</li>
				  <li class="list-group-item" url="${ctx}/st/role/search?_m=init" txt="角色管理">角色管理</li>
				</ul>
			</div>
			
		</div>
	</div>
	<div data-options="region:'south',border:false" style="height:24px;background:#EBF7FD;padding:10px;">south region</div>
	<div data-options="region:'center',border:false">
		<div class="easyui-tabs main-tab" data-options="tabHeight:30,tabWidth:110,height:'100%',width:'100%'"  id="main-tab">
			<div title='<i class="fa fa-home fa-lg" aria-hidden="true"></i>我的主页' id="homepage">
			sdsdsdds
			</div>
		</div>
	</div>
    <script type="text/javascript">
		function setTabs(){
			$('#main-tab').tabs({
				plain:true,
				narrow: true,
				border:false,
			})
		}
		function mainload(url,txt){
			if($('#main-tab').tabs('exists',txt)){
				$('#main-tab').tabs('select',txt);
				var tab = $('#main-tab').tabs('getSelected');
				tab.panel('refresh', url);
			}else{
				$('#main-tab').tabs('add',{
					id:'1',
					title: txt,
					href: url,
					closable: true
				});
			}
		}
		function homepage(){
			//var tab = $('#main-tab').tabs('getSelected');
			//tab.panel('refresh', "${ctx}/homepage");
		}
		
	</script>
</body>
</html>
>>>>>>> branch 'master' of https://git.oschina.net/matt8848/CarTrace.git
