<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" uri="/WEB-INF/tld/klnst.tld"  %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<script>
	$(function(){
		
		var rolelist = $('#rolelist').datagrid({
			filterBtnIconCls:'icon-filter',
			remoteFilter:true
		});
		rolelist.datagrid('enableFilter', [
		   {
			field:'name',
			type:'text',
			op:['contains','equal','notequal','less','greater']
		   },
		   {
			field:'roleType',
			type:'combobox',
			options:{
				valueField: 'value',
				textField: 'label',
				data: [
					{label: '企业',value: '1'},
					{label: '教职人员',value: '2'}
				]
		    },
			op:['equal']
		   }
		 ]);
		$("#btnroleadd").bind("click",function(){
			$('<div></div>').dialog({
        	    title: '增加角色信息',
        	    width: 600,
        	    height:300,
        	    closed: false,
        	    cache: false,
        	    href: '${ctx}/st/role/insert?_m=init',
        	    modal: true,
        	    onClose : function() {
                    $(this).dialog('destroy');
                }
        	});
		});
    })
    function getsummary(id){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/role/audit/prj/get?_m=exec&id="+id);
	}
	
    function transtype(value){
		switch(value)
		{
		case "1":
		    return "企业";
		    break;
		default:
			return "教职人员";		
		}
	}
    function transagefromidcard(value){
    	var date = new Date();
    	var year = date.getFullYear(); 
    	var birthday_year = parseInt(value.substr(6,4));
    	var userage= year - birthday_year;
    	return userage;
	}
    function transgenderfromidcard(value){
    	if (parseInt(value.substr(16, 1)) % 2 == 1) { 
   		   return "男";
   		} else { 
   			return "女";
   		} 
	}
	function updaterole(id){
		$('<div></div>').dialog({
    	    title: '修改角色信息',
    	    width: 600,
    	    height:300,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/st/role/update?_m=init&code='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function delrole(id){
		$.ajax({
			url : '${ctx}/st/role/delete?_m=exec&code=' + id,
			type : 'GET',
			success : function(data, status, xhr) {
				//var data = eval('(' + data + ')');
		    	if(data.errcode==0){
					$.messager.alert('提示信息','已删除!','info',refresh());
				}else{
					$.messager.alert('提示信息','删除失败!','error');
				}
			}
		});
	}
	function roleassignuser(id){
		$('<div></div>').dialog({
    	    title: '分配用户',
    	    width: 800,
    	    height:500,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/st/role/assign/user?_m=init&code='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function roleassignmenu(id){
		$('<div></div>').dialog({
    	    title: '分配功能',
    	    width: 800,
    	    height:500,
    	    closed: false,
    	    cache: false,
    	    href: '${ctx}/st/role/assign/menu?_m=init&code='+id,
    	    modal: true,
    	    onClose : function() {
                $(this).dialog('destroy');
            }
    	});
	}
	function refresh(){
		var tab = $('#main-tab').tabs('getSelected');
		tab.panel('refresh', "${ctx}/st/role/search?_m=init");
	}
</script>
<table id="rolelist"  style="width:100%;height:100%"
		data-options="rownumbers:true,singleSelect:true,pagination:true,toolbar:'#roletb',url:'${ctx}/st/role/search?_m=load',method:'get'">
	<thead>
		<tr>
			<th data-options="field:'code',width:120">代码</th>
			<th data-options="field:'name',width:200">名称</th>
			<th data-options="field:'descript',width:300">描述</th>
			<th data-options="field:'t',width:300,align:'center',formatter:function(value,row,index){return '<a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:updaterole(\''+row.code+'\');&quot;>修改</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:delrole(\''+row.code+'\');&quot;>删除</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:roleassignuser(\''+row.code+'\');&quot;>分配用户</a><a class=&quot;a-btn&quot; href=&quot;javascript:;&quot; onclick=&quot;javascript:roleassignmenu(\''+row.code+'\');&quot;>分配功能</a>'}"></th>
		</tr>
	</thead>
</table>
<div id="roletb" style="padding:2px 5px;">
	<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" id="btnroleadd">增加</a>
</div>
<div id="f_roleopera" style="width:500px;z-index: 9999;display:none;"></div>