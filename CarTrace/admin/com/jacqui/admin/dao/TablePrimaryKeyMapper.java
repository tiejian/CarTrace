package com.jacqui.admin.dao;

import com.jacqui.admin.domain.TablePrimaryKey;
import com.jacqui.core.mybatis.MyBatisRepository;
@MyBatisRepository
public interface TablePrimaryKeyMapper {
	void getKey(TablePrimaryKey parameters);
}
