package com.jacqui.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jacqui.admin.dao.TablePrimaryKeyMapper;
import com.jacqui.admin.domain.TablePrimaryKey;
/**
 * 用于生成主键
 * @ClassName: StUserService   
 * @Description: 系统用户 
 * @author 白展堂
 * 
 */
@Service
public class TablePrimaryKeyService {
	
	@Autowired
	private TablePrimaryKeyMapper tablePrimaryKeyMapper;
	

	public String getCompanyId(String canton){
		StringBuffer sb = new StringBuffer();
		sb.append(canton);
		TablePrimaryKey p = new TablePrimaryKey();
		p.setStub(canton);
		tablePrimaryKeyMapper.getKey(p);
		sb.append(p.getKey());
		return sb.toString();
	}

}
