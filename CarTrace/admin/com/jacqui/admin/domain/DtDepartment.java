package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: DtDepartment  
 * @Description: 院系 
 * @author 白展堂
 * 
 */
public class DtDepartment implements Serializable{

    private static final long serialVersionUID = 1L;

    private String dm; //代码
    
    private String mc; //名称
    
    private int zt; //状态
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getDm(){
    	return dm;
    }
    public void setDm(String dm){
        this.dm = dm;
    }
    public String getMc(){
    	return mc;
    }
    public void setMc(String mc){
        this.mc = mc;
    }
    public int getZt(){
    	return zt;
    }
    public void setZt(int zt){
        this.zt = zt;
    }
	public Map<String, String> getLabels() {
		labels.put("dm", "代码");
		labels.put("mc", "名称");
		labels.put("zt", "状态");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}