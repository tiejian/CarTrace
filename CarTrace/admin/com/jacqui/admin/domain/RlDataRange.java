package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: RlDataRange  
 * @Description: 权限范围 
 * @author 白展堂
 * 
 */
public class RlDataRange implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //主键
    
    private String companyCode; //业户标识
    
    private String companyName; //业户名称
    
    private String userId; //用户Id
    
    private String dataRange; //权限范围0无权1全部2本企业3本部门
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getCompanyCode(){
    	return companyCode;
    }
    public void setCompanyCode(String companyCode){
        this.companyCode = companyCode;
    }
    public String getCompanyName(){
    	return companyName;
    }
    public void setCompanyName(String companyName){
        this.companyName = companyName;
    }
    public String getUserId(){
    	return userId;
    }
    public void setUserId(String userId){
        this.userId = userId;
    }
    public String getDataRange(){
    	return dataRange;
    }
    public void setDataRange(String dataRange){
        this.dataRange = dataRange;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "主键");
		labels.put("companyCode", "业户标识");
		labels.put("companyName", "业户名称");
		labels.put("userId", "用户Id");
		labels.put("dataRange", "权限范围0无权1全部2本企业3本部门");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}