package com.jacqui.admin.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Maps;
import com.jacqui.prj.domain.BnExpert;
/**
 * 
 * @ClassName: StRole  
 * @Description: 系统角色信息表 
 * @author 白展堂
 * 
 */
public class StRole implements Serializable{

    private static final long serialVersionUID = 1L;

    private String code; //代码
    
    private String name; //名称
    
    private String descript; //描述
    
    private List<StMenu> menus;
    
//    private Set<StUser> users;
    
    private List<BnExpert> users;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getDescript(){
    	return descript;
    }
    public void setDescript(String descript){
        this.descript = descript;
    }
	public Map<String, String> getLabels() {
		labels.put("code", "代码");
		labels.put("name", "名称");
		labels.put("descript", "描述");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public List<StMenu> getMenus() {
		return menus;
	}
	public void setMenus(List<StMenu> menus) {
		this.menus = menus;
	}
	public List<BnExpert> getUsers() {
		return users;
	}
	public void setUsers(List<BnExpert> users) {
		this.users = users;
	}
}