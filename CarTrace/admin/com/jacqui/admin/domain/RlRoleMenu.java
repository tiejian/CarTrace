package com.jacqui.admin.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: RlRoleMenu  
 * @Description: 角色权限分配 
 * @author 白展堂
 * 
 */
public class RlRoleMenu implements Serializable{

    private static final long serialVersionUID = 1L;

    private String roleCode; //角色代码
    
    private String menuCode; //菜单代码
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getRoleCode(){
    	return roleCode;
    }
    public void setRoleCode(String roleCode){
        this.roleCode = roleCode;
    }
    public String getMenuCode(){
    	return menuCode;
    }
    public void setMenuCode(String menuCode){
        this.menuCode = menuCode;
    }
	public Map<String, String> getLabels() {
		labels.put("roleCode", "角色代码");
		labels.put("menuCode", "菜单代码");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}