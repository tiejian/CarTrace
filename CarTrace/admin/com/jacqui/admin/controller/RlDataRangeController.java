package com.jacqui.admin.controller;

import java.io.File;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.admin.service.RlDataRangeService;
import com.jacqui.admin.domain.RlDataRange;
import com.jacqui.constant.Constant;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.web.BaseController;
import com.jacqui.core.utils.CompressUtil;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.SystemUtil;
import com.jacqui.core.utils.TimeUtils;
/**
 * 
 * @ClassName: RlDataRangeController 
 * @Description: 权限范围 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/rl/data/rl_data_range")
public class RlDataRangeController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(RlDataRangeController.class);
	
	@Autowired
	public RlDataRangeService rlDataRangeService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public String searching(Model model,@RequestParam("page") int page,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<RlDataRange> list = Lists.newArrayList();
		list = rlDataRangeService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		RlDataRange rlDataRange = rlDataRangeService.findUniqueBy(parameters);
		return rlDataRange;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,RlDataRange rlDataRange,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			rlDataRangeService.insert(rlDataRange);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,RlDataRange rlDataRange,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			rlDataRangeService.update(rlDataRange);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,RlDataRange rlDataRange,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			rlDataRangeService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			rlDataRangeService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
