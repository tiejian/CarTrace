package com.jacqui.admin.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.admin.domain.DtDepartment;
import com.jacqui.admin.service.DtDepartmentService;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.web.BaseController;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
/**
 * 
 * @ClassName: DtDepartmentController 
 * @Description: 院系 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/dt/department")
public class DtDepartmentController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(DtDepartmentController.class);
	
	@Autowired
	public DtDepartmentService dtDepartmentService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "department/department_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<DtDepartment> retObject = new PageResult<>();
		retObject = dtDepartmentService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		List<DtDepartment> list= retObject.getList();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", retObject.getList());
		map.put("total", retObject.getTotalCount());
		map.put("pageCurrent", page);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<DtDepartment> list = Lists.newArrayList();
		list = dtDepartmentService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		DtDepartment dtDepartment = dtDepartmentService.findUniqueBy(parameters);
		return dtDepartment;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "department/department_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,DtDepartment dtDepartment,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			dtDepartment.setZt(1);
			dtDepartmentService.insert(dtDepartment);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(@RequestParam String dm,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("dm", dm);
		DtDepartment dtDepartment = dtDepartmentService.findUniqueBy(parameters);
		model.addAttribute("dtDepartment", dtDepartment);
		return "department/department_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,DtDepartment dtDepartment,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			dtDepartmentService.update(dtDepartment);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("dm") String dm,Model model,DtDepartment dtDepartment,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			dtDepartmentService.deleteById(dm);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			dtDepartmentService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
