package com.jacqui.dict.service;



import java.text.ParseException;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import com.jacqui.dict.dao.DtZjcxMapper;
import com.jacqui.dict.domain.DtZjcx;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.persistence.Criterion;
import com.jacqui.core.persistence.DynamicSpecifications;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.form.PageResult;
import com.jacqui.service.BaseService;

import com.google.common.collect.Maps;

/**
 * 
 * @ClassName: DtZjcxService   
 * @Description: 准驾车型 
 * @author 白展堂
 
 */
@Service
public class DtZjcxService extends BaseService{

   private static Logger logger = LoggerFactory.getLogger(DtZjcxService.class);
	
	@Autowired
	public DtZjcxMapper dtZjcxMapper;
	
	
	/**
	 * 
	 * @Title: paging4DynamicTblName 
	 * @Description: 表名为动态参数的分页查询
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<DtZjcx> paging4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities,Integer page,Integer limit) {
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,tblName);
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return dtZjcxMapper.paging4DynamicTblName(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount4DynamicTblName 
	 * @Description: <p>表名为动态参数的总记录数查询，当数据量大的时候原有分页带有的总记录数方法会很消耗性能</p>
	 * <p>可通过此方法单独计算总记录数</p>
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities) {
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,tblName);
		return dtZjcxMapper.pagingCount4DynamicTblName(criteria);
	}
	/**
	 * 
	 * @Title: paging 
	 * @Description: 表名固定的分页方法 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<DtZjcx> paging(List<SearchFilter> filters,Integer page,Integer limit) {
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"dt_zjcx");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return dtZjcxMapper.paging(criteria,pageBounds);
	}
	/**
	 * @Title: pagingExt 
	 * @Description: 表名固定的分页方法，直接返回总记录数, 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型
	 * @throws ParseException
	 */
	public PageResult pagingWithTotal(List<SearchFilter> filters,Integer page,Integer limit){
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"dt_zjcx");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		List<DtZjcx> list = dtZjcxMapper.paging(criteria,pageBounds);
		PageList<DtZjcx> pageList = (PageList)list;
		PageResult result = new PageResult();
		result.setList(pageList);
		result.setTotalCount(pageList.getPaginator().getTotalCount());
		result.setTotalPage(pageList.getPaginator().getTotalPages());
		return result;
	}
	/**
	 * 
	 * @Title: paging4elastic 
	 * @Description: 全文检索的分页方法
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<DtZjcx> paging4elastic(List<SearchFilter> filters,Integer page,Integer limit) throws ParseException{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"dt_zjcx");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return dtZjcxMapper.paging4elastic(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount 
	 * @Description: 表名固定的记录总数方法
	 * @param filters
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount(List<SearchFilter> filters) {
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		Criteria criteria = new Criteria(criterions,"dt_zjcx");
		return dtZjcxMapper.pagingCount(criteria);
	}
	/**
	 * 
	 * @Title: findBy 
	 * @Description: 根据属性查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<DtZjcx> findBy(Map<String, Object> parameters) {
		return dtZjcxMapper.findBy(parameters);
	}
    /**
	 * 
	 * @Title: findBy 
	 * @Description: 根据主键查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
    public DtZjcx findUniqueById(String id){
        HttpServletRequest request = getHttpServletRequest();
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("id",id);
        //parameters.put("__user__",user.getCertificateNum());
        //parameters.put("__tblName__","dt_zjcx");
        //parameters.put("xzq",user.getDistrictCode());
		return dtZjcxMapper.findUniqueBy(parameters);
	}
	/**
     * @Title: findUniqueById 
     * @Description: 根据属性查询
     * @param id
     * @return DsjSkrkxx    返回类型 
     */
    public DtZjcx findUniqueBy(Map<String, Object> parameters){
		return dtZjcxMapper.findUniqueBy(parameters);
	}
    /**
     * @Title: insert 
     * @Description: 插入操作
     * @param dsjSkrkxx
     * @return String    返回类型 
     */
    public String insert(DtZjcx dtZjcx){
    	dtZjcxMapper.insert(dtZjcx);
		return  String.valueOf(dtZjcx.getCode());
	}
    /**
     * @Title: update 
     * @Description: 更新操作 
     * @param dsjSkrkxx
     */
    public void update(DtZjcx dtZjcx){
		dtZjcxMapper.update(dtZjcx);
	}
    /**
     * @Title: deleteById 
     * @Description: 根据主键删除
     * @param id
     */
    public void deleteById(String code){
		dtZjcxMapper.deleteById(code);
	}
    /**
     * @Title: deleteBy
     * @Description: 根据属性删除
     * @param id
     */
    public void deleteBy(Map<String, Object> parameters){
		dtZjcxMapper.deleteBy(parameters);
	}
	/**
	 * @Title: getHttpServletRequest
     * @Description: 可以在service层获取request,仅限地不使用dubbo的情况。使用dubbo此方法无用
     * @return void    返回类型 
	 */
	private static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}
}
