package com.jacqui.dict.dao;

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.jacqui.dict.domain.DtGw;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.mybatis.MyBatisRepository;
/**
 * 
 * @ClassName: DtGwMapper  
 * @Description: 岗位 
 * @author 白展堂
 *
 */
@MyBatisRepository
public interface DtGwMapper {
    /**
     * @Description 动态表名的分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<DtGw>
     */
    List<DtGw> paging4DynamicTblName(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 动态表名的分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount4DynamicTblName(Criteria criteria);
    /**
     * @Description 分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<DtGw>
     */
    List<DtGw> paging(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 用于全文检索的分页，需要指定查询字段
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<DtGw>
     */
    List<DtGw> paging4elastic(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 获取分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount(Criteria criteria);
    /**
     * @Description 根据条件查询
     * @param parameters
     * @return List<DtGw>
     */
    List<DtGw> findBy(Map<String, Object> parameters);
    /**
     * @Description 根据条件查询并返回唯一值
     * @param parameters
     * @return DtGw
     */
    DtGw findUniqueBy(Map<String, Object> parameters);
    /**
     * @Description 插入记录
     * @param stUser
     * @return 返回插入影响的行数
     */
    int insert(DtGw dtGw);
    /**
     * @Description 更新记录
     * @param DtGw
     * @return 返回更新影响的行数
     */
    int update(DtGw dtGw);
    /**
     * @Description 根据主键删除
     * @param code
     */
    void deleteById(String code);
    /**
     * @Description 批量删除
     * @param parameters 批量删除条件组合
     */
    void deleteBy(Map<String, Object> parameters);
}