package com.jacqui.dict.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: DtCyzt  
 * @Description: 从业状态 
 * @author 白展堂
 * 
 */
public class DtCyzt implements Serializable{

    private static final long serialVersionUID = 1L;

    private String code; //代码
    
    private String name; //名称
    
    private int displayOrder; //显示顺序
    
    private int status; //状态0失效1有效
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getDisplayOrder(){
    	return displayOrder;
    }
    public void setDisplayOrder(int displayOrder){
        this.displayOrder = displayOrder;
    }
    public int getStatus(){
    	return status;
    }
    public void setStatus(int status){
        this.status = status;
    }
	public Map<String, String> getLabels() {
		labels.put("code", "代码");
		labels.put("name", "名称");
		labels.put("displayOrder", "显示顺序");
		labels.put("status", "状态0失效1有效");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}