package com.jacqui.hrmanager.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.hrmanager.service.BnDepartmentInfoService;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.hrmanager.domain.BnCompanyInfo;
import com.jacqui.hrmanager.domain.BnDepartmentInfo;
import com.jacqui.admin.domain.StUser;
import com.jacqui.constant.Constant;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.persistence.SearchFilter.Operator;
import com.jacqui.core.web.BaseController;
import com.jacqui.core.utils.CompressUtil;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.utils.SystemUtil;
import com.jacqui.core.utils.TimeUtils;
/**
 * 
 * @ClassName: BnDepartmentInfoController 
 * @Description: 部门信息 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/hrmgr/dept")
public class BnDepartmentInfoController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(BnDepartmentInfoController.class);
	
	@Autowired
	public BnDepartmentInfoService bnDepartmentInfoService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "hrmanager/dept_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		StUser user = (StUser) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		filters.add(new SearchFilter("company_id",Operator.EQ,user.getCompanyId()));
		PageResult<BnProjectInfo> retObject = new PageResult<>();
		retObject = bnDepartmentInfoService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		return paging(retObject.getList(),retObject.getTotalCount(),page);
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String companyId=request.getParameter("compid");
		if(StringUtils.isNotBlank(companyId)){
			parameters.put("company_id", companyId);
		}
		List<BnDepartmentInfo> list = Lists.newArrayList();
		list = bnDepartmentInfoService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		BnDepartmentInfo bnDepartmentInfo = bnDepartmentInfoService.findUniqueBy(parameters);
		return bnDepartmentInfo;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "hrmanager/dept_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,BnDepartmentInfo bnDepartmentInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			StUser user = (StUser) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			bnDepartmentInfo.setCompanyId(user.getCompanyId());
			bnDepartmentInfo.setCreateAt(TimeUtils.getDateNow());
			bnDepartmentInfo.setCreateBy(user.getStaffCode());
			bnDepartmentInfoService.insert(bnDepartmentInfo);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(@RequestParam String id,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnDepartmentInfo bnDepartmentInfo = bnDepartmentInfoService.findUniqueBy(parameters);
		model.addAttribute("bnDepartmentInfo", bnDepartmentInfo);
		return "hrmanager/dept_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,BnDepartmentInfo bnDepartmentInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			StUser user = (StUser) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			bnDepartmentInfo.setCompanyId(user.getCompanyId());
			bnDepartmentInfo.setUpdateAt(TimeUtils.getDateNow());
			bnDepartmentInfo.setUpdateBy(user.getStaffCode());
			bnDepartmentInfoService.update(bnDepartmentInfo);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,BnDepartmentInfo bnDepartmentInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnDepartmentInfoService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnDepartmentInfoService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
