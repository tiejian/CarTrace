package com.jacqui.hrmanager.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.admin.domain.StUser;
import com.jacqui.constant.Constant;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.utils.TimeUtils;
import com.jacqui.core.web.BaseController;
import com.jacqui.hrmanager.domain.BnCompanyInfo;
import com.jacqui.hrmanager.service.BnCompanyInfoService;
import com.jacqui.prj.domain.BnProjectInfo;
/**
 * 
 * @ClassName: BnCompanyInfoController 
 * @Description: 企业信息 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/hrmgr/company")
public class BnCompanyInfoController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(BnCompanyInfoController.class);
	
	@Autowired
	public BnCompanyInfoService bnCompanyInfoService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "hrmanager/company_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<BnProjectInfo> retObject = new PageResult<>();
		retObject = bnCompanyInfoService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		return paging(retObject.getList(),retObject.getTotalCount(),page);
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<BnCompanyInfo> list = Lists.newArrayList();
		list = bnCompanyInfoService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get/{id}",params="_m=exec")
	@ResponseBody
	public Object get(@PathVariable("id") String id,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnCompanyInfo bnCompanyInfo = bnCompanyInfoService.findUniqueBy(parameters);
		return bnCompanyInfo;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "hrmanager/company_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,BnCompanyInfo bnCompanyInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			StUser user = (StUser) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			bnCompanyInfo.setCreateAt(TimeUtils.getDateNow());
			bnCompanyInfo.setCreateBy(user.getStaffCode());
			bnCompanyInfoService.insert(bnCompanyInfo);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(@RequestParam String id,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnCompanyInfo bnCompanyInfo = bnCompanyInfoService.findUniqueBy(parameters);
		model.addAttribute("bnCompanyInfo", bnCompanyInfo);
		return "hrmanager/company_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,BnCompanyInfo bnCompanyInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			StUser user = (StUser) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			bnCompanyInfo.setUpdateAt(TimeUtils.getDateNow());
			bnCompanyInfo.setUpdateBy(user.getStaffCode());
			bnCompanyInfoService.update(bnCompanyInfo);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,BnCompanyInfo bnCompanyInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnCompanyInfoService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnCompanyInfoService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
