package com.jacqui.hrmanager.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnDepartmentInfo  
 * @Description: 部门信息 
 * @author 白展堂
 * 
 */
public class BnDepartmentInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //主键
    
    private String code; //部门（科室）标识
    
    private String name; //部门（科室）名称
    
    private String shortName; //部门（科室）简称
    
    private String address; //部门（科室）地址
    
    private String parentCode; //上级父标识
    
    private String companyId; //所属业户（企业）ID
    
    private String canton; //所属行政区划
    
    private String pic; //经营负责人
    
    private String picPhone; //经营负责人手机号
    
    private String sosNum; //应急电话
    
    private String faxNum; //传真号
    
    private Date foundDate; //核发日期
    
    private String foundDateStr;//核发日期-对应的字符型日期
    private Date cancelDate; //有效日期起
    
    private String cancelDateStr;//有效日期起-对应的字符型日期
    private String deptFunc; //经营范围
    
    private Date createAt; //创建时间
    
    private String createAtStr;//创建时间-对应的字符型日期
    private String createBy; //创建人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private String updateBy; //修改人
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getShortName(){
    	return shortName;
    }
    public void setShortName(String shortName){
        this.shortName = shortName;
    }
    public String getAddress(){
    	return address;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getParentCode(){
    	return parentCode;
    }
    public void setParentCode(String parentCode){
        this.parentCode = parentCode;
    }
    public String getCompanyId(){
    	return companyId;
    }
    public void setCompanyId(String companyId){
        this.companyId = companyId;
    }
    public String getCanton(){
    	return canton;
    }
    public void setCanton(String canton){
        this.canton = canton;
    }
    public String getPic(){
    	return pic;
    }
    public void setPic(String pic){
        this.pic = pic;
    }
    public String getPicPhone(){
    	return picPhone;
    }
    public void setPicPhone(String picPhone){
        this.picPhone = picPhone;
    }
    public String getSosNum(){
    	return sosNum;
    }
    public void setSosNum(String sosNum){
        this.sosNum = sosNum;
    }
    public String getFaxNum(){
    	return faxNum;
    }
    public void setFaxNum(String faxNum){
        this.faxNum = faxNum;
    }
    public Date getFoundDate(){
    	return foundDate;
    }
    public void setFoundDate(Date foundDate){
        this.foundDate = foundDate;
    }
    public String getFoundDateStr(){
    	return foundDateStr;
    }
    public void setFoundDateStr(String foundDateStr){
        this.foundDateStr = foundDateStr;
    }	
    public Date getCancelDate(){
    	return cancelDate;
    }
    public void setCancelDate(Date cancelDate){
        this.cancelDate = cancelDate;
    }
    public String getCancelDateStr(){
    	return cancelDateStr;
    }
    public void setCancelDateStr(String cancelDateStr){
        this.cancelDateStr = cancelDateStr;
    }	
    public String getDeptFunc(){
    	return deptFunc;
    }
    public void setDeptFunc(String deptFunc){
        this.deptFunc = deptFunc;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "主键");
		labels.put("code", "部门（科室）标识");
		labels.put("name", "部门（科室）名称");
		labels.put("shortName", "部门（科室）简称");
		labels.put("address", "部门（科室）地址");
		labels.put("parentCode", "上级父标识");
		labels.put("companyId", "所属业户（企业）ID");
		labels.put("canton", "所属行政区划");
		labels.put("pic", "经营负责人");
		labels.put("picPhone", "经营负责人手机号");
		labels.put("sosNum", "应急电话");
		labels.put("faxNum", "传真号");
		labels.put("foundDate", "核发日期");
		labels.put("cancelDate", "有效日期起");
		labels.put("deptFunc", "经营范围");
		labels.put("createAt", "创建时间");
		labels.put("createBy", "创建人");
		labels.put("updateAt", "修改时间");
		labels.put("updateBy", "修改人");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}