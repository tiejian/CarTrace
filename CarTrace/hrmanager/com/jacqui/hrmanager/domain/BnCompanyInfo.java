package com.jacqui.hrmanager.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnCompanyInfo  
 * @Description: 企业信息 
 * @author 白展堂
 * 
 */
public class BnCompanyInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //主键
    
    private String code; //业户标识
    
    private String name; //业户名称
    
    private String shortName; //业户简称
    
    private String address; //业户地址
    
    private String parentCode; //上级父标识
    
    private String canton; //所属行政区划
    
    private String opPic; //经营负责人
    
    private String opPicPhone; //经营负责人手机号
    
    private String soPic; //安全负责人
    
    private String soPicPhone; //手机号（安全负责人）
    
    private String sosTel; //应急电话
    
    private String faxTel; //传真号
    
    private String licenseNum; //经营许可证号
    
    private Date issueAt; //核发日期
    
    private String issueAtStr;//核发日期-对应的字符型日期
    private Date expiryStart; //有效日期起
    
    private String expiryStartStr;//有效日期起-对应的字符型日期
    private Date expiryEnd; //有效日期止
    
    private String expiryEndStr;//有效日期止-对应的字符型日期
    private int opType; //营运类型
    
    private int companyLevel; //企业等级
    
    private int staffCount; //从业人员数量
    
    private int vehicleCount; //车辆数
    
    private int opArea; //经营区域
    
    private String opScope; //经营范围
    
    private Date createAt; //创建时间
    
    private String createAtStr;//创建时间-对应的字符型日期
    private String createBy; //创建人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private String updateBy; //修改人
    
    private String adminAccount;
    
    private String adminPasswd;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getCode(){
    	return code;
    }
    public void setCode(String code){
        this.code = code;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getShortName(){
    	return shortName;
    }
    public void setShortName(String shortName){
        this.shortName = shortName;
    }
    public String getAddress(){
    	return address;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public String getParentCode(){
    	return parentCode;
    }
    public void setParentCode(String parentCode){
        this.parentCode = parentCode;
    }
    public String getCanton(){
    	return canton;
    }
    public void setCanton(String canton){
        this.canton = canton;
    }
    public String getOpPic(){
    	return opPic;
    }
    public void setOpPic(String opPic){
        this.opPic = opPic;
    }
    public String getOpPicPhone(){
    	return opPicPhone;
    }
    public void setOpPicPhone(String opPicPhone){
        this.opPicPhone = opPicPhone;
    }
    public String getSoPic(){
    	return soPic;
    }
    public void setSoPic(String soPic){
        this.soPic = soPic;
    }
    public String getSoPicPhone(){
    	return soPicPhone;
    }
    public void setSoPicPhone(String soPicPhone){
        this.soPicPhone = soPicPhone;
    }
    public String getSosTel(){
    	return sosTel;
    }
    public void setSosTel(String sosTel){
        this.sosTel = sosTel;
    }
    public String getFaxTel(){
    	return faxTel;
    }
    public void setFaxTel(String faxTel){
        this.faxTel = faxTel;
    }
    public String getLicenseNum(){
    	return licenseNum;
    }
    public void setLicenseNum(String licenseNum){
        this.licenseNum = licenseNum;
    }
    public Date getIssueAt(){
    	return issueAt;
    }
    public void setIssueAt(Date issueAt){
        this.issueAt = issueAt;
    }
    public String getIssueAtStr(){
    	return issueAtStr;
    }
    public void setIssueAtStr(String issueAtStr){
        this.issueAtStr = issueAtStr;
    }	
    public Date getExpiryStart(){
    	return expiryStart;
    }
    public void setExpiryStart(Date expiryStart){
        this.expiryStart = expiryStart;
    }
    public String getExpiryStartStr(){
    	return expiryStartStr;
    }
    public void setExpiryStartStr(String expiryStartStr){
        this.expiryStartStr = expiryStartStr;
    }	
    public Date getExpiryEnd(){
    	return expiryEnd;
    }
    public void setExpiryEnd(Date expiryEnd){
        this.expiryEnd = expiryEnd;
    }
    public String getExpiryEndStr(){
    	return expiryEndStr;
    }
    public void setExpiryEndStr(String expiryEndStr){
        this.expiryEndStr = expiryEndStr;
    }	
    public int getOpType(){
    	return opType;
    }
    public void setOpType(int opType){
        this.opType = opType;
    }
    public int getCompanyLevel(){
    	return companyLevel;
    }
    public void setCompanyLevel(int companyLevel){
        this.companyLevel = companyLevel;
    }
    public int getStaffCount(){
    	return staffCount;
    }
    public void setStaffCount(int staffCount){
        this.staffCount = staffCount;
    }
    public int getVehicleCount(){
    	return vehicleCount;
    }
    public void setVehicleCount(int vehicleCount){
        this.vehicleCount = vehicleCount;
    }
    public int getOpArea(){
    	return opArea;
    }
    public void setOpArea(int opArea){
        this.opArea = opArea;
    }
    public String getOpScope(){
    	return opScope;
    }
    public void setOpScope(String opScope){
        this.opScope = opScope;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "主键");
		labels.put("code", "业户标识");
		labels.put("name", "业户名称");
		labels.put("shortName", "业户简称");
		labels.put("address", "业户地址");
		labels.put("parentCode", "上级父标识");
		labels.put("canton", "所属行政区划");
		labels.put("opPic", "经营负责人");
		labels.put("opPicPhone", "经营负责人手机号");
		labels.put("soPic", "安全负责人");
		labels.put("soPicPhone", "手机号（安全负责人）");
		labels.put("sosTel", "应急电话");
		labels.put("faxTel", "传真号");
		labels.put("licenseNum", "经营许可证号");
		labels.put("issueAt", "核发日期");
		labels.put("expiryStart", "有效日期起");
		labels.put("expiryEnd", "有效日期止");
		labels.put("opType", "营运类型");
		labels.put("companyLevel", "企业等级");
		labels.put("staffCount", "从业人员数量");
		labels.put("vehicleCount", "车辆数");
		labels.put("opArea", "经营区域");
		labels.put("opScope", "经营范围");
		labels.put("createAt", "创建时间");
		labels.put("createBy", "创建人");
		labels.put("updateAt", "修改时间");
		labels.put("updateBy", "修改人");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public String getAdminAccount() {
		return adminAccount;
	}
	public void setAdminAccount(String adminAccount) {
		this.adminAccount = adminAccount;
	}
	public String getAdminPasswd() {
		return adminPasswd;
	}
	public void setAdminPasswd(String adminPasswd) {
		this.adminPasswd = adminPasswd;
	}
}