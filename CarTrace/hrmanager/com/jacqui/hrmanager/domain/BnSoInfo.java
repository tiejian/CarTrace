package com.jacqui.hrmanager.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnSoInfo  
 * @Description: 安全管理员信息表Security Officer 
 * @author 白展堂
 * 
 */
public class BnSoInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String staffId; //员工号
    
    private String idCardNo; //身份证信息
    
    private String name; //姓名
    
    private String licenseNo; //证书编号
    
    private Date drawAt; //证书初领时间
    
    private String drawAtStr;//证书初领时间-对应的字符型日期
    private Date validEnd; //证书到期日期
    
    private String validEndStr;//证书到期日期-对应的字符型日期
    private Date servieStart; //安全工作起始日期
    
    private String servieStartStr;//安全工作起始日期-对应的字符型日期
    private String createBy; //创建人
    
    private Date createAt; //创建时间
    
    private String createAtStr;//创建时间-对应的字符型日期
    private String updateBy; //修改人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getStaffId(){
    	return staffId;
    }
    public void setStaffId(String staffId){
        this.staffId = staffId;
    }
    public String getIdCardNo(){
    	return idCardNo;
    }
    public void setIdCardNo(String idCardNo){
        this.idCardNo = idCardNo;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getLicenseNo(){
    	return licenseNo;
    }
    public void setLicenseNo(String licenseNo){
        this.licenseNo = licenseNo;
    }
    public Date getDrawAt(){
    	return drawAt;
    }
    public void setDrawAt(Date drawAt){
        this.drawAt = drawAt;
    }
    public String getDrawAtStr(){
    	return drawAtStr;
    }
    public void setDrawAtStr(String drawAtStr){
        this.drawAtStr = drawAtStr;
    }	
    public Date getValidEnd(){
    	return validEnd;
    }
    public void setValidEnd(Date validEnd){
        this.validEnd = validEnd;
    }
    public String getValidEndStr(){
    	return validEndStr;
    }
    public void setValidEndStr(String validEndStr){
        this.validEndStr = validEndStr;
    }	
    public Date getServieStart(){
    	return servieStart;
    }
    public void setServieStart(Date servieStart){
        this.servieStart = servieStart;
    }
    public String getServieStartStr(){
    	return servieStartStr;
    }
    public void setServieStartStr(String servieStartStr){
        this.servieStartStr = servieStartStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
	public Map<String, String> getLabels() {
		labels.put("staffId", "员工号");
		labels.put("idCardNo", "身份证信息");
		labels.put("name", "姓名");
		labels.put("licenseNo", "证书编号");
		labels.put("drawAt", "证书初领时间");
		labels.put("validEnd", "证书到期日期");
		labels.put("servieStart", "安全工作起始日期");
		labels.put("createBy", "创建人");
		labels.put("createAt", "创建时间");
		labels.put("updateBy", "修改人");
		labels.put("updateAt", "修改时间");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}