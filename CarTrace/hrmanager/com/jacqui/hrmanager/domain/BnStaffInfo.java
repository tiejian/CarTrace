package com.jacqui.hrmanager.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnStaffInfo  
 * @Description: 工作人员基础信息表 
 * @author 白展堂
 * 
 */
public class BnStaffInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String staffId; //工号
    
    private String idCardNo; //身份证号
    
    private String name; //姓名
    
    private String address; //住址
    
    private int sex; //性别
    
    private String sexView;
    
    private int age; //年龄
    
    private int healthCond; //健康状况
    
    private int education; //学历
    
    private String phoneNum; //联系电话
    
    private int jobPosts; //岗位
    
    private String jobTitle; //职务
    
    private String photoPath; //照片
    
    private Date joinAt; //入职时间
    
    private String joinAtStr;//入职时间-对应的字符型日期
    private String companyCode; //所属业户（企业）ID
    
    private String deptCode; //所属部门（科室）（ID）
    
    private int status; //人员状态0停用1正常
    
    private Date createAt; //添加时间
    
    private String createAtStr;//添加时间-对应的字符型日期
    private String createBy; //添加人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private String updateBy; //修改人
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getStaffId(){
    	return staffId;
    }
    public void setStaffId(String staffId){
        this.staffId = staffId;
    }
    public String getIdCardNo(){
    	return idCardNo;
    }
    public void setIdCardNo(String idCardNo){
        this.idCardNo = idCardNo;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getAddress(){
    	return address;
    }
    public void setAddress(String address){
        this.address = address;
    }
    public int getSex(){
    	return sex;
    }
    public void setSex(int sex){
        this.sex = sex;
    }
    public int getAge(){
    	return age;
    }
    public void setAge(int age){
        this.age = age;
    }
    public int getHealthCond(){
    	return healthCond;
    }
    public void setHealthCond(int healthCond){
        this.healthCond = healthCond;
    }
    public int getEducation(){
    	return education;
    }
    public void setEducation(int education){
        this.education = education;
    }
    public String getPhoneNum(){
    	return phoneNum;
    }
    public void setPhoneNum(String phoneNum){
        this.phoneNum = phoneNum;
    }
    public int getJobPosts(){
    	return jobPosts;
    }
    public void setJobPosts(int jobPosts){
        this.jobPosts = jobPosts;
    }
    public String getJobTitle(){
    	return jobTitle;
    }
    public void setJobTitle(String jobTitle){
        this.jobTitle = jobTitle;
    }
    public String getPhotoPath(){
    	return photoPath;
    }
    public void setPhotoPath(String photoPath){
        this.photoPath = photoPath;
    }
    public Date getJoinAt(){
    	return joinAt;
    }
    public void setJoinAt(Date joinAt){
        this.joinAt = joinAt;
    }
    public String getJoinAtStr(){
    	return joinAtStr;
    }
    public void setJoinAtStr(String joinAtStr){
        this.joinAtStr = joinAtStr;
    }	
    public String getCompanyCode(){
    	return companyCode;
    }
    public void setCompanyCode(String companyCode){
        this.companyCode = companyCode;
    }
    public String getDeptCode(){
    	return deptCode;
    }
    public void setDeptCode(String deptCode){
        this.deptCode = deptCode;
    }
    public int getStatus(){
    	return status;
    }
    public void setStatus(int status){
        this.status = status;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
	public Map<String, String> getLabels() {
		labels.put("staffId", "工号");
		labels.put("idCardNo", "身份证号");
		labels.put("name", "姓名");
		labels.put("address", "住址");
		labels.put("sex", "性别");
		labels.put("age", "年龄");
		labels.put("healthCond", "健康状况");
		labels.put("education", "学历");
		labels.put("phoneNum", "联系电话");
		labels.put("jobPosts", "岗位");
		labels.put("jobTitle", "职务");
		labels.put("photoPath", "照片");
		labels.put("joinAt", "入职时间");
		labels.put("companyCode", "所属业户（企业）ID");
		labels.put("deptCode", "所属部门（科室）（ID）");
		labels.put("status", "人员状态0停用1正常");
		labels.put("createAt", "添加时间");
		labels.put("createBy", "添加人");
		labels.put("updateAt", "修改时间");
		labels.put("updateBy", "修改人");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public String getSexView() {
		if(this.sex==1)
			return "男";
		else
			return "女";
	}
	public void setSexView(String sexView) {
		this.sexView = sexView;
	}
}