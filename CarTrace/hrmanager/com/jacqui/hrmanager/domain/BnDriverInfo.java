package com.jacqui.hrmanager.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnDriverInfo  
 * @Description: 驾驶员信息 
 * @author 白展堂
 * 
 */
public class BnDriverInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String staffId; //员工号
    
    private String idCardNo; //身份证信息
    
    private String name; //姓名
    
    private String drLicense; //驾驶证号
    
    private String allowDrType; //准驾车型
    
    private String docuNo; //档案编号
    
    private Date drDrawAt; //初次领证日期
    
    private String drDrawAtStr;//初次领证日期-对应的字符型日期
    private String issuedBy; //发证机关
    
    private Date validStart; //有效起始日期
    
    private String validStartStr;//有效起始日期-对应的字符型日期
    private int validPeriod; //有效期限
    
    private int empStatus; //从业状态
    
    private String empLicense; //从业资格证号
    
    private Date empDrawAt; //从业资格证初领时间
    
    private String empDrawAtStr;//从业资格证初领时间-对应的字符型日期
    private String empIssuedBy; //从业资格证发证机关
    
    private Date empIssuedAt; //从业资格证发放时间
    
    private String empIssuedAtStr;//从业资格证发放时间-对应的字符型日期
    private Date empValidEnd; //从业资格证有效期至
    
    private String empValidEndStr;//从业资格证有效期至-对应的字符型日期
    private String subjectCode; //专业类型
    
    private Date healthExamDate; //上次体检时间
    
    private String healthExamDateStr;//上次体检时间-对应的字符型日期
    private String drLicenseAttach; //机动车驾驶证类附件名称
    
    private String empLicenseAttach; //从业资格证类附件名称
    
    private String createBy; //创建人
    
    private Date createAt; //创建时间
    
    private String createAtStr;//创建时间-对应的字符型日期
    private String updateBy; //修改人
    
    private Date updateAt; //修改时间
    
    private String updateAtStr;//修改时间-对应的字符型日期
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getStaffId(){
    	return staffId;
    }
    public void setStaffId(String staffId){
        this.staffId = staffId;
    }
    public String getIdCardNo(){
    	return idCardNo;
    }
    public void setIdCardNo(String idCardNo){
        this.idCardNo = idCardNo;
    }
    public String getName(){
    	return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getDrLicense(){
    	return drLicense;
    }
    public void setDrLicense(String drLicense){
        this.drLicense = drLicense;
    }
    public String getAllowDrType(){
    	return allowDrType;
    }
    public void setAllowDrType(String allowDrType){
        this.allowDrType = allowDrType;
    }
    public String getDocuNo(){
    	return docuNo;
    }
    public void setDocuNo(String docuNo){
        this.docuNo = docuNo;
    }
    public Date getDrDrawAt(){
    	return drDrawAt;
    }
    public void setDrDrawAt(Date drDrawAt){
        this.drDrawAt = drDrawAt;
    }
    public String getDrDrawAtStr(){
    	return drDrawAtStr;
    }
    public void setDrDrawAtStr(String drDrawAtStr){
        this.drDrawAtStr = drDrawAtStr;
    }	
    public String getIssuedBy(){
    	return issuedBy;
    }
    public void setIssuedBy(String issuedBy){
        this.issuedBy = issuedBy;
    }
    public Date getValidStart(){
    	return validStart;
    }
    public void setValidStart(Date validStart){
        this.validStart = validStart;
    }
    public String getValidStartStr(){
    	return validStartStr;
    }
    public void setValidStartStr(String validStartStr){
        this.validStartStr = validStartStr;
    }	
    public int getValidPeriod(){
    	return validPeriod;
    }
    public void setValidPeriod(int validPeriod){
        this.validPeriod = validPeriod;
    }
    public int getEmpStatus(){
    	return empStatus;
    }
    public void setEmpStatus(int empStatus){
        this.empStatus = empStatus;
    }
    public String getEmpLicense(){
    	return empLicense;
    }
    public void setEmpLicense(String empLicense){
        this.empLicense = empLicense;
    }
    public Date getEmpDrawAt(){
    	return empDrawAt;
    }
    public void setEmpDrawAt(Date empDrawAt){
        this.empDrawAt = empDrawAt;
    }
    public String getEmpDrawAtStr(){
    	return empDrawAtStr;
    }
    public void setEmpDrawAtStr(String empDrawAtStr){
        this.empDrawAtStr = empDrawAtStr;
    }	
    public String getEmpIssuedBy(){
    	return empIssuedBy;
    }
    public void setEmpIssuedBy(String empIssuedBy){
        this.empIssuedBy = empIssuedBy;
    }
    public Date getEmpIssuedAt(){
    	return empIssuedAt;
    }
    public void setEmpIssuedAt(Date empIssuedAt){
        this.empIssuedAt = empIssuedAt;
    }
    public String getEmpIssuedAtStr(){
    	return empIssuedAtStr;
    }
    public void setEmpIssuedAtStr(String empIssuedAtStr){
        this.empIssuedAtStr = empIssuedAtStr;
    }	
    public Date getEmpValidEnd(){
    	return empValidEnd;
    }
    public void setEmpValidEnd(Date empValidEnd){
        this.empValidEnd = empValidEnd;
    }
    public String getEmpValidEndStr(){
    	return empValidEndStr;
    }
    public void setEmpValidEndStr(String empValidEndStr){
        this.empValidEndStr = empValidEndStr;
    }	
    public String getSubjectCode(){
    	return subjectCode;
    }
    public void setSubjectCode(String subjectCode){
        this.subjectCode = subjectCode;
    }
    public Date getHealthExamDate(){
    	return healthExamDate;
    }
    public void setHealthExamDate(Date healthExamDate){
        this.healthExamDate = healthExamDate;
    }
    public String getHealthExamDateStr(){
    	return healthExamDateStr;
    }
    public void setHealthExamDateStr(String healthExamDateStr){
        this.healthExamDateStr = healthExamDateStr;
    }	
    public String getDrLicenseAttach(){
    	return drLicenseAttach;
    }
    public void setDrLicenseAttach(String drLicenseAttach){
        this.drLicenseAttach = drLicenseAttach;
    }
    public String getEmpLicenseAttach(){
    	return empLicenseAttach;
    }
    public void setEmpLicenseAttach(String empLicenseAttach){
        this.empLicenseAttach = empLicenseAttach;
    }
    public String getCreateBy(){
    	return createBy;
    }
    public void setCreateBy(String createBy){
        this.createBy = createBy;
    }
    public Date getCreateAt(){
    	return createAt;
    }
    public void setCreateAt(Date createAt){
        this.createAt = createAt;
    }
    public String getCreateAtStr(){
    	return createAtStr;
    }
    public void setCreateAtStr(String createAtStr){
        this.createAtStr = createAtStr;
    }	
    public String getUpdateBy(){
    	return updateBy;
    }
    public void setUpdateBy(String updateBy){
        this.updateBy = updateBy;
    }
    public Date getUpdateAt(){
    	return updateAt;
    }
    public void setUpdateAt(Date updateAt){
        this.updateAt = updateAt;
    }
    public String getUpdateAtStr(){
    	return updateAtStr;
    }
    public void setUpdateAtStr(String updateAtStr){
        this.updateAtStr = updateAtStr;
    }	
	public Map<String, String> getLabels() {
		labels.put("staffId", "员工号");
		labels.put("idCardNo", "身份证信息");
		labels.put("name", "姓名");
		labels.put("drLicense", "驾驶证号");
		labels.put("allowDrType", "准驾车型");
		labels.put("docuNo", "档案编号");
		labels.put("drDrawAt", "初次领证日期");
		labels.put("issuedBy", "发证机关");
		labels.put("validStart", "有效起始日期");
		labels.put("validPeriod", "有效期限");
		labels.put("empStatus", "从业状态");
		labels.put("empLicense", "从业资格证号");
		labels.put("empDrawAt", "从业资格证初领时间");
		labels.put("empIssuedBy", "从业资格证发证机关");
		labels.put("empIssuedAt", "从业资格证发放时间");
		labels.put("empValidEnd", "从业资格证有效期至");
		labels.put("subjectCode", "专业类型");
		labels.put("healthExamDate", "上次体检时间");
		labels.put("drLicenseAttach", "机动车驾驶证类附件名称");
		labels.put("empLicenseAttach", "从业资格证类附件名称");
		labels.put("createBy", "创建人");
		labels.put("createAt", "创建时间");
		labels.put("updateBy", "修改人");
		labels.put("updateAt", "修改时间");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}