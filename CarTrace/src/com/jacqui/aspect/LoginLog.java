package com.jacqui.aspect;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * 
 * @ClassName: LoginLog 
 * @Description:用于登录日志的切面 注解
 * @author ilaotou@qq.com
 * @date 2014年11月8日 上午9:43:57 
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface LoginLog {
	String action() default "LOGIN";
}
