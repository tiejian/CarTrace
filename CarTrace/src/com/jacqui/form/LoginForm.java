package com.jacqui.form;

import javax.validation.constraints.Size;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.NotEmpty;
import org.apache.commons.codec.digest.DigestUtils;

public class LoginForm {

	@NotEmpty(message = "请输入身份证号")
	@Size(min = 15, max = 18)
	private String username;
	
	@NotEmpty(message = "请输入密码")
	@Size(min = 1, max = 64)
	private String password;

	private String verCode;
	
	private String randCode;
	
	private String ipAddr;
	
	private String viewtimes;
	
	

	public String getRandCode() {
		return randCode;
	}

	public void setRandCode(String randCode) {
		this.randCode = randCode;
	}


	public String getVerCode() {
		return verCode;
	}

	public void setVerCode(String verCode) {
		this.verCode = verCode;
	}

	/** 
	 * @return ipAddr 
	 */
	public String getIpAddr() {
		return ipAddr;
	}

	/** 
	 * @param ipAddr 要设置的 ipAddr 
	 */
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public String getViewtimes() {
		return viewtimes;
	}

	public void setViewtimes(String viewtimes) {
		this.viewtimes = viewtimes;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = StringUtils.isNotBlank(password)?DigestUtils.md5Hex(password):password;
	}

}
