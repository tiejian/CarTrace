package com.jacqui.filter;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 
 * @author xiaolaotou 
 * 实现在线用户统计,同时存储sessionid
 * 
 */
public class ActiveUserListener implements HttpSessionListener {

	private static int sessionCount = 0;

	private static Map sessionMaps = new HashMap(); // 存放session的集合类

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		HttpSession session = arg0.getSession();
		String sessionId = session.getId();
		sessionMaps.put(sessionId, session);
		sessionCount++;
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		sessionCount--;
		String sessionId = arg0.getSession().getId();
		sessionMaps.remove(sessionId);
	}

	public static int getSessionCount() {
		return sessionCount;
	}

	public static Map getSessionMaps() {
		return sessionMaps;
	}
}
