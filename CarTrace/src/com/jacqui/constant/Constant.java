package com.jacqui.constant;

public class Constant {
	
	public static String SESSION_USER_KEY="session_user_key";
	public static String SESSION_USER_ID_KEY="session_user_id_key";
	public static String SESSION_ID_KEY="session_id_key";
	public static String SESSION_MENU_KEY="session_menu_key";
	
	public final static String SESSION_USER_DATA_AREA="session_date_area"; //全市sq,市本级:sbj,所属行政区:ssxzq,所属单位:ssdw,单位本级:dwbj
	public final static String SESSION_USER_XZQ="session_user_xzq";//行政区
	public final static String SESSION_USER_BM="session_user_bm"; // 部门代码 
	
	
	
	public final static String AES_PWKEY="ilaotou@qq.com";
    public final static String GROUP_SUM_CODE_DS="ds_hj";
    public final static String GROUP_SUM_CODE_GS="gs_hj";
    public final static String GROUP_SUM_CODE="all_hj";
    public final static String GROUP_SUM_NAME_DS="地税合计";
    public final static String GROUP_SUM_NAME_GS="国税合计";
    public final static String GROUP_SUM_NAME="合计";
    
    public final static String GROUP_SUM_CODE_SQ="shiqu_hj";
    public final static String GROUP_SUM_CODE_QX="quxian_hj";
    public final static String GROUP_SUM_NAME_SQ="市区合计";
    public final static String GROUP_SUM_NAME_QX="县区合计";
    
    
    
    
    
}
