package com.jacqui.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.jacqui.admin.dao.TablePrimaryKeyMapper;
import com.jacqui.admin.domain.TablePrimaryKey;

public class BaseService {

	@Autowired
	private TablePrimaryKeyMapper tablePrimaryKeyMapper;
	

	public String getId(String stub){
		StringBuffer sb = new StringBuffer();
		sb.append(stub);
		TablePrimaryKey p = new TablePrimaryKey();
		p.setStub(stub);
		tablePrimaryKeyMapper.getKey(p);
		sb.append(p.getKey());
		return sb.toString();
	}
}
