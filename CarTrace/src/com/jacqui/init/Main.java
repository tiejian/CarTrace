package com.jacqui.init;

import java.io.IOException;

import org.springframework.context.support.ClassPathXmlApplicationContext;



public class Main {

	public static volatile boolean running=true;
	public static void main(String[] args) throws IOException {
//		ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:applicationProvider.xml");
		ClassPathXmlApplicationContext context =new ClassPathXmlApplicationContext(new String[] {"applicationContext.xml","applicationProvider.xml"});
		context.start();
		System.out.println("启动完毕！");
		synchronized(Main.class){
			while(running){
				try {
					Main.class.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	}

}
