package com.jacqui.controller;

import javax.servlet.ServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 
 * @ClassName: MenuController 
 * @Description: 左侧bar
 * @author ilaotou@qq.com
 * @date 2014年9月22日 下午4:24:03 
 *
 */
@Controller
@RequestMapping(value="menu")
public class MenuController {
	@RequestMapping(value = "syssetting")
	public String syssetting(Model model,ServletRequest request){
		return "leftsidebar/syssettingbar";
	}
	@RequestMapping(value = "search")
	public String search(Model model,ServletRequest request){
		return "leftsidebar/searchbar";
	}
	@RequestMapping(value = "private")
	public String privatebar(Model model,ServletRequest request){
		return "leftsidebar/privatebar";
	}
}
