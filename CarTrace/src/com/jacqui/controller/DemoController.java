package com.jacqui.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Maps;

@Controller
@RequestMapping(value="demo")
public class DemoController {
	
	@RequestMapping(value="list",method=RequestMethod.GET)
	public String list(Model model,HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> parameters = Maps.newHashMap();
		return "prj/project_list";
	}
	
	@RequestMapping(value="form",method=RequestMethod.GET)
	public String form(Model model,HttpServletRequest request, HttpServletResponse response){
	    return "demo/form";  
	}
	@RequestMapping(value="form/base",method=RequestMethod.GET)
	public String formbase(Model model,HttpServletRequest request, HttpServletResponse response){
	    return "demo/profile_info";  
	}
	@RequestMapping(value="form/logo",method=RequestMethod.GET)
	public String formlogo(Model model,HttpServletRequest request, HttpServletResponse response){
	    return "demo/profile_logo";  
	}
	@RequestMapping(value="gis",method=RequestMethod.GET)
	public String gis(Model model,HttpServletRequest request, HttpServletResponse response){
	    return "demo/gis";  
	}

}
