package com.jacqui.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Maps;
import com.jacqui.admin.domain.StUser;
import com.jacqui.admin.service.StUserService;
import com.jacqui.constant.Constant;
import com.jacqui.form.LoginForm;
import com.jacqui.form.LoginValidator;

@Controller
public class LoginController {
	
	@Autowired
	public StUserService stUserService;
	
	@Autowired
	@Qualifier("loginValidator")
    protected LoginValidator loginValidator;
	
	
	private String sysCode="zhzs_yun";
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(this.loginValidator);
	}
	
	@RequestMapping(value = "redirect/login", method = RequestMethod.GET)
	public String redirect_to_login_page(Model model,
			HttpServletRequest request, HttpServletResponse response) {
		model.addAttribute("loginForm", new LoginForm());
		model.addAttribute("key", "none");
		model.addAttribute("errorCount", 0);
		return "login";
	}
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public String redirect_to_login_page(LoginForm loginForm,Model model,BindingResult result,HttpServletRequest request, HttpServletResponse response){
		String username = loginForm.getUsername();
		String password = loginForm.getPassword();
		
		loginValidator.validate(loginForm, result);
		
		if(result.hasErrors()){
			return "login";
		}
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("staffCode", username);
		parameters.put("passwd", password);
		StUser user = stUserService.findUniqueBy(parameters);
		if(user==null){
			return "login";
		}
		WebUtils.setSessionAttribute(request, Constant.SESSION_USER_KEY, user);
//		model.addAttribute("menus", menus);
	    return "redirect:/index";  
	}
	
	
	@RequestMapping(value="log/in",method=RequestMethod.POST)
	public String redirect_to_login_in(LoginForm loginForm,BindingResult result,Model model,HttpServletRequest request, HttpServletResponse response){
		return "redirect:/index"; 
	}

}
