                                                                                                  	package com.jacqui.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;

import com.google.common.collect.Lists;
import com.jacqui.core.form.JSonReturnObject;
import com.jacqui.core.utils.FileUtil;
import com.jacqui.core.utils.TimeUtils;

import net.sf.json.JSONObject;

/**
 * 
 * @ClassName: UploadExcelController
 * @Description: 数据采集(上传excel)
 * @author ilaotou@qq.com
 * @date 2013年10月22日 下午2:33:39
 *
 */
@Controller
public class UploadController {
	
	public Map<String, String> FILE_TYPE_MAP = new HashMap<String, String>();
	

	/**
	 * 数据上报excel
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/uploader", method = RequestMethod.POST, params = "m=upload",produces="text/html;charset=utf-8")
	public @ResponseBody String upload_attach(@RequestParam String folder,HttpServletRequest request,HttpServletResponse response) throws IOException{
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		FILE_TYPE_MAP.clear();
		getOfficeType();
		InputStream is = null;
		ByteArrayOutputStream bytestream = null;
		MultipartFile multipartFile = null;
		File uploadFile=null;
		String realFileName="";
		String reportFileName="";
		JSonReturnObject result=result = new JSonReturnObject();
		List attachmap = Lists.newArrayList();
		try {
			DefaultMultipartHttpServletRequest multipartRequest=(DefaultMultipartHttpServletRequest)request;
			Iterator iterator = multipartRequest.getFileNames();
			while (iterator.hasNext()) {
				// 循环取出每一个附件
				multipartFile = multipartRequest.getFile((String) iterator.next());
				is = multipartFile.getInputStream();
				bytestream = new ByteArrayOutputStream();
				String fileType;
				for (int i = 0; i < 10; i++) {
					int a = is.read();
					bytestream.write(a);
				}
				byte imgdata[] = bytestream.toByteArray();
				if (multipartFile != null) {
					realFileName = multipartFile.getOriginalFilename();
					realFileName=realFileName.replaceAll("\\s*", "");
					fileType = StringUtils.substringAfterLast(realFileName, ".");
					
					String ctxPath = request.getSession().getServletContext().getRealPath("/")+ File.separator + "upload"+File.separator+folder+File.separator;
					// 创建文件
					File dirPath = new File(ctxPath);
					if (!dirPath.exists()) {
						dirPath.mkdir();
					}
					reportFileName=new StringBuilder().append(StringUtils.substringBeforeLast(realFileName, "."))
							                          .append("_")
							                          .append(TimeUtils.getSpecifiedStandardDate4(new Date()))
							                          .append(".")
							                          .append(fileType).toString();
					uploadFile = new File(new StringBuilder()
							.append(ctxPath)
							.append(reportFileName)
							.toString());
					FileUtil.copyForChannel(multipartFile, uploadFile); 
				}
				if(!uploadFile.exists()){
					result.setErrCode("2");
					result.setErrMsg("文件上传发生错误");
					JSONObject jsonObject = JSONObject.fromObject(result);
					out.print(jsonObject);
					out.close();
					return null;
				}
				attachmap.add(reportFileName);
				is.close();
				bytestream.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			result.setErrCode("2");
			result.setErrMsg("文件上传错误");
			JSONObject jsonObject = JSONObject.fromObject(result);
			out.print(jsonObject);
			out.close();
			return null;
		} finally {
			if(null!=is){
				is.close();
			}
			if(null!=bytestream){
				bytestream.close();
			}
		}
		result.setErrCode("0");
		result.setObj(attachmap);
		JSONObject jsonObject = JSONObject.fromObject(result);
		out.print(jsonObject);
		out.close();
		return null;
	}
	
    public String getFileHexString(byte[] b) {
		StringBuilder stringBuilder = new StringBuilder();
		if (b == null || b.length <= 0) {
			return null;
		}
		for (int i = 0; i < b.length; i++) {
			int v = b[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}
    public void getOfficeType(){
    	 FILE_TYPE_MAP.put("xlsx", "504b0304140000000800");
    	 FILE_TYPE_MAP.put("xls", "d0cf11e0a1b11ae10000");
    	 FILE_TYPE_MAP.put("zip",  "504b0304140000000800");
    	 FILE_TYPE_MAP.put("doc", "d0cf11e0a1b11ae10000");
    	 FILE_TYPE_MAP.put("xlsx", "504b0304140006000800");// docx文件 
    	 
    }
    public String getFileTypeByStream(byte[] b) {
		String filetypeHex = String.valueOf(getFileHexString(b));
		Iterator<Entry<String, String>> entryiterator = FILE_TYPE_MAP
				.entrySet().iterator();
		while (entryiterator.hasNext()) {
			Entry<String, String> entry = entryiterator.next();
			String fileTypeHexValue = entry.getValue().toUpperCase();
			if (filetypeHex.toUpperCase().startsWith(fileTypeHexValue)) {
				return entry.getKey();
			}
		}
		return null;
	}
    
    @RequestMapping(value = "/uploader", params = "_m=file",produces="text/html;charset=utf-8")
	public @ResponseBody Object upload(HttpServletRequest request,HttpServletResponse response) throws IOException, ParseException {
		response.setContentType("text/html; charset=UTF-8");
		String functionCode = request.getParameter("f");
		String requirePath = request.getParameter("p");
		PrintWriter out = response.getWriter();
		FILE_TYPE_MAP.clear();
		getOfficeType();
		InputStream is = null;
		ByteArrayOutputStream bytestream = null;
		MultipartFile multipartFile = null;
		File uploadFile=null;
		String realFileName="";
		String reportFileName="";
		int result=0;
		JSonReturnObject obj=new JSonReturnObject();
		try {
			DefaultMultipartHttpServletRequest multipartRequest=(DefaultMultipartHttpServletRequest)request;
			Iterator iterator = multipartRequest.getFileNames();
			while (iterator.hasNext()) {
				// 循环取出每一个附件
				multipartFile = multipartRequest.getFile((String) iterator.next());
				is = multipartFile.getInputStream();
				bytestream = new ByteArrayOutputStream();
				String fileType="";
				for (int i = 0; i < 10; i++) {
					int a = is.read();
					bytestream.write(a);
				}
				byte imgdata[] = bytestream.toByteArray();
				if (multipartFile != null) {
					realFileName = multipartFile.getOriginalFilename();
					realFileName=realFileName.replaceAll("\\s*", "");
					fileType=realFileName.substring(StringUtils.lastIndexOf(realFileName, ".")+1);
					String ctxPath = request.getSession().getServletContext().getRealPath("/")+ File.separator + "upload"+File.separator;
					// 创建文件
					File dirPath = new File(ctxPath);
					if (!dirPath.exists()) {
						dirPath.mkdir();
					}
					reportFileName=new StringBuilder().append(TimeUtils.getSpecifiedStandardDate4(new Date()))
							                          .append(".")
							                          .append(fileType).toString();
					uploadFile = new File(new StringBuilder()
							.append(ctxPath)
							.append(reportFileName)
							.toString());
					FileCopyUtils.copy(multipartFile.getBytes(), uploadFile);
					obj.setObj(reportFileName);
				}
				is.close();
				bytestream.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			obj.setErrCode("0");
			JSONObject jsonObject = JSONObject.fromObject(obj);
			out.print(jsonObject);
			out.close();
			return null;
		} finally {
			if(null!=is){
				is.close();
			}
			if(null!=bytestream){
				bytestream.close();
			}
		}
		obj.setErrCode("1");
		JSONObject jsonObject = JSONObject.fromObject(obj);
		out.print(jsonObject);
		out.close();
		return null;
	}
}
