package com.jacqui.mybatis.logger;

import java.sql.Connection;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.executor.statement.RoutingStatementHandler;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.util.WebUtils;

import com.github.miemiedev.mybatis.paginator.support.PropertiesHelper;
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.persistence.Criterion;

@Intercepts({ @Signature(type = StatementHandler.class, method = "prepare", args = { Connection.class }) })
public class LogInterceptor implements Interceptor {
	 private static Logger logger = LoggerFactory.getLogger(LogInterceptor.class);
	static ExecutorService Pool;
	private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();
	private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();
	private static final ReflectorFactory DEFAULT_OBJECT_REFLECTOR_FACTORY = new DefaultReflectorFactory();

	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		logger.debug("****************** Log and Audit interceptor is  starting *******************");
		HttpServletRequest request = getHttpServletRequest();
		String __user__ = "";
		if(null!=request){
			__user__  = (String) WebUtils.getSessionAttribute(request, "__user__");
		}
		String __type__= "";//操作类型
		String __attach__ = "";//附加信息
		if (!(invocation.getTarget() instanceof RoutingStatementHandler)) {
			return invocation.proceed();
		}
		// 提取statement
		RoutingStatementHandler statementHandler = (RoutingStatementHandler) invocation	.getTarget();
		MetaObject metaStatementHandler = MetaObject.forObject(	statementHandler, DEFAULT_OBJECT_FACTORY,DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_OBJECT_REFLECTOR_FACTORY);
		// 分离代理对象链
		while (metaStatementHandler.hasGetter("h")) {
			Object object = metaStatementHandler.getValue("h");
			metaStatementHandler = MetaObject.forObject(object,	DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_OBJECT_REFLECTOR_FACTORY);
		}
		// 分离最后一个代理对象的目标类
		while (metaStatementHandler.hasGetter("target")) {
			Object object = metaStatementHandler.getValue("target");
			metaStatementHandler = MetaObject.forObject(object,	DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_OBJECT_REFLECTOR_FACTORY);
		}
		String originalSql = (String) metaStatementHandler.getValue("delegate.boundSql.sql");
		Configuration configuration = (Configuration) metaStatementHandler.getValue("delegate.configuration");
		Object parameterObject = metaStatementHandler.getValue("delegate.boundSql.parameterObject");
		// 进行处理
		BoundSql boundSql = statementHandler.getBoundSql();
		String sql = boundSql.getSql();
//		String __user__ = "";
		String __tblName__ = "";
		if (parameterObject instanceof Map) {
			parameterObject = (HashMap) parameterObject;
			for (Entry entry : ((HashMap<Object, Object>) parameterObject).entrySet()) {
				Object objKey = entry.getKey();
				Object objValue = entry.getValue();
				if(objKey instanceof String){
					if(objKey.equals("__user__")){
						__user__ = (String) objValue;
					}
					if(objKey.equals("__tblName__")){
						__tblName__ = (String) objValue;
					}
					if(objKey.equals("__type__")){
						__type__ = (String) objValue;
					}
					if(objKey.equals("__attach__")){
						__attach__ = (String) objValue;
					}
				}
				if (objValue instanceof Criteria) {
//					System.out.println(((Criteria) obj).getTblName());
//					System.out.println(((Criteria) obj).getCriterions());
					__tblName__ = ((Criteria) objValue).getTblName();
					List<Criterion> ll = ((Criteria) objValue).getCriterions();
					for (Criterion criterion : ll) {
						if (criterion.getCondition().equals("__user__")) {
							__user__ = (String) criterion.getValue();
						}
						if (criterion.getCondition().equals("__type__")) {
							__type__ = (String) criterion.getValue();
						}
						if (criterion.getCondition().equals("__attach__")) {
							__attach__ = (String) criterion.getValue();
						}
					}
				}
			}
		}

		List<ParameterMapping> list = boundSql.getParameterMappings();
		String newSql = StringUtils.replaceOnce(sql, "select","select /* __user__ = "+__user__ +"*/");
		Object returnValue = null;
		metaStatementHandler.setValue("delegate.boundSql.sql", newSql);
		returnValue = invocation.proceed();
		if(StringUtils.isNotBlank(__user__)){
			String logSql = showSql(configuration,boundSql);
			insertTask(configuration,__user__,__tblName__,logSql,__type__,__attach__);
		}
		logger.debug("****************** Log and Audit interceptor is  starting ******************");
		return returnValue;
	}
	/**
	 * 
	 * @Title: insertTask 
	 * @Description: 记录操作日志 
	 * @param configuration
	 * @param uid 用户唯一标识
	 * @param tblName 用于构造日志表的表名称
	 * @param sql 原始的查询SQL
	 * @param type 操作类型，导出、查询等
	 * @param attach 操作类型，导出、查询等
	 * @date 2014年9月30日 下午6:09:29
	 */
	private void insertTask(final Configuration configuration, final String uid, final String tblName, final String sql,final String type,final String attach){
		final Callable insetLogTask = new Callable() {
            public Object call() throws Exception {
            	LogSqlHelp.excuteLogRecord(configuration, uid, tblName, sql,type,attach);
				return null;
            }
        };
        Future insetLogFutrue = call(insetLogTask, true);
	}
	
	private <T> Future<T> call(Callable callable, boolean async){
        if(async){
             return Pool.submit(callable);
        }else{
            FutureTask<T> future = new FutureTask(callable);
            future.run();
            return future;
        }
    }

	@Override
	public Object plugin(Object target) {
		logger.debug("****************** Log and Audit interceptor plugin method is  starting *******************");
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		PropertiesHelper propertiesHelper = new PropertiesHelper(properties);
		setPoolMaxSize(propertiesHelper.getInt("poolMaxSize",0));

	}
	public void setPoolMaxSize(int poolMaxSize) {
        if(poolMaxSize > 0){
            logger.debug("poolMaxSize: {} ", poolMaxSize);
            Pool = Executors.newFixedThreadPool(poolMaxSize);
        }else{
            Pool = Executors.newCachedThreadPool();
        }
    }
	
	public String showSql(Configuration configuration, BoundSql boundSql) {
		Object parameterObject = boundSql.getParameterObject();
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
		if (parameterMappings.size() > 0 && parameterObject != null) {
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
				sql = sql.replaceFirst("\\?",getParameterValue(parameterObject));

			} else {
				MetaObject metaObject = configuration.newMetaObject(parameterObject);
				for (ParameterMapping parameterMapping : parameterMappings) {
					String propertyName = parameterMapping.getProperty();
					if (metaObject.hasGetter(propertyName)) {
						Object obj = metaObject.getValue(propertyName);
						sql = sql.replaceFirst("\\?", getParameterValue(obj));
					} else if (boundSql.hasAdditionalParameter(propertyName)) {
						Object obj = boundSql.getAdditionalParameter(propertyName);
						sql = sql.replaceFirst("\\?", getParameterValue(obj));
					}
				}
			}
		}
		return sql;
	}
	private static String getParameterValue(Object obj) {
		String value = null;
		if (obj instanceof String) {
			value = "'" + obj.toString() + "'";
		} else if (obj instanceof Date) {
			DateFormat formatter = DateFormat.getDateTimeInstance(
					DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
			value = "'" + formatter.format(new Date()) + "'";
		} else {
			if (obj != null) {
				value = obj.toString();
			} else {
				value = "";
			}

		}
		return value;
	}
	
	private static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}

}
