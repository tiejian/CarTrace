package com.jacqui.mybatis.logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.session.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jacqui.core.utils.TimeUtils;

public class LogSqlHelp {
	private static Logger logger = LoggerFactory.getLogger(LogSqlHelp.class);
  /**
   * 
   * @Title: excuteLogRecord 
   * @Description: 生成查询操作日志 
   * @param configuration
   * @param uid 用户唯一标识
   * @param tblName 用于构造日志表的表名称
   * @param sql 原查询SQL
   * @param type 操作类型
   * @param attach 附加信息
   * @throws SQLException
   * @throws ParseException 
   * @date 2014年9月30日 下午6:15:09
   */
	public static void excuteLogRecord(final Configuration configuration, String uid, String tblName, String sql,String type,String attach) throws SQLException, ParseException {
		StringBuilder sb = new StringBuilder();
		sb.append("insert into sys_qry_log(id,table_name,table_o_name,user_idcard,create_at,operate_sql,s_type,attach) value(?,?,?,?,?,?,?,?)")
		  .append(UUID.randomUUID().toString())
		  .append("\",\"")
		  .append(tblName)
		  .append("\",\"")
		  .append(tblName)
		  .append("\",\"")
		  .append(uid)
		  .append("\",\"")
		  .append(TimeUtils.getSpecifiedStandardTime(TimeUtils.getDateNow()))
		  .append("\",\"")
		  .append(sql)
		  .append("\",\"")
		  .append(type)
		  .append("\",\"")
		  .append(attach)
		  .append("\")");
        String insertSql ="insert into sys_qry_log(id,table_name,table_o_name,user_idcard,create_at,operate_sql,s_type,attach) value(?,?,?,?,?,?,?,?)";
        logger.debug("Log Insert SQL [{}] ", insertSql);
		Connection connection = null;
		PreparedStatement insertStmt = null;
		ResultSet rs = null;
		try {
			connection = configuration.getEnvironment().getDataSource().getConnection();
			insertStmt = connection.prepareStatement(insertSql);
			insertStmt.setString(1, UUID.randomUUID().toString());
			insertStmt.setString(2, tblName);
			insertStmt.setString(3, tblName);
			insertStmt.setString(4, uid);
			insertStmt.setString(5, TimeUtils.getSpecifiedStandardTime(TimeUtils.getDateNow()));
			insertStmt.setString(6, sql);
			insertStmt.setString(7, StringUtils.isBlank(type)?"qry":type);
			insertStmt.setString(8, attach);
			insertStmt.executeUpdate();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} finally {
				try {
					if (insertStmt != null) {
						insertStmt.close();
					}
				} finally {
					if (connection != null && !connection.isClosed()) {
						connection.close();
					}
				}
			}
		}
	}
}
