package com.jacqui.prj.domain;

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: BnProjectInfo  
 * @Description: 项目信息 
 * @author 白展堂
 * 
 */
public class BnProjectInfo implements Serializable{

    private static final long serialVersionUID = 1L;

    private String id; //id
    
    private String prjNo; //项目编号
    
    private String prjName; //身份证号
    
    private String prjStart; //项目起时间
    
    private String prjEnd; //项目止时间
    
    private String parta; //委托单位
    
    private String partb; //承担单位/负责人
    
    private String prjGoal; //项目建设目标
    
    private String prjContent; //项目主要内容
    
    private String prjSpecial; //项目的特色与创新
    
    private String prjFile;
    
    private String createBy;
    private Date createAt;
    private String updateBy;
    private Date updateAt;
    private Date applyAt;
    private int status;
    
    private String branch;
    
    private Map<String,String> labels = Maps.newHashMap();
  
    public String getId(){
    	return id;
    }
    public void setId(String id){
        this.id = id;
    }
    public String getPrjNo(){
    	return prjNo;
    }
    public void setPrjNo(String prjNo){
        this.prjNo = prjNo;
    }
    public String getPrjName(){
    	return prjName;
    }
    public void setPrjName(String prjName){
        this.prjName = prjName;
    }
    public String getPrjStart(){
    	return prjStart;
    }
    public void setPrjStart(String prjStart){
        this.prjStart = prjStart;
    }
    public String getPrjEnd(){
    	return prjEnd;
    }
    public void setPrjEnd(String prjEnd){
        this.prjEnd = prjEnd;
    }
    public String getParta(){
    	return parta;
    }
    public void setParta(String parta){
        this.parta = parta;
    }
    public String getPartb(){
    	return partb;
    }
    public void setPartb(String partb){
        this.partb = partb;
    }
    public String getPrjGoal(){
    	return prjGoal;
    }
    public void setPrjGoal(String prjGoal){
        this.prjGoal = prjGoal;
    }
    public String getPrjContent(){
    	return prjContent;
    }
    public void setPrjContent(String prjContent){
        this.prjContent = prjContent;
    }
    public String getPrjSpecial(){
    	return prjSpecial;
    }
    public void setPrjSpecial(String prjSpecial){
        this.prjSpecial = prjSpecial;
    }
	public Map<String, String> getLabels() {
		labels.put("id", "id");
		labels.put("prjNo", "项目编号");
		labels.put("prjName", "身份证号");
		labels.put("prjStart", "项目起时间");
		labels.put("prjEnd", "项目止时间");
		labels.put("parta", "委托单位");
		labels.put("partb", "承担单位/负责人");
		labels.put("prjGoal", "项目建设目标");
		labels.put("prjContent", "项目主要内容");
		labels.put("prjSpecial", "项目的特色与创新");
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
	public String getCreateBy() {
		return createBy;
	}
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateAt() {
		return updateAt;
	}
	public void setUpdateAt(Date updateAt) {
		this.updateAt = updateAt;
	}
	public Date getApplyAt() {
		return applyAt;
	}
	public void setApplyAt(Date applyAt) {
		this.applyAt = applyAt;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getPrjFile() {
		return prjFile;
	}
	public void setPrjFile(String prjFile) {
		this.prjFile = prjFile;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
}