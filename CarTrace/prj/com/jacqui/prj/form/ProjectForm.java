package com.jacqui.prj.form;

import java.util.List;

import com.jacqui.prj.domain.BnProjectAdmin;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.prj.domain.BnResearcher;

public class ProjectForm {
	
	private BnProjectInfo bnProjectInfo;
	private BnProjectAdmin bnProjectAdmin;
	private String researcheres;
	private List<BnResearcher> researches;
	public BnProjectInfo getBnProjectInfo() {
		return bnProjectInfo;
	}
	public void setBnProjectInfo(BnProjectInfo bnProjectInfo) {
		this.bnProjectInfo = bnProjectInfo;
	}
	public BnProjectAdmin getBnProjectAdmin() {
		return bnProjectAdmin;
	}
	public void setBnProjectAdmin(BnProjectAdmin bnProjectAdmin) {
		this.bnProjectAdmin = bnProjectAdmin;
	}
	public List<BnResearcher> getResearches() {
		return researches;
	}
	public void setResearches(List<BnResearcher> researches) {
		this.researches = researches;
	}
	public String getResearcheres() {
		return researcheres;
	}
	public void setResearcheres(String researcheres) {
		this.researcheres = researcheres;
	}
	

}
