package com.jacqui.prj.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.constant.Constant;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.persistence.SearchFilter.Operator;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.prj.domain.BnExpert;
import com.jacqui.prj.domain.BnProjectAdmin;
import com.jacqui.prj.domain.BnProjectAudit;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.prj.domain.BnResearcher;
import com.jacqui.prj.service.BnProjectAdminService;
import com.jacqui.prj.service.BnProjectAuditService;
import com.jacqui.prj.service.BnProjectInfoService;
import com.jacqui.prj.service.BnResearcherService;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@RequestMapping(value = "/yuan/audit")
public class YuanAuditController {
	
	@Autowired
	public BnProjectInfoService bnProjectInfoService;
	
	@Autowired
	public BnProjectAdminService bnProjectAdminService;
	
	@Autowired
	public BnResearcherService bnResearcherService;
	 
	@Autowired
	public BnProjectAuditService bnProjectAuditService;
	
	
	@RequestMapping(value = "prj/search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "prj/yuan_project_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "prj/search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request,ServletResponse response) {
		BnExpert bnExpert = (BnExpert) WebUtils.getSessionAttribute((HttpServletRequest) request,Constant.SESSION_USER_KEY);
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		List<Integer> con = Lists.newArrayList();
		con.add(2);//提交审核
		con.add(7);//专家未通过
		SearchFilter filter = new SearchFilter("status",Operator.IN,con);
		SearchFilter filter1 = new SearchFilter("branch",Operator.EQ,bnExpert.getDepartment());
		
		filters.add(filter);
		filters.add(filter1);
		PageResult<BnProjectInfo> retObject = new PageResult<>();
		retObject = bnProjectInfoService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		List<BnProjectInfo> list= retObject.getList();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", retObject.getList());
		map.put("total", retObject.getTotalCount());
		map.put("pageCurrent", page);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "prj/get",params="_m=exec")
	public Object get(Model model,@RequestParam String id,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnProjectInfo bnProjectInfo = bnProjectInfoService.findUniqueBy(parameters);
		parameters.clear();
		parameters.put("prjId", id);
		BnProjectAdmin admin = bnProjectAdminService.findUniqueBy(parameters);
		List<BnResearcher>  researcheres = bnResearcherService.findBy(parameters);
		model.addAttribute("bnProjectInfo", bnProjectInfo);
		model.addAttribute("bnProjectAdmin", admin);
		model.addAttribute("researcheres", researcheres);
		return "prj/yuan_project_view";
	}
	//状态0删除1待审2院审通过并提交专家审核3院审未通过4院部终止5专家审核通过6专家审核未通过7专家终止
	@RequestMapping(value = "pass",params="_m=exec")
	@ResponseBody
	public Object pass(Model model,BnProjectAudit audit,ServletRequest request) {
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			BnExpert expert = (BnExpert) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			audit.setCreateBy(expert.getIdcard());
			audit.setStatus(3);
			bnProjectAuditService.insert(audit);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	@RequestMapping(value = "nopass",params="_m=exec")
	@ResponseBody
	public Object nopass(Model model,BnProjectAudit audit,ServletRequest request) {
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			audit.setStatus(4);
			BnExpert expert = (BnExpert) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			audit.setCreateBy(expert.getIdcard());
			bnProjectAuditService.insert(audit);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	@RequestMapping(value = "cancel",params="_m=exec")
	@ResponseBody
	public Object cancel(Model model,BnProjectAudit audit,ServletRequest request) {
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			audit.setStatus(5);
			BnExpert expert = (BnExpert) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_KEY);
			audit.setCreateBy(expert.getIdcard());
			bnProjectAuditService.insert(audit);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}

}
