package com.jacqui.prj.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.web.BaseController;
import com.jacqui.prj.domain.BnProjectAdmin;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.prj.domain.BnResearcher;
import com.jacqui.prj.form.ProjectForm;
import com.jacqui.prj.service.BnProjectAdminService;
import com.jacqui.prj.service.BnProjectInfoService;
import com.jacqui.prj.service.BnResearcherService;
import com.thoughtworks.xstream.tools.benchmark.targets.Person;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
/**
 * 
 * @ClassName: BnProjectInfoController 
 * @Description: 项目信息 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/bn/project/bn_project_info")
public class BnProjectInfoController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(BnProjectInfoController.class);
    
    //BnProjectInfo bnProjectInfo,BnProjectAdmin bnProjectAdmin,ArrayList<ResearcherForm> researches
   /* @InitBinder("bnProjectInfo")  
    public void initBinderbnProjectInfo(WebDataBinder binder) {    
        binder.setFieldDefaultPrefix("bnProjectInfo.");    
    } 
    @InitBinder("bnProjectAdmin")  
    public void initBinderbnProjectAdmin(WebDataBinder binder) {    
        binder.setFieldDefaultPrefix("bnProjectAdmin.");    
    } 
    @InitBinder("researches")  
    public void initBinderresearches(WebDataBinder binder) {    
        binder.setFieldDefaultPrefix("researches.");    
    } 
    */
    
	
	@Autowired
	public BnProjectInfoService bnProjectInfoService;
	
	@Autowired
	public BnProjectAdminService bnProjectAdminService;
	
	@Autowired
	public BnResearcherService bnResearcherService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "prj/project_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<BnProjectInfo> retObject = new PageResult<>();
		retObject = bnProjectInfoService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		List<BnProjectInfo> list= retObject.getList();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", retObject.getList());
		map.put("total", retObject.getTotalCount());
		map.put("pageCurrent", page);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<BnProjectInfo> list = Lists.newArrayList();
		list = bnProjectInfoService.findBy(parameters);
		return list;
	}
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		BnProjectInfo bnProjectInfo = bnProjectInfoService.findUniqueBy(parameters);
		return bnProjectInfo;
	}
	
	@RequestMapping(value = "insert/researcher",params="_m=init")
	public String insertresearcherInit(Model model,ServletRequest request) {
		return "prj/project_insert_researcher";
	} 
	
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "prj/project_insert";
	} 
	@RequestMapping(value = "insert/baseinfo",params="_m=init")
	public String insertBaseInfoInit(Model model,ServletRequest request) {
		return "prj/project_baseinfo_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=draft")
	@ResponseBody
	public Object insertdraft(Model model,ProjectForm project,ServletRequest request){
		
		
		String research = request.getParameter("researcheres");
//		String[]
		Map<String,Object> parameters = Maps.newHashMap();
//		JSONObject object = JSONObject.fromObject(project.getResearcheres());
		JSONArray jsonarray = JSONArray.fromObject(project.getResearcheres());  
		List<BnResearcher> researches = (List)JSONArray.toCollection(jsonarray, BnResearcher.class);
		project.setResearches(researches);
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.insert(project,request.getServletContext().getRealPath("/"),true);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,ProjectForm project,ServletRequest request){
		
		
		String research = request.getParameter("researcheres");
//		String[]
		Map<String,Object> parameters = Maps.newHashMap();
//		JSONObject object = JSONObject.fromObject(project.getResearcheres());
		JSONArray jsonarray = JSONArray.fromObject(project.getResearcheres());  
		List<BnResearcher> researches = (List)JSONArray.toCollection(jsonarray, BnResearcher.class);
		project.setResearches(researches);
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.insert(project,request.getServletContext().getRealPath("/"),false);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(Model model,@RequestParam String id,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnProjectInfo bnProjectInfo = bnProjectInfoService.findUniqueBy(parameters);
		parameters.clear();
		parameters.put("prjId", id);
		BnProjectAdmin bnProjectAdmin = bnProjectAdminService.findUniqueBy(parameters);
		List<BnResearcher>  researcheres = bnResearcherService.findBy(parameters);
		model.addAttribute("bnProjectInfo", bnProjectInfo);
		model.addAttribute("bnProjectAdmin", bnProjectAdmin);
		model.addAttribute("researcheres", researcheres);
		return "prj/project_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=draft")
	@ResponseBody
	public Object updatedraft(Model model,ProjectForm project,ServletRequest request){
		String research = request.getParameter("researcheres");
//		String[]
		Map<String,Object> parameters = Maps.newHashMap();
//		JSONObject object = JSONObject.fromObject(project.getResearcheres());
		JSONArray jsonarray = JSONArray.fromObject(project.getResearcheres());  
		List<BnResearcher> researches = (List)JSONArray.toCollection(jsonarray, BnResearcher.class);
		project.setResearches(researches);
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.update(project,request.getServletContext().getRealPath("/"),true);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;   
	}
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,ProjectForm project,ServletRequest request){
		String research = request.getParameter("researcheres");
//		String[]
		Map<String,Object> parameters = Maps.newHashMap();
//		JSONObject object = JSONObject.fromObject(project.getResearcheres());
		JSONArray jsonarray = JSONArray.fromObject(project.getResearcheres());  
		List<BnResearcher> researches = (List)JSONArray.toCollection(jsonarray, BnResearcher.class);
		project.setResearches(researches);
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.update(project,request.getServletContext().getRealPath("/"),false);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;   
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,BnProjectInfo bnProjectInfo,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnProjectInfoService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
