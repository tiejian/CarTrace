package com.jacqui.prj.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.WebUtils;

import com.alibaba.fastjson.JSON;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jacqui.prj.service.BnExpertService;
import com.jacqui.prj.domain.BnExpert;
import com.jacqui.prj.domain.BnProjectInfo;
import com.jacqui.admin.domain.DtDepartment;
import com.jacqui.admin.service.DtDepartmentService;
import com.jacqui.constant.Constant;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.form.PageResult;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.web.BaseController;
import com.jacqui.core.utils.CompressUtil;
import com.jacqui.core.utils.JsonDateValueProcessor;
import com.jacqui.core.utils.JsonReturn;
import com.jacqui.core.utils.Servlets;
import com.jacqui.core.utils.SystemUtil;
import com.jacqui.core.utils.TimeUtils;
/**
 * 
 * @ClassName: BnExpertController 
 * @Description: 专家信息 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Controller
@RequestMapping(value = "/bn/expert")
public class BnExpertController extends BaseController{

    private static Logger logger = LoggerFactory.getLogger(BnExpertController.class);
	
	@Autowired
	public BnExpertService bnExpertService;
	
	@Autowired
	public DtDepartmentService dtDepartmentService;
	
	/**
	 * @Description 
	 * @param model
	 * @param request
	 * @return 
	 */
	@RequestMapping(value = "index",params="_m=init")
	public String idxInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "";
	} 
	/**
	 * @description 分页查询初始化页面
	 * @param model
	 * @param request
	 * @return
	 */
    @RequestMapping(value = "search",params="_m=init")
	public String searchInit(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		return "expert/expert_list";
	} 
	/**
     * @description  分页查询
     * @param model
     * @param page 当前页，由页面传入
     * @param request
     * @return
     */
	@RequestMapping(value = "search",params="_m=load")
	@ResponseBody
	public Object searching(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		String page = request.getParameter("pageCurrent");
		if(StringUtils.isBlank(page)){
			page="1";
		}
		System.out.println(page.length());
		List<SearchFilter> filters = Lists.newArrayList();
		filters = SearchFilter.parseToList(Servlets.getParametersStartingWith(request, "filter"));
		PageResult<BnExpert> retObject = new PageResult<>();
		retObject = bnExpertService.pagingWithTotal(filters, Integer.valueOf(page), 20);
		Map<String, Object> map = new HashMap<String, Object>();
		List<BnExpert> list= retObject.getList();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", retObject.getList());
		map.put("total", retObject.getTotalCount());
		map.put("pageCurrent", page);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	/**
	 * @description 查询列表
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "find",params="_m=exec")
	@ResponseBody
	public Object find(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		List<BnExpert> list = Lists.newArrayList();
		list = bnExpertService.findBy(parameters);
		return list;
	}
	@RequestMapping(value = "findenable",params="_m=exec")
	@ResponseBody
	public Object findenable(Model model,@RequestParam String code, ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("roleCode", code);
		List<BnExpert> list = Lists.newArrayList();
		list = bnExpertService.findenable(parameters);
		for(BnExpert expert : list){
			parameters.clear();
			if(StringUtils.isNotBlank(expert.getDepartment())){
				parameters.put("dm", expert.getDepartment());
				DtDepartment department  = dtDepartmentService.findUniqueBy(parameters);
				expert.setDepartment(department.getMc());
			}else{
				expert.setDepartment("专家");
			}
		}
		return list;
	}
	@RequestMapping(value = "finddisable",params="_m=exec")
	@ResponseBody
	public Object finddisable(Model model,@RequestParam String code,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("roleCode", code);
		List<BnExpert> list = Lists.newArrayList();
		list = bnExpertService.finddisable(parameters);
		for(BnExpert expert : list){
			parameters.clear();
			if(StringUtils.isNotBlank(expert.getDepartment())){
				parameters.put("dm", expert.getDepartment());
				DtDepartment department  = dtDepartmentService.findUniqueBy(parameters);
				expert.setDepartment(department.getMc());
			}else{
				expert.setDepartment("专家");
			}
		}
		return list;
	}
	
	@RequestMapping(value = "findexpertenable",params="_m=exec")
	@ResponseBody
	public Object findexpertenable(Model model,@RequestParam String prjid,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("prjid", prjid);
		List<BnExpert> list = Lists.newArrayList();
		list = bnExpertService.findexpertenable(parameters);
		return list;
	}
	@RequestMapping(value = "findexpertdisable",params="_m=exec")
	@ResponseBody
	public Object findexpertdisable(Model model,@RequestParam String prjid,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("prjid", prjid);
		List<BnExpert> list = Lists.newArrayList();
		list = bnExpertService.findexpertdisable(parameters);
		return list;
	}
	
	/**
	 * @description 查询唯一
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "get",params="_m=exec")
	@ResponseBody
	public Object get(Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		BnExpert bnExpert = bnExpertService.findUniqueBy(parameters);
		return bnExpert;
	}
	/**
	 * @description 初始化增加页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params="_m=init")
	public String insertInit(Model model,ServletRequest request) {
		return "expert/expert_insert";
	} 
	/**
	 * @description  增加
	 * @param model 
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "insert",params = "_m=exec")
	@ResponseBody
	public Object insert(Model model,BnExpert bnExpert,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnExpert.setUserType(1);
			bnExpertService.insert(bnExpert);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 初始化修改页面
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params="_m=init")
	public String updateInit(@RequestParam("id") String id,Model model,ServletRequest request) {
		Map<String,Object> parameters = Maps.newHashMap();
		parameters.put("id", id);
		BnExpert bnExpert = bnExpertService.findUniqueById(id);
		model.addAttribute("bnExpert", bnExpert);
		return "expert/expert_update";
	} 
	/**
	 * @description 修改
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "update",params = "_m=exec")
	@ResponseBody
	public Object update(Model model,BnExpert bnExpert,ServletRequest request){
		
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			bnExpert.setUserType(1);
			bnExpertService.update(bnExpert);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 删除根据主键
	 * @param id 主键
	 * @param model
	 * @param bnCompanyInfo
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "delete",params = "_m=exec")
	@ResponseBody
	public Object deleteById(@RequestParam("id") String id,Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnExpertService.deleteById(id);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	/**
	 * @description 批量删除
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "bacthdelete",params = "_m=exec")
	@ResponseBody
	public Object bacthdelete(Model model,ServletRequest request){
		Map<String,Object> parameters = Maps.newHashMap();
		JsonReturn json = new JsonReturn();
		try{
			//TODO 参数化处理
			bnExpertService.deleteBy(parameters);
		}catch(Exception e){
			e.printStackTrace();
			json.setErrcode("1");
			json.setErrmsg("操作失败");
		}
		return json;  
	}
	
	
}
