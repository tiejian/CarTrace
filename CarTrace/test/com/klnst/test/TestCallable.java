package com.klnst.test;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.carrotsearch.ant.tasks.junit4.dependencies.com.google.common.collect.Maps;
import com.jacqui.admin.dao.TablePrimaryKeyMapper;
import com.jacqui.admin.domain.TablePrimaryKey;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/applicationContext.xml"})
public class TestCallable {
	
	@Autowired
	private TablePrimaryKeyMapper tablePrimaryKeyMapper;
	
	@Test
	public void testIndexGenerate() throws Exception {
		TablePrimaryKey p = new TablePrimaryKey();
		p.setStub("220203");
		tablePrimaryKeyMapper.getKey(p);
		System.out.println(p.getKey());
	}
}
