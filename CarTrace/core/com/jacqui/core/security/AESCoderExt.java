package com.jacqui.core.security;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class AESCoderExt {
	/*
	 * 加密用的Key 可以用26个字母和数字组成 此处使用AES-128-CBC加密模式，key需要为16位。
	 */
	//private String sKey = "smkldospdosldaaa";// key，可自行修改
	private String ivParameter = "1234567891234567";// 偏移量,可自行修改
	private static AESCoderExt instance = null;

	private AESCoderExt() {

	}

	public static AESCoderExt getInstance() {
		if (instance == null)
			instance = new AESCoderExt();
		return instance;
	}

	public static  byte[] Encrypt(String encData, String secretKey, String vector) throws Exception {

		if (secretKey == null) {
			return null;
		}
//		if (secretKey.length() != 16) {
//			return null;
//		}
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] raw = secretKey.getBytes();
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		IvParameterSpec iv = new IvParameterSpec(vector.getBytes());// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] encrypted = cipher.doFinal(encData.getBytes("utf-8"));
//		return new BASE64Encoder().encode(encrypted);// 此处使用BASE64做转码。
		return encrypted;
	}

	// 加密
	public byte[] encrypt(String sSrc,String sKey) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] raw = sKey.getBytes();
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());// 使用CBC模式，需要一个向量iv，可增加加密算法的强度
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
		byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));
//		return new BASE64Encoder().encode(encrypted);// 此处使用BASE64做转码。
		return encrypted;
	}

	// 解密
	public String decrypt(byte[] encrypted,String sKey) throws Exception {
		try {
			byte[] raw = sKey.getBytes("ASCII");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(ivParameter.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
//			byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);// 先用base64解密
			byte[] original = cipher.doFinal(encrypted);
			String originalString = new String(original, "utf-8");
			return originalString;
		} catch (Exception ex) {
			return null;
		}
	}

	public String decrypt(byte[] encrypted, String key, String ivs) throws Exception {
		try {
			byte[] raw = key.getBytes("ASCII");
			SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			IvParameterSpec iv = new IvParameterSpec(ivs.getBytes());
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
//			byte[] encrypted1 = new BASE64Decoder().decodeBuffer(sSrc);// 先用base64解密
			byte[] original = cipher.doFinal(encrypted);
			String originalString = new String(original, "utf-8");
			return originalString;
		} catch (Exception ex) {
			return null;
		}
	}

//	public static String encodeBytes(byte[] bytes) {
//		StringBuffer strBuf = new StringBuffer();
//
//		for (int i = 0; i < bytes.length; i++) {
//			strBuf.append((char) (((bytes[i] >> 4) & 0xF) + ((int) 'a')));
//			strBuf.append((char) (((bytes[i]) & 0xF) + ((int) 'a')));
//		}
//
//		return strBuf.toString();
//	}
	/**将二进制转换成16进制  
	 * @param buf  
	 * @return  
	 */   
	public static String parseByte2HexStr(byte buf[]) {   
	        StringBuffer sb = new StringBuffer();   
	        for (int i = 0; i < buf.length; i++) {   
	                String hex = Integer.toHexString(buf[i] & 0xFF);   
	                if (hex.length() == 1) {   
	                        hex = '0' + hex;   
	                }   
	                sb.append(hex.toUpperCase());   
	        }   
	        return sb.toString();   
	}  
	/**将16进制转换为二进制  
	 * @param hexStr  
	 * @return  
	 */   
	public static byte[] parseHexStr2Byte(String hexStr) {   
	        if (hexStr.length() < 1)   
	                return null;   
	        byte[] result = new byte[hexStr.length()/2];   
	        for (int i = 0;i< hexStr.length()/2; i++) {   
	                int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);   
	                int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);   
	                result[i] = (byte) (high * 16 + low);   
	        }   
	        return result;   
	}

	public static void main(String[] args) throws Exception {
		// 需要加密的字串
		String cSrc = "[{\"request_no\":\"100\"100045\"}]";

		// 加密
		long lStart = System.currentTimeMillis();
		
		byte[] encryptResult = AESCoderExt.getInstance().encrypt(cSrc,"9&mdJAR!U8tCq^Ww");
		String enString  = AESCoderExt.getInstance().parseByte2HexStr(encryptResult);
		System.out.println("加密后的字串是：" + enString);

		long lUseTime = System.currentTimeMillis() - lStart;
		System.out.println("加密耗时：" + lUseTime + "毫秒");
		// 解密
		lStart = System.currentTimeMillis();
		byte[] DeString = AESCoderExt.getInstance().parseHexStr2Byte(enString);
		String DeStringResult = AESCoderExt.getInstance().decrypt(DeString,"9&mdJAR!U8tCq^Ww");
		System.out.println("解密后的字串是：" + DeStringResult);
		lUseTime = System.currentTimeMillis() - lStart;
		System.out.println("解密耗时：" + lUseTime + "毫秒");
//		
//		String base = "ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*1234567890abcdefghigklmnopqrstuvwxyz";
//		Random random = new Random();
//		for(int j=0;j<9;j++){
//			StringBuffer sb  = new StringBuffer();
//			for(int i=0;i<16;i++){
//				int num = random.nextInt(base.length());
//				sb.append(base.charAt(num));
//			}
//			System.out.print("\""+sb.toString()+"\",");
//		}
//		
		
	}
}
