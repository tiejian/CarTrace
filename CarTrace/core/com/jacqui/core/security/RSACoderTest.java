package com.jacqui.core.security;

import java.security.InvalidKeyException;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.junit.Before;
import org.junit.Test;

public class RSACoderTest {  
	private String publicKey;  
    private String privateKey;  
  
    @Before  
    public void setUp() throws Exception {  
        Map<String, Object> keyMap = RSACoder.initKey();  
  
        publicKey = RSACoder.getPublicKey(keyMap);  
        privateKey = RSACoder.getPrivateKey(keyMap);  
        System.err.println("公钥: \n\r" + publicKey);  
        System.err.println("私钥： \n\r" + privateKey);  
    }  
  
    @Test  
    public void test() throws Exception {  
        System.err.println("公钥加密——私钥解密");  
        String inputStr = "1245";  
        byte[] data = inputStr.getBytes("UTF-8");  
        byte[] encodedData = RSACoder.encryptByPublicKey(data, publicKey);  
        byte[] decodedData = RSACoder.decryptByPrivateKey(encodedData, privateKey);  
  
        String outputStr = new String(decodedData);  
        System.err.println("加密后: " + bytesToString(encodedData));
        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr); 
        String tmp="106--62-117-44-94-24--64-125-11-107--12--25-26--12-71-97--35-2-95--12-124--21-2-112-51-22-67-30-105--96-4--98--65--20-78--117--25--51-1-32--50-57--104-78--122--14-127-113-76--71--49--31--95-101--57-95-75--17--43--46-5-63-85-13--72--76-102-76--120-96--123-27--82-54-36--24--57-79-86--70--57--2--36-72-6--62--89--90-56-63--120--69--110-6--97-21--38--54--61--48-68-115-100--37--22-11-89--128--25--76--119-114--122--61--34-20-116-72-27--85-119--46-64-66-117-117--29-101-";
        System.err.println("长度: " + tmp.length());  
        byte[] data1 = bytesToString(encodedData).getBytes("UTF-8");
        byte[] decodedData1 = RSACoder.decryptByPrivateKey(data1, privateKey);  
        System.err.println("再解密后: " + new String(decodedData1));  
//        assertEquals(inputStr, outputStr);  
    }  
  
//    @Test  
//    public void testSign() throws Exception {  
//        System.err.println("私钥加密——公钥解密");  
//        String inputStr = "sign";  
//        byte[] data = inputStr.getBytes();  
//        byte[] encodedData = RSACoder.encryptByPrivateKey(data, privateKey);  
//        byte[] decodedData = RSACoder.decryptByPublicKey(encodedData, publicKey);  
//  
//        String outputStr = new String(decodedData);  
//        System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);  
////        assertEquals(inputStr, outputStr);  
//  
//        System.err.println("私钥签名——公钥验证签名");  
//        // 产生签名  
//        String sign = RSACoder.sign(encodedData, privateKey);  
//        System.err.println("签名:\r" + sign);  
//  
//        // 验证签名  
//        boolean status = RSACoder.verify(encodedData, publicKey, sign);  
//        System.err.println("状态:\r" + status);  
////        assertTrue(status);  
//    }  
	protected String bytesToString(byte[] encrytpByte) {
		String result = "";
		for (Byte bytes : encrytpByte) {
			result += bytes.toString() + " ";
		}
		return result;
	}
}  