package com.jacqui.core.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/**
 * 
 * @author 小老头
 * @version 1.0
 * @since 1.0
 */
public abstract class Coder {
	public static final String KEY_SHA = "SHA";
	public static final String KEY_MD5 = "MD5";

	/**
	 * BASE64解密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	protected static byte[] decryptBASE64(String key) throws Exception {
		return (new BASE64Decoder()).decodeBuffer(key);
	}

	/**
	 * BASE64加密
	 * 
	 * @param key
	 * @return
	 * @throws Exception
	 */
	protected static String encryptBASE64(byte[] key) throws Exception {
		return (new BASE64Encoder()).encodeBuffer(key);
	}

	/**
	 * MD5加密
	 * 
	 * @param obj
	 * @return
	 */
	protected static byte[] encryptMD5(byte[] obj) {

		try {
			MessageDigest md5 = MessageDigest.getInstance(KEY_MD5);
			md5.update(obj);

			return md5.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * SHA加密
	 * 
	 * @param obj
	 * @return
	 */
	protected static byte[] encryptSHA(byte[] obj) {

		try {
			MessageDigest sha = MessageDigest.getInstance(KEY_SHA);
			sha.update(obj);

			return sha.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return null;
	}
}
