/*******************************************************************************
 * Copyright (c) 2005, 2014 springside.github.io
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 *******************************************************************************/
package com.jacqui.core.persistence;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;
import com.jacqui.core.utils.Collections3;

public class DynamicSpecifications {

	public static List<Criterion> bySearchFilter(final Collection<SearchFilter> filters){
		List<Criterion> predicates = Lists.newArrayList();
		if (Collections3.isNotEmpty(filters)) {
			for (SearchFilter filter : filters) {
				// logic operator
				switch (filter.operator) {
				case EQ:
					predicates.add(new Criterion(filter.fieldName+"=", filter.value));
					break;
				case NEQ:
					predicates.add(new Criterion(filter.fieldName+"<>", filter.value));
					break;
				case LIKE:
					predicates.add(new Criterion(filter.fieldName+" like", "%" + filter.value + "%"));
					break;
				case NOTLIKE:
					predicates.add(new Criterion(filter.fieldName+" not like", "%" + filter.value + "%"));
					break;
				case GT:
					predicates.add(new Criterion(filter.fieldName+">",  filter.value));
					break;
				case LT:
					predicates.add(new Criterion(filter.fieldName+"<",  filter.value));
					break;
				case GTE:
					predicates.add(new Criterion(filter.fieldName+">=",  filter.value));
					break;
				case LTE:
					predicates.add(new Criterion(filter.fieldName+"<=",  filter.value));
					break;
				case IN:
					predicates.add(new Criterion(filter.fieldName+" in ",  filter.value));
					break;
				case IS:
					predicates.add(new Criterion(filter.fieldName+ " is null "));
					break;
				case ISN:
					predicates.add(new Criterion(filter.fieldName+ " is not null "));
					break;
				default:
					predicates.add(new Criterion(filter.fieldName,  filter.value));
					break;
				}
			}
		}
		return predicates;
	}
}
