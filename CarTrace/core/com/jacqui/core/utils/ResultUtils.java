package com.jacqui.core.utils;

import java.util.Date;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;


public class ResultUtils {

	private static JsonConfig config = null;
	static{
		config = new JsonConfig();
//		config.registerJsonValueProcessor(java.sql.Date.class, new JsonValueProcessorImpl());
		config.registerJsonValueProcessor(Date.class, new JsonValueProcessorImpl());
	}
	
	public static String convert2Json(Map<String,Object> map){
		JsonConfig jsonConfig = new JsonConfig();
//		[{"list":[{"updateBy":"","prjContent":"32","updateAt":"","status":1,"parta":"23","labels":{"id":"id","prjContent":"项目主要内容","prjSpecial":"项目的特色与创新","prjName":"身份证号","parta":"委托单位","partb":"承担单位/负责人","prjNo":"项目编号","prjGoal":"项目建设目标","prjStart":"项目起时间","prjEnd":"项目止时间"},"partb":"23","prjNo":"23","createBy":"","id":"23fe3a42-2a04-11e6-87b6-4cbb58685bf7","prjSpecial":"23","createAt":"2016-06-04 00:00:00","prjName":"32","prjGoal":"32","prjStart":"1980-08-08","applyAt":"2016-06-04 00:00:00","prjEnd":"1980-08-08"},{"updateBy":"","prjContent":"32","updateAt":"","status":1,"parta":"233232","labels":{"id":"id","prjContent":"项目主要内容","prjSpecial":"项目的特色与创新","prjName":"身份证号","parta":"委托单位","partb":"承担单位/负责人","prjNo":"项目编号","prjGoal":"项目建设目标","prjStart":"项目起时间","prjEnd":"项目止时间"},"partb":"32","prjNo":"3232","createBy":"","id":"6c97b0ad-2a04-11e6-87b6-4cbb58685bf7","prjSpecial":"32","createAt":"2016-06-04 00:00:00","prjName":"32","prjGoal":"32","prjStart":"1980-08-08","applyAt":"2016-06-04 00:00:00","prjEnd":"1980-08-08"}]","total":"2","pageCurrent":"1"}]
        return "[{\"list\":" + JSONArray.fromObject(map.get("list"),config).toString() + "\",\"total\":\""+String.valueOf(map.get("total"))+ "\",\"pageCurrent\":\"" +String.valueOf(map.get("pageCurrent")) +"\"}]";
	}
	
	
	
	public static String emptyResult(){
		return "{\"rows\":[],\"total\":\"0\"}";
	}
	
}
