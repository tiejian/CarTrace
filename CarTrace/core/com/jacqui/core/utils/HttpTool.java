package com.jacqui.core.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class HttpTool {


	public static boolean httppost(String uri) {
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 120000);
		HttpConnectionParams.setSoTimeout(httpParams, 20000);
		HttpClient httpclient = new DefaultHttpClient(httpParams);
		boolean ret = false;
		try {
			HttpPost httpost = new HttpPost(uri);

			HttpResponse response = httpclient.execute(httpost);

			if (response.getStatusLine().getStatusCode() == 200) {
				ret = true;
			} else {
				ret = false;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			ret = false;
		} catch (IOException e) {
			e.printStackTrace();
			ret = false;
		} catch (Exception e) {
			e.printStackTrace();
			ret = false;
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
		return ret;
	}

	public static void testGameAlarm() throws ClientProtocolException,IOException, URISyntaxException {
		HttpClient httpclient = new DefaultHttpClient();
		try {
			HttpPost httpost = new HttpPost("http://localhost:9090/nbservice/game/gamealarm.xml");
			HttpResponse response = httpclient.execute(httpost);
			System.out.println("Login form get: " + response.getStatusLine());
		} finally {
			httpclient.getConnectionManager().shutdown();
		}
	}
	public static String doRequest(String urlString, HashMap<String,String> params,String method) throws Exception, ConnectException
	 {
		try{
	    	URL url = new URL (urlString);
	        HttpURLConnection uc = (HttpURLConnection) url.openConnection();
//	        uc.setRequestProperty("Authorization", "Basic " + encoding);
	        uc.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)");
	        uc.setRequestProperty("Accept","application/x-ms-application, image/jpeg, application/xaml+xml, image/gif, image/pjpeg, application/x-ms-xbap, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/msword, */*");
	        uc.setRequestProperty("Accept-Language", "zh-cn");
	        uc.setRequestProperty("UA-CPU", "x86");
	        uc.setRequestProperty("Accept-Encoding", "gzip, deflate");
	        uc.setRequestProperty("ContentType", "application/x-www-form-urlencoded");
	        uc.setRequestProperty("Connection", "Keep-Alive");
	        uc.setRequestProperty("Cache-Control", "no-cache");
	        uc.setRequestProperty("Charset", "gbk");
	        uc.setDoInput(true);
	        uc.setDoOutput(true);
	        uc.setRequestMethod(method);
	        if (params != null && !params.isEmpty()) {
	        	StringBuffer buf = new StringBuffer();
	        	for(String key : params.keySet()){
	        		 buf.append("&").append(key).append("=").append(params.get(key));
	        	}
	        	buf.deleteCharAt(0);
	            uc.getOutputStream().write(buf.toString().getBytes("gbk"));  
	            uc.getOutputStream().close();  
	        }  
	     
	        
	        StringBuffer sb = new StringBuffer();
	        
	        InputStream content = uc.getInputStream();
	        BufferedReader in = new BufferedReader (new InputStreamReader (content,"gbk"));
	        String line = in.readLine();//will refactory
		          while ((line = in.readLine()) != null) {
		             sb.append(line);
		            }
	        in.close();
	        System.out.println(new String(sb.toString()));
	        return sb.toString().trim();
		}catch(java.net.ConnectException e){
			throw new java.net.ConnectException();
		}catch(IOException e){
			throw new Exception(e);
		}

	 }
	
	public static String request4BasicAuth(String url) throws ClientProtocolException, IOException{
		
		HttpHost targetHost = new HttpHost("localhost", 8080, "http");
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		credsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("administrator", "11"));
		 
		AuthCache authCache = new BasicAuthCache();
		authCache.put(targetHost, new BasicScheme());
		 
		// Add AuthCache to the execution context
		final HttpClientContext context = HttpClientContext.create();
		context.setCredentialsProvider(credsProvider);
		context.setAuthCache(authCache);
		HttpClient client = HttpClientBuilder.create().build();
		URI uri = URI.create(url);  
		HttpResponse response = client.execute(new HttpGet(uri), context);
		HttpEntity entity = response.getEntity();  
		String backContent="";
		if (entity != null) {              
		    //start 读取整个页面内容  
			InputStream is = entity.getContent();  
			BufferedReader in = new BufferedReader(new InputStreamReader(is));   
			StringBuffer buffer = new StringBuffer();   
			String line = "";  
			while ((line = in.readLine()) != null) {  
			    buffer.append(line);  
			}  
		 //end 读取整个页面内容  
		    backContent = buffer.toString();  
		}  
		System.out.println(backContent);
		int statusCode = response.getStatusLine().getStatusCode();
		return backContent;
		
	}
	public static void main(String [] args){
		try {
			HttpTool.request4BasicAuth("http://win-h0bsdfoj50v/ReportServer");
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
