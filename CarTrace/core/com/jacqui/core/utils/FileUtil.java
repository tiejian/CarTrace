package com.jacqui.core.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import org.springframework.web.multipart.MultipartFile;




public class FileUtil {

	public static void copyForChannel(File f1,File f2){
		FileInputStream in = null;
		FileOutputStream out = null;
		FileChannel inC = null;
		FileChannel outC = null;
		try{
        int length=2097152;
         in=new FileInputStream(f1);
        out=new FileOutputStream(f2);
        inC=in.getChannel();
        outC=out.getChannel();
        ByteBuffer b=null;
        while(true){
            if(inC.position()==inC.size()){
                inC.close();
                outC.close();
                return;
            }
            if((inC.size()-inC.position())<length){
                length=(int)(inC.size()-inC.position());
            }else
                length=2097152;
            b=ByteBuffer.allocateDirect(length);
            inC.read(b);
            b.flip();
            outC.write(b);
            outC.force(false);
        }
		}catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(in != null)
				try {
					in.close();
				} catch (IOException e) {
					in = null;
					e.printStackTrace();
				}
			if(out !=null)
				try {
					out.close();
				} catch (IOException e) {
					out = null;
					e.printStackTrace();
				}
			if(inC != null)
				try {
					inC.close();
				} catch (IOException e) {
					inC = null;
					e.printStackTrace();
				}
			if(outC != null)
				try {
					outC.close();
				} catch (IOException e) {
					outC = null;
					e.printStackTrace();
				}
		}
    }
	
	public static void copyForChannel(MultipartFile file,File file2){
		FileOutputStream fs = null;
		InputStream stream = null;
		try{
	    fs=new FileOutputStream(file2);
        byte[] buffer =new byte[1024*1024];
        int bytesum = 0;
        int byteread = 0; 
        stream = file.getInputStream();
        while ((byteread=stream.read(buffer))!=-1)
        {
           bytesum+=byteread;
           fs.write(buffer,0,byteread);
           fs.flush();
        } 
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(fs != null)
				try {
					fs.close();
				} catch (IOException e) {
					fs = null;
					e.printStackTrace();
				}
			if(stream != null)
				try {
					stream.close();
				} catch (IOException e) {
					stream = null;
					e.printStackTrace();
				}      
		}
    }
	
	
	
}
