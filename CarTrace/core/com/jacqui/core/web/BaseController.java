package com.jacqui.core.web;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.util.WebUtils;

import com.jacqui.constant.Constant;
import com.jacqui.core.excel.ExcelUtilExt;
import com.jacqui.core.excel.ExcelUtilExt2;
import com.jacqui.core.excel.ExportSetInfo;
import com.jacqui.core.utils.JsonDateValueProcessor;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class BaseController {
	
	private static final int BUFFER_SIZE = 4096;
	
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	
	public JSONObject paging(List<?> list,long totalCount,String currentPage) {
		Map<String, Object> map = new HashMap<String, Object>();
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new JsonDateValueProcessor("yyyy-MM-dd"));
		map.put("rows", list);
		map.put("total", totalCount);
		map.put("pageCurrent", currentPage);
		JSONObject jsonObject = JSONObject.fromObject( map,jsonConfig );  
		return jsonObject;
	}
	
	/**
	 * 
	 * @Title: getUserExcel 
	 * @Description: 生成Excel文件
	 * @param map
	 * @param fullName 带路径的文件全称
	 * @param sheetText 标签页名称
	 * @param title 标题列表
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @return boolean    返回类型 
	 * @throws
	 */
	public boolean getUserExcel(LinkedHashMap<String, List> map,String fullName,String title,String[] head,String[] fields,String[] mergeCells) throws IllegalArgumentException,IOException, IllegalAccessException {
		FileOutputStream out = new FileOutputStream(fullName);
		List<String[]> headNames = new ArrayList<String[]>();
		headNames.add(head);
		List<String[]> fieldNames = new ArrayList<String[]>();
		fieldNames.add(fields);
		ExportSetInfo setInfo = new ExportSetInfo();
		setInfo.setObjsMap(map);
		setInfo.setFieldNames(fieldNames);
		setInfo.setTitles(new String[] { title});
		setInfo.setHeadNames(headNames);
		setInfo.setOut(out);
		setInfo.setMergeCells(mergeCells);
		ExcelUtilExt.export2Excel(setInfo);
		return true;
	}
	/**
	 * @throws InvalidFormatException 
	 * 
	 * @Title: getUserExcel 
	 * @Description: 生成Excel文件
	 * @param map
	 * @param fullName 带路径的文件全称
	 * @param sheetText 标签页名称
	 * @param title 标题列表
	 * @param templateUri 模板路径
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @return boolean    返回类型 
	 * @throws
	 */
	public boolean getUserExcelFromTemplate(LinkedHashMap<String, List> map,String fullName,String sheetText,String[] fields,String[] mergeCells,String templateUri,int headCount) throws IllegalArgumentException,IOException, IllegalAccessException, InvalidFormatException {
		FileOutputStream out = new FileOutputStream(fullName);
		List<String[]> headNames = new ArrayList<String[]>();
		List<String[]> fieldNames = new ArrayList<String[]>();
		fieldNames.add(fields);
		ExportSetInfo setInfo = new ExportSetInfo();
		setInfo.setObjsMap(map);
		setInfo.setFieldNames(fieldNames);
		setInfo.setTitles(new String[] { sheetText});
		setInfo.setHeadNames(headNames);
		setInfo.setMergeCells(mergeCells);
		setInfo.setOut(out);
		ExcelUtilExt2.export2Excel(setInfo,templateUri,headCount);
		return true;
	}
	/**
	 * 
	 * @Title: fileDownload 
	 * @Description: 文件下载
	 * @param fullName 带路径的文件全称
	 * @param fileName 文件名称
	 * @param request
	 * @param response
	 * @throws IOException
	 * @return void    返回类型 
	 * @throws
	 */
	public void fileDownload(String fullName,String fileName,ServletRequest request, HttpServletResponse response) throws IOException {
		 
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		File file = new File(fullName);
		response.setCharacterEncoding("UTF-8");
		// 写明要下载的文件的大小
		response.setContentLength((int) file.length());
		//response.setContentType("application/octet-stream");
		response.setContentType("application/x-msdownload");
		response.reset();
		
		String enableFileName = new String(fileName.getBytes("GBK"),"ISO-8859-1");
		response.setHeader("Content-Disposition", "attachment; filename="+enableFileName);
		response.setHeader("Connection", "close");
		try{
			bis = new BufferedInputStream(new FileInputStream(fullName));
		    bos = new BufferedOutputStream(response.getOutputStream());
		    byte[] buff = new byte[BUFFER_SIZE];
		    int bytesRead;
		    while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
		        bos.write(buff, 0, bytesRead);
		    }
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			bis.close();
			bos.close();
		}
	}
	
	public class Dropdwon4TableField{
		private String field;
		private String text;
		public String getField() {
			return field;
		}
		public void setField(String field) {
			this.field = field;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		Dropdwon4TableField(){}
		
		public Dropdwon4TableField(String field,String text){
			this.field=field;
			this.text=text;
		}
	}
	protected Map<String,Object> appendSessionCondition(ServletRequest request,Map<String,Object> param){
		String userDateArea="";
//		String userXzq="";
//		String userBmdm="";
//		public final static String SESSION_USER_DATA_AREA="session_date_area"; //全市qs,市本级:sbj,所属行政区:ssxzq,所属单位:ssdw,单位本级:dwbj
//		public final static String SESSION_USER_XZQ="session_user_xzq";//行政区
//		public final static String SESSION_USER_BM="session_user_bm"; // 部门代码 
		userDateArea = (String) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_DATA_AREA);
//		if(userDateArea.equals("qs")){
//			
//		}
//		if(userDateArea.equals("ssxzq")){
//			if(StringUtils.isBlank((String)param.get("xzq"))){
//				param.put("xzq", (String) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_XZQ));	
//			}
//		}
//		if(userDateArea.equals("ssdw")){
//			if(StringUtils.isBlank((String)param.get("hsjgDm"))){
//				param.put("hsjgDm", (String) WebUtils.getSessionAttribute((HttpServletRequest) request, Constant.SESSION_USER_BM));
//			}
//		}
		return param;
	}

}
