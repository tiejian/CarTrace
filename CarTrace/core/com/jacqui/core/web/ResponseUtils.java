package com.jacqui.core.web;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

/**
 * @author 孙飞
 * 
 */
public class ResponseUtils {
	
	public static void write(PageContext pagecontext, String s)
			throws JspException {
		JspWriter jspwriter = pagecontext.getOut();
		try {
			jspwriter.print(s);
		} catch (IOException ioexception) {
			throw new JspException(ioexception.toString());
		}
	}
}
