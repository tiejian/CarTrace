<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${r"${pageContext.request.contextPath}"}" />
<script>
    var $grid;
	$(function () {
	    $('#dialog4search').jqm({toTop:true});
		$grid = $("#maingrid4").css({"border":"0","float":"left"}).ligerGrid({
				columns: [
				    <#list tPropertys as prop>
				     <#if prop.javaField != 'id' && prop.javaField != 'logId' && prop.javaField != 'sysLogId' && prop.javaField != 'cjAt'>
				    { display: '${prop.cluComment}', name: '${prop.javaField}', width: 200 }<#if prop_has_next>,</#if>
				    </#if>
    				</#list>
					], 
				url: '${r"${ctx}"}/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/${org}/${tableName}/search?_m=query&_tb=${r"${_tb}"}&_logid=${r"${id}"}',
				delayLoad: false,
				pageSize: 50,
				rownumbers:true,
				root :'Rows',                          
				record:'Total',                        
				pageParmName :'page',               
				pagesizeParmName:'pagesize',
				allowAdjustColWidth:true,
				onSuccess:function(data){
					if (typeof data.errcode != "undefined")
					{
					   window.location.href="NoLoginOrSessionOut.jsp";
					} 
				},
				parms:[
					{ name: "_con", value: $("#_con").val() }
				],
				onError:function(){
					ajaxError();
				},
				onDblClickRow: function(row){
					browse();
				},
				width: '100%',
				height:'100%',
				heightDiff:-35
		});
            $(".grid-toolbar").ligerToolBar({
			background: false,
			items: [{type: 'button',text: '查询',icon: 'search',disable: false,click: function () {f_search()}}]
		});

	    $("#gobackpage").bind("click",function(){
			var flag="${r"${flag}"}";
			if(flag==0){
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/bm/tables?code=${r"${org.orgCode}"}");
			}
			if(flag==1){
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/main/redirect/timeline/index");
			}
			if(flag==2){
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/private/search/tables?orgcode=${r"${org.orgCode}"}");
			}
	    });

	});
</script>
<div class="grid-nav">系统管理>用户日志</div>
<div class="grid-toolbar"></div>
<div id="maingrid4" style="margin:0; padding:0"></div>
<div id="dialog4search" class="jqmWindow">
</div>
<input type=hidden id="_con" name="_con" value=""/>
<script>
    var browseWin;
	
	var browseWin;
    function f_search1(){
		browseWin=$.ligerDialog.open({ title:"查询条件设置", url: '${r"${ctx}"}/dsj/dsj_skrkxx/search?_m=condition_init&_tb=${r"${_tb}"}&r='+Math.random(), width: 800,height:400, showMax: true, showToggle: false, isResize: true, isHidden: false,slide: false,allowClose: false});
	}
	function browseClose(){
		browseWin.hide();
	}
		
	function f_search(){
		$('#dialog4search').html("");
		$('#dialog4search').load("${r"${ctx}"}/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/${org}/${tableName}/search?_m=condition_init&_tb=${r"${_tb}"}");
		$('#dialog4search').css({
			width: 800,
			height:400,
			"text-align": "center"
		}).jqmShow();	
	}
	function query(data){
		$("#_con").val(data);
		$grid.setParm("_con",data);
		$grid.loadData();//加载数据
	}
	
</script>

