<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${r"${pageContext.request.contextPath}"}" />
<script>
    var manager, g;
	var expr = [{ operator: 'GT', text: '大于' },{ operator: 'LT', text: '小于' },{ operator: 'EQ', text: '等于' },{ operator: 'NEQ', text: '不等于' },{ operator: 'GTE', text: '大于等于' },{ operator: 'LTE', text: '小于等于' },{ operator: 'LIKE', text: '包含' },{ operator: 'NOTLIKE', text: ' 不包含' }];
	$(function () {
		g =  manager =$("#maingridCon").css({"border":"0","float":"left"}).ligerGrid({
				columns: [
					{ display: '字段', name: 'fieldName', width: 200 , editor: { type: 'select', data: ${r"${dw_field}"}, valueColumnName: 'fieldName',displayColumnName: 'cluComment' },render: function (item)
                    {
                        //alert(item.cluComment);
						for (var i = 0; i < ${r"${dw_field}"}.length; i++)
                        {
                            if ( ${r"${dw_field}"}[i]['fieldName'] == item.fieldName)
                                return ${r"${dw_field}"}[i]['cluComment']
                        }
                        return item.cluComment;
                    }},
					{ display: '表达式', name: 'operator', width: 100 , editor: { type: 'select', data: expr, valueColumnName: 'operator',displayColumnName: 'text' },render: function (item)
                    {
						for (var i = 0; i < expr.length; i++)
                        {
                            if (expr[i]['operator'] == item.operator)
                                return expr[i]['text']
                        }
                        return item.text;
                    }},
					{ display: '值', name: 'value', width: 250,editor: { type: 'text' } }
					], 
				delayLoad: false,
				pageSize: 10,
				rownumbers:false,
				root :'Rows',                          
				record:'Total',                        
				pageParmName :'page',               
				pagesizeParmName:'pagesize',
				onSuccess:function(data){
					if (typeof data.errcode != "undefined")
					{
					   window.location.href="NoLoginOrSessionOut.jsp";
					} 
				},
				onError:function(){
					ajaxError();
				},
				onDblClickRow: function(row){
					browse();
				},
				rowHeight:28,
				width: 800,
				height:370,
			    enabledEdit: true,
				clickToEdit: true
		});
        $("#con-toolbar").ligerToolBar({
			background: false,
			items: [{type: 'button',text: '增加',icon: 'add',disable: false,click: function () {f_addcon()}},{ line:true },{type: 'button',text: '查询',icon: 'search2',disable: false,click: function () {f_getData()}}]
		});

	});
</script>
<div style="position:relative;float:left;width:100%;height:100%; border: 1px solid #c9c9c9;">
<div class="grid-toolbar" id="con-toolbar"></div>
<div id="maingridCon" style="margin:0; padding:0"></div>
</div>
<script>
	function f_addcon(){
		manager.addEditRow();
	}
	function f_getData()
	{ 
		var data = manager.getData();
		//alert(JSON.stringify(data));
		$('#dialog4search').jqmHide();
		query(JSON.stringify(data));
	} 
</script>