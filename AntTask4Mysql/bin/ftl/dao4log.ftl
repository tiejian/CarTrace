package ${pkg};

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import ${domainPkg}.${clzName};
import com.klnst.core.persistence.Criteria;
import com.klnst.core.persistence.Criterion;
import com.klnst.core.mybatis.MyBatisRepository;

import com.klnst.domain.SysQryLog;

import java.util.Date;
/**
 * 
 * @ClassName: ${clzName}Mapper  
 * @Description: ${tableComment} 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@MyBatisRepository
public interface ${clzName}Mapper {

    List<${clzName}> paging4DynamicTblName(Criteria criteria,PageBounds pageBounds);
    
    long pagingCount4DynamicTblName(Criteria criteria);
    
    List<${clzName}> paging(Criteria criteria,PageBounds pageBounds);
    
    long pagingCount(Criteria criteria);
    
    List<${clzName}> findBy(Map<String, Object> parameters);
    
    ${clzName} findUniqueBy(Map<String, Object> parameters);
    
    int insert(${clzName} ${clzName?uncap_first});
    
    int update(${clzName} ${clzName?uncap_first});
    
    void deleteById(String ${keyProperty});
    
    List<SysQryLog> traceByLogId(String logId,PageBounds pageBounds);
}