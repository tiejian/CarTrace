package com.klnst.generator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;

import com.klnst.database.DbReader;
import com.klnst.database.TableColumn;
import com.klnst.entity.TableInfo;
import com.klnst.task.XmlReader;
import com.klnst.util.FreemakerUtil;
import com.klnst.util.Tools;
import com.klnst.util.XmlUtils;

import freemarker.template.TemplateException;

public class Generator {

	public List<TableInfo> list=new ArrayList<TableInfo>();;
	
	/**
	 * @throws SQLException 
	 * 
	 * @Title: getAllTableInfo 
	 * @Description: 取得所有table信息 
	 * @param xmlFilePath
	 * @return void    返回类型 
	 * @throws
	 */
	public void getAllTableInfo(String xmlFilePath) throws SQLException{
		List<Element> tbls  = XmlReader.readTableInfo(xmlFilePath);
		for(Element e: tbls){
			if(e.getName().equals("table")){
				 Map<String, String> map=  XmlUtils.getNodeAttrMap(e);
				 TableInfo t = new TableInfo();
				 t.setTableName(map.get("tableName"));
				 t.setTableComment(map.get("tableComment"));
				 t.setDomainObjectName(StringUtils.isBlank(map.get("domainObjectName"))?Tools.transDb2JavaName(map.get("tableName")):map.get("domainObjectName"));
				 t.setTargetPackage(map.get("targetPackage"));
				 t.setTargetPath(map.get("targetPath"));
				 t.setTableColumns(getAllColumn(map.get("tableName"),xmlFilePath));
				 t.setTargetDaoPackage(map.get("targetDaoPackage"));
				 t.setTargetServicePackage(map.get("targetServicePackage"));
				 t.setTargetServiceProviderPackage(map.get("targetServiceProviderPackage"));
				 t.setTargetFullTextServicePackage(map.get("targetFullTextServicePackage"));
				 t.setTargetControllerPackage(map.get("targetControllerPackage"));
				 t.setTargetPageFolder(map.get("targetPageFolder"));
				 t.setTargetKeyword(map.get("targetKeyword"));
				 t.setTargetType(map.get("targetType"));
				 t.setModel(map.get("model"));
				 Element eId = XmlUtils.getChild(e,"generatedKey");
				 Attribute key = XmlUtils.getAttribute(eId,"column");
				 t.setKey(key.getValue());
				 this.list.add(t);
				 System.out.println("表：" + map.get("tableName")+" ----------表主键："+ key.getValue());
				 
			}
		}
	}
	private List<TableColumn> getAllColumn(String tableName,String filePath) throws SQLException{
		DbReader db = new DbReader(filePath);
		List<TableColumn>  cl = db.getTableColumn(tableName);
		return cl;
	}
	public void genDomain() throws IOException, TemplateException{
		for(TableInfo table:list){
			FreemakerUtil.init();
			FreemakerUtil.genDomain(table);
		}
	}
	public void genMapper() throws IOException, TemplateException{
		for(TableInfo table:list){
			FreemakerUtil.init();
			FreemakerUtil.genMapper(table);
		}
	}
	public void genDao() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetDaoPackage())){
				FreemakerUtil.init();
				FreemakerUtil.genDao(table);
			}
		}
	}
	public void genService() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetServicePackage())){
				FreemakerUtil.init();
				FreemakerUtil.genService(table);
			}
		}
	}
	public void genServiceProvider() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetServiceProviderPackage())){
				FreemakerUtil.init();
				FreemakerUtil.genServiceProvider(table);
			}
		}
	}
	public void genFulltextService() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetKeyword())){
				FreemakerUtil.init();
				FreemakerUtil.genFulltextService(table);
			}
		}
	}
	public void genController() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetControllerPackage())){
				FreemakerUtil.init();
				FreemakerUtil.genController(table);
			}
		}
	}
	public void genList() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetPageFolder())){
				FreemakerUtil.init();
				FreemakerUtil.genListPage(table);
				FreemakerUtil.genConditionPage(table);
			}
		}
	}
	public void genList4qjPage() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetPageFolder())){
				FreemakerUtil.init();
				FreemakerUtil.genList4qjPage(table);
//				FreemakerUtil.genConditionPage(table);
			}
		}
	}
	
}
