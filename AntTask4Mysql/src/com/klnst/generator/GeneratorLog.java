package com.klnst.generator;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;

import com.klnst.database.DbReader;
import com.klnst.database.TableColumn;
import com.klnst.entity.TableInfo;
import com.klnst.task.XmlReader;
import com.klnst.util.FreemakerUtil4Log;
import com.klnst.util.Tools;
import com.klnst.util.XmlUtils;

import freemarker.template.TemplateException;

public class GeneratorLog {
public List<TableInfo> list=new ArrayList<TableInfo>();;
	
	/**
	 * @throws SQLException 
	 * 
	 * @Title: getAllTableInfo 
	 * @Description: 取得所有table信息 
	 * @param xmlFilePath
	 * @return void    返回类型 
	 * @throws
	 */
	public void getAllTableInfo(String xmlFilePath) throws SQLException{
		List<Element> tbls  = XmlReader.readTableInfo(xmlFilePath);
		for(Element e: tbls){
			if(e.getName().equals("table")){
				 Map<String, String> map=  XmlUtils.getNodeAttrMap(e);
				 TableInfo t = new TableInfo();
				 t.setTableName(map.get("tableName"));
				 t.setTableComment(map.get("tableComment"));
				 t.setDomainObjectName(StringUtils.isBlank(map.get("domainObjectName"))?Tools.transDb2JavaName(map.get("tableName")):map.get("domainObjectName"));
				 t.setTargetPackage(map.get("targetPackage"));
				 t.setTargetPath(map.get("targetPath"));
				 t.setTableColumns(getAllColumn(map.get("tableName"),xmlFilePath));
				 t.setTargetDaoPackage(map.get("targetDaoPackage"));
				 t.setTargetServicePackage(map.get("targetServicePackage"));
				 t.setTargetControllerPackage(map.get("targetControllerPackage"));
				 t.setTargetPageFolder(map.get("targetPageFolder"));
				 t.setModel(map.get("model"));
				 Element eId = XmlUtils.getChild(e,"generatedKey");
				 Attribute key = XmlUtils.getAttribute(eId,"column");
				 t.setKey(key.getValue());
				 this.list.add(t);
				 System.out.println("表：" + map.get("tableName")+" ----------表主键："+ key.getValue());
				 
			}
		}
	}
	private List<TableColumn> getAllColumn(String tableName,String xmlFilePath) throws SQLException{
		DbReader db = new DbReader(xmlFilePath);
		List<TableColumn>  cl = db.getTableColumn(tableName);
		return cl;
	}
	public void genDomain() throws IOException, TemplateException{
		for(TableInfo table:list){
			FreemakerUtil4Log.init();
			FreemakerUtil4Log.genDomain(table);
		}
	}
	public void genMapper() throws IOException, TemplateException{
		for(TableInfo table:list){
			FreemakerUtil4Log.init();
			FreemakerUtil4Log.genMapper(table);
		}
	}
	public void genDao() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetDaoPackage())){
				FreemakerUtil4Log.init();
				FreemakerUtil4Log.genDao(table);
			}
		}
	}
	public void genService() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetServicePackage())){
				FreemakerUtil4Log.init();
				FreemakerUtil4Log.genService(table);
			}
		}
	}
	public void genController() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetControllerPackage())){
				FreemakerUtil4Log.init();
				FreemakerUtil4Log.genController(table);
			}
		}
	}
	public void genList() throws IOException, TemplateException{
		for(TableInfo table:list){
			if(StringUtils.isNotBlank(table.getTargetPageFolder())){
				FreemakerUtil4Log.init();
				FreemakerUtil4Log.genListPage(table);
				FreemakerUtil4Log.genConditionPage(table);
			}
		}
	}
}
