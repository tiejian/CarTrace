package com.klnst.task;

import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

import com.klnst.util.XmlUtils;

public class XmlReader {
	
	public  static Map<String, String> readProjectInfo(String filePath){
//		String filePath="D:/projects/WORKSPACE_JEE/AntTask4PostgreSql/generatorConfiguration_vm.xml";
		Document document = XmlUtils.getDocument(filePath);
		Element root = XmlUtils.getRootNode(document);
		List<Element> ele = XmlUtils.getChildElements(root,"info");
		for(Element e: ele){
//			System.out.println(e.getName());
			if(e.getName().equals("info")){
				return  XmlUtils.getNodeAttrMap(e);
			}
		}
		return null;
	}
	
	
	public  static Map<String, String> readDbInfo(String filePath){
//		String filePath="D:/projects/WORKSPACE_JEE/AntTask4PostgreSql/generatorConfiguration_vm.xml";
		Document document = XmlUtils.getDocument(filePath);
		Element root = XmlUtils.getRootNode(document);
		List<Element> ele = XmlUtils.getChildElements(root,"jdbcConnection");
		for(Element e: ele){
//			System.out.println(e.getName());
			if(e.getName().equals("jdbcConnection")){
				return  XmlUtils.getNodeAttrMap(e);
			}
		}
		return null;
	}
	public  static List<Element> readTableInfo(String filePath){
		Document document = XmlUtils.getDocument(filePath);
		Element root = XmlUtils.getRootNode(document);
		List<Element> ele = XmlUtils.getChildElements(root,"table");
		return ele;
	}
	
	public static void main(String[] args) {
		String filePath="D:/workspace/AntTask/generatorConfiguration.xml";
		Document document = XmlUtils.getDocument(filePath);
		Element root = XmlUtils.getRootNode(document);
		List<Element> ele = XmlUtils.getChildElements(root,"jdbcConnection");
		for(Element e: ele){
			System.out.println(e.getName());
			Map<String, String> map =  XmlUtils.getNodeAttrMap(e);
			System.out.println(map);
		}
//		XmlReader.readTableInfo();
	}
}
