package com.klnst.task;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.klnst.generator.Generator;
import com.klnst.util.StringUtility;
import com.klnst.util.messages.Messages;

import freemarker.template.TemplateException;

public class GeneratorAntTask extends Task {
	
	private String configfile;
	private boolean verbose;
	private boolean overwrite;
	
	@Override
	public void execute() throws BuildException {
		// TODO Auto-generated method stub
		super.execute();
		System.out.println("读取配置");
		if (!StringUtility.stringHasValue(configfile)) {
            throw new BuildException(Messages.getString("RuntimeError.0")); //$NON-NLS-1$
        }
		File configurationFile = new File(configfile);
        if (!configurationFile.exists()) {
            throw new BuildException(Messages.getString("RuntimeError.1", configfile)); //$NON-NLS-1$
        }
        Generator generator = new Generator();
        try {
			generator.getAllTableInfo(configfile);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        try {
        	Map<String, String> map = XmlReader.readProjectInfo(configfile);
        	if(StringUtils.isNotBlank((String)map.get("projectType"))){
        		String prjType = (String)map.get("projectType");
//        		if(prjType.equals("web")){
//        			generator.genController();
//        		}else if(prjType.equals("server")){
//        			generator.genDomain();
//        			generator.genMapper();
//        			generator.genDao();
//        			generator.genService();
//        			generator.genServiceProvider();
//        			generator.genList();
//        			generator.genList4qjPage();
////        			generator.genFulltextService();
//        		}else{
//        			generator.genController();
//        			generator.genDomain();
//        			generator.genMapper();
//        			generator.genDao();
//        			generator.genService();
//        			generator.genServiceProvider();
//        			generator.genList();
//        			generator.genList4qjPage();
////        			generator.genFulltextService();
//        		}
        		
        		generator.genDomain();
    			generator.genMapper();
    			generator.genDao();
    			generator.genService();
//    			generator.genServiceProvider();
//    			generator.genList();
//    			generator.genList4qjPage();
    			generator.genController();
        	}
			
			
		} catch (IOException  e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public String getConfigfile() {
		return configfile;
	}

	public void setConfigfile(String configfile) {
		this.configfile = configfile;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

	public static void main(String[] args){
		GeneratorAntTask task = new GeneratorAntTask();
		task.execute();
	}
}
