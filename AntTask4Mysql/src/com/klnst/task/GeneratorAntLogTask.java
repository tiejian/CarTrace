package com.klnst.task;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.klnst.generator.GeneratorLog;
import com.klnst.util.StringUtility;
import com.klnst.util.messages.Messages;

import freemarker.template.TemplateException;

public class GeneratorAntLogTask extends Task {
	
	private String configfile;
	private boolean verbose;
	private boolean overwrite;
	
	@Override
	public void execute() throws BuildException {
		// TODO Auto-generated method stub
		super.execute();
		System.out.println("读取配置");
		if (!StringUtility.stringHasValue(configfile)) {
            throw new BuildException(Messages.getString("RuntimeError.0")); //$NON-NLS-1$
        }
		File configurationFile = new File(configfile);
        if (!configurationFile.exists()) {
            throw new BuildException(Messages.getString("RuntimeError.1", configfile)); //$NON-NLS-1$
        }
        GeneratorLog generator = new GeneratorLog();
        try {
			generator.getAllTableInfo(configfile);
		} catch (SQLException e) {
			e.printStackTrace();
		}
        try {
			generator.genDomain();
			generator.genMapper();
			generator.genDao();
			generator.genService();
			generator.genController();
			generator.genList();
		} catch (IOException  e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public String getConfigfile() {
		return configfile;
	}

	public void setConfigfile(String configfile) {
		this.configfile = configfile;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}

	public boolean isOverwrite() {
		return overwrite;
	}

	public void setOverwrite(boolean overwrite) {
		this.overwrite = overwrite;
	}

	public static void main(String[] args){
		GeneratorAntLogTask task = new GeneratorAntLogTask();
		task.execute();
	}
}
