package com.klnst.database;

public class TableColumn {
	
	private String fieldName;
	
	private String cluComment;
	
	private String dataType;
	
	private String characterMaximumLength;
	
	private String characterOctetLength;
	
	private String cluType;
	
	private String javaType;
	
	private String javaField;
	
	private String jdbcType;

	public String getCluComment() {
		return cluComment;
	}

	public void setCluComment(String cluComment) {
		this.cluComment = cluComment;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getCharacterMaximumLength() {
		return characterMaximumLength;
	}

	public void setCharacterMaximumLength(String characterMaximumLength) {
		this.characterMaximumLength = characterMaximumLength;
	}

	public String getCharacterOctetLength() {
		return characterOctetLength;
	}

	public void setCharacterOctetLength(String characterOctetLength) {
		this.characterOctetLength = characterOctetLength;
	}

	public String getCluType() {
		return cluType;
	}

	public void setCluType(String cluType) {
		this.cluType = cluType;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getJavaField() {
		return javaField;
	}

	public void setJavaField(String javaField) {
		this.javaField = javaField;
	}

	public String getJdbcType() {
		return jdbcType;
	}

	public void setJdbcType(String jdbcType) {
		this.jdbcType = jdbcType;
	}
	
	

}
