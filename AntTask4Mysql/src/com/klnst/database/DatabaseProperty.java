package com.klnst.database;

public class DatabaseProperty {
	private String dbSign;
	private String sid;
	private String host;
	private String port;
	private String userName;
	private String password;
	public String getSid() {
		return sid;
	}
	public void setSid(String sid) {
		this.sid = sid;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDbSign() {
		return dbSign;
	}
	public void setDbSign(String dbSign) {
		this.dbSign = dbSign;
	}
}
