package com.klnst.util;

import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;

public class TestXml {
	
	

	public static void main(String[] args) {
		String filePath="D:/WORKSPACE_JEE/AntTask/generatorConfiguration.xml";
		Document document = XmlUtils.getDocument(filePath);
		Element root = XmlUtils.getRootNode(document);
		List<Element> ele = XmlUtils.getChildElements(root,"jdbcConnection");
		for(Element e: ele){
			System.out.println(e.getName());
			Map<String, String> map =  XmlUtils.getNodeAttrMap(e);
			System.out.println(map);
		}
	}

}
