package com.klnst.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.klnst.database.TableColumn;
import com.klnst.entity.TableInfo;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
/**
 * 
 * @ClassName: FreemakerUtil 
 * @Description: TODO(这里用一句话描述这个类的作用) 
 * @author ilaotou@qq.com
 * @date 2014年9月18日 下午1:54:34 
 *
 */
public class FreemakerUtil {
	public static String path="";
	private static Configuration cfg =null;
	
	public static void init() throws IOException {
		path = FreemakerUtil.class.getResource("/").getPath()+"/ftl/";
		cfg = new Configuration();
		cfg.setLocale(java.util.Locale.SIMPLIFIED_CHINESE);
		cfg.setDirectoryForTemplateLoading(new File(path));
		cfg.setCacheStorage(new freemarker.cache.MruCacheStorage(20,250)); 
		cfg.setObjectWrapper(new DefaultObjectWrapper()); 
	}
	public static boolean genDomain(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetPackage(), ".", "/");
		File file = new File(table.getTargetPath() +"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+".java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("domain.ftl");
		String daoPkg = table.getTargetDaoPackage();
		if(StringUtils.isBlank(daoPkg)){
			daoPkg = StringUtils.substringBeforeLast(table.getTargetPackage(), ".");
		}
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetPackage());
		root.put("daoPkg", daoPkg);
		root.put("tableName", table.getTableName());
		root.put("tableComment", table.getTableComment());
		root.put("clzName", table.getDomainObjectName());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genMapper(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Mapper.xml";
		System.out.println("mapper xml文件 : " + fileName);
		Template temp = cfg.getTemplate("mapper.ftl");
		String daoPkg = table.getTargetDaoPackage();
		if(StringUtils.isBlank(daoPkg)){
			daoPkg = StringUtils.substringBeforeLast(table.getTargetPackage(), ".");
		}
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetPackage());
		root.put("daoPkg", daoPkg);
		root.put("tableName", table.getTableName());
		if(StringUtils.isNotBlank(table.getTargetKeyword())){
			String[] arr = StringUtils.split(table.getTargetKeyword(),",");
			root.put("keyfields", arr);
		}
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genDao(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetDaoPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Mapper.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("dao.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetDaoPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genService(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetServicePackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Service.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("service.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetServicePackage());
		root.put("pkgProvider", table.getTargetServiceProviderPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public static boolean genServiceProvider(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetServiceProviderPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"ServiceProvider.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("serviceProvider.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetServiceProviderPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public static boolean genFulltextService(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetFullTextServicePackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"FulltextService.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("servicefulltext.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetFullTextServicePackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		if(StringUtils.isNotBlank(table.getTargetKeyword())){
			String[] arr = StringUtils.split(table.getTargetKeyword(),",");
			root.put("keyfields", arr);
		}
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	
	public static boolean genController(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetControllerPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+Tools.transFirstUpper(table.getDomainObjectName())+"Controller.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("controller.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("serviceProviderPkg", table.getTargetServiceProviderPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genListPage(TableInfo table) throws IOException, TemplateException{
		File file = new File(table.getTargetPageFolder());
		if(!file.exists()) file.mkdirs();
		String fileName =table.getTargetPageFolder() +"/"+StringUtils.lowerCase(table.getTableName())+"_list.jsp";
		System.out.println("页面 : " + fileName);
		Template temp = cfg.getTemplate("list.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genList4qjPage(TableInfo table) throws IOException, TemplateException{
		File file = new File(table.getTargetPageFolder());
		if(!file.exists()) file.mkdirs();
		String fileName =table.getTargetPageFolder() +"/"+StringUtils.lowerCase(table.getTableName())+"_qj_list.jsp";
		System.out.println("页面 : " + fileName);
		Template temp = cfg.getTemplate("list4qj.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genConditionPage(TableInfo table) throws IOException, TemplateException{
		File file = new File(table.getTargetPageFolder());
		if(!file.exists()) file.mkdirs();
		String fileName =table.getTargetPageFolder() +"/" +StringUtils.lowerCase(table.getTableName())+"_list_condition.jsp";
		System.out.println("页面 : " + fileName);
		Template temp = cfg.getTemplate("list_condition.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	private static String getOrgPrefix(String tableName){
		String[] arr = StringUtils.split(tableName,"_");
		return arr[1];
	}
}
