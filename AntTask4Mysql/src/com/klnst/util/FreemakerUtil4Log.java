package com.klnst.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.klnst.entity.TableInfo;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class FreemakerUtil4Log {
	public static String path="";
	private static Configuration cfg =null;
	
	public static void init() throws IOException {
		path = FreemakerUtil4Log.class.getResource("/").getPath()+"/ftl/";
		cfg = new Configuration();
		cfg.setLocale(java.util.Locale.SIMPLIFIED_CHINESE);
		cfg.setDirectoryForTemplateLoading(new File(path));
		cfg.setCacheStorage(new freemarker.cache.MruCacheStorage(20,250)); 
		cfg.setObjectWrapper(new DefaultObjectWrapper()); 
	}
	public static boolean genDomain(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetPackage(), ".", "/");
		File file = new File(table.getTargetPath() +"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+".java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("domain.ftl");
		String daoPkg = table.getTargetDaoPackage();
		if(StringUtils.isBlank(daoPkg)){
			daoPkg = StringUtils.substringBeforeLast(table.getTargetPackage(), ".");
		}
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetPackage());
		root.put("daoPkg", daoPkg);
		root.put("tableName", table.getTableName());
		root.put("tableComment", table.getTableComment());
		root.put("clzName", table.getDomainObjectName());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genMapper(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Mapper.xml";
		System.out.println("mapper xml文件 : " + fileName);
		Template temp = cfg.getTemplate("mapper4log.ftl");
		String daoPkg = table.getTargetDaoPackage();
		if(StringUtils.isBlank(daoPkg)){
			daoPkg = StringUtils.substringBeforeLast(table.getTargetPackage(), ".");
		}
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetPackage());
		root.put("daoPkg", daoPkg);
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genDao(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetDaoPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Mapper.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("dao4log.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetDaoPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genService(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetServicePackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+table.getDomainObjectName()+"Service.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("service4log.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetServicePackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("model", table.getModel());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genController(TableInfo table) throws IOException, TemplateException{
		String clzPath = StringUtils.replace(table.getTargetControllerPackage(), ".", "/");
		File file = new File(table.getTargetPath()+"/" +clzPath);
		if(!file.exists()) file.mkdirs();
		
		System.out.println("clzPath : " + clzPath);
		String fileName =table.getTargetPath() +"/" +clzPath+ "/"+Tools.transFirstUpper(table.getTableName())+"_Controller.java";
		System.out.println("JAVA文件 : " + fileName);
		Template temp = cfg.getTemplate("controller4log.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genListPage(TableInfo table) throws IOException, TemplateException{
		File file = new File(table.getTargetPageFolder());
		if(!file.exists()) file.mkdirs();
		String fileName =table.getTargetPageFolder() +"/"+StringUtils.lowerCase(table.getTableName())+"_list.jsp";
		System.out.println("页面 : " + fileName);
		Template temp = cfg.getTemplate("list4log.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	public static boolean genConditionPage(TableInfo table) throws IOException, TemplateException{
		File file = new File(table.getTargetPageFolder());
		if(!file.exists()) file.mkdirs();
		String fileName =table.getTargetPageFolder() +"/" +StringUtils.lowerCase(table.getTableName())+"_list_condition.jsp";
		System.out.println("页面 : " + fileName);
		Template temp = cfg.getTemplate("list_condition.ftl");
		Map root = new HashMap(); 
		root.put("pkg", table.getTargetControllerPackage());
		root.put("domainPkg", table.getTargetPackage());
		root.put("daoPkg", table.getTargetDaoPackage());
		root.put("servicePkg", table.getTargetServicePackage());
		root.put("tableName", table.getTableName());
		root.put("clzName", table.getDomainObjectName());
		root.put("tableComment", table.getTableComment());
		root.put("key",table.getKey());
		root.put("keyProperty",Tools.transFirstLower(Tools.transDb2JavaName(table.getKey())));
		root.put("tPropertys", table.getTableColumns());
		root.put("prefix", getPrefix(table.getTableName()));
		root.put("org", getOrgPrefix(table.getTableName()));
		/* 将模板和数据模型合并 */ 
		try {
			Writer out = new OutputStreamWriter(new FileOutputStream(fileName));
			temp.process(root, out);
			out.flush();
			out.close();
			return true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	private static String getPrefix(String tableName){
		String[] arr = StringUtils.split(tableName,"_");
		return arr[1];
	}
	private static String getOrgPrefix(String tableName){
		String[] arr = StringUtils.split(tableName,"_");
		return arr[2];
	}
}
