package com.klnst.util;

import org.apache.commons.lang.StringUtils;

public class Tools {
	public static String transDbField2JavaField(String dbField){
		String[] section = dbField.split("_");
		StringBuilder sb = new StringBuilder(64);
		for(String string : section){
			sb.append(trans(string));
		}
		return transFirstLower(sb.toString());
	}
	public static String transDb2JavaName(String dbField){
		String[] section = dbField.split("_");
		StringBuilder sb = new StringBuilder(64);
		for(String string : section){
			sb.append(trans(string));
		}
		return sb.toString();
	}
	public static String trans(String str){
		return str = str.substring(0,1).toUpperCase()+ str.substring(1).toLowerCase(); 
	}
	public static String transFirstUpper(String str){
		return str = str.substring(0,1).toUpperCase()+ str.substring(1); 
	}
	public static String transFirstLower(String str){
		return str = str.substring(0,1).toLowerCase()+ str.substring(1); 
	}
	public static String getShortVoName(String voName){
		return StringUtils.substring(voName, 0, voName.length()-2);
	}
	public static void main(String[] args){
		System.out.println(transDb2JavaName("cj_aa"));
	}
}
