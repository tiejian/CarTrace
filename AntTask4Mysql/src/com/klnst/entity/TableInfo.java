package com.klnst.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.klnst.database.TableColumn;

/**
 * 
 * @ClassName: TableInfo 
 * @Description: 表信息
 * @author ilaotou@qq.com
 * @date 2014年9月17日 上午9:04:49 
 *
 */
public class TableInfo {

	private String tableName;
	private String tableComment;
	private String domainObjectName;
	private String targetPackage;
	private String targetPath;
	private String targetDaoPackage;
	private String targetServicePackage;
	private String targetServiceProviderPackage;
	private String targetType;//用于WEB还是SERVER
	
	public String getTargetType() {
		return targetType;
	}
	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}
	public String getTargetServiceProviderPackage() {
		return targetServiceProviderPackage;
	}
	public void setTargetServiceProviderPackage(String targetServiceProviderPackage) {
		this.targetServiceProviderPackage = targetServiceProviderPackage;
	}
	private String targetFullTextServicePackage;
	private String targetKeyword;
	private String targetControllerPackage;
	private String targetPageFolder;
	private String model;
	private String key;
	private List<TableColumn> tableColumns;
	private Map<String,String> columnEntity = new HashMap<String,String>();
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getDomainObjectName() {
		return domainObjectName;
	}
	public void setDomainObjectName(String domainObjectName) {
		this.domainObjectName = domainObjectName;
	}
	public String getTargetPackage() {
		return targetPackage;
	}
	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}
	public String getTargetPath() {
		return targetPath;
	}
	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}
	public Map<String, String> getColumnEntity() {
		return columnEntity;
	}
	public void setColumnEntity(Map<String, String> columnEntity) {
		this.columnEntity = columnEntity;
	}
	public List<TableColumn> getTableColumns() {
		return tableColumns;
	}
	public void setTableColumns(List<TableColumn> tableColumns) {
		this.tableColumns = tableColumns;
	}
	public String getTargetDaoPackage() {
		return targetDaoPackage;
	}
	public void setTargetDaoPackage(String targetDaoPackage) {
		this.targetDaoPackage = targetDaoPackage;
	}
	public String getTargetServicePackage() {
		return targetServicePackage;
	}
	public void setTargetServicePackage(String targetServicePackage) {
		this.targetServicePackage = targetServicePackage;
	}
	public String getTargetControllerPackage() {
		return targetControllerPackage;
	}
	public void setTargetControllerPackage(String targetControllerPackage) {
		this.targetControllerPackage = targetControllerPackage;
	}
	public String getTargetPageFolder() {
		return targetPageFolder;
	}
	public void setTargetPageFolder(String targetPageFolder) {
		this.targetPageFolder = targetPageFolder;
	}
	public String getTableComment() {
		return tableComment;
	}
	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getTargetFullTextServicePackage() {
		return targetFullTextServicePackage;
	}
	public void setTargetFullTextServicePackage(String targetFullTextServicePackage) {
		this.targetFullTextServicePackage = targetFullTextServicePackage;
	}
	public String getTargetKeyword() {
		return targetKeyword;
	}
	public void setTargetKeyword(String targetKeyword) {
		this.targetKeyword = targetKeyword;
	}
	
}
