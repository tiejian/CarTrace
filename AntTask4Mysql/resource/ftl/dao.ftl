package ${pkg};

import java.util.List;
import java.util.Map;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import ${domainPkg}.${clzName};
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.mybatis.MyBatisRepository;
/**
 * 
 * @ClassName: ${clzName}Mapper  
 * @Description: ${tableComment} 
 * @author 白展堂
 *
 */
@MyBatisRepository
public interface ${clzName}Mapper {
    /**
     * @Description 动态表名的分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<${clzName}>
     */
    List<${clzName}> paging4DynamicTblName(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 动态表名的分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount4DynamicTblName(Criteria criteria);
    /**
     * @Description 分页
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<${clzName}>
     */
    List<${clzName}> paging(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 用于全文检索的分页，需要指定查询字段
     * @param criteria 条件组合
     * @param pageBounds 分页对象
     * @return List<${clzName}>
     */
    List<${clzName}> paging4elastic(Criteria criteria,PageBounds pageBounds);
    /**
     * @Description 获取分页记录数
     * @param criteria 条件组合
     * @return 记录数
     */
    long pagingCount(Criteria criteria);
    /**
     * @Description 根据条件查询
     * @param parameters
     * @return List<${clzName}>
     */
    List<${clzName}> findBy(Map<String, Object> parameters);
    /**
     * @Description 根据条件查询并返回唯一值
     * @param parameters
     * @return ${clzName}
     */
    ${clzName} findUniqueBy(Map<String, Object> parameters);
    /**
     * @Description 插入记录
     * @param stUser
     * @return 返回插入影响的行数
     */
    int insert(${clzName} ${clzName?uncap_first});
    /**
     * @Description 更新记录
     * @param ${clzName}
     * @return 返回更新影响的行数
     */
    int update(${clzName} ${clzName?uncap_first});
    /**
     * @Description 根据主键删除
     * @param ${keyProperty}
     */
    void deleteById(String ${keyProperty});
    /**
     * @Description 批量删除
     * @param parameters 批量删除条件组合
     */
    void deleteBy(Map<String, Object> parameters);
}