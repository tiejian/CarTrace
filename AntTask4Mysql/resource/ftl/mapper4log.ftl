<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${daoPkg}.${clzName}Mapper" >
	<resultMap id="BaseResultMap" type="${pkg}.${clzName}" >
	<#list tPropertys as prop>
    <result column="${prop.fieldName}" property="${prop.javaField}" jdbcType="${prop.jdbcType}" />
    </#list>
  </resultMap>
  
   <!-- 单条记录跟踪时映射 -->
  <resultMap id="LogTraceResultMap" type="com.klnst.domain.SysQryLog" >
    <result column="user_idcard" property="userIdcard" jdbcType="VARCHAR" />
    <result column="operator_name" property="operator" jdbcType="VARCHAR" />
    <result column="org_code" property="orgCode" jdbcType="VARCHAR" />
    <result column="org_name" property="orgName" jdbcType="VARCHAR" />
    <result column="create_at" property="createAt" jdbcType="TIMESTAMP" />
  </resultMap>
  
  <!-- 列属性列表 -->
  <sql id="Base_Column_List" >
     <#list tPropertys as prop>
     ${prop.fieldName}<#if prop_has_next>,</#if>
     </#list>
  </sql>
  
  <!-- 自定义条件的where列表 -->
  <sql id="Specification_Where_Clause" >
    <where >
        <trim prefix="(" suffix=")" prefixOverrides="and" >
            <foreach collection="criterions" item="criterion" >
              <choose >
                <when test="criterion.noValue and criterion.condition!='__user__'" >
                  and ${r"${criterion.condition}"}
                </when>
                <when test="criterion.singleValue and criterion.condition!='__user__'" >
                  and ${r"${criterion.condition}"} ${r"#{criterion.value}"}
                </when>
                <when test="criterion.betweenValue and criterion.condition!='__user__'" >
                  and ${r"${criterion.condition}"} ${r"#{criterion.value}"} and ${r"#{criterion.secondValue}"}
                </when>
                <when test="criterion.listValue and criterion.condition!='__user__'" >
                  and ${r"${criterion.condition}"}
                  <foreach collection="criterion.value" item="listItem" open="(" close=")" separator="," >
                    ${r"#{listItem}"}
                  </foreach>
                </when>
              </choose>
            </foreach>
         </trim>
    </where>
  </sql>
  
  <!-- 根据属性值查询的where条件 -->
  <sql id="Custom_Where_Clause" >
    <where >
        <trim prefix="" suffix="" prefixOverrides="and" >
            <#list tPropertys as prop>
		      <if test="${prop.javaField} != null" >
		        and ${prop.fieldName}=${r"#{"}${prop.javaField},jdbcType=${prop.jdbcType}${r"}"}
		      </if>
		    </#list>
        </trim>
    </where>
  </sql>
  
  <!-- 分页查询-动态表名 -->
  <select id="paging4DynamicTblName" resultMap="BaseResultMap" parameterType="com.klnst.core.persistence.Criteria">
    select 
    <include refid="Base_Column_List" />
    from ${r"${tblName}"}
    <include refid="Specification_Where_Clause" />
  </select>
   <!-- -动态表名-分页查询的记录数，当数据量比较大时通过paging方法返回记录数性能很低，需要单独计算记录数-->
  <select id="pagingCount4DynamicTblName" resultType="long" parameterType="com.klnst.core.persistence.Criteria" >
    select count(1) 
    from ${r"${tblName}"}
    <include refid="Specification_Where_Clause" />
  </select>
  
  <!-- 分页查询 -->
  <select id="paging" resultMap="BaseResultMap" parameterType="com.klnst.core.persistence.Criteria">
    select 
    <include refid="Base_Column_List" />
    from ${tableName}
    <include refid="Specification_Where_Clause" />
  </select>
  
  <!-- 分页查询的记录数，当数据量比较大时通过paging方法返回记录数性能很低，需要单独计算记录数-->
  <select id="pagingCount" resultType="long" parameterType="com.klnst.core.persistence.Criteria" >
    select count(1) 
    from ${tableName}
    <include refid="Specification_Where_Clause" />
  </select>
  
   <!-- 根据属性值查询返回列表 -->
   <select id="findBy" resultMap="BaseResultMap" parameterType="map">
    select 
    <include refid="Base_Column_List" />
    from ${tableName}
    <include refid="Custom_Where_Clause" />
  </select>
  
    <!-- 根据属性值查询返回唯一值 -->
   <select id="findUniqueBy" resultType="${pkg}.${clzName}" parameterType="map">
    select 
    <include refid="Base_Column_List" />
    from ${tableName}
    <include refid="Custom_Where_Clause" />
  </select>
  
  <!-- Insert操作 -->
  <insert id="insert" parameterType="${pkg}.${clzName}" >
    <selectKey resultType="java.lang.String" keyProperty="${keyProperty}" order="BEFORE" >
     SELECT UUID()
    </selectKey>
    insert into ${tableName}
     <trim prefix="(" suffix=")" suffixOverrides="," >
      <#list tPropertys as prop>
      <if test="${prop.javaField} != null" >
        ${prop.fieldName},
      </if>
      </#list>
    </trim>
    <trim prefix="values (" suffix=")" suffixOverrides="," >
    <#list tPropertys as prop>
      <if test="${prop.javaField} != null" >
        ${r"#{"}${prop.javaField},jdbcType=${prop.jdbcType}${r"}"},
      </if>
    </#list>
    </trim>
  </insert>
  
  <!-- update操作 -->
   <update id="updateById" parameterType="${pkg}.${clzName}" >
    update ${tableName}
    <set >
      <#list tPropertys as prop>
      <if test="${prop.javaField} != null" >
        ${prop.fieldName} = ${r"#{"}${prop.javaField},jdbcType=${prop.jdbcType}${r"}"},
      </if>
      </#list>
    </set>
    where ${key} = ${r"#{"}${keyProperty},jdbcType=VARCHAR}
  </update>
  
  <!-- 根据主键删除-->
  <delete id="deleteById" parameterType="string" >
    delete from ${tableName} where ${key}=${r"#{"}${keyProperty}${r"}"}
  </delete>
  
  <!--单条记录跟踪  -->
  <select id="traceByLogId" resultMap="LogTraceResultMap" parameterType="string" >
  	SELECT 
  	   d.`org_name`,
  	   c.`operator_name`,
  	   b.`user_idcard`,
  	   b.`create_at`
  	FROM 
  	   ${tableName} a ,
  	   sys_qry_log b,
  	   sys_operator c,
  	   sys_organization d 
  	WHERE a.sys_log_id = b.`id` 
  	       AND b.`user_idcard` = c.`certificate_num` 
  	       AND c.`org_code`=d.`org_code`
  	       AND a.`id`=${r"#{logId,jdbcType=VARCHAR}"}
    ORDER BY b.create_at DESC
  </select>
</mapper>