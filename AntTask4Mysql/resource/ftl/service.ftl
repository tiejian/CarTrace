package ${pkg};



import java.text.ParseException;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import ${daoPkg}.${clzName}Mapper;
import ${domainPkg}.${clzName};
import com.jacqui.core.persistence.Criteria;
import com.jacqui.core.persistence.Criterion;
import com.jacqui.core.persistence.DynamicSpecifications;
import com.jacqui.core.persistence.SearchFilter;
import com.jacqui.core.form.ConditionEntity;
import com.jacqui.core.form.PageResult;
import com.jacqui.service.BaseService;

import com.google.common.collect.Maps;

/**
 * 
 * @ClassName: ${clzName}Service   
 * @Description: ${tableComment} 
 * @author 白展堂
 
 */
@Service
public class ${clzName}Service extends BaseService{

   private static Logger logger = LoggerFactory.getLogger(${clzName}Service.class);
	
	@Autowired
	public ${clzName}Mapper ${clzName?uncap_first}Mapper;
	
	
	/**
	 * 
	 * @Title: paging4DynamicTblName 
	 * @Description: 表名为动态参数的分页查询
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<${clzName}> paging4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities,Integer page,Integer limit) <#if model?exists && model="business">throws ParseException</#if>{
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		Criterion criterion_user = new Criterion("__user__",user.getCertificateNum());
		Criterion criterion_xzq = new Criterion("xzq like ",user.getDistrictCode()+"%");
		criterions.add(criterion);
		criterions.add(criterion_user);
		criterions.add(criterion_xzq);
		</#if>
		Criteria criteria = new Criteria(criterions,tblName);
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return ${clzName?uncap_first}Mapper.paging4DynamicTblName(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount4DynamicTblName 
	 * @Description: <p>表名为动态参数的总记录数查询，当数据量大的时候原有分页带有的总记录数方法会很消耗性能</p>
	 * <p>可通过此方法单独计算总记录数</p>
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities) <#if model?exists && model="business">throws ParseException</#if>{
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		Criterion criterion_user = new Criterion("__user__",user.getCertificateNum());
		Criterion criterion_xzq = new Criterion("xzq like ",user.getDistrictCode()+"%");
		criterions.add(criterion);
		criterions.add(criterion_user);
		criterions.add(criterion_xzq);
		</#if>
		Criteria criteria = new Criteria(criterions,tblName);
		return ${clzName?uncap_first}Mapper.pagingCount4DynamicTblName(criteria);
	}
	/**
	 * 
	 * @Title: paging 
	 * @Description: 表名固定的分页方法 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<${clzName}> paging(List<SearchFilter> filters,Integer page,Integer limit) <#if model?exists && model="business">throws ParseException</#if>{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions,"${tableName}");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return ${clzName?uncap_first}Mapper.paging(criteria,pageBounds);
	}
	/**
	 * @Title: pagingExt 
	 * @Description: 表名固定的分页方法，直接返回总记录数, 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型
	 * @throws ParseException
	 */
	public PageResult pagingWithTotal(List<SearchFilter> filters,Integer page,Integer limit){
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("create_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions,"${tableName}");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		List<${clzName}> list = ${clzName?uncap_first}Mapper.paging(criteria,pageBounds);
		PageList<${clzName}> pageList = (PageList)list;
		PageResult result = new PageResult();
		result.setList(pageList);
		result.setTotalCount(pageList.getPaginator().getTotalCount());
		result.setTotalPage(pageList.getPaginator().getTotalPages());
		return result;
	}
	/**
	 * 
	 * @Title: paging4elastic 
	 * @Description: 全文检索的分页方法
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<${clzName}> paging4elastic(List<SearchFilter> filters,Integer page,Integer limit) throws ParseException{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions,"${tableName}");
		PageBounds pageBounds = new PageBounds(page, limit,true);
		return ${clzName?uncap_first}Mapper.paging4elastic(criteria,pageBounds);
	}
	/**
	 * 
	 * @Title: pagingCount 
	 * @Description: 表名固定的记录总数方法
	 * @param filters
	 * @return long    返回类型 
	 * @throws ParseException
	 *
	 */
	public long pagingCount(List<SearchFilter> filters) <#if model?exists && model="business">throws ParseException</#if>{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions,"${tableName}");
		return ${clzName?uncap_first}Mapper.pagingCount(criteria);
	}
	/**
	 * 
	 * @Title: findBy 
	 * @Description: 根据属性查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<${clzName}> findBy(Map<String, Object> parameters) <#if model?exists && model="business">throws ParseException</#if>{
	    <#if model?exists && model="business">
	    parameters.put("cj_at",TimeUtils.getDateNow());
	    </#if>
		return ${clzName?uncap_first}Mapper.findBy(parameters);
	}
    /**
	 * 
	 * @Title: findBy 
	 * @Description: 根据主键查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
    public ${clzName} findUniqueById(String id){
        HttpServletRequest request = getHttpServletRequest();
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("id",id);
        //parameters.put("__user__",user.getCertificateNum());
        //parameters.put("__tblName__","${tableName}");
        //parameters.put("xzq",user.getDistrictCode());
		return ${clzName?uncap_first}Mapper.findUniqueBy(parameters);
	}
	/**
     * @Title: findUniqueById 
     * @Description: 根据属性查询
     * @param id
     * @return DsjSkrkxx    返回类型 
     */
    public ${clzName} findUniqueBy(Map<String, Object> parameters){
		return ${clzName?uncap_first}Mapper.findUniqueBy(parameters);
	}
    /**
     * @Title: insert 
     * @Description: 插入操作
     * @param dsjSkrkxx
     * @return String    返回类型 
     */
    public String insert(${clzName} ${clzName?uncap_first}){
    	${clzName?uncap_first}Mapper.insert(${clzName?uncap_first});
		return  String.valueOf(${clzName?uncap_first}.get${keyProperty?cap_first}());
	}
    /**
     * @Title: update 
     * @Description: 更新操作 
     * @param dsjSkrkxx
     */
    public void update(${clzName} ${clzName?uncap_first}){
		${clzName?uncap_first}Mapper.update(${clzName?uncap_first});
	}
    /**
     * @Title: deleteById 
     * @Description: 根据主键删除
     * @param id
     */
    public void deleteById(String ${keyProperty}){
		${clzName?uncap_first}Mapper.deleteById(${keyProperty});
	}
    /**
     * @Title: deleteBy
     * @Description: 根据属性删除
     * @param id
     */
    public void deleteBy(Map<String, Object> parameters){
		${clzName?uncap_first}Mapper.deleteBy(parameters);
	}
	/**
	 * @Title: getHttpServletRequest
     * @Description: 可以在service层获取request,仅限地不使用dubbo的情况。使用dubbo此方法无用
     * @return void    返回类型 
	 */
	private static HttpServletRequest getHttpServletRequest(){
		ServletRequestAttributes sra= (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(sra==null){
			return null;
		}else{
			return sra.getRequest();
		}
	}
}
