package ${pkg};



import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.wencong.core.form.ConditionEntity;
import com.wencong.core.persistence.SearchFilter;
import com.wencong.core.form.PageResult;

import ${domainPkg}.${clzName};


/**
 * 
 * <p>${tableComment}</p>
 * @ClassName: ${clzName}Service   
 * @author 白展堂
 * 
 */
public interface ${clzName}ServiceProvider {

	
	
	/**
	 * 
	 * @Title: paging4DynamicTblName 
	 * @Description: 表名为动态参数的分页查询
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 *
	 */
	public List<${clzName}> paging4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities,Integer page,Integer limit) <#if model?exists && model="business">throws ParseException</#if>;
	/**
	 * @Description: <p>表名为动态参数的总记录数查询，当数据量大的时候原有分页带有的总记录数方法会很消耗性能，可通过此方法单独计算总记录数<</p>
	 * @Title: pagingCount4DynamicTblName 
	 * @param user
	 * @param tblName
	 * @param conditionEntities
	 * @return long    返回类型 
	 * @throws ParseException
	 */
	public long pagingCount4DynamicTblName(String tblName,List<ConditionEntity> conditionEntities) <#if model?exists && model="business">throws ParseException</#if>;
	/**
	 * @Title: paging 
	 * @Description: 表名固定的分页方法 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 */
	public List<${clzName}> paging(List<SearchFilter> filters,Integer page,Integer limit) <#if model?exists && model="business">throws ParseException</#if>;
	/**
	 * @Title: pagingExt 
	 * @Description: 表名固定的分页方法，直接返回总记录数, 
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 */
	public PageResult pagingWithTotal(List<SearchFilter> filters,Integer page,Integer limit)<#if model?exists && model="business">throws ParseException</#if>;
	/**
	 * 
	 * @Title: paging4elastic 
	 * @Description: 全文检索的分页方法
	 * @param filters
	 * @param pageBounds
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 */
	public List<${clzName}> paging4elastic(List<SearchFilter> filters,Integer page,Integer limit) throws ParseException;
	/**
	 * @Title: pagingCount 
	 * @Description: 表名固定的记录总数方法
	 * @param filters
	 * @throws ParseException
	 * @return long    返回类型 
	 * @throws ParseException
	 */
	public long pagingCount(List<SearchFilter> filters) <#if model?exists && model="business">throws ParseException</#if>;
	/**
	 * @Title: findBy 
	 * @Description: 根据属性查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 */
	public List<${clzName}> findBy(Map<String, Object> parameters) <#if model?exists && model="business">throws ParseException</#if>;
    /**
	 * @Title: findBy 
	 * @Description: 根据主键查询
	 * @param parameters
	 * @return List<DsjSkrkxx>    返回类型 
	 * @throws ParseException
	 */
    public ${clzName} findUniqueById(String id);
	/**
     * @Title: findUniqueById 
     * @Description: 根据属性查询
     * @param id
     * @return DsjSkrkxx    返回类型 
     */
    public ${clzName} findUniqueBy(Map<String, Object> parameters);
    /**
     * @Title: insert 
     * @Description: 插入操作
     * @param dsjSkrkxx
     * @return String    返回类型 
     */
    public String insert(${clzName} ${clzName?uncap_first});
    /**
     * @Title: update 
     * @Description: 更新操作 
     * @param dsjSkrkxx
     * @return void    返回类型 
     */
    public void update(${clzName} ${clzName?uncap_first});
    /**
     * @Title: deleteById 
     * @Description: 根据主键删除
     * @param id
     * @return void    返回类型 
     */
    public void deleteById(String ${keyProperty});
    /**
     * @Title: deleteBy 
     * @Description: 根据主键删除
     * @param id
     * @return void    返回类型 
     */
    public void deleteBy(Map<String, Object> parameters);
	
}
