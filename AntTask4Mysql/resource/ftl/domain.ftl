package ${pkg};

import java.io.Serializable;

import java.util.Map;
import java.util.Date;
import com.google.common.collect.Maps;
/**
 * 
 * @ClassName: ${clzName}  
 * @Description: ${tableComment} 
 * @author 白展堂
 * 
 */
public class ${clzName} implements Serializable{

    private static final long serialVersionUID = 1L;

  <#list tPropertys as prop>
    private ${prop.javaType} ${prop.javaField}; //<#if (prop.cluComment)??>${prop.cluComment}</#if>
    
    <#if (prop.codeType?has_content)>
    private String ${prop.javaField}Name;//<#if (prop.cluComment)??>${prop.cluComment}-代码对应的名称</#if>
    </#if>
    <#if prop.javaType?upper_case="DATE">
    private String ${prop.javaField}Str;//<#if (prop.cluComment)??>${prop.cluComment}-对应的字符型日期</#if>
    </#if>
  </#list>
    private Map<String,String> labels = Maps.newHashMap();
  
  <#list tPropertys as prop>
    public ${prop.javaType} get${prop.javaField?cap_first}(){
    	return ${prop.javaField};
    }
    public void set${prop.javaField?cap_first}(${prop.javaType} ${prop.javaField}){
        this.${prop.javaField} = ${prop.javaField};
    }
    <#if (prop.codeType?has_content)>
    public String get${prop.javaField?cap_first}Name(){
        return ${prop.javaField}Name;
    }
    public void set${prop.javaField?cap_first}Name(String ${prop.javaField}Name){
        this.${prop.javaField}Name = ${prop.javaField}Name;
    }	
    </#if>
    <#if prop.javaType?upper_case="DATE">
    public String get${prop.javaField?cap_first}Str(){
    	return ${prop.javaField}Str;
    }
    public void set${prop.javaField?cap_first}Str(String ${prop.javaField}Str){
        this.${prop.javaField}Str = ${prop.javaField}Str;
    }	
    </#if>
  </#list>
	public Map<String, String> getLabels() {
		<#list tPropertys as prop>
		labels.put("${prop.javaField}", "<#if (prop.cluComment)??>${prop.cluComment}<#else>${prop.javaField}</#if>");
	    </#list>
		return labels;
	}
	public void setLabels(Map<String, String> labels) {
		this.labels = labels;
	}
}