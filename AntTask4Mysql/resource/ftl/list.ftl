<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${r"${pageContext.request.contextPath}"}" />
<script>
    var $grid;
	$(function () {
	    /*
		$(document).bind("contextmenu",function(){return false;});   
		$(document).bind("selectstart",function(){return false;}); 
		$(document).keydown(function(){return key(arguments[0])});   
        */
	    $('#dialog4search').jqm({toTop:true});
		$grid = $("#maingrid4").css({"border":"0","float":"left"}).ligerGrid({
				columns: [
				    <#list tPropertys as prop>
				    <#if prop.javaField != 'id' && prop.javaField != 'cjAt'&& prop.javaField != 'mmonth'&& prop.javaField != 'qquarter'&& prop.javaField != 'yyear'&& prop.javaField != 'xzq'&&prop.cluComment?index_of("法人")==-1&&prop.cluComment?index_of("身份")==-1&&prop.cluComment?index_of("电话")==-1&&prop.cluComment?index_of("手机")==-1&&prop.cluComment?index_of("法定")==-1&&prop.cluComment?index_of("权利人代码")==-1&&prop.cluComment?index_of("区域标志")==-1>
				    { display: '${prop.cluComment}', name: '${prop.javaField}', width: 200 }<#if prop_has_next>,</#if>
				    </#if>
    				</#list>
					], 
				url: '${r"${ctx}"}/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/${tableName}/search?_m=query&_tb=${r"${_tb}"}&datarangetype=${r"${datarangetype}"}&datarange=${r"${datarange}"}',
				delayLoad: false,
				pageSize: 50,
				rownumbers:true,
				root :'Rows',                          
				record:'Total',                        
				pageParmName :'page',               
				pagesizeParmName:'pagesize',
				allowAdjustColWidth:true,
				onSuccess:function(data){
					if (typeof data.errcode != "undefined")
					{
					   window.location.href="NoLoginOrSessionOut.jsp";
					} 
				},
				parms:[
					{ name: "_con", value: $("#_con").val() }
				],
				onError:function(){
					ajaxError();
				},
				onDblClickRow: function(row){
					browse();
				},
				width: '100%',
				height:'100%',
				heightDiff:-35
		});
            $(".grid-toolbar").ligerToolBar({
			background: false,
			items: [{type: 'button',text: '导出',icon: 'down',disable: false,click: function () {f_excel()}},{type: 'button',text: '查询',icon: 'search',disable: false,click: function () {f_search()}},{type: 'button',text: '跟踪',icon: 'communication',disable: false,click: function () {f_log()}}]
		});

	    $("#gobackpage").bind("click",function(){
			var flag="${r"${flag}"}";
			if(flag==0){
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/bm/tables?code=${r"${org.orgCode}"}");
			}
			if(flag==1){
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/main/redirect/timeline/index");
			}
			if(flag==2){//个人-本月数据
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/private/current/month/index");
			}
			if(flag==3){//元数据查询
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/private/metadata/search?_m=init");
			}
			if(flag==4){//数据查询
				$("#main").html("");
				$("#main").load("${r"${ctx}"}/private/custom/search?_m=init");
			}
	    });

	});
	function key(e){  
		var keynum;  
		if(window.event) // IE  
		  {  
			keynum = e.keyCode;  
		  }  
		else if(e.which) // Netscape/Firefox/Opera  
		  {  
			keynum = e.which;  
		  }  
		if(keynum == 17){ alert("禁止复制内容！");return false;}  
	}  
</script>
<div class="grid-toolbar"></div>
<div id="maingrid4" style="margin:0; padding:0"></div>
<div id="dialog4search" class="jqmWindow">
</div>
<input type=hidden id="_con" name="_con" value=""/>
<script>
    var browseWin;
	
	var browseWin;
    function f_search1(){
		browseWin=$.ligerDialog.open({ title:"查询条件设置", url: '${r"${ctx}"}/dsj/dsj_skrkxx/search?_m=condition_init&_tb=${r"${_tb}"}&r='+Math.random(), width: 800,height:400, showMax: true, showToggle: false, isResize: true, isHidden: false,slide: false,allowClose: false});
	}
	function browseClose(){
		browseWin.hide();
	}
		
	function f_search(){
		$('#dialog4search').html("");
		$('#dialog4search').load("${r"${ctx}"}/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/${tableName}/search?_m=condition_init&_tb=${r"${_tb}"}");
		$('#dialog4search').css({
			width: 800,
			height:400,
			"text-align": "center"
		}).jqmShow();	
	}
	function f_excel(){
		var page=$grid["options"]["newPage"];
		var url="${r"${ctx}"}/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/${tableName}/search?_m=excel&_tb=${r"${_tb}"}&datarangetype=${r"${datarangetype}"}&datarange=${r"${datarange}"}&pagesize=50&page="+page+"&_con="+encodeURI($("#_con").val());
        //var manager = $.ligerDialog.waitting('正在导出,请稍候...'); setTimeout(function () {window.open(url);manager.close(); }, 1000);
        window.open(url);
	}
	function query(data){
		$("#_con").val(data);
		$grid.setParm("_con",data);
		$grid.loadData();//加载数据
	}
	
	function f_log(){
		var row = $grid.getSelectedRow();
        if (!row) { 
        	 $.ligerDialog.warn('请至少选择一行数据');
			 return; 
        }else{
			browseWin=$.ligerDialog.open({ title:"数据查询跟踪", url: '${r"${ctx}"}/log/${tableName?substring(0,tableName?index_of('_',0) )}/${prefix}/log_${tableName}/tracelog?_tb=${r"${_tb}"}&_m=init&id='+row.id, load:true,width: 800,height:400,allowClose: true,top:200,isHidden:false});
		}
	}
	
</script>

