package ${pkg};

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import ${daoPkg}.${clzName}Mapper;
import ${domainPkg}.${clzName};
import com.klnst.core.persistence.Criteria;
import com.klnst.core.persistence.Criterion;
import com.klnst.core.persistence.DynamicSpecifications;
import com.klnst.core.persistence.SearchFilter;
import com.klnst.core.form.ConditionEntity;
import com.klnst.core.utils.TimeUtils;
import com.klnst.domain.SysOperator;
import com.klnst.domain.SysQryLog;

import com.google.common.collect.Maps;

/**
 * 
 * @ClassName: ${clzName}Service   
 * @Description: ${tableComment} 
 * @author ilaotou@qq.com
 * This class is generatored by AutoTool
 */
@Service
public class ${clzName}Service {
	
	@Autowired
	public ${clzName}Mapper ${clzName?uncap_first}Mapper;
	
	public List<${clzName}> paging4DynamicTblName(SysOperator user,String tblName,List<ConditionEntity> conditionEntities,PageBounds pageBounds) <#if model?exists && model="business">throws ParseException</#if>{
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		Criterion criterion1 = new Criterion("__user__",user.getCertificateNum());
		criterions.add(criterion);
		criterions.add(criterion1);
		</#if>
		Criteria criteria = new Criteria(criterions,tblName);
		return ${clzName?uncap_first}Mapper.paging4DynamicTblName(criteria,pageBounds);
	}
	
	public long pagingCount4DynamicTblName(SysOperator user,String tblName,List<ConditionEntity> conditionEntities) <#if model?exists && model="business">throws ParseException</#if>{
		List<SearchFilter> filters = SearchFilter.parse(conditionEntities);
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		Criterion criterion1 = new Criterion("__user__",user.getCertificateNum());
		criterions.add(criterion);
		criterions.add(criterion1);
		</#if>
		Criteria criteria = new Criteria(criterions,tblName);
		return ${clzName?uncap_first}Mapper.pagingCount4DynamicTblName(criteria);
	}
	
	public List<${clzName}> paging(List<SearchFilter> filters,PageBounds pageBounds) <#if model?exists && model="business">throws ParseException</#if>{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions);
		return ${clzName?uncap_first}Mapper.paging(criteria,pageBounds);
	}
	
	public long pagingCount(List<SearchFilter> filters) <#if model?exists && model="business">throws ParseException</#if>{
		List<Criterion> criterions = DynamicSpecifications.bySearchFilter(filters);
		<#if model?exists && model="business">
		Criterion criterion = new Criterion("cj_at<",TimeUtils.getDateNow());
		criterions.add(criterion);
		</#if>
		Criteria criteria = new Criteria(criterions);
		return ${clzName?uncap_first}Mapper.pagingCount(criteria);
	}
	
	public List<${clzName}> findBy(Map<String, Object> parameters) <#if model?exists && model="business">throws ParseException</#if>{
	    <#if model?exists && model="business">
	    parameters.put("cj_at",TimeUtils.getDateNow());
	    </#if>
		return ${clzName?uncap_first}Mapper.findBy(parameters);
	}
    
    public ${clzName} findUniqueById(String id){
        Map<String, Object> parameters = Maps.newHashMap();
        parameters.put("id",id);
		return ${clzName?uncap_first}Mapper.findUniqueBy(parameters);
	}
    public ${clzName} findUniqueBy(Map<String, Object> parameters){
		return ${clzName?uncap_first}Mapper.findUniqueBy(parameters);
	}
    
    public String insert(${clzName} ${clzName?uncap_first}){
    	${clzName?uncap_first}Mapper.insert(${clzName?uncap_first});
		return  String.valueOf(${clzName?uncap_first}.get${keyProperty?cap_first}());
	}
    
    public void update(${clzName} ${clzName?uncap_first}){
		${clzName?uncap_first}Mapper.update(${clzName?uncap_first});
	}
    
    public void deleteById(String ${keyProperty}){
		${clzName?uncap_first}Mapper.deleteById(${keyProperty});
	}
	
	public List<SysQryLog> traceByLogId(String logId,PageBounds pageBounds){
		return ${clzName?uncap_first}Mapper.traceByLogId(logId,pageBounds);
	}

}
